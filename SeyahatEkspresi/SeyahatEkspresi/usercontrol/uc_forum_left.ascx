﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_forum_left.ascx.cs" Inherits="SeyahatEkspresi.usercontrol.uc_forum_left" %>
<div class="right-slide">
    <div class="search-box">
        <asp:TextBox runat="server" placeholder="Arama" />
        <asp:LinkButton runat="server" CssClass="forum_src_btn" />
    </div>
    <h3>Son Yazılanlar</h3>
    <div class="replies-course">
        <%for (int i = 0; i < 3; i++)
            { %>


        <div class="course-slide">
            <div class="name">
                <img src="/assets/img/testimonial-img1.jpg" alt=""><a href="#">Dieter</a> on <a href="#">E-Learn 2015</a>
            </div>
            <div class="date">29 Day ago</div>
        </div>
        <%   } %>
    </div>
    <h3>Son Başlıklar</h3>
    <div class="replies-course topics">
        <%for (int i = 0; i < 3; i++)
            { %>
        <div class="course-slide">
            <div class="name">
                <a href="#">Dieter</a> on by
                <img src="/assets/img/testimonial-img1.jpg" alt="">
                <a href="#">E-Learn 2015</a>
            </div>
            <div class="date">29 Day ago</div>
        </div>
        <%   } %>
    </div>
    <h3>Forum İstatistikleri</h3>
    <ul class="working-list">
        <li>Replies<span>5</span></li>
        <li>Topics<span>10</span></li>
        <li>Topic Tags<span>7</span></li>
        <li>Registered Users<span>888</span></li>
        <li>Forums<span>5</span></li>
    </ul>
</div>
