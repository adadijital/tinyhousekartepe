﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="advert_list.ascx.cs" Inherits="SeyahatEkspresi.usercontrol.advert_list" %>
<div class="card mb-0">
    <div class="card-body">
        <div class="item2-gl ">
            <div class="item2-gl-nav d-flex justify-content-between">
                <h6 class="mb-0 mt-2">
                    <asp:Literal ID="ltrCount" runat="server"></asp:Literal>
                    sonuç listeleniyor </h6>

                <div class="d-flex">
                    <label class="mr-2 mt-1 mb-sm-1">Sırala:</label>
                    <asp:Literal ID="ltrSort" runat="server" />
                </div>
            </div>

            <div class="row">
                <asp:Repeater ID="rptAdvertList" runat="server">
                    <ItemTemplate>
                        <div class="col-lg-6 col-md-12 col-xl-4 mt-4">
                            <div class="card overflow-hidden">
                                <div class="item-card9-img">
                                    <div class="item-card9-imgs">
                                        <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>"></a>
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["site_url2"] %>/uploads/ads/268x150/<%#Eval("main_image") %>" alt="<%#Eval("advert_name") %>" class="cover-image">
                                    </div>
                                    <div class="item-card9-icons"><a href="#" class="item-card9-icons1 wishlist"><i class="fa fa fa-heart-o"></i></a></div>
                                </div>
                                <div class="card-body">
                                    <div class="item-card9">
                                        <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>" class="text-dark mt-2">
                                            <h4 class="font-weight-semibold mt-1"><%#Eval("advert_name") %> </h4>
                                        </a>
                                        <div class="item-card9-desc">
                                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>" class="mr-4"><span class=""><i class="fa fa-map-marker text-muted mr-1"></i><%#Eval("geo_city_name") %></span></a>
                                            <%--<a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>" class=""><span class=""><i class="fa fa-calendar-o text-muted mr-1"></i><%#Eval("ads_view") %> Kişi İnceledi</span></a>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="item-card9-footer d-flex">
                                        <div class="item-card9-cost">
                                            <h4 class="text-dark font-weight-semibold mb-0 mt-0"><%#Eval("sell_price") %> TL</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

        <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(document).on('change', '.sorti', function () {
            window.location = $(this).val();
        });
    });
</script>
