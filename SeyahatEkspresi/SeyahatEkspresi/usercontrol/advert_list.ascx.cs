﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using static SeyahatEkspresi.Core.AdvertManager;

namespace SeyahatEkspresi.usercontrol
{
    public partial class advert_list : System.Web.UI.UserControl
    {
        public List<AdvertData> AdvertList;
        public string CurrentUrl;
        public int AdvertCount;
        public int PageSize = 15;
        public int PageIndex = 1;
        public string PagerLink;
        public string FilterUrl = string.Empty;
        public List<FilterModel> CurrentFilters = new List<FilterModel>();
        JavaScriptSerializer _JScript = new JavaScriptSerializer();
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentUrl = Request.RawUrl;
            try
            {
                if (!Page.IsPostBack)
                {
                    if (AdvertList != null)
                    {
                        if (AdvertList.Count > 0)
                        {
                            AdvertCount = AdvertList.Count();
                            ltrCount.Text = AdvertCount.ToString();
                            _fillSort();
                            if (AdvertCount > PageSize)
                            {
                                rptAdvertList.DataSource = AdvertList
                                 .Skip((PageIndex - 1) * PageSize)
                                 .Take(PageSize)
                                 .ToList();
                                rptAdvertList.DataBind();
                                Paging(AdvertCount);
                            }
                            else
                            {
                                rptAdvertList.DataSource = AdvertList;
                                rptAdvertList.DataBind();
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Log.Add(ex);
            }
        }

        void _fillSort()
        {
            try
            {
                StringBuilder fl = new StringBuilder();
                fl.AppendLine("<select class='sorti form-control form-control select-sm w-100'>");
                fl.AppendLine(_setSortFilter("sort", "0", "Sıralama Seç"));
                fl.AppendLine(_setSortFilter("sort", "1", "Fiyat Artan"));
                fl.AppendLine(_setSortFilter("sort", "2", "Fiyat Düşen"));
                fl.AppendLine("</select>");
                ltrSort.Text = fl.ToString();
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }

        }

        public string _setSortFilter(string _name, string _value, string _text)
        {
            #region
            bool isSelected = false;
            string url = string.Empty;

            try
            {
                List<FilterModel> filters = new List<FilterModel>();

                if (CurrentFilters.Count > 0)
                {
                    filters = CurrentFilters.NoRefCopy();

                    var currentFilter = filters
                        .Where(q => q.name == _name)
                        .FirstOrDefault();

                    if (currentFilter !=null)
                    {
                        if (currentFilter.values.Contains(_value))
                            isSelected = true;

                        filters.Remove(currentFilter);
                    }

                    filters.Add(new FilterModel { name = _name, values = new List<string> { _value } });
                }
                else
                    filters.Add(new FilterModel { name = _name, values = new List<string> { _value } });

                if (filters.Count > 0)
                {
                    if (CurrentUrl.Contains("-p="))
                    {
                        string[] arrUrl = CurrentUrl.Split(new string[] { "-p=" }, StringSplitOptions.None);
                        CurrentUrl = arrUrl[0] + "-p=1";
                    }

                    if (CurrentUrl.Contains("-f="))
                    {
                        string[] arrUrl = CurrentUrl.Split(new string[] { "-f=" }, StringSplitOptions.None);
                        CurrentUrl = arrUrl[0] + "-f=";
                    }
                    else
                    {
                        CurrentUrl += "-f=";
                    }

                    url = (CurrentUrl + _JScript.Serialize(filters));
                }
                else
                    url = CurrentUrl.Replace("-f=", string.Empty);
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }

            return "<option value='" + url + "' " + (isSelected ? " selected='selected' " : "") + ">" + _text + "</option>";
            #endregion
        }

        public void Paging(int TotalCount)
        {
            try
            {
                #region paging
                ltrPaging.Text = "";
     
                ltrPaging.Text += "<div class='center-block text-center'>";

                ltrPaging.Text += "<ul class='pagination mb-0'>";


                int loop = 0;
                if (TotalCount > PageSize)
                {
                    loop = TotalCount / PageSize;
                    if (TotalCount % PageSize != 0) loop++;
                    int lCount = 0;

                    if (loop > 1)
                    {
                        if ((PageIndex + PageSize) <= loop)
                            lCount = (PageIndex + (PageSize - 1));
                        else
                            lCount = (PageIndex + (loop - PageIndex));


                        if (PageIndex > 5)
                            ltrPaging.Text += "<li class='page-item'><a href='" + PagerLink.ToString() + "-p=" + (PageIndex - 1) + FilterUrl + "'>«</a></li>";

                        if (PageSize < lCount)
                        {
                            ltrPaging.Text += "<li class='page-item'><a href=\"javascript:void(0);\">...</a></li>";
                        }


                        int starterIndex = (PageIndex < 10) ? 1 : PageIndex;



                        for (int i = starterIndex; i <= lCount; i++)
                        {
                            if (PageIndex == i)
                                ltrPaging.Text += "<li class='page-item active'><a href='#'>" + i + "</a></li>";
                            else
                                ltrPaging.Text += "<li class='page-item'><a  href='" + PagerLink + "-p=" + i + FilterUrl + "'>" + i + "</a></li>";
                        }



                        if (PageIndex < lCount)
                        {
                            if (lCount < loop)
                                ltrPaging.Text += "<li class='page-item disabled'><a href='javascript:void(0);'>...</a></li>";

                            ltrPaging.Text += "<li class='page-item'><a href='" + PagerLink + "-p=" + (PageIndex + 1) + "" + FilterUrl + "'>»</a></li>";
                        }
                    }

                    ltrPaging.Text += "</ul>";

                    ltrPaging.Text += "</div>";


                }

                #endregion
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }
    }
}