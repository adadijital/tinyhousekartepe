﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_list_filter.ascx.cs" Inherits="SeyahatEkspresi.usercontrol.uc_list_filter" %>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Kategoriler</h3>
    </div>
    <div class="card-body">
        <div class="closed" id="container">
            <asp:Literal ID="ltrCats" runat="server"></asp:Literal>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Konum</h3>
    </div>
    <div class="card-body">
        <div class="filter-blcok">
            <div class="check-slide">
                <asp:DropDownList ID="drpCityList" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpCityList_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </div>
            <asp:Panel ID="pnlTown" runat="server" Visible="false">
                <div class="check-slide mt-4">
                    <asp:DropDownList ID="drpTownList" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="drpTownList_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlQuarter" runat="server" Visible="false">
                <div class="check-slide mt-4">
                    <asp:DropDownList ID="drpQuarter" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="drpQuarter_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </asp:Panel>

        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Fiyat</h3>
    </div>
    <div class="card-body">
        <div class="filter-blcok">
            <div class="row">


                <div class="input-box col-5 px-2">
                    <asp:TextBox ID="txtMinPrice" CssClass="form-control" runat="server" placeholder="En Az"></asp:TextBox>
                </div>
                <div class="input-box col-5 px-1">
                    <asp:TextBox ID="txtMaxPrice" CssClass="form-control" runat="server" placeholder="En Çok"></asp:TextBox>
                </div>

                <div class="input-box col-2 px-1">
                    <asp:LinkButton CssClass="btn btn-secondary" runat="server" ID="lnkFiltrele" OnClick="lnkFiltrele_Click"><i class="fa fa-arrow-right"></i></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</div>
