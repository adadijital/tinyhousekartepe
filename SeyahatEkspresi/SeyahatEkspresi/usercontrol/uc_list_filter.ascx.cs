﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.usercontrol
{
    public partial class uc_list_filter : System.Web.UI.UserControl
    {
        tinyhous_dbEntities db = new tinyhous_dbEntities();
        
        CategoryManager _categoryManager = new CategoryManager();
        public string CurrentUrl = "/";
        public int CurrentCategory = 0;
        List<categories_lang> allCategories;
        int current = 0;
        string kategoriler = "";
        JavaScriptSerializer _JScript = new JavaScriptSerializer();
        public List<FilterModel> CurrentFilters = new List<FilterModel>();
        string strFilter = string.Empty;
        List<FilterModel> _filters = new List<FilterModel>();
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentUrl = Request.RawUrl;
            _buildLeftMenu();
        }

        private void _buildLeftMenu()
        {
            try
            {

                allCategories = db.categories_lang.Where(q => q.categories.is_active && q.lang_id == Localization.Language
                          && q.categories.relation_ads_category.Where(q2 => q2.ads.is_active
                          && q2.ads.is_cancel != true
                          && q2.ads.is_active
                          && q2.ads.ads_lang.Where(q3 => q3.lang_id == Localization.Language).Count() > 0).Count() > 0
                      ).ToList();


                var result = _categoryManager.SetCatBreadCrumb(CurrentCategory, Localization.Language, Localization.LanguageStr);
                //ltrBreadCrumb.Text += "<ul>";
                //foreach (var item in result)
                //{
                //    current++;
                //    ltrBreadCrumb.Text += "<li class='cl" + current.ToString() + "'><a href='" + item.link + "'><b>" + item.text + "</b></a></li>";

                //}
                //ltrBreadCrumb.Text += "</ul>";

                SubCategories(CurrentCategory, 0);
                ltrCats.Text = kategoriler;




                if (!IsPostBack)
                {
                    var _cityList = db.geo_cities.OrderBy(o => o.PlakaNo).ToList();

                    if (_cityList.Count() > 0)
                    {
                        if (!IsPostBack)
                        {
                            drpCityList.DataTextField = "city_name";
                            drpCityList.DataValueField = "id";
                            drpCityList.DataSource = _cityList;
                            drpCityList.DataBind();
                            drpCityList.Items.Insert(0, "Şehir Seçiniz");
                        }
                    }
                }



                if (Request.QueryString["filter"] != null)
                    strFilter = Request.QueryString["filter"].ToString();

                try
                {
                    if (!string.IsNullOrEmpty(strFilter))
                    {
                        _filters = _JScript.Deserialize<List<FilterModel>>(strFilter);

                        if (_filters.Count > 0)
                        {
                            foreach (var filter in _filters)
                            {
                                if (filter.name == "city")
                                {
                                    int city_id = filter.values.FirstOrDefault().toInt();
                                    drpCityList.SelectedValue = city_id.ToString();

                                    if (!IsPostBack)
                                    {
                                        var _townList = db.geo_town.Where(q => q.city_id == city_id).ToList();

                                        if (_townList.Count() > 0)
                                        {
                                            drpTownList.DataTextField = "town_name";
                                            drpTownList.DataValueField = "id";
                                            drpTownList.DataSource = _townList;
                                            drpTownList.DataBind();
                                            drpTownList.Items.Insert(0, "İlçe Seçiniz");

                                        }

                                        pnlTown.Visible = true;
                                    }

                                }
                                else if (filter.name == "town")
                                {
                                    int town_id = filter.values.FirstOrDefault().toInt();

                                    var _Town = db.geo_town.Where(q => q.id == town_id).FirstOrDefault();

                                    if (!IsPostBack)
                                    {

                                        var _quarterList = db.geo_quarter.Where(q => q.town_id == town_id).ToList();

                                        if (_quarterList.Count() > 0)
                                        {
                                            drpQuarter.DataTextField = "quarter_name";
                                            drpQuarter.DataValueField = "id";
                                            drpQuarter.DataSource = _quarterList;
                                            drpQuarter.DataBind();
                                            drpQuarter.Items.Insert(0, "Mahalle Seçiniz");
                                        }

                                        drpCityList.SelectedValue = db.geo_cities.Where(q => q.id == _Town.city_id).FirstOrDefault().id.ToString();
                                        pnlTown.Visible = true;
                                        pnlQuarter.Visible = true;
                                    }

                                }
                            }
                        }
                    }
                }
                catch (Exception ex) { Log.Add(ex); }

            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        void SubCategories(int topCatID, int index = 0)
        {
            try
            {
                List<categories_lang> altSubCats = allCategories.Where(w =>  w.lang_id == Localization.Language
                 && w.categories.relation_ads_category.Where(q2 => q2.ads.is_active 
                            && q2.ads.is_cancel != true
                            && q2.ads.is_active
                            && q2.ads.ads_lang.Where(q3 => q3.lang_id == Localization.Language).Count() > 0).Count() > 0

                ).ToList();


                if (altSubCats.Count == 0)
                    return;


                kategoriler += "<div class='filter-product-checkboxs'>";

                index++;
                foreach (categories_lang item in altSubCats)
                {

                    string link = "/" + Localization.LanguageStr + "/ilanlar-" + item.category_name.ToURL() + "-c-" + item.category_id.ToString();
                    kategoriler += "<label class='custom-control custom-checkbox mb-3'><a class='text-dark' href='" + link + "'  title='" + item.category_name + "'>" + item.category_name + "</a>";
                  //  SubCategories(item.category_id, index);
                    kategoriler += "</label>";

                }
                kategoriler += "</div>";

            }
            catch (Exception ex)
            {
                //  genel.AddErrorLog(1, ex);
            }
        }

        protected void drpCityList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                string url = string.Empty;
                url = _setFilter("city", drpCityList.SelectedValue.ToString());
                if (!string.IsNullOrEmpty(url))
                    Response.Redirect(url, false);

            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }
        protected void drpTownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string url = string.Empty;
                url = _setFilter("town", drpTownList.SelectedValue.ToString());
                if (!string.IsNullOrEmpty(url))
                    Response.Redirect(url, false);
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }
        protected void lnkFiltrele_Click(object sender, EventArgs e)
        {
            #region
            try
            {
                string url = string.Empty;
                decimal minPrice = txtMinPrice.Text.Trim().todecimal();
                decimal maxPrice = txtMaxPrice.Text.Trim().todecimal();
                if (minPrice > 0 && maxPrice > 0)
                {
                    url = _setFilter("price", minPrice.ToString(), maxPrice.ToString());
                }
                else if (minPrice > 0)
                {
                    url = _setFilter("price", minPrice.ToString(), "0");
                }
                else if (minPrice > 0)
                {
                    url = _setFilter("price", "0", minPrice.ToString());
                }
                if (!string.IsNullOrEmpty(url))
                    Response.Redirect(url, false);
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
            #endregion
        }

        string _setFilter(string _name, string _value, string _value2 = "", int index = 0)
        {

            #region
            bool isChecked = false;
            string url = string.Empty;

            try
            {
                List<FilterModel> filters = new List<FilterModel>();

                if (CurrentFilters.Count() > 0)
                {
                    filters = CurrentFilters.NoRefCopy();

                    var currentFilter = filters
                        .Where(q => q.name == _name)
                        .FirstOrDefault();

                    if (_name == "price")
                    {
                        #region Price
                        if (currentFilter == null)
                            filters.Add(new FilterModel { name = _name, values = new List<string> { _value, _value2 } });
                        else
                        {
                            if (!currentFilter.values.Contains(_value))
                            {
                                currentFilter.values.RemoveAt(0);
                                currentFilter.values.Insert(0, _value);
                            }

                            if (!currentFilter.values.Contains(_value2))
                            {
                                currentFilter.values.RemoveAt(1);
                                currentFilter.values.Insert(1, _value2);
                            }
                        }
                        #endregion
                    }
                    else if (_name == "city")
                    {
                        #region city
                        if (currentFilter == null)
                            filters.Add(new FilterModel { name = _name, values = new List<string> { _value } });
                        else
                        {
                            int length = currentFilter.values.Count;
                            for (int i = 0; i < length; i++)
                                currentFilter.values.RemoveAt(i);

                            currentFilter.values.Insert(0, _value);
                        }

                        #endregion
                    }
                    else if (_name == "town")
                    {
                        #region town
                        if (currentFilter == null)
                            filters.Add(new FilterModel { name = _name, values = new List<string> { _value } });
                        else
                        {
                            int length = currentFilter.values.Count;
                            for (int i = 0; i < length; i++)
                                currentFilter.values.RemoveAt(i);

                            currentFilter.values.Insert(0, _value);
                        }

                        #endregion
                    }
                    else if (_name == "quarter")
                    {
                        #region quarter
                        if (currentFilter == null)
                            filters.Add(new FilterModel { name = _name, values = new List<string> { _value } });
                        else
                        {
                            int length = currentFilter.values.Count;
                            for (int i = 0; i < length; i++)
                                currentFilter.values.RemoveAt(i);

                            currentFilter.values.Insert(0, _value);
                        }

                        #endregion
                    }
                    else
                    {
                        #region Other

                        if (currentFilter == null)
                            filters.Add(new FilterModel { name = _name, values = new List<string> { _value } });
                        else
                        {
                            if (currentFilter.values.Count() > 0)
                            {
                                if (!currentFilter.values.Contains(_value))
                                    currentFilter.values.Add(_value);
                                else
                                {
                                    isChecked = true;
                                    currentFilter.values.Remove(_value);

                                    if (currentFilter.values.Count() == 0)
                                        filters.Remove(currentFilter);
                                }
                            }
                            else
                                filters.Remove(currentFilter);
                        }

                        #endregion
                    }
                }
                else
                {
                    if (_name == "price")
                        filters.Add(new FilterModel { name = _name, values = new List<string> { _value, _value2 } });
                    else
                        filters.Add(new FilterModel { name = _name, values = new List<string> { _value } });
                }


                if (filters.Count() > 0)
                {
                    if (CurrentUrl.Contains("-f="))
                    {
                        string[] arrUrl = CurrentUrl.Split(new string[] { "-f=" }, StringSplitOptions.None);
                        CurrentUrl = arrUrl[0] + "-f=";
                    }
                    else
                        CurrentUrl += "-f=";

                    url = (CurrentUrl + _JScript.Serialize(filters));
                }
                else
                    url = CurrentUrl.Replace("-f=", string.Empty);

            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }


            switch (_name)
            {
                case "price":
                    return url;
                case "city":
                    return url;
                case "town":
                    return url;
                case "quarter":
                    return url;
                default:
                    if (isChecked)
                        return "<input class='ckFilter checked' type='checkbox' name='tour' value='" + url + "' checked style='display:none;' />";
                    else
                        return "<input class='ckFilter' type='checkbox' name='tour' value='" + url + "' style='display:none;' />";
            }
            #endregion
        }

        protected void drpQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string url = string.Empty;
                url = _setFilter("quarter", drpQuarter.SelectedValue.ToString());
                if (!string.IsNullOrEmpty(url))
                    Response.Redirect(url, false);
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }
    }
}