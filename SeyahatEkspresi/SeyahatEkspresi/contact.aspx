﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="SeyahatEkspresi.contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">
                        <p>
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                          <a href="javascript:void(0)">İletişim</a>
                        </p>
                        <h1>İletişim</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="container mt-5 mb-3">
        <div class="row pb-5">
            <div class="iletisim-contact-cover iletisim-sp">
                <div class="iletisim-left">
                    <div class="iletisim-content-top">
                        <h1>Bize Ulaşın</h1>
                        <p>7/24 Günün her saati yanınızdayız, destek
                            <br>
                            ekibimizle görüşebilirsiniz.</p>
                    </div>
                    <div class="iletisim-content">
                        <div class="iletisim-content-inner">
                            <div class="iletisim-content-inner-logo"><i class="fas fa-phone"></i></div>
                            <div class="iletisim-content-inner-contact">
                                <h4>Telefon</h4>
                                <p>
                                    <asp:HyperLink ID="hypPhone1" runat="server"></asp:HyperLink>
                                </p>
                                <p>
                                    <asp:HyperLink ID="hypPhone2" runat="server"></asp:HyperLink></p>
                            </div>

                        </div>
                        <div class="iletisim-content-inner">
                            <div class="iletisim-content-inner-logo"><i class="far fa-envelope-open"></i></div>
                            <div class="iletisim-content-inner-contact">
                                <h4>E-mail</h4>
                                <p>
                                    <asp:HyperLink ID="hypEmail" runat="server"></asp:HyperLink>
                                </p>
                                <p>
                                    <asp:HyperLink ID="hypEmail2" runat="server"></asp:HyperLink></p>
                            </div>
                        </div>
                        <div class="iletisim-content-inner">
                            <div class="iletisim-content-inner-logo"><i class="fas fa-map-marker-alt"></i></div>
                            <div class="iletisim-content-inner-contact">
                                <h4>Adres</h4>
                                <p>
                                    <asp:Literal ID="ltrAddres" runat="server"></asp:Literal></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="iletisim-right">
                    <div class="mekan-form iletisim-form">
                        <div class="mekan-form-header">
                            <h1>Aklınıza takılan herşeyi
                                <br>
                                bize sorabilirsiniz.</h1>
                        </div>
                        <div class="mekan-form-textbox">
                            <label for="">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtNameSurname" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtNameSurname" runat="server" placeholder="Adınız"></asp:TextBox>
                                <i class="far fa-user"></i>
                            </label>
                            <label for="">
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtEmail" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtEmail" runat="server" placeholder="E-Posta"></asp:TextBox>
                                <i class="far fa-envelope-open"></i>
                            </label>
                        </div>
                        <div class="textarea-cover">
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtMessage" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtMessage" runat="server" placeholder="Mesajınız" TextMode="MultiLine"  CssClass="form-control"></asp:TextBox>
                            <div class="btn-cover">
                                <%--<a href="#">Gönder</a>--%>
                                <asp:Button ID="btnGonder" runat="server" Text="Gönder" OnClick="btnGonder_Click" CssClass="form-gonder-btn" ValidationGroup="validateCredential2" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <asp:Literal ID="ltrMap" runat="server"></asp:Literal>
            <%--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d203889.6793532433!2d35.30646202956362!3d37.015134180005575!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1528bf7b3bf7280f%3A0x41f93bc7a9af1045!2sGoldeks%20Tekstil!5e0!3m2!1str!2str!4v1630629490490!5m2!1str!2str" width="100%" height="450" style="border: 0;" allowfullscreen="" loading="lazy"></iframe>--%>
        </div>
    </div>


</asp:Content>
