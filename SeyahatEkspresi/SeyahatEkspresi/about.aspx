﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="about.aspx.cs" Inherits="SeyahatEkspresi.about" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">
                        <p>
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                          <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/hakkimizda">Hakkımızda</a>
                        </p>
                        <h1>Hakkımızda</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container hakkimizda-content-cover">
        <div class="row">
            <div class="hakkimizda-content-top-header-cover">
                <div class="col-lg-12 hakkimizda-content-top-header">
                    <h2>
                        <asp:Literal ID="ltrHakkimizdaTitle" runat="server"></asp:Literal>
                    </h2>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="hakkimizda-content-top-text-cover">
                <div class="col-lg-12 hakkimizda-content-top-text">
                    <p>
                        <%--<span>seyahatekspresi.com </span>Türkiye genelinde yüzlerce bağımsız konaklama hizmeti sunan otellerin yanı sıra self-catering konaklamalar da dâhil olmak üzere müşterilerine internet üzerindeki en geniş konaklama seçeneklerini sunmaktadır. Şirket; otel fiyatları, otel özellikleri ve müsaitlik bilgilerini tek bir kaynakta sunmaktadır.--%>
                        <asp:Literal ID="ltrHakkimizdaDesc" runat="server"></asp:Literal>


                    </p>
                  
                 
                </div>
            </div>
        </div>

    </div>




    <!--<section class="sptb">
        <div class="container">
            <div class="text-justify">
                <h1 class="mb-4">
                    <asp:Literal ID="ltrHakkimizdaTitle2" runat="server"></asp:Literal></h1>
                <asp:Literal ID="ltrHakkimizdaDesc2" runat="server"></asp:Literal>
            </div>
        </div>
    </section>-->





    <div class="container qa-content-cover" id="sss">
        <div class="row">
            <div class="col-lg-12 qa-content mb-2">
                <h1>Konaklama hakkında sık
                    <br>
                    sorulan sorular</h1>
            </div>
        </div>
        <div class="row qa-card-cover mb-5">
            <%-- <asp:Repeater ID="rptSSS" runat="server">
                <ItemTemplate>
                       <div class="col-lg-4 qa-card-col">
                <div class="qa-content-card">
                    <div class="qa-content-card-header">
                        <h3>
                            <a class="accordion-section-title" href="javascript:void(0)" tabhref="#accordion-<%# Container.ItemIndex %>">
                                <%#Eval("sss_title") %>
                            </a>
                        </h3>
                        <p class="mobil-show"><i class="fas fa-chevron-down"></i></p>
                    </div>
                   <%-- <div class="qa-content-card-text" id="accordion-<%# Container.ItemIndex %>">--%>
            <%--    <div class="qa-content-card-text accordion-section-content" id="accordion-<%# Container.ItemIndex %>">
                        <p>
                            <%#Eval("sss_desc") %>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 <%# Container.ItemIndex ==1 ? "qa-card-cover-sp" :"d-none"%>">
                <div class="qa-content-sp-card">
                    <div class="qa-content-card-sp-header">
                        <h3>Sormak istediğiniz bir şey var mı?</h3>
                        <%--<p><i class="fas fa-chevron-down"></i></p>--%>
            <%-- </div>
                    <div class="qa-content-card-sp-btn">
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar">Tüm Konaklamalar </a>
                    </div>
                </div>
            </div>
                </ItemTemplate>
            </asp:Repeater>--%>


            <div class="accordion-container">
                <div class="acc-content">
                    <a href="javascript:void(0)">İmplant Nedir?
                                    <i class="fa fa-plus"></i>
                    </a>
                    <div class="acc-inner-content">
                        <p>İmplant; eksik dişlerin yerine konması amacıyla, çene kemiğine yerleştirilen vida şeklindeki yapay bir diş köküdür. Çene kemiği titanyumu vücudun bir parçası olarak algıladığından, implantlar çoğunlukla titanyumdan yapılmaktadır ve implantların yüksek doku uyumu nedeniyle başarı oranı oldukça yüksektir. Diş hekimliğinde çok yaygın kullanım alanı bulma nedeni; tek diş eksikliklerinden, hiç dişi olmayan hastalara kadar, çok geniş bir yelpazede uygulanabilir olmasıdır. Bu uygulama, komşu dişlere hiç müdahale edilmeden gerçekleştirilir. Gerek çiğneme hissinin normale en yakın olması, gerekse diğer dişlere müdahale etmeden tedavi imkânı yaratması; yaşadığımız yüzyılda implant içeren yöntemleri, en çok uygulanan tedavi yöntemlerinden biri haline getirmiştir.</p>
                    </div>
                </div>
                <div class="acc-content">
                    <a href="javascript:void(0)">İmplant Aşamaları nelerdir?
                                    <i class="fa fa-plus"></i>
                    </a>
                    <div class="acc-inner-content">
                        <p>İmplant uygulaması genellikle iki aşamada gerçekleştirilir. Daha ileri cerrahi uygulama gerektirmeyen, standart bir implant uygulaması için ilk aşamada implant yerleştirilecek bölgeye lokal anestezi yapılarak bölgenin anestezisi sağlandıktan sonra diş eti dikkatlice kaldırılır. Daha önceden belirlenmiş olan kemik kalınlığı ve yüksekliğine uygun olarak implant için kemikte yer hazırlanır ve buraya implant yerleştirilir. Eğer bölgede çekilecek bir diş varsa ve altındaki kemik implanta müsaitse bu bölgeye de immediate (hemen) implant uygulaması yapılabilir. İmplant uygulamasının ikinci aşamasında, implantın kemik ile bütünleşmesi için kemiğin durumuna ve uygulanan çeneye göre belli bir süre beklendikten sonra üzeri kapalı olan implantın üzeri açılarak dişi taklit eden kısım yerleştirilir ve protez işlemlerine başlanır.</p>
                    </div>
                </div>
                <div class="acc-content">
                    <a href="javascript:void(0)">İmplant sağlığa zarar verir mi?
                                    <i class="fa fa-plus"></i>
                    </a>
                    <div class="acc-inner-content">
                        <p>İmplant tedavisi genellikle iki aşamada gerçekleştirilir. Daha ileri cerrahi uygulama gerektirmeyen, standart bir implant uygulaması için ilk aşamada implant yerleştirilecek bölgeye lokal anestezi yapılarak bölgenin anestezisi sağlandıktan sonra diş eti dikkatlice kaldırılır. Daha önceden belirlenmiş olan kemik kalınlığı ve yüksekliğine uygun olarak implant için kemikte yer hazırlanır ve buraya implant yerleştirilir. Eğer bölgede çekilecek bir diş varsa ve altındaki kemik implanta müsaitse bu bölgeye de immediate (hemen) implant uygulaması yapılabilir. İmplant uygulamasının ikinci aşamasında, implantın kemik ile bütünleşmesi için kemiğin durumuna ve uygulanan çeneye göre belli bir süre beklendikten sonra üzeri kapalı olan implantın üzeri açılarak dişi taklit eden kısım yerleştirilir ve protez işlemlerine başlanır.</p>
                    </div>
                </div>
                <div class="acc-content">
                    <a href="javascript:void(0)">İmplant tedavisi ne kadar sürer?
                                    <i class="fa fa-plus"></i>
                    </a>
                    <div class="acc-inner-content">
                        <p>İmplant tedavisinin sürecini hastanın ağız içindeki vaziyeti belirler. Bu süre kemiğin yapısına ve kullanılan implanta bağlı olarak değişkenlik gösterebilmektedir. Bir hafta da bitirebilen tedaviler olduğu gibi üç ayda bitirilen tedaviler de olabilir.</p>
                    </div>
                </div>
                <div class="acc-content">
                    <a href="javascript:void(0)">Üç boyutlu çene ve diş tomografisi?
                                    <i class="fa fa-plus"></i>
                    </a>
                    <div class="acc-inner-content">
                        <p>Klasik diş röntgeni ile arasındaki en büyük fark, diğer röntgenlerde görüntünün bir defterin sayfası gibi 2 boyutta görünmesidir. Bu da görüntünün kesin ve net aktarılmamış olması demektir. 3d çene ve diş tomografisi sayesinde görüntülenmesi istenen çene kemiği 3 boyutlu olarak izlenir. Bu da çene kemiklerinin ve dişlerin kalınlık, uzunluk ve genişlik bakımından kesitler halinde görülmesine olanak sağlar</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
     

    <script>
        $(document).ready(function () {
            $(".acc-content > a").on("click", function () {
                if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                    $(this)
                        .siblings(".acc-inner-content")
                        .slideUp(200);
                    $(".acc-content  > a i")
                        .removeClass("fa-minus")
                        .addClass("fa-plus");
                } else {
                    $(".acc-content > a i")
                        .removeClass("fa-minus")
                        .addClass("fa-plus");
                    $(this)
                        .find("i")
                        .removeClass("fa-plus")
                        .addClass("fa-minus");
                    $(".acc-content  > a").removeClass("active");
                    $(this).addClass("active");
                    $(".acc-inner-content").slideUp(200);
                    $(this)
                        .siblings(".acc-inner-content")
                        .slideDown(200);
                }
            });
        });
    </script>
    <!--JQuery-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</asp:Content>
