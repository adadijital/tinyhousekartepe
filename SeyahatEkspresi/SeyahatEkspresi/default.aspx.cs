﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace SeyahatEkspresi
{
    public partial class _default : Base
    {

        public int category_id = 0;
        AdvertManager _advertManager = new AdvertManager();
        MemberManager _memberManager = new MemberManager();
        List<SearchModel> _searchList = new List<SearchModel>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                var _sliderList = db.slider_lang.Where(q => q.lang_id == Localization.Language && q.slider.is_active).OrderBy(o => o.slider.display_order).Select(s => new
                {
                    s.id,
                    s.lang_id,
                    s.slider.is_active,
                    s.slider.link,
                    s.img_path,
                    s.slider_title,
                    s.slider_desc
                }).ToList();

                if (_sliderList.Count() > 0)
                {
                    rptSlider.DataSource = _sliderList;
                    rptSlider.DataBind();

                    //rptSlide2.DataSource = _sliderList;
                    //rptSlide2.DataBind();
                }


                if(!IsPostBack)
                {
                    var _cities = db.geo_cities.Where(q => q.ads.Where(q2=> q2.is_deleted==false).Count() > 0).OrderBy(o => o.PlakaNo).ToList();

                    if (_cities.Count() > 0)
                    {
                        foreach (var item in _cities)
                        {
                            _searchList.Add(new SearchModel{icon= "<i class='fas fa-map-marker-alt' aria-hidden='true'></i>", key = item.city_name.GrowfirstCharacter(), value = item.city_name.GrowfirstCharacter() });
                        }

                        
                
                    }
                }

           
               
         

                var _parentCat = db.categories_lang.Where(q => q.categories.is_active && q.lang_id == Localization.Language && q.categories.parent_id == 0 && q.categories.icon_path != null && q.categories.is_deleted==false).Select(s => new
                {
                    s.category_id,
                    s.category_name,
                    s.categories.icon_path,
                    s.categories.display_order,
                    ads_count = s.categories.relation_ads_category.Where(q2 => q2.ads.is_deleted == false).Count(),
                }).OrderBy(o => o.display_order).ToList();

                if (_parentCat.Count() > 0)
                {

                    foreach (var item in _parentCat)
                    {
                        _searchList.Add(new SearchModel {icon= "<i class='fa fa-home' aria-hidden='true'></i>" ,key = item.category_name.GrowfirstCharacter(), value = item.category_name.GrowfirstCharacter() });
                    }

                    rptParentCategory.DataSource = _parentCat;
                    rptParentCategory.DataBind();
                }



                rptSearchModelList.DataSource = _searchList;
                rptSearchModelList.DataBind();

                rptSearchModelList2.DataSource = _searchList;
                rptSearchModelList2.DataBind();

                var _district = db.geo_town.Where(q => q.img_path != null).ToList();

                if (_district.Count() > 0)
                {
                    rptDistrict.DataSource = _district;
                    rptDistrict.DataBind();

                    rptDistrict2.DataSource = _district;
                    rptDistrict2.DataBind();
                }

                //var _blog = db.blog_lang.Where(q => q.lang_id == Localization.Language).Select(s=> new { 
                //s.blog_title,
                //s.blog_desc,
                //s.blog.created_time,
                //    s.blog_id,
                //    s.blog.blog_category.blog_category_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,

                //    s.blog.image_path,
                //    s.blog.view_count,

                //}).OrderBy(o => o.created_time).Take(10).ToList();

                //if(_blog.Count()>0)
                //{
                //    rptBlog.DataSource = _blog;
                //    rptBlog.DataBind();


                //}

                var _showCase = _advertManager.getLastAdvertShowcase();

                if (_showCase.Count() > 0)
                {
                    pnlShowCase.Visible = true;
                    rptShowCase.DataSource = _showCase;
                    rptShowCase.DataBind();

                }
                else
                    pnlShowCase.Visible = false;

                var _lastAds = _advertManager.getLastAdvertRandom();

                if (_lastAds.Count() > 0)
                {
                    rptRandom.DataSource = _lastAds;
                    rptRandom.DataBind();
                }

            }
            catch (Exception ex)
            {

                Log.Add(ex);
            }
        }

        protected void rptSlider_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                HyperLink hypLink = (HyperLink)e.Item.FindControl("hypLink");
                string link = DataBinder.Eval(e.Item.DataItem, "link").ToString();

                if (!string.IsNullOrEmpty(link))
                {
                    hypLink.Visible = true;
                    hypLink.NavigateUrl = link;
                }
                else
                    hypLink.Visible = false;
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenField hdnStartDate = this.Master.FindControl("hdnStartDate") as HiddenField;
                HiddenField hdnFinishDate = this.Master.FindControl("hdnFinishDate") as HiddenField;
                //string city_id = drpLocation.SelectedValue.ToString();
                string _searchKey = txtMainSearch.Text.Trim();
                string start_date = hdnStartDate.Value;
                string finish_date = hdnFinishDate.Value;
                string person_count = drpPersonCount.SelectedValue.ToString();

               Response.Redirect("/" + Localization.LanguageStr + "/arama/" + _searchKey + "-" + start_date + "-" + finish_date + "-" + person_count, false);

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (_memberManager.IsLoggedIn)
                {
                    long _id = Convert.ToInt64(e.CommandArgument);
                    int member_id = Session["member_id"].toInt();

                    if (!(db.member_wishlist.Where(q => q.member_id == member_id && q.ads_id == _id).Count() > 0))
                    {
                        member_wishlist _newWish = new member_wishlist();
                        _newWish.member_id = member_id;
                        _newWish.ads_id = _id;
                        _newWish.created_time = DateTime.Now;

                        db.member_wishlist.Add(_newWish);

                        if (db.SaveChanges() > 0)
                        {
                            showWarningBasic(this.Master, "Favori ilanlarınıza eklenmiştir", "Tebrikler");
                        }

                    }
                    else
                        showWarningBasic(this.Master, "Bu ilanı daha önce favorilerinize eklemişsiniz", "Dikkat");
                }
                else
                    showWarningBasic(this.Master, "İlanı favorilerinize eklemek için üye girişi yapmalısınız", "Dikkat");

            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void LinkButton22_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenField hdnStartDate = this.Master.FindControl("hdnStartDate") as HiddenField;
                HiddenField hdnFinishDate = this.Master.FindControl("hdnFinishDate") as HiddenField;

               // string city_id = drpLocation2.SelectedValue.ToString();
                string start_date = hdnStartDate.Value;
                string finish_date = hdnFinishDate.Value;
                string person_count = drpPersonCount2.SelectedValue.ToString();

               // Response.Redirect("/" + Localization.LanguageStr + "/arama/" + city_id + "-" + start_date + "-" + finish_date + "-" + person_count, false);

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        public class SearchModel
        {
            public string key { get; set; }
            public string value { get; set; }
            public string icon { get; set; }

        }
        }
}