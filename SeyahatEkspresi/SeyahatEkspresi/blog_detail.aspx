﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="blog_detail.aspx.cs" Inherits="SeyahatEkspresi.blog_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">

                        <asp:Repeater ID="rptCategories" runat="server">
                            <ItemTemplate>
                                <p>
                                    <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/ilanlar-<%#SeyahatEkspresi.cExtensions.ToURL(Eval("category_name").ToString()) %>-c-<%#Eval("cat_id") %>" title="<%#Eval("category_name") %>">
                                <%#Eval("category_name") %> 
                            </a>
                                </p>

                            </ItemTemplate>
                        </asp:Repeater>
                        <h1>
                            <asp:Literal ID="ltrAdvertName" runat="server"></asp:Literal>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--  <section>
        <div class="bannerimg cover-image bg-background3" data-image-src="/assets/img/banner2.jpg">
            <div class="header-text mb-0">
                <div class="container">
                    <div class="text-center text-white ">
                        <h1 class=""> <asp:Literal ID="ltrHeaderTitle" runat="server"></asp:Literal></h1>
                        <ol class="breadcrumb text-center">
                            <li class="breadcrumb-item"><a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>">Anasayfa</a></li>
                            <li class="breadcrumb-item"><a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/haberler">Blog</a></li>
                            <li class="breadcrumb-item active text-white" aria-current="page"><asp:Literal ID="ltrHeaderTitle2" runat="server"></asp:Literal></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>-->

    <div class="container mt-5 mb-5">
        <div class="row d-flex">
            <div class="col-6">
                <div class="konak-detay-slider-cover">
                    <div class="konak-detay-slider-left">
                        <div class="konak-detay-slider-left-item">
                            <a data-fancybox="gallery" runat="server" id="lnkFancy">
                                <asp:Image ID="Image2" runat="server" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="konak-detay-slider-right col-6">
                <asp:Repeater ID="rptImages" runat="server">
                    <ItemTemplate>

                        <div class="img-content col-4">
                            <div class="konak-detay-slider-item-inner-cover">
                                <div class="konak-detay-slider-item-inner ">
                                    <a href="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/L/<%#Eval("img_name") %>" data-fancybox="gallery">
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("img_name") %>" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

    <!--  <section class="sptb">
   <div class="container">
      <div class="row">
         <div class="d-block mx-auto col-lg-8 col-md-12">
            <div class="card">
               <div class="card-body">
                  <div class="item7-card-img">
                     <asp:Image ID="Image1" runat="server" CssClass="w-100" />
                     <div class="item7-card-text"> <span class="badge badge-info"><asp:Literal ID="ltrCategory" runat="server"></asp:Literal></span> </div>
                  </div>
                  <div class="item7-card-desc d-flex mb-2 mt-3">
                     <a href="#"><i class="fa fa-calendar-o text-muted mr-2"></i><asp:Literal ID="ltrDateDay" runat="server"></asp:Literal> <asp:Literal ID="ltrMonth" runat="server"></asp:Literal></a> 
                      
                     <div class="ml-auto"> <a href="#"><i class="fa fa-eye text-muted mr-2"></i><asp:Literal ID="ltrView" runat="server"></asp:Literal> Okunma</a> </div>
                  </div>
                  <a href="#" class="text-dark">
                     <h2 class="font-weight-semibold"><asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h2>
                  </a>
                  <asp:Literal ID="ltrDesc2" runat="server"></asp:Literal>
               </div>
            </div>
           
         </div>
      </div>
   </div>
</section>-->

    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="konak-detay-content-text-cover">
                    <div class="konak-detay-content-text-header">
                        <h1>
                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        </h1>
                    </div>
                    <div class="konak-detay-content-text-header-bottom">
                        <div>
                            <p>
                                <i class="fas fa-user-alt"></i>
                                <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <i class="fas fa-bed"></i>
                                <asp:Literal ID="Literal4" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <i class="fas fa-bath"></i>
                                <asp:Literal ID="Literal5" runat="server"></asp:Literal>
                            </p>
                        </div>
                        <div>
                            <a href="javascrpt:void(0)" class="share-inline"><i class="fas fa-share-alt"></i>Paylaş </a>
                        </div>
                    </div>
                    <div class="col-md-12 share-content">
                        <div class="addthis_inline_share_toolbox"></div>
                    </div>
                    <div class="konak-detay-content-text">
                        <p>

                            <asp:Literal ID="ltrDesc" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div class="konak-detay-list-cover">
                        <p> <asp:Literal ID="ltrTourDay" runat="server"></asp:Literal>  </p>

                        <div class="konak-detay-list-item-cover ">
                            <div class="konak-detay-list-item ">
                                <asp:Repeater ID="rptDefinition" runat="server">
                                    <ItemTemplate>
                                        <div class="konak-detay-content-text tur-detay">
                                            <h1>1. Gün</h1>
                                            <asp:Literal ID="ltrTourDaydesc" runat="server"></asp:Literal>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="col-lg-4">
                <div class="konak-detay-fiyat-cover">
                    <div class="konak-deta-fiyat">
                        <div class="konak-detay-inner">
                            <%--<p class="eskifiyat second-eskifiyat"><i class="fas fa fa-try"></i>2400 </p>--%>
                            <p class="konak-detay-guncel-fiyat">
                                <i class="fas fa fa-try"></i>
                                <asp:Literal ID="ltrPrices" runat="server"></asp:Literal>
                                <span>/ 1 gece </span>
                            </p>
                        </div>
                        <div class="second-detay-cover">
                            <div class="konak-detay-inner second-detay">
                                <p class="konak-page-fiyat-header">Giriş Tarihi </p>
                                <label class="konak-page-fiyat-date-input">
                                    <i class="far fa-calendar-alt"></i>
                                    <input placeholder="Lütfen Tarih Giriniz" type="text" id="input1" disabled="disabled">
                                </label>
                            </div>
                        </div>
                        <div class="konak-detay-inner">
                            <p class="konak-page-fiyat-header">Çıkış Tarihi </p>
                            <label class="konak-page-fiyat-date-input">
                                <i class="far fa-calendar-alt"></i>
                                <input placeholder="Lütfen Tarih Giriniz" type="text" id="input2" disabled="disabled">
                            </label>
                        </div>
                        <div class="konak-detay-inner last-konak-inner">

                            <a href="javascript:void(0)" class="randevuButton">Rezervasyon Yap</a>

                        </div>
                    </div>
                </div>
                <div class="konak-detay-info-cover">
                    <div class="konak-detay-info-content k-top">
                        <h1>Kolay ve hızlı
                            <br>
                            rezervasyon</h1>
                        <p>Ön ödemeyi yapın oteliniz sizin için ayrılsın. </p>
                    </div>
                    <div class="konak-detay-info-content k-bottom">
                        <div class="konak-detay-info-content-text">
                            <p>Toplam rezervasyon tutarınızın sadece <span>%50</span> ’sini ödeyerek rezervasyonunuzu tamamlayabilirsiniz.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="outher-container pb-5 pt-5">
        <div class="container-fluid mb-5">
            <div class="row  slider-header mb-5">
                <div class="col-lg-12">
                    <div class="swiper-top-inner">
                        <h1>Benzer Turlar</h1>
                        <a href="#">Tüm Turlar
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="swiper mySwiper CategorySwiper">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="Repeater1" runat="server">
                                <ItemTemplate>
                                    <div class="col-lg-4">
                                        <div class="konak-content-cover">
                                            <div class="konak-content-top">
                                                <div class="konak-content-top-img tur" style="background-image: url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>)" second-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("second_image")%>" first-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>"></div>
                                                <p>
                                                    <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/haber-detay/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("blog_title").ToString()) %>-i-<%#Eval("blog_id") %>"></a>
                                                </p>
                                                <div class="hover-share"><a href="#"><i class="fas fa-share-alt"></i>Paylaş </a></div>
                                            </div>
                                            <div class="konak-content-bottom">
                                                <p class="eskifiyat"><i class="fas fa-lira-sign"></i>2400</p>
                                                <div class="guncelfiyat">
                                                    <div class="guncelfiyat-inner">
                                                        <p><i class="fas fa-lira-sign"></i><%#Eval("sell_price") %>   <span class="tur-text">/ 1 hafta</span>  </p>
                                                    </div>
                                                    <div class="guncelfiyat-inner">
                                                        <a href="#">Rezervasyon  <i class="fas fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row  slider-header mb-5">
                <div class="col-lg-12">
                    <div class="swiper-top-inner konak-detay-swiper-peg">
                        <div class="swiper-button-cover">
                            <div class="second-swiper-arrows-button">
                                <div class="swiper-second-prev "><i class="fas fa-arrow-left"></i></div>
                                <div class="swiper-second-next "><i class="fas fa-arrow-right"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
