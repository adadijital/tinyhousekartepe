﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class blog_list : Base
    {
        int type;
        int page_count;
        string pageUrl = "/"+Localization.LanguageStr+"/haberler";
        int page_id = 0;
        int category_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["page"] != null)
                    page_id = Convert.ToInt32(Request.QueryString["page"]);

                int carpilmis = (page_id != 0) ? page_id * 100 : 0;
                page_count = 10;


                if (Request.QueryString["category_id"] != null)
                {
                    category_id = Request.QueryString["category_id"].toInt();

                    var _List = db.blog_lang.Where(q => q.lang_id == Localization.Language && q.blog.category_id == category_id).Select(s => new {
                        s.blog_id,
                        s.blog.blog_category.blog_category_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,
                        s.blog.created_time,
                        s.blog_title,
                        s.blog_desc,
                        s.blog.image_path,
                        s.blog.view_count,
                    }).OrderByDescending(o => o.blog_id).ToList();


                    if (_List.Count > 0)
                    {
                        int loop_count = _List.Count() / page_count;

                        if (_List.Count() % page_count != 0) loop_count++;

                        for (int i = 1; i <= loop_count; i++)
                        {

                            if (i == page_id)
                                ltrPaging.Text += "<li class=\"page-item active\"><a class=\"page-link\" href=" + pageUrl + "-p-" + i + ">" + i + "</a></li>";
                            else
                                ltrPaging.Text += "<li class=\"page-item\"><a class=\"page-link\" href=" + pageUrl + "-p-" + i + ">" + i + "</a></li>";

                        }

                        rptBlogList.DataSource = _List.Skip((page_id - 1) * page_count).Take(page_count);
                        rptBlogList.DataBind();
                    }
                }
                else
                {
                    var _List = db.blog_lang.Where(q => q.lang_id == Localization.Language).Select(s => new {
                        s.blog_id,
                        s.blog.blog_category.blog_category_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,
                        s.blog.created_time,
                        s.blog_title,
                        s.blog_desc,
                        s.blog.image_path,
                        s.blog.view_count,
                    }).OrderByDescending(o => o.blog_id).ToList();


                    if (_List.Count > 0)
                    {
                        int loop_count = _List.Count() / page_count;

                        if (_List.Count() % page_count != 0) loop_count++;

                        for (int i = 1; i <= loop_count; i++)
                        {

                            if (i == page_id)
                                ltrPaging.Text += "<li class=\"page-item active\"><a class=\"page-link\" href=" + pageUrl + "-p-" + i + ">" + i + "</a></li>";
                            else
                                ltrPaging.Text += "<li class=\"page-item\"><a class=\"page-link\" href=" + pageUrl + "-p-" + i + ">" + i + "</a></li>";

                        }

                        rptBlogList.DataSource = _List.Skip((page_id - 1) * page_count).Take(page_count);
                        rptBlogList.DataBind();
                    }
                }
             

                var _recent = db.blog_lang.Where(q => q.lang_id == Localization.Language).Select(s => new {
                    s.blog_id,
                    s.blog.blog_category.blog_category_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,
                    s.blog.created_time,
                    s.blog_title,
                    s.blog_desc,
                    s.blog.image_path,
                    s.blog.view_count,
                }).OrderByDescending(o => o.view_count).Take(10).ToList();

                if(_recent !=null)
                {
                    rptRecent.DataSource = _recent;
                    rptRecent.DataBind();

                }

                //var _categoryList = db.blog_category_lang.Select(s => new {

                //    s.lang_id,
                //    s.blog_category.is_active,
                //    s.blog_category_id,
                //    s.category_name
                //}).Where(q => q.lang_id == Localization.Language && q.is_active).ToList();





                //if(_categoryList.Count()>0)
                //{
                //    rptCategoryList.DataSource = _categoryList;
                //    rptCategoryList.DataBind();
                //}
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

    
    }
}