﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using static SeyahatEkspresi.Core.AdvertManager;

namespace SeyahatEkspresi
{
    public partial class advert_list : Base
    {
        AdvertManager _advertManager = new AdvertManager();
        int category_id;

        CategoryManager _categoryManager = new CategoryManager();
        List<FilterModel> _filters = new List<FilterModel>();
        JavaScriptSerializer _JScript = new JavaScriptSerializer();
        string strFilter = string.Empty;
        public int AdvertCount;
        public int PageSize = 15;
        public int PageIndex = 1;
        string PagerLink;
        string FilterUrl;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                btnHepsi.NavigateUrl = "/"+ SeyahatEkspresi.Core.Localization.LanguageStr.ToString()+"/ilanlar";

                if (Request.QueryString["category_id"] != null)
                {
                    category_id = Request.QueryString["category_id"].toInt();

                    if (Request.QueryString["page"] != null)
                    {
                        PageIndex = Request.QueryString["page"].toInt();
                    }

                    if (Request.QueryString["filter"] != null)
                        strFilter = Request.QueryString["filter"].ToString();


                    if (!Page.IsPostBack)
                    {

                        #region Filters
                        try
                        {
                            if (!string.IsNullOrEmpty(strFilter))
                            {
                                _filters = _JScript.Deserialize<List<FilterModel>>(strFilter);
                                FilterUrl = "-f=" + strFilter;
                            }



                        }
                        catch (Exception ex) { Log.Add(ex); }
                        #endregion
                    }

                    var _parentCat = db.categories_lang.Where(q => q.categories.is_active && q.lang_id == Localization.Language && q.categories.parent_id == 0 && q.categories.is_deleted == false).ToList();

                    if (_parentCat.Count() > 0)
                    {
                        rptCategories.DataSource = _parentCat;
                        rptCategories.DataBind();
                        rptCategories1.DataSource = _parentCat;
                        rptCategories1.DataBind();
                    }


                    var cat = db.categories.FirstOrDefault(f => f.id == category_id && f.is_active == true);
                    string cat_name = cat.categories_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name;
                    PagerLink = "/" + Localization.LanguageStr + "/ilanlar-" + cat.categories_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name.ToURL() + "-c-" + category_id;
                    FilterUrl =  strFilter;

                    ltrCategoryName.Text = cat_name;
                    ltrCategoryName2.Text = cat_name;

                    var _advertList = _advertManager.getAdvertListByCategoryID(category_id);


                    if (_advertList.Count() > 0)
                    {
                        ltrCount.Text = _advertList.Count().ToString();
                        AdvertCount = _advertList.Count();
                        rptAdvertList.DataSource = _filterAdverts(_advertList)
                              .Skip((PageIndex - 1) * PageSize)
                              .Take(PageSize)
                              .ToList();
                        rptAdvertList.DataBind();
                        Paging(AdvertCount);

                    }
                    else
                    {
                        ltrCount.Text = "0";
                    }

                }
                else
                {

                    //tüm ilanlar

                    if (Request.QueryString["page"] != null)
                    {
                        PageIndex = Request.QueryString["page"].toInt();
                    }

                    if (Request.QueryString["filter"] != null)
                        strFilter = Request.QueryString["filter"].ToString();


                    if (!Page.IsPostBack)
                    {
                        btnHepsi.CssClass = "btnActive";

                        #region Filters
                        try
                        {
                            if (!string.IsNullOrEmpty(strFilter))
                            {
                                _filters = _JScript.Deserialize<List<FilterModel>>(strFilter);
                                FilterUrl = "-f=" + strFilter;
                            }



                        }
                        catch (Exception ex) { Log.Add(ex); }
                        #endregion
                    }

                    var _parentCat = db.categories_lang.Where(q => q.categories.is_active && q.lang_id == Localization.Language && q.categories.parent_id == 0 && q.categories.is_deleted == false).ToList();

                    if (_parentCat.Count() > 0)
                    {
                        rptCategories.DataSource = _parentCat;
                        rptCategories.DataBind();
                        rptCategories1.DataSource = _parentCat;
                        rptCategories1.DataBind();
                    }


                    var cat = db.categories.FirstOrDefault(f => f.id == category_id && f.is_active == true);
                    string cat_name = "Konaklama";               
                    PagerLink = "/" + Localization.LanguageStr + "/ilanlar";
                    FilterUrl = strFilter;

                    ltrCategoryName.Text = cat_name;
                    ltrCategoryName2.Text = cat_name;

                    var _advertList = _advertManager.getAdvertListAll();


                    if (_advertList.Count() > 0)
                    {
                        ltrCount.Text = _advertList.Count().ToString();
                        AdvertCount = _advertList.Count();
                        rptAdvertList.DataSource = _filterAdverts(_advertList)
                              .Skip((PageIndex - 1) * PageSize)
                              .Take(PageSize)
                              .ToList();
                        rptAdvertList.DataBind();
                        Paging(AdvertCount);

                    }
                    else
                    {
                        ltrCount.Text = "0";
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        public void Paging(int TotalCount)
        {
            try
            {
                #region paging
                ltrPaging.Text = "";

                ltrPaging.Text += "<div class='konak-content-bottom-nav-num center-block text-center'>";

                ltrPaging.Text += "<ul class='pagination mb-0'>";


                int loop = 0;
                if (TotalCount > PageSize)
                {
                    pnlPaging.Visible = true;
                       loop = TotalCount / PageSize;
                    if (TotalCount % PageSize != 0) loop++;
                    int lCount = 0;

                    if (loop > 1)
                    {
                        if ((PageIndex + PageSize) <= loop)
                            lCount = (PageIndex + (PageSize - 1));
                        else
                            lCount = (PageIndex + (loop - PageIndex));


                        if (PageIndex > 5)
                            ltrPaging.Text += "<li class='page-item'><a href='" + PagerLink.ToString() + "-p=" + (PageIndex - 1) + FilterUrl + "'>«</a></li>";

                        if (PageSize > lCount)
                        {
                            ltrPaging.Text += "<li class='page-item'><a href=\"javascript:void(0);\">...</a></li>";
                        }


                        int starterIndex = (PageIndex < 10) ? 1 : PageIndex;



                        for (int i = starterIndex; i <= lCount; i++)
                        {
                            if (PageIndex == i)
                                ltrPaging.Text += "<li class='page-item active'><a href='" + PagerLink + "'>" + i + "</a></li>";
                            else
                                ltrPaging.Text += "<li class='page-item'><a  href='" + PagerLink + "-p=" + i + FilterUrl + "'>" + i + "</a></li>";
                        }



                        if (PageIndex < lCount)
                        {
                            if (lCount < loop)
                                ltrPaging.Text += "<li class='page-item disabled'><a href='javascript:void(0);'>...</a></li>";

                            ltrPaging.Text += "<li class='page-item'><a href='" + PagerLink + "-p=" + (PageIndex + 1) + "" + FilterUrl + "'>»</a></li>";
                        }
                    }

                    ltrPaging.Text += "</ul>";

                    ltrPaging.Text += "</div>";


                }
                else
                {
                    pnlPaging.Visible = false;
                }

                #endregion
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }

        List<AdvertData> _filterAdverts(List<AdvertData> _advertList, string page = "")
        {
            #region Filter Products
            try
            {
                if (_filters.Count > 0)
                {
                    foreach (var filter in _filters)
                    {


                        if (filter.name == "price")
                        {
                            List<decimal> _filterPrices = new List<decimal>();

                            foreach (var value in filter.values)
                                _filterPrices.Add(value.todecimal());


                            decimal price1 = _filterPrices[0].todecimal();
                            decimal price2 = _filterPrices[1].todecimal();

                            if (price1 > 0 && price2 > 0)
                            {
                                _advertList = _advertList
                                    .Where(q => q.sell_price >= price1 && q.sell_price <= price2)
                                    .ToList();
                            }
                            else if (price1 > 0)
                            {
                                _advertList = _advertList
                                    .Where(q => q.sell_price >= price1)
                                    .ToList();
                            }
                            else if (price2 > 0)
                            {
                                _advertList = _advertList
                                    .Where(q => q.sell_price <= price2)
                                    .ToList();
                            }
                        }
                        else if (filter.name == "city")
                        {
                            int filterCity = filter.values.FirstOrDefault().toInt();

                            if (filterCity > 0)
                                _advertList = _advertList.Where(q => q.geo_city_id == filterCity).ToList();

                        }
                        else if (filter.name == "town")
                        {
                            int filterTown = filter.values.FirstOrDefault().toInt();

                            if (filterTown > 0)
                                _advertList = _advertList.Where(q => q.geo_town_id == filterTown).ToList();

                        }
                        else if (filter.name == "quarter")
                        {
                            int filterQuarter = filter.values.FirstOrDefault().toInt();

                            if (filterQuarter > 0)
                                _advertList = _advertList.Where(q => q.geo_quarter_id == filterQuarter).ToList();

                        }
                        else if (filter.name == "sort")
                        {
                            string filterSort = filter.values.FirstOrDefault();
                            switch (filterSort)
                            {
                                case "0":
                                    string url = Request.RawUrl;
                                    if (url.Contains("-f="))
                                    {
                                        string[] arrUrl = url.Split(new string[] { "-f=" }, StringSplitOptions.None);
                                        url = arrUrl[0];
                                        Response.Redirect(url, false);
                                    }
                                    break;
                                case "1"://fiyata göre artan
                                    _advertList = _advertList.OrderBy(q => q.sell_price).ToList();
                                    break;
                                case "2": //fiyata göre azalan
                                    _advertList = _advertList.OrderByDescending(q => q.sell_price).ToList();
                                    break;


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }

            return _advertList;
            #endregion
        }

    }
}