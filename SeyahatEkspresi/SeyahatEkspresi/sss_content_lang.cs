//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeyahatEkspresi
{
    using System;
    using System.Collections.Generic;
    
    public partial class sss_content_lang
    {
        public int id { get; set; }
        public int sss_id { get; set; }
        public byte lang_id { get; set; }
        public string sss_title { get; set; }
        public string sss_desc { get; set; }
    
        public virtual lang lang { get; set; }
        public virtual sss_content sss_content { get; set; }
    }
}
