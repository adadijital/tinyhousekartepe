﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="blog_list.aspx.cs" Inherits="SeyahatEkspresi.blog_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">
                        <p>
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/ilanlar-<%#SeyahatEkspresi.cExtensions.ToURL(Eval("category_name").ToString()) %>-c-<%#Eval("cat_id") %>" title="<%#Eval("category_name") %>">
                                <%#Eval("category_name") %> 
                            </a>
                        </p>
                        <h1>
                            <asp:Literal ID="ltrTourName" runat="server"></asp:Literal></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- <section>
        <div class="bannerimg cover-image bg-background3" data-image-src="/assets/img/banner2.jpg">
            <div class="header-text mb-0">
                <div class="container">
                    <div class="text-center text-white ">
                        <h1 class="">Blog</h1>
                        <ol class="breadcrumb text-center">
                            <li class="breadcrumb-item"><a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>">Anasayfa</a></li>
                            <li class="breadcrumb-item active text-white" aria-current="page">Blog</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>-->




    


    <div class="container mb-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="konak-page-content-header-cover">
                    <div class="konak-page-content-header">
                        <p><span>
                            <asp:Literal ID="ltrCount" runat="server"></asp:Literal></span> Adet Tur seçeneği listelenmektedir</p>
                        <label>
                            <p>Sıralama Yöntemi
                                <asp:Literal ID="ltrSort" runat="server" />
                                <div>
                                    <img class="filter-icon" src="/assets/img/Group 107.png"></div>
                            </p>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="konak-page-filter-menu-cover">
                    <div class="konak-page-filter-menu">
                        <a href="#">Hepsi </a>
                        <a href="#">Yurt İçi</a>
                        <a href="#">Yurt Dışı</a>
                        <a href="#">Kültür Turları</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- <section class="sptb">
   <div class="container">
      <div class="row">
         <div class="col-xl-12 col-lg-12 col-md-12">
            <div class="row">
               <asp:Repeater ID="rptBlogList" runat="server">
        <ItemTemplate>

                   
               <div class="col-xl-4 col-lg-6 col-md-12">
                  <div class="card">
                     <div class="item7-card-img">
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/detay/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("blog_title").ToString()) %>-i-<%#Eval("blog_id") %>"></a> <img src="/uploads/blog/<%#Eval("image_path") %>" alt="img" class="cover-image"> 
                        <div class="item7-card-text"> <span class="badge badge-secondary"><%#Eval("category_name") %></span> </div>
                     </div>
                     <div class="card-body">
                        <div class="item7-card-desc d-flex mb-2">
                           <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/detay/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("blog_title").ToString()) %>-i-<%#Eval("blog_id") %>"><i class="fa fa-calendar-o text-muted mr-2"></i><%#Eval("created_time", "{0:%d}") %> <%#Eval("created_time", "{0:MMMM}") %></a> 
                           <div class="ml-auto"> <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/detay/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("blog_title").ToString()) %>-i-<%#Eval("blog_id") %>"><i class="fa fa-eye text-muted mr-2"></i><%#Eval("view_count") %> Okunma</a> </div>
                        </div>
                        <a href="#" class="text-dark">
                           <h4 class="font-weight-semibold"><%#Eval("blog_title") %></h4>
                        </a>
                        <p><%# Eval("blog_desc").ToString().Length > 180 ? Eval("blog_desc").ToString().Substring(0,180 )+" ...": Eval("blog_desc") %></p>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/detay/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("blog_title").ToString()) %>-i-<%#Eval("blog_id") %>" class="btn btn-secondary btn-sm">Yazıya Git</a> 
                     </div>
                  </div>
               </div>
           </ItemTemplate>
    </asp:Repeater>
            </div>
            <div class="center-block text-center">
               <ul class="pagination mb-0">
                 <asp:Literal ID="ltrPaging" runat="server"></asp:Literal>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>-->


    <div class="container mt-5 mb-5">
        <div class="row mb-5">
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                    <div class="col-lg-4">
                        <div class="konak-content-cover">
                            <div class="konak-content-top">
                                <div class="konak-content-top-img tur" style="background-image: url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>)" second-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("second_image")%>" first-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>"></div>
                                <p>
                                   <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/haber-detay/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("blog_title").ToString()) %>-i-<%#Eval("blog_id") %>"></a>
                                </p>
                                <div class="hover-share"><a href="#"><i class="fas fa-share-alt"></i>Paylaş </a></div>
                            </div>
                            <div class="konak-content-bottom">
                                <p class="eskifiyat"><i class="fas fa-lira-sign"></i>2400</p>
                                <div class="guncelfiyat">
                                    <div class="guncelfiyat-inner">
                                        <p><i class="fas fa-lira-sign"></i> <%#Eval("sell_price") %>   <span class="tur-text">/ 1 hafta</span>  </p>
                                    </div>
                                    <div class="guncelfiyat-inner">
                                        <a href="#">Rezervasyon  <i class="fas fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>


   <!-- <section class="sptb">
        <div class="container">
            <div class="section-title center-block text-center">
                <%--  <h1>Kategoriler</h1>
                <div class="row">
                    <asp:Repeater ID="rptCategoryList" runat="server">
        <ItemTemplate>
            <div class="col-4 col-sm-3 col-md-3 col-lg-6"><a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/haberler-c-<%#Eval("blog_category_id") %>"><%#Eval("category_name") %></a></div>
        </ItemTemplate>
    </asp:Repeater>
                </div>--%>
                <h1>En Çok Okunanlar</h1>
            </div>
            <div id="small-categories" class="owl-carousel owl-carousel-icons2">
                <asp:Repeater ID="rptRecent" runat="server">
                    <ItemTemplate>
                        <div class="item">
                            <div class="card mb-0">
                                <div class="card-body">
                                    <div class="cat-item text-center">
                                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/haber-detay/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("blog_title").ToString()) %>-i-<%#Eval("blog_id") %>"></a>
                                        <div class="cat-img">
                                            <img src="/uploads/blog/<%#Eval("image_path") %>" class="w-100 h-100" alt="img">
                                        </div>
                                        <div class="cat-desc">
                                            <h5 class="mb-0"><%#Eval("blog_title") %> </h5>
                                            <small class="d-block"><%#Eval("created_time", "{0:%d}") %> <%#Eval("created_time", "{0:MMMM}") %></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>-->
</asp:Content>
