﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static SeyahatEkspresi.Core.AdvertManager;

namespace SeyahatEkspresi
{
    public partial class advert_list_search : Base
    {
        AdvertManager _advertManager = new AdvertManager();
        int category_id;
        int pageNo = 1;
        string SearchKey;
        int city_id;
        string start_date;
        string finish_date;
        string person_count;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["search_key"] != null)
                {
                    SearchKey = Session["key"].ToString();



                    if (SearchKey != null)
                    {
                        if (!Page.IsPostBack)
                        {
                            try
                            {
                                var advertList = _advertManager.getAdvertListBySearch(SearchKey, Localization.Language);
                                rptAdvertList.DataSource = advertList;
                                rptAdvertList.DataBind();

                                ltrCategoryName.Text = SearchKey;
                                ltrCategoryName2.Text = SearchKey;

                            }
                            catch (Exception ex)
                            {
                                Response.Write(ex);

                            }
                        }
                    }
                }
                else
                {
                    if (Request.QueryString["search_key2"] != null)
                    {
                        SearchKey = Request.QueryString["search_key2"].ToString();

                        if (Request.QueryString["start_date"] !=null)
                            start_date = Request.QueryString["start_date"].ToString();

                        if (Request.QueryString["finish_date"] != null)
                            finish_date = Request.QueryString["finish_date"].ToString();


                        if (Request.QueryString["person_count"] != null)
                            person_count = Request.QueryString["person_count"].ToString();

                

                        var _result = db.ads_lang
                            .Where(q => q.lang_id == Localization.Language
                            && q.ads.is_active && q.ads.is_deleted == false
                            && q.ads.ads_images.Count() > 0
                            && (q.ads_name.Contains(SearchKey)
                            || q.description.Contains(SearchKey)
                            || q.ads.geo_cities.city_name.Contains(SearchKey)
                            || q.ads.geo_town.town_name.Contains(SearchKey)
                            || q.ads.ad_info_value.FirstOrDefault().ad_info_value_lang.Where(q2 => q2.lang_id == 1 && q2.ad_info_value.Contains(SearchKey)).Count() > 0)
                 ).Select(s => new AdvertData
                 {
                     market_price = s.ads.market_price,
                     kisi_sayisi = s.kisi_sayisi,
                     yatak_sayisi = s.yatak_sayisi,
                     banyo_sayisi = s.banyo_sayisi,
                     isinma_turu = s.isinma_turu,

                     konum_durumu = s.konum_durumu,
                     advert_name = s.ads_name,
                     advert_id = s.ads_id,
                     display_order = s.ads.display_order,
                     main_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                     sell_price = s.ads.sell_price,
                     created_time = s.ads.created_time,
                     geo_city_name = s.ads.geo_cities.city_name,
                     geo_town_name = s.ads.geo_town.town_name,
                     geo_quarter_name = s.ads.geo_quarter.quarter_name,
                     ads_no = s.ads.ads_no,
                     member_name_surname = s.ads.member_id > 0 ? s.ads.members.name_surname : "Diwada",
                     ads_view = s.ads.ads_view,
                     geo_city_id = s.ads.geo_city_id,
                     geo_town_id = s.ads.geo_town_id,
                     geo_quarter_id = s.ads.geo_quarter_id,
                     first_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                     second_image = s.ads.ads_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_name,
                 });

                        if (city_id > 0)
                        {
                            _result = _result.Where(q => q.geo_city_id == city_id);
                        }

                        //if (!string.IsNullOrEmpty(person_count))
                        //{
                        //    _result = _result.Where(q => q.kisi_sayisi.Contains(person_count));
                        //}

                        rptAdvertList.DataSource = _result.OrderBy(o => o.display_order).ToList<AdvertData>();
                        rptAdvertList.DataBind();

                        ltrCategoryName.Text = "Seçtiğiniz filtrelere uygun konaklama seçeneklerimiz";
                        ltrCategoryName2.Text = "Seçtiğiniz filtrelere uygun konaklama seçeneklerimiz";

                    }
                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

    }
}