﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class tour_list : Base
    {
        int cat_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["cat_id"] != null)
            {
                cat_id = Request.QueryString["cat_id"].toInt();

                var _detail = db.tour_category.Where(q => q.id == cat_id).FirstOrDefault();

                if (_detail != null)
                {
                    ltrTourName.Text = _detail.category_name;
                }

                var _catList = db.tour_category.Where(q => q.is_active).OrderBy(o => o.display_order).ToList();

                if (_catList.Count() > 0)
                {
                    rptCat.DataSource = _catList;
                    rptCat.DataBind();
                    rptCat1.DataSource = _catList;
                    rptCat1.DataBind();
                }

                var _list = db.our_tour.Select(s => new
                {
                    s.tour_category.is_active,
                    s.display_order,
                    s.tour_name,
                    s.tour_time,
                    s.hotel_status,
                    s.personal_count,
                    s.id,
                    first_image = s.tour_images.OrderBy(o => o.display_order).FirstOrDefault().img_path,
                    second_image = s.tour_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_path,
                    s.market_price,
                    s.sell_price,
                }).Where(q => q.is_active && q.id == cat_id).OrderBy(o => o.display_order).ToList();

                if (_list.Count() > 0)
                {
                    ltrCount.Text = _list.Count.ToString();
                    rptTourList.DataSource = _list;
                    rptTourList.DataBind();
                }
            }
            else
            {
                ltrTourName.Text = "Turlar";

                var _catList = db.tour_category.Where(q => q.is_active).OrderBy(o => o.display_order).ToList();

                if (_catList.Count() > 0)
                {
                    rptCat.DataSource = _catList;
                    rptCat.DataBind();
                    rptCat1.DataSource = _catList;
                    rptCat1.DataBind();
                }

                var _list = db.our_tour.Select(s => new
                {
                    s.tour_category.is_active,
                    s.display_order,
                    s.tour_name,
                    s.tour_time,
                    s.hotel_status,
                    s.personal_count,
                    s.id,
                    first_image = s.tour_images.OrderBy(o => o.display_order).FirstOrDefault().img_path,
                    second_image = s.tour_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_path,
                    s.market_price,
                    s.sell_price,
                }).Where(q => q.is_active).OrderBy(o => o.display_order).ToList();

                if (_list.Count() > 0)
                {
                    ltrCount.Text = _list.Count.ToString();
                    rptTourList.DataSource = _list;
                    rptTourList.DataBind();
                }
            }
        }
    }
}