﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class mesafeliSatisSozlesmesi :Base
    {
        int page_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["page_id"] !=null)
            {
                page_id = Request.QueryString["page_id"].toInt();

                var _Detail = db.page_lang.Where(q => q.lang_id == Localization.Language && q.page_id == page_id).FirstOrDefault();

                var _detail2 = db.page_content.Where(q => q.page_id == page_id && q.lang_id == Localization.Language).FirstOrDefault();
       

                if (_Detail !=null)
                {
                    ltrPagename.Text = _Detail.page_name;
                    ltrPagename2.Text = _Detail.page_name;
                    ltrPageTitle.Text = _Detail.page_title;
                    ltrPageDesc.Text = _detail2.page_content_text;
                }
            }
        }
    }
}