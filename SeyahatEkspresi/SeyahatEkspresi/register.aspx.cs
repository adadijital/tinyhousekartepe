﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class register : Base
    {
        MemberManager _memberManager = new MemberManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["email"]))
                    txtLoginEmail.Text = Request.QueryString["email"].ToString();

                if (_memberManager.IsLoggedIn)
                    Response.Redirect("/"+Localization.LanguageStr,false);
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void lnkRegister_Click(object sender, EventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(txtNameSurname.Text.Trim()))
                {
                    if (!string.IsNullOrEmpty(txtRegisterEmail.Text.Trim()))
                    {
                        if (txtRegisterEmail.Text.IsEmail())
                        {
                            if (!(db.members.Where(q => q.is_active && q.email == txtRegisterEmail.Text.Trim() && q.is_deleted == false).Count() > 0))
                            {
                                if (!string.IsNullOrEmpty(txtRegisterPassword.Text.Trim()))
                                {
                                    if (txtRegisterPassword.Text.Trim().Length >= 6)
                                    {
                                        if (txtRegisterPassword.Text.Trim().Length <= 15)
                                        {
                                            if (txtRegisterPasswordAgain.Text.Trim() == txtRegisterPassword.Text.Trim())
                                            {
                                                string _password = txtRegisterPasswordAgain.Text.Trim().ToMD5();
                                                members _newMember = new members();
                                                _newMember.name_surname = txtNameSurname.Text.Trim();
                                                _newMember.email = txtRegisterEmail.Text.Trim();
                                                _newMember.password = _password;
                                                _newMember.is_active = true;
                                                _newMember.created_time = DateTime.Now;
                                                _newMember.created_ip = Base.VisitorIP;
                                                _newMember.is_deleted = false;
                                                _newMember.phone = txtRegisterPhone.Text.Trim();


                                                if (ckPerson.Checked)
                                                {
                                                    _newMember.account_type = 0; //kişisel hesap

                                                }

                                                if (ckCompany.Checked)
                                                {
                                                    if (!string.IsNullOrEmpty(txtCompanyName.Text.Trim()))
                                                    {
                                                        if (!string.IsNullOrEmpty(txtTaxOffice.Text.Trim()))
                                                        {
                                                            if (!string.IsNullOrEmpty(txtTaxNumber.Text.Trim()))
                                                            {
                                                                _newMember.account_type = 1; //firma hesabı
                                                                _newMember.company_name = txtCompanyName.Text.Trim();
                                                                _newMember.tax_number = txtTaxNumber.Text.Trim();
                                                                _newMember.tax_office = txtTaxOffice.Text.Trim();
                                                            }
                                                            else
                                                                showWarningBasic(this.Master, "Kurumsal hesap oluşturmak için vergi numarası girmelisiniz..", "Dikkat");
                                                        }
                                                        else
                                                            showWarningBasic(this.Master, "Kurumsal hesap oluşturmak için vergi dairesi girmelisiniz..", "Dikkat");
                                                    }
                                                    else
                                                        showWarningBasic(this.Master, "Kurumsal hesap oluşturmak için firma adı girmelisiniz..", "Dikkat");
                                                }

                                                db.members.Add(_newMember);

                                                if (db.SaveChanges() > 0)
                                                {
                                                    //hoşgeldin maili gönder
                                                    Email.HosGeldin(_newMember);
                                                    if (_memberManager.Login(txtRegisterEmail.Text.Trim(), txtRegisterPasswordAgain.Text.Trim(), true, true))
                                                    {
                                                        string cookie_content = cEncrytion.EncryptString(txtRegisterEmail.Text.Trim() + "-ogrccc-" + txtRegisterPasswordAgain.Text.Trim());
                                                        Response.Cookies["ogr_cck"].Value = cookie_content;
                                                        Response.Redirect("/" + Localization.LanguageStr+"/hesabim", false);

                                                    }
                                                    else
                                                        showWarningBasic(this.Master, "Giriş esnasında bir hata oluştu lütfen daha sonra tekrar deneyin", "Dikkat");
                                                }
                                                else
                                                    showWarningBasic(this.Master, "İşlem sırasında bir hata oluştu lütfen daha sonra tekrar deneyin", "Dikkat");

                                            }
                                            else
                                                showWarningBasic(this.Master, "Girdiğiniz şifreler bir biriyle uyuşmuyor.", "Dikkat");
                                        }
                                        else
                                            showWarningBasic(this.Master, "Şifreniz maksimum 15 haneli olmalıdır", "Dikkat");
                                    }
                                    else
                                        showWarningBasic(this.Master, "Şifreniz minimum 6 haneli olmalıdır", "Dikkat");
                                }
                                else
                                    showWarningBasic(this.Master, "Lütfen şifre alanını doldurun", "Dikkat");
                            }
                            else
                                showWarningBasic(this.Master, "Girdiğiniz e-posta adresi ile kayıtlı aktif bir hesap zaten var.", "Dikkat");

                        }
                        else
                            showWarningBasic(this.Master, "Lütfen geçerli bir e-mail adresi giriniz", "Dikkat");
                    }
                    else
                        showWarningBasic(this.Master, "Lütfen e-mail alanını doldurun", "Dikkat");
                }
                else
                    showWarningBasic(this.Master, "Lütfen ad soyad alanını doldurun", "Dikkat");
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void lnkGirisYap_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtLoginEmail.Text.Trim()))
                {
                    if (txtLoginEmail.Text.Trim().IsEmail())
                    {
                        if (!string.IsNullOrEmpty(txtLoginPassword.Text.Trim()))
                        {
                            if (_memberManager.Login(txtLoginEmail.Text.Trim(), txtLoginPassword.Text.Trim(), true, true))
                            {
                                string cookie_content = cEncrytion.EncryptString(txtLoginEmail.Text.Trim() + "-ogrccc-" + txtLoginPassword.Text.Trim());
                                Response.Cookies["ogr_cck"].Value = cookie_content;
                                Response.Redirect("/" + Localization.LanguageStr, false);

                            }
                            else
                                showWarningBasic(this.Master, "Giriş esnasında bir hata oluştu lütfen daha sonra tekrar deneyin", "Dikkat");
                        }
                        else
                            showWarningBasic(this.Master, "Giriş yapmak için şifrenizi girmelisiniz", "Dikkat");
                    }
                    else
                        showWarningBasic(this.Master, "Giriş yapmak için geçerli bir e-posta adresinizi girmelisiniz", "Dikkat");
                }
                else
                    showWarningBasic(this.Master, "Giriş yapmak için e-posta adresinizi girmelisiniz", "Dikkat");
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }
    }
}