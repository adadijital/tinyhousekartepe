﻿////axios.get("http://localhost:8080/api/Adverts/GetAdvertsShowcase")
////.then( response => {
////    console.log(response.data); //sunucudan dönen data, js objesi
////    console.log(response.status); //HTTP durum kodu
////    console.log(response.statusText); //HTTP durum mesajı
////    console.log(response.headers); //sunucudan dönen header bilgileri
////    console.log(response.config); //istek ile gönderilen konfigürasyon objesi
////});

//import { Swiper, SwiperSlide } from '/swiper/vue';
//import '/swiper/css';


//export default {
//    components: {
//        Swiper,
//        SwiperSlide,
//    },
//    setup() {
//        const mykonakSwiper = (swiper) => {
//            console.log(swiper);
//        };
//        //const onSlideChange = () => {
//        //    console.log('slide change');
//        //};
//        return {
//            mykonakSwiper,
//            onSlideChange,
//        };
//    },
//};


const headers = {
    'Content-Type': 'text/plain'
};



var ShowCaseApp = new Vue({
    el: '#pnlShowCase',
    data: {
        showCaseResult: "",
    },
    created: function () {
        this.loadValues();
    },
    methods: {
        loadValues: function () {
            this.info = "yukleniyor...";
            var app = this;

            axios.get("/api/Adverts/GetAdvertsShowcase",{ headers })
                .then(function (response) {
                    app.showCaseResult = response.data.Data;

                })
                .catch(function (error) {
                    app.showCaseResult = "Hata:" + error;
                });
        }
    }

});


var GetLastAdverts = new Vue({
    el: '#pnlLastAdverts',
    data: {
        lastAdvertsResult: "",
    },
    created: function () {
        this.loadValues();
    },
    methods: {
        loadValues: function () {
            this.info = "yukleniyor...";
            var app = this;

            axios.get("/api/Adverts/GetLastAdverts")
                .then(function (response) {
                    app.lastAdvertsResult = response.data.Data;


                })
                .catch(function (error) {
                    app.lastAdvertsResult = "Hata:" + error;
                });
        }
    }

});

var GetHomeParentCategory = new Vue({
    el: '#pnlHomeParentCategory',
    data: {
        homeParentCategorResult: "",
        //seo_url: "",
    },
    created: function () {
        this.loadValues();
    },
    methods: {
        loadValues: function () {
            this.info = "yukleniyor...";
            var app = this;

            axios.get("/api/Adverts/GetHomeParentCategories")
                .then(function (response) {
                    if (response.data.Status) {
                        app.homeParentCategorResult = response.data.Data;
                        //app.seo_url = response.data.Data.category_name;
                        // console.log(response.data.Data.category_name);

                        //response.data.Data.forEach((item) => {
                        //    app.seo_url += item.category_name;
                        //    console.log("found: ", item.category_name)

                        //});

                    }
                    else {
                        app.homeParentCategorResult = "Hata:" + error;
                        console.log("Hata:" + error);
                    }


                })
                .catch(function (error) {
                    app.homeParentCategorResult = "Hata:" + error;
                });

        }
    }

});


