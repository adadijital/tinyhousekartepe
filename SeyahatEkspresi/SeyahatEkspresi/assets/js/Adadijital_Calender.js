﻿$(window).on("load", function () {
    //var dates = [[05, 01, 2022], [15, 01, 2022], [25, 01, 2022], [23, 01, 2022], [05, 01, 2022]];
    var dates = [["05/01/2022"], ["15/01/2022"], ["25/01/2022"], ["05/02/2022"], ["22/02/2022"]];
    var _selectDates = new Array();

    $.datetimepicker.setLocale("tr");

    $("#input1").datepicker({
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
        minDate: 1,
        dateFormat: 'dd/mm/yy',
        numberOfMonths: [2, 1],
    });

    $("#input2").datepicker({
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
        minDate: 1,
        dateFormat: 'dd/mm/yy',
        numberOfMonths: [2, 1],
    });

    var start = 0;
    var reset = 0;
    var startDate;
    var cal_click = 0;
    var in_range_count = 0;
    var date_storage = [];
 
    $(".displayCalender").datepicker({
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
        minDate: 1,
        dateFormat: 'dd/mm/yy',
        numberOfMonths: [2, 1],
        beforeShowDay: function (date) {
            updateDatePickerCells($(this), date);

            var date1 = $.datepicker.parseDate('dd/mm/yy', $("#input1").val());
            var date2 = $.datepicker.parseDate('dd/mm/yy', $("#input2").val());



            var year = date.getFullYear(), month = date.getMonth(), day = date.getDate();
            for (var i = 0; i < dates.length; ++i) {
                if (day == dates[i][0].split('/')[0] && month == dates[i][0].split('/')[1] - 1 && year == dates[i][0].split('/')[2]) {
                    return [false, "booked dddd-" + date.getDate() + "_" + date.getMonth() + 1 + "_" + date.getFullYear()];
                }
            }
            return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "selected dddd-" + date.getDate() + "_" + date.getMonth() + 1 + "_" + date.getFullYear() : "dddd-" + date.getDate() + "_" + date.getMonth() + 1 + "_" + date.getFullYear(), ("0" + (date.getDate())).slice(-2) + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear()];
        },
        onSelect: function (dateText, date) {
            $(this).change();
            var daysOfYear = [];
            var date1 = $.datepicker.parseDate('dd/mm/yy', $("#input1").val());
            var date2 = $.datepicker.parseDate('dd/mm/yy', $("#input2").val());

            var day = date.selectedDay,
                mon = ("0" + (date.selectedMonth + 1)).slice(-2),
                year = date.selectedYear;

            
          

            if (cal_click == 2) {
                cal_click = 0;
                reset = 1;
                in_range_count = 0;
                startDate = "";
                StopDate = "";
                endDate = "";

            }

            if (!date1 || date2) {
                daysOfYear = [];
                $("#ContentPlaceHolder1_txtDate").val(dateText);
                $("#input1").val(dateText);
            }
            else {
                $("#input2").val(dateText);
                $("#ContentPlaceHolder1_txtDate2").val(dateText);

            }


            if (cal_click == 0) {
                startDate = dateText;
                daysOfYear = [];
                date_storage.push(day);
                date_storage.push(mon);
                date_storage.push(year);
                cal_click++;
            }
            else {
               
                var i = 0;
                var endDate = dateText;
                var StopDate = new Date(endDate);
  
                for (var d = new Date(startDate); d <= StopDate; d.setDate(d.getDate() + 1)) {
                    var date = new Date(d);
                    daysOfYear.push(("0" + (date.getDate() + 1)).slice(-2) + '/' + ("0" + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear());
                    i++;
                    in_range_count++;
                }
     

                console.log(daysOfYear);
                cal_click++;
           

            }

            setTimeout(function () {
                var endDate = dateText;
                var dateCheck = startDate;
                var i = 0;

                var StopDate = new Date(endDate);
                var k = (in_range_count - 1);
                for (var d = new Date(startDate); d <= StopDate; d.setDate(d.getDate() + 1)) {
                    var date = new Date(d);
                
                    i++;
                }

                if (daydiff(parseDate(startDate), parseDate(endDate)) < 0) {
                    cal_click = 0;
                    startDate = dateText;
                    daysOfYear = [];
                    date_storage = [];
                    date_storage.push(day);
                    date_storage.push(mon);
                    date_storage.push(year);
                    cal_click++;
                    reset = 0;
                    start = 0;
                } else {
                    if (reset == 1) {
                        reset = 0;
                    } 
                }

                start++;

            }, 80);

       
         
            disableSelect();

         //   $(".randevuButton").click();
       
        }
    }).on('change', function (date) {

    });

    var specificPrices = { "31/01/2022": "$300", "26/01/2022": "$63", "02/02/2022": "$169.99", "15/025/2022": "$63", "22/02/2022": "$63" }
    function updateDatePickerCells(dp, date) {
  
        setTimeout(function () {

            $('.ui-datepicker td > *').each(function (index, elem) {

                var checkDate = elem.parentElement.title;
                var specificPrice;
                if (specificPrices[checkDate]) {
                    specificPrice = specificPrices[checkDate];
                } else {
                    specificPrice = '';
                }

                var className = 'datepicker-content-' + CryptoJS.MD5(specificPrice).toString();
                addCSSRule('.ui-datepicker td a.' + className + ':after {content: "' + specificPrice + '"; display:flex;width:40px;justify-content:center}');
                $(this).addClass(className);
            });



        }, 0);
    }

    var dynamicCSSRules = [];
    function addCSSRule(rule) {
        if ($.inArray(rule, dynamicCSSRules) == -1) {
            $('head').append('<style>' + rule + '</style>');
            dynamicCSSRules.push(rule);
        }
    }

    function disableSelect() {


        const _date1 = new Date($("#input1").datepicker("getDate"));
        const _date2 = new Date($("#input2").datepicker("getDate"));

        const diffTime = Math.abs(_date2 - _date1);
        const days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        var _r;
        var _yakala;
        for (var i = _date1.getDate(); i <= _date2.getDate(); i++) {
            _r = $(".dddd-" + i + "_" + _date1.getMonth() + 1 + "_" + _date1.getFullYear());
            if (_r.hasClass("booked")) {

                _yakala = true;
            }

            if (_yakala === true) {
                setTimeout(function () { $(".selected").removeClass("selected"); }, 100);
                $("#input1").val("");
                $("#input2").val("");
                /*console.log(_r);*/
            }

        }

    }

    function pad(n) {
        return n < 10 ? '0' + n : n
    }

    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[0] - 1, mdy[1]);
    }

    function daydiff(first, second) {
        return Math.round((second - first) / (1000 * 60 * 60 * 24));
    }

    function formatDate(dateStr) {
        const d = new Date(dateStr);
        return d.getDate().toString().padStart(2, '0') + '/' + ("0" + (d.getMonth() + 1)).slice(-2) + '/' + d.getFullYear() ;
    }

});