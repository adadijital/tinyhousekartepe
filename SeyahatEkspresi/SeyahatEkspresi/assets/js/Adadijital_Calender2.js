﻿$(document).ready(function () {
    var _price = $("#hiddenPrice").text().trim();
    var _weekPrice = 0;

    var calcPrice = 0;


    $('head').append('<style>.ui-datepicker td:not(.ui-datepicker-week-end) a:after{content:"' + _price + ' ₺"; display:flex;font-size:11px;width:40px;justify-content:center;}</style>');
    $('head').append('<style>.ui-datepicker td:not(.ui-datepicker-week-end) span:after{content:"' + _price + ' ₺"; display:flex;font-size:11px;width:40px;justify-content:center;}</style>');


    if ($("#ContentPlaceHolder1_hdnWeekPrice").val() != '' && $("#ContentPlaceHolder1_hdnWeekPrice").val() != ' ') {
        _weekPrice = parseFloat($("#ContentPlaceHolder1_hdnWeekPrice").val().trim());

        setTimeout(function () {
            $('head').append('<style>.ui-datepicker-week-end a:after{content:"' + _weekPrice + ' ₺"; display:flex;font-size:11px;width:40px;justify-content:center;}</style>');
        }, 100);

    }
    else {
        $('head').append('<style>.ui-datepicker td a:after{content:"' + _price + ' ₺"; display:flex;font-size:11px;width:40px;justify-content:center;}</style>');
    }

   

    $.expr[":"].containsExact = function (obj, index, meta, stack) {
        return (obj.textContent || obj.innerText || $(obj).text() || "") == meta[3];
    };

  

    $("#input1").datepicker({
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
        minDate: 1,
        dateFormat: 'dd/mm/yy',
        numberOfMonths: [1, 1],
    });

    $("#input2").datepicker({
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
        minDate: 1,
        dateFormat: 'dd/mm/yy',
        numberOfMonths: [1, 1],
    });


    var rezervDates = new Array();
    var cal_click = 0;
    var startDate;
    var in_range_count = 0;
    var date_storage = [];
    var start = 0;
    var reset = 0;
    var daysOfYear = [];

    var i_h = 0;
    var calc_range = true;
    $('.year-calendar').datepicker({
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Paz", "Pzt", "Sa", "Çar", "Per", "Cum", "Cmt"],
        //minDate: 90,
        minDate: 'dateToday',
        language: 'tr',
        firstDay: 1,
        beforeShow: function () {
            if ($(window).width() < 768) {
                return { numberOfMonths: [1, 1] };
            } else {
                return { numberOfMonths: [1, 1] };
            }
        },
        beforeShowDay: function (date) {

            updateDatePickerCells($(this), date);
            var date1 = $.datepicker.parseDate('dd/mm/yy', $("#input1").val());
            var date2 = $.datepicker.parseDate('dd/mm/yy', $("#input2").val());
            var year = date.getFullYear(), month = date.getMonth(), day = date.getDate();
            for (var i = 0; i < rezervDates.length; ++i) {
                if (day == rezervDates[i].split('/')[0] && month == rezervDates[i].split('/')[1] - 1 && year == rezervDates[i].split('/')[2]) {
                    return [false, "booked dddd-" + ("0" + (date.getDate())).slice(-2) + "_" + ("0" + (date.getMonth() + 1)).slice(-2) + "_" + date.getFullYear()];

                }
            }

            return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "selected dddd-" + ("0" + (date.getDate())).slice(-2) + "_" + ("0" + (date.getMonth() + 1)).slice(-2) + "_" + date.getFullYear() : "dddd-" + ("0" + (date.getDate())).slice(-2) + "_" + date.getMonth() + 1 + "_" + date.getFullYear()];
        },
        onSelect: function (dateText, inst) {
            var day = inst.selectedDay, mon = inst.selectedMonth, year = inst.selectedYear;
            if (cal_click == 2) {
                cal_click = 0;
                reset = 1;
                in_range_count = 0;
                daysOfYear = [];
                date_storage = [];
                startDate = "";
                StopDate = "";
                endDate = "";
                $('.start-range').removeClass('start-range');
                $('.in-range').removeClass("in-range");
                $('.end-range').removeClass("end-range");
                reset = 0;
            }

            if (cal_click == 0) {

                startDate = dateText;
                daysOfYear = [];
                date_storage.push(day);
                date_storage.push(mon);
                date_storage.push(year);
                cal_click++;
            }
            else { // End Date selected 

                var endDate = dateText;
                var dateCheck = startDate;
                var i = 0;
                var StopDate = new Date(endDate);

                var k = (in_range_count - 1);

                for (var d = new Date(startDate); d <= StopDate; d.setDate(d.getDate() + 1)) {
                    var date = new Date(d);
                    daysOfYear.push(date.getDate() + '/' + ("0" + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear());
                    if (inst.dpDiv.find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:not(.ui-priority-secondary):containsExact(' + date.getDate() + ')').closest("td").hasClass("ui-datepicker-week-end")) {
                        calcPrice += parseFloat(_weekPrice);
                     
                    }
                    else {
                        calcPrice += parseFloat(_price);
                    }

                   // console.log(inst.dpDiv.find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:not(.ui-priority-secondary):containsExact(' + date.getDate() + ')').closest("td").attr('class'));


                    if (i > 0) {

                        inst.dpDiv.find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:not(.ui-priority-secondary):containsExact(' + date.getDate() + ')').closest("td").addClass("in-range");
                    }
                    i++;
                    in_range_count++;
                }

                $("#ContentPlaceHolder1_hdnSelectDays").val(daysOfYear);
                cal_click++;
            }
            setTimeout(function () {
                var endDate = dateText;
                var dateCheck = startDate;
                var i = 0;

                var StopDate = new Date(endDate);
                var k = (in_range_count - 1);
                var _yakala = false;

                if (parseDate(startDate) > parseDate(endDate) || parseDate(startDate) === parseDate(endDate)) {
                    $("#input1").val("");
                    $("#input2").val("");
                    $(".ui-state-active").removeClass("ui-state-active");
                    $(".ui-datepicker-current-day ").removeClass("ui-datepicker-current-day");
                    return false;
                }

                for (var d = new Date(startDate); d <= StopDate; d.setDate(d.getDate() + 1)) {

                    var date = new Date(d);
                    if (i > 0 && i < k) {

                      
                      //  console.log(inst.dpDiv.find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:after'));


                        if (inst.dpDiv.find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:not(.ui-priority-secondary):containsExact(' + date.getDate() + ')').closest("td").attr("class") == undefined) {

                            setTimeout(function () {
                                $(".year-calendar").datepicker();

                                daysOfYear = [];
                                $(".selected").removeClass("selected");
                                //$(".in-range").removeClass("in-range");

                                //$(".end-range").removeClass("end-range");
                                //$(".start-range").removeClass("start-range");

                                $('.start-range').removeClass('start-range');
                                $('.in-range').removeClass("in-range");
                                $('.end-range').removeClass("end-range");

                                $(".ui-state-active").removeClass("ui-state-active");
                                $(".ui-datepicker-current-day").removeClass("ui-datepicker-current-day");
                                date_storage = [];
                                cal_click = 0;
                                $("#input1").val("");
                                $("#input2").val("");
                                start = 0;
                                i_h = 0;
                                calc_range = false;
                                reset = 0;
                            }, 100);

                        }
                        else {
                            inst.dpDiv.find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:not(.ui-priority-secondary):containsExact(' + date.getDate() + ')').closest("td").addClass("in-range");
                        }

                    }
                    i++;
                }

                if (daydiff(parseDate(startDate), parseDate(endDate)) < 0) {
                    cal_click = 0;
                    startDate = dateText;
                    daysOfYear = [];
                    date_storage = [];
                    date_storage.push(day);
                    date_storage.push(mon);
                    date_storage.push(year);
                    cal_click++;
                    reset = 0;
                    start = 0;
                    inst.dpDiv.find('.start-range').removeClass('start-range');
                    inst.dpDiv.find('.end-range').removeClass('end-range');
                    inst.dpDiv.find('.in-range').removeClass('in-range');

                    $("#input1").val("");
                    $("#input2").val("");
                } else {
                    if (reset == 1) {
                        date_storage = [];
                        $("#input1").val("");
                        $("#input2").val("");
                        $('.start-range').removeClass('start-range');
                        $('.in-range').removeClass("in-range");
                        $('.end-range').removeClass("end-range");
                        reset = 0;
                    } else {
                        if (startDate !== endDate) {
                            setTimeout(function () {
                                inst.dpDiv.find('[data-year="' + date_storage[2] + '"][data-month="' + date_storage[1] + '"] a:containsExact(' + date_storage[0] + ')').closest("td").addClass('start-range');

                                var d = new Date(startDate);
                                var fmt1 = $.datepicker.formatDate("dd/mm/yy", d);
                                $("#input1").val(fmt1);
                                $("#ContentPlaceHolder1_txtDate").val(fmt1);

                            }, 100);


                            if (start > 0) {

                                inst.dpDiv.find('a.ui-state-active:not(.ui-priority-secondary)').closest("td").removeClass('in-range');
                                inst.dpDiv.find('a.ui-state-active:not(.ui-priority-secondary)').closest("td").addClass('end-range');
                                setTimeout(function () {
                                    if (inst.dpDiv.find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:not(.ui-priority-secondary):containsExact(' + date.getDate() + ')').closest("td").hasClass("ui-datepicker-week-end")) {
                                        calcPrice -= parseFloat(_weekPrice);

                                    }
                                    else {
                                        calcPrice -= parseFloat(_price);
                                     

                                    }

                                    var d2 = new Date(endDate);
                                    var fmt2 = $.datepicker.formatDate("dd/mm/yy", d2);
                                    $("#input2").val(fmt2);
                                    $("#ContentPlaceHolder1_txtDate2").val(fmt2);
                                    $("#totalPrice").html(calcPrice + " TL");

                                    var _dates = parseFloat(i - 1);
                                    $("#selectDate").html("Toplam " + _dates + " Gece");

                                }, 100);


                          

                                setTimeout(function () {
                                    if ($("#input1").val() != '' && $("#input2").val() != '') {
                                        calcPrice = 0;
                                        $(".randevuButton").click();
                                 
                                    }
                                }, 150);


                            }
                        }
                    }
                    start++;
                }



            }, 80);
            //disableSelect();

 
    
        },

    });


    $(document).on("mouseenter", ".ui-datepicker-calendar td", function () {
        if ($(".ui-datepicker-calendar td").hasClass("in-range")) {

            return false;
        }
        if ($(this).hasClass("booked")) {
            return false;
        }

        var day_h = $(this).text(),
            month_h = parseInt($(this).closest("td").data("month")) + 1;
        year_h = $(this).closest("td").data("year");
        endDate_h = month_h + "/" + day_h + "/" + year_h;
        if (typeof (startDate) != 'undefined') {
            var k = daydiff(parseDate(startDate), parseDate(endDate_h));
            var StopDate_h = new Date(endDate_h);

            for (var d = new Date(startDate); d <= StopDate_h; d.setDate(d.getDate() + 1)) {
                $('.end-range').removeClass("end-range");
                var date = new Date(d);
                if (i_h > 0) {
                    if (i_h === k) {

                        $(".ui-datepicker-calendar").find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:not(.ui-priority-secondary):containsExact(' + date.getDate() + ')').closest("td").addClass("end-range");

                    }
                    else {
                        $(".ui-datepicker-calendar").find('[data-year="' + date.getFullYear() + '"][data-month="' + (date.getMonth()) + '"] a:not(.ui-priority-secondary):containsExact(' + date.getDate() + ')').closest("td").addClass("hover-range");
                    }
                }

                if (calc_range) {
                    i_h++;
                }
            }
        }
        // disableSelect();
    });

    $(document).on("mouseleave", ".ui-datepicker-calendar td", function () {
        $('.hover-range').removeClass("hover-range");


    });

    var debounce;
    $(window).resize(function () {
        clearTimeout(debounce);
        if ($(window).width() < 768) {
            debounce = setTimeout(function () {
                $('.year-calendar').datepicker('option', 'dayNamesMin', ['Pzr', 'Pzt', "Sl", "Çrş", "Prş", "Cm", "Cmt"]);
                $('#input1').datepicker('option', 'dayNamesMin', ['Pzr', 'Pzt', "Sl", "Çrş", "Prş", "Cm", "Cmt"]);
                $('#input2').datepicker('option', 'dayNamesMin', ['Pzr', 'Pzt', "Sl", "Çrş", "Prş", "Cm", "Cmt"]);
                debounceDatepicker(1);
            }, 250);
        } else {
            debounce = setTimeout(function () {
                debounceDatepicker(2)
            }, 250);
        }
    }).trigger('resize');

    function debounceDatepicker(no) {
        $(".year-calendar").datepicker("option", "numberOfMonths", no);

        //$('.year-calendar').datepicker('option', 'columnFormat', 'dd');


    }


    var _id = $("#ContentPlaceHolder1_hdnID").val();

    $.ajax({
        type: "POST",
        url: "/advert_detail.aspx/getReservastion",
        data: "{'ads_id': '" + _id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

            var _d = msg.d;

            var data = $.parseJSON(_d);


            for (var i = 0; i < data.length; i++) {

                rezervDates.push(data[i].date);
            }




        },
        error: function (error) {
            toastr.options = {
                "closeButton": true,
                "debug": true,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.error('Bir hata oluştu. Geocoding kaydedilemedi ' + error.msg, 'Dikkat !')
        },
        fail: function (xhr, textStatus, errorThrown) {
            toastr.options = {
                "closeButton": true,
                "debug": true,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.error('Bir hata oluştu. Geocoding kaydedilemedi 2', 'Dikkat !')
        }
    });



});

/*-----------------------------------------
-------------------------------------------
-------- Function Date Difference ------------
-------------------------------------------*/
function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[0] - 1, mdy[1]);
}

function daydiff(first, second) {
    return Math.round((second - first) / (1000 * 60 * 60 * 24));
}


function disableSelect() {


    const _date1 = new Date($("#input1").datepicker("getDate"));
    const _date2 = new Date($("#input2").datepicker("getDate"));


    var _r;

    for (var i = _date1.getDate(); i <= _date2.getDate(); i++) {
        var _yakala = false

        _r = $(".dddd-" + i + "_" + ("0" + (_date1.getMonth() + 1)).slice(-2) + "_" + _date1.getFullYear());

        if (_r.hasClass("booked")) {

            _yakala = true;
        }

        if (_yakala === true) {

            setTimeout(function () {
                daysOfYear = [];
                $(".selected").removeClass("selected");
                $(".in-range").removeClass("in-range");
                $(".end-range").removeClass("end-range");
                $(".ui-state-active").removeClass("ui-state-active");
                $(".ui-datepicker-current-day").removeClass("ui-datepicker-current-day");

                cal_click = 0;
                $("#input1").val("");
                $("#input2").val("");


            }, 100);



        }

    }

}

var specificPrices = { "31/01/2022": "$300", "26/01/2022": "$63", "02/02/2022": "$169.99", "15/025/2022": "$63", "22/02/2022": "$63" };




function updateDatePickerCells(dp, date) {
    setTimeout(function () {
        $('.ui-datepicker td > *').each(function (index, elem) {
            // addCSSRule('.ui-datepicker td a:after {content: "' + $("#hiddenPrice").text() + '"; display:flex}');
            var checkDate = elem.parentElement.title;
            var specificPrice;
            if (specificPrices[checkDate]) {
                specificPrice = specificPrices[checkDate];
            } else {
                specificPrice = '';
            }
            var className = 'datepicker-content-' + CryptoJS.MD5(specificPrice).toString();
            //addCSSRule('.ui-datepicker td a.' + className + ':after {content: "' + specificPrice + '"; display:flex}');
            $(this).addClass(className);
        });
    }, 0);
}

var dynamicCSSRules = [];
function addCSSRule(rule) {
    if ($.inArray(rule, dynamicCSSRules) == -1) {
        $('head').append('<style>' + rule + '</style>');
        dynamicCSSRules.push(rule);
    }
}

