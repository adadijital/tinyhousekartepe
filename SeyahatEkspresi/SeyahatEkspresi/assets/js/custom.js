﻿$(document).ready(function () {


    var _width = $(window).width();
    if (_width < 1024) {
        $('.accordion-section-title').click(function (e) {
            var currentAttrvalue = $(this).attr('tabHref');
            if ($(e.target).is('.active')) {
                $(this).removeClass('active');
                $(this).parent().parent().find(".fa-chevron-down").css("transform", "rotateX(360deg)");
                $('.accordion-section-content:visible').slideUp(300);

            } else {
                $('.accordion-section-title').removeClass('active').filter(this).addClass('active');

                $(this).parent().parent().find(".fa-chevron-down").css("transform", "rotateX(180deg)");
                $('.accordion-section-content').slideUp(300).filter(currentAttrvalue).slideDown(300);
            }
        });
    };



    $(".randevuButton").click(function () {
        var _date1 = $("#input1").val();
        var _date2 = $("#input2").val();

        if (_date1 != null && _date1 != "" && _date2 != null) {

            $(".randevuModal").addClass("show");
            $("body").addClass("overflow-hidden");
        }
        else {
            toastr.options = {
                "closeButton": true,
                "debug": true,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.warning("Lütfen müsaitlik durumunu kontrol ederek giriş ve çıkış tarihi seçiniz. ", 'Dikkat !')
        }



    });



    $(".randevuButton2").click(function () {
        var _date1 = $(".datepicker").val();
        var _date2 = $(".datepicker1").val();

        if (_date1 != null && _date1 != "" && _date2 != null) {

            $(".randevuModal").addClass("show");
            $("body").addClass("overflow-hidden");
        }
        else {
            toastr.options = {
                "closeButton": true,
                "debug": true,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.warning("Giriş ve çıkış tarihi seçiniz. ", 'Dikkat !')
        }



    });

    $(".randevuModalClose").click(function () {
        $(".randevuModal").removeClass("show"), $("body").removeClass("overflow-hidden")
    });

    setTimeout(function () {
        $('.shareButon').click(function () {
            $(this).find("img").hide();
            $(this).find(".card-share-content").addClass("show");
        });
    }, 150);

    //$('.shareButon').hover(function () {
    //    $(".card-share-content").show();

    //}).mouseleave(function () {

    //});

    $(".share-content").hide();


    $(".shortSearch").hide();
 

    setTimeout(function () {
        var e;
        $('.swiper-slide-center').each(function (i) {
            if (i % 2 == 0) {

                e = $(this).html();
                $(this).hide();
            }

            if (i % 2 == 1) {
                $(this).append(e);
            }
        });
    }, 100);





});

var slider = new Swiper(".myFirstSwiper", {
    slidesPerView: 1,
    spaceBetween: 65,
    clickable: true,
    slideToClickedSlide: true,
    pagination: {
        el: ".swiper-first-peg",
    },
    breakpoints: {
        640: {
            slidesPerView: 1,

        },
        768: {
            slidesPerView: 1,

        },
        1024: {
            slidesPerView: 1,

        },
    }
});

setTimeout(function () {

    var swiper = new Swiper(".mykonakSwiper", {
        slidesPerView: 1.3,

        clickable: false,
        slideToClickedSlide: false,
        //slidesOffsetAfter: 250,
        navigation: {
            nextEl: '.swiper-second-next1',
            prevEl: '.swiper-second-prev1',
        },
        breakpoints: {
            480: {
                slidesPerView: 1.2,
                spaceBetween: 5,
                loop: !0,
            },
            640: {
                slidesPerView: 1.4,
                spaceBetween: 10,
                loop: !0

            },
            768: {
                slidesPerView: 1.4,

                spaceBetween: 10,
                loop: !0

                // slidesOffsetBefore: 350,
            },
            1024: {
                slidesPerView: 2,

                spaceBetween: 10,
                loop: !0
                //   slidesOffsetBefore: 350,
            },
            1200: {
                slidesPerView: 3,
                spaceBetween: 30,
                //slidesOffsetAfter: 250,
                //   slidesOffsetBefore: 350,
            },
        }
    });


    var swiper = new Swiper(".CategorySwiper", {
        slidesPerView: 1.5,
        spaceBetween: 20,
        clickable: true,
        slideToClickedSlide: true,
        loop: false,
        //slidesOffsetAfter: 160,
        navigation: {
            nextEl: '.swiper-second-next',
            prevEl: '.swiper-second-prev',
        },

        breakpoints: {
            480: {
                slidesPerView: 1.5,
                spaceBetween: 5,
                slidesOffsetBefore: 0,
            },
            640: {
                slidesPerView: 1.4,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 2.4,
                spaceBetween: 20,
                slidesOffsetBefore: 80,
            },
            1024: {
                slidesPerView: 2.6,
                spaceBetween: 20,
                slidesOffsetBefore: 90,
            },
            1366: {
                slidesPerView: 4.6,
                spaceBetween: 20,
                slidesOffsetBefore: 120,
            },
            1440: {
                slidesPerView: 4.6,
                spaceBetween: 20,
                slidesOffsetBefore: 150,
            },
            1500: {
                slidesPerView: 4.6,
                spaceBetween: 20,
                slidesOffsetBefore: 360,
            },


        }
    });

    var swiper = new Swiper(".mySecondSwiper", {
        slidesPerView: 1.4,
        spaceBetween: 20,
        clickable: true,
        slideToClickedSlide: true,
        breakpoints: {
            640: {
                slidesPerView: "auto",
                spaceBetween: 40,
                slidesOffsetBefore: 0,
            },
            768: {
                slidesPerView: "auto",
                spaceBetween: 20,
                slidesOffsetBefore: 350,
            },
            1024: {
                slidesPerView: "auto",
                spaceBetween: 20,
                slidesOffsetBefore: 350,
            },
        }
    });
    var swiper = new Swiper(".myAdvertMenuSlider", {
        slidesPerView: 3.2,
        spaceBetween: 20,
        clickable: true,
        slideToClickedSlide: true,
        breakpoints: {
            640: {
                slidesPerView: 3.2,
                spaceBetween: 40,
                slidesOffsetBefore: 50,
            },
            768: {
                slidesPerView: 4.4,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 5.4,
                spaceBetween: 20,
            },
        }
    });
    var swiper = new Swiper(".myTourMenuSlider", {
        slidesPerView: 3.2,
        spaceBetween: 20,
        clickable: true,
        slideToClickedSlide: true,
        breakpoints: {
            640: {
                slidesPerView: 3.2,
                spaceBetween: 40,
                slidesOffsetBefore: 0,
            },
            768: {
                slidesPerView: 4.4,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 5.4,
                spaceBetween: 20,
            },
        }
    });
    var swiper = new Swiper(".myAdvertDetail", {
        slidesPerView: 1,
        spaceBetween: 20,
        clickable: true,
        slideToClickedSlide: true,
        pagination: {
            el: ".swiper-images-peg",
        },

    });

    /*lokasyonlar*/
    var swiper = new Swiper(".myThirdSwiper", {
        slidesPerView: 1,
        spaceBetween: 20,
        clickable: true,
        navigation: {
            nextEl: '.swiper-second-next',
            prevEl: '.swiper-second-prev',
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            1200: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
        }

    });


    var swiper = new Swiper(".DistrictSwiper", {
        slidesPerView: 1.2,
        spaceBetween: 5,
        clickable: true,
        slidesOffsetAfter: 100,
        navigation: {
            nextEl: '.swiper-second-next',
            prevEl: '.swiper-second-prev',
        },
        breakpoints: {
            640: {
                slidesPerView: 1.2,
                spaceBetween: 20,
                loop: !0
            },
            768: {
                slidesPerView: 1.2,
                spaceBetween: 5,
                loop: !0
            },
            1024: {
                slidesPerView: 2,
                spaceBetween: 5,
                loop: !0
            },
            1200: {
                slidesPerView: 3,
                spaceBetween: 5,
                loop: !0
            },
        }

    });

    var swiper = new Swiper(".myBlogSwiper", {
        slidesPerView: 1.4,
        spaceBetween: 20,
        clickable: true,
        slideToClickedSlide: true,
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 40,
                slidesOffsetBefore: 0,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20,
                slidesOffsetBefore: 350,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 20,
                slidesOffsetBefore: 350,
            },
        }
    });



}, 100);

setTimeout(function () {
    var _width = $(window).width();
    if (_width > 1024) {
        $('.konak-content-cover').hover(function () {

            var _att = $(this).find(".konak-content-top-img").attr("second-image");
            $(this).find(".konak-content-top-img").css("background-image", "url(" + _att + ")").fadeIn(1500);



        }).mouseleave(function () {
            var _att = $(this).find(".konak-content-top-img").attr("first-image");

            $(this).find(".konak-content-top-img").css("background-image", "url(" + _att + ")").fadeIn(1500);

        });
    }

}, 200);

var swiper = new Swiper(".myTourSwiper", {
    slidesPerView: 1.4,
    spaceBetween: 10,
    clickable: true,
    slideToClickedSlide: true,
    navigation: {
        nextEl: '.swiper-second-next',
        prevEl: '.swiper-second-prev',
    },
    breakpoints: {
        480: {
            slidesPerView: 1.2,
            spaceBetween: 5,
            loop: !0
        },
        640: {
            slidesPerView: 1.4,
            spaceBetween: 10,
            loop: !0

        },
        768: {
            slidesPerView: 1.4,

            spaceBetween: 10,
            loop: !0

        },
        1024: {
            slidesPerView: 2,

            spaceBetween: 10,
            loop: !0
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 30,
            //slidesOffsetAfter: 250,
        },
    }
});

var swiper = new Swiper(".CategorySwiper2", {
    slidesPerView: 1.3,
    spaceBetween: 10,
    clickable: false,
    slideToClickedSlide: false,
    //slidesOffsetAfter: 250,
    navigation: {
        nextEl: '.swiper-second-next1',
        prevEl: '.swiper-second-prev1',
    },
    breakpoints: {
        480: {
            slidesPerView: 1.2,
            spaceBetween: 5,
            loop: !0
        },
        640: {
            slidesPerView: 1.4,
            spaceBetween: 10,
            loop: !0

        },
        768: {
            slidesPerView: 1.4,

            spaceBetween: 10,
            loop: !0

            // slidesOffsetBefore: 350,
        },
        1024: {
            slidesPerView: 2,

            spaceBetween: 10,
            loop: !0
            //   slidesOffsetBefore: 350,
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 30,
            slidesOffsetAfter: 250,
            //   slidesOffsetBefore: 350,
        },
    }
});

var swiper = new Swiper(".mykonak2Swiper", {
    slidesPerView: 1.4,
    spaceBetween: 20,
    clickable: true,
    slideToClickedSlide: true,

    breakpoints: {
        640: {
            slidesPerView: "auto",
            spaceBetween: 40,
            slidesOffsetBefore: 0,
        },
        768: {
            slidesPerView: "auto",
            spaceBetween: 20,
            slidesOffsetBefore: 350,
        },
        1024: {
            slidesPerView: "auto",
            spaceBetween: 20,
            slidesOffsetBefore: 350,
        },
    }
});


$(".share-inline").click(
    function () {
        $(".share-content").fadeIn(500);
        $(".share-content").show();

    });

//$(".konak-detay-content-text-cover").mouseout(function () {
//    $(".share-content").fadeOut(500);
//    $(".share-content").hide();
//});

//$(document).ready(function () {
//    $('.accordion-section-title').click(function (e) {
//        var currentAttrvalue = $(this).attr('tabHref');




$('.counter').counterUp({
    delay: 10,
    time: 2000
});
$('.counter').addClass('animated fadeInDownBig');
$('h3').addClass('animated fadeIn');


var swiper2 = new Swiper(".mySwiper2", {
    direction: "vertical",
    spaceBetween: 50,
    pagination: {
        el: ".swiper-pagination",
        clickable: true
    }
});

$(".swiper-slide-center")

var modal = document.querySelectorAll(".modal-siralama-yontem");
var modalbtn = document.getElementsByClassName(".siralama-btn");
//const modalkapat = document.getElementsByClassName(".modal-kapat")

//modalbtn.addEventListener('click', function () {
//    modal.style.display = "flex";
//});
$('.modal-siralama-yontem').removeClass('show');
$('.siralama-btn').click(function () {

    if (!$('.modal-siralama-yontem').hasClass('show'))
        $('.modal-siralama-yontem').addClass('show');
    else {
        $('.modal-siralama-yontem').removeClass('show');
    }

});

$('.modal-siralama-yontem').addClass('hide');



$('.searchOpenButton').click(function () {

    $('.searchSection').addClass('show');
    $(".searchForm .txtSearch").focus();
});

$('.searchCloseButton').click(function () {
    $('.searchSection').removeClass('show')
})


$('body').on('click', '.modal-header .close', function () {
    $("#myModal").modal('hide');
});

$(".hamburger-menu-mob").click(function () {
    $(".modal-menu-mob").addClass('show');
});

$(".modal-cls-btn").click(function () {
    $(".modal-menu-mob").removeClass('show');
});




$.datepicker._defaults.onAfterUpdate = null;

var datepicker__updateDatepicker = $.datepicker._updateDatepicker;
$.datepicker._updateDatepicker = function (inst) {
    datepicker__updateDatepicker.call(this, inst);

    var onAfterUpdate = this._get(inst, 'onAfterUpdate');
    if (onAfterUpdate) onAfterUpdate.apply((inst.input ? inst.input[0] : null), [(inst.input ? inst.input.val() : ''), inst]);
}


$(function () {

    $(".txtMainSearch").focus(function () {
        $(".shortSearch").toggle();
    });

    $(".txtMainSearch").focusout(function () {
        setTimeout(function () {
            $(".shortSearch").toggle();
        },100);
    });



    $(".keyValue").click(function () {
        console.log(this);
        $(".txtMainSearch").val("");
        $(".txtMainSearch").val(this.innerText);
    });


    $(".startDate input").datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy',
        changeMonth: false,
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pz", "Pt", "Sl", "Çr", "Pr", "Cm", "Ct"],
        monthNamesShort: ["Ock", "Şbt", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
        beforeShowDay: function (date) {
            var date1 = $.datepicker.parseDate('dd/mm/yy', $("#hdnStartDate").val());
            var date2 = $.datepicker.parseDate('dd/mm/yy', $("#hdnFinishDate").val());

            return [true, ""]

            //return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
        },
        onSelect: function (selectedDate, inst) {
            $("#hdnStartDate").val(selectedDate);

        },
        onClose: function () {
            delete $(this).data().datepicker.first;
            $(this).data().datepicker.inline = false;
        }
    }).position({
        my: 'bottom',
        at: 'left bottom',

    });

    $(".finishDate input").datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy',
        changeMonth: false,
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pz", "Pt", "Sl", "Çr", "Pr", "Cm", "Ct"],
        monthNamesShort: ["Ock", "Şbt", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
        beforeShowDay: function (date) {
            var date1 = $.datepicker.parseDate('dd/mm/yy', $("#hdnStartDate").val());
            var date2 = $.datepicker.parseDate('dd/mm/yy', $("#hdnFinishDate").val());

            return [true, ""]

            //return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
        },
        onSelect: function (selectedDate, inst) {
            $("#hdnFinishDate").val(selectedDate);

        },
        onClose: function () {
            delete $(this).data().datepicker.first;
            $(this).data().datepicker.inline = false;
        }
    }).position({
        my: 'bottom',
        at: 'left bottom',

    });


    $("#jrange2 input").datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy',
        changeMonth: false,
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pz", "Pt", "Sl", "Çr", "Pr", "Cm", "Ct"],
        monthNamesShort: ["Ock", "Şbt", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
        onSelect: function (selectedDate) {
            if (!$(this).data().datepicker.first) {
                $(this).data().datepicker.inline = true
                $(this).data().datepicker.first = selectedDate;
            } else {
                if (selectedDate > $(this).data().datepicker.first) {
                    $(this).val($(this).data().datepicker.first + " - " + selectedDate);
                    $("#hdnStartDate").val($(this).data().datepicker.first);
                    $("#hdnFinishDate").val(selectedDate);

                } else {
                    $("#hdnStartDate").val(selectedDate);
                    $("#hdnFinishDate").val($(this).data().datepicker.first);

                    $(this).val(selectedDate + " - " + $(this).data().datepicker.first);
                }
                $(this).data().datepicker.inline = false;
            }
        },
        onClose: function () {
            delete $(this).data().datepicker.first;
            $(this).data().datepicker.inline = false;
        }
    }).position({
        my: 'bottom',
        at: 'left bottom',

    });



    $(".datepicker").datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy',
        changeMonth: false,
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pz", "Pt", "Sl", "Çr", "Pr", "Cm", "Ct"],
        monthNamesShort: ["Ock", "Şbt", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
        beforeShowDay: function (date) {
            var date1 = $.datepicker.parseDate('dd/mm/yy', $("#hdnStartDate").val());
            var date2 = $.datepicker.parseDate('dd/mm/yy', $("#hdnFinishDate").val());

            return [true, ""]

            //return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
        },
        beforeShow: function (input, inst) {
            var $this = $(this);
            var cal = inst.dpDiv;
            var top = $this.offset().top + $this.outerHeight();
            var left = $this.offset().left;

            setTimeout(function () {
                cal.css({
                    'width': 350,
                    //'left': left-80
                });
            }, 10);
        },
        onSelect: function (selectedDate, inst) {
            if (!$(this).data().datepicker.first) {
                $(this).data().datepicker.inline = true
                $(this).data().datepicker.first = selectedDate;

            } else {
                if (selectedDate > $(this).data().datepicker.first) {
                    $(this).val($(this).data().datepicker.first);
                    $(".datepicker1").val(selectedDate);
                    $("#hdnStartDate").val($(this).data().datepicker.first);
                    $("#hdnFinishDate").val(selectedDate);

                    $("#ContentPlaceHolder1_txtDate").val($(this).data().datepicker.first);
                    $("#ContentPlaceHolder1_txtDate2").val(selectedDate);
                } else {
                    $("#hdnStartDate").val(selectedDate);
                    $("#hdnFinishDate").val($(this).data().datepicker.first);

                    $("#ContentPlaceHolder1_txtDate").val(selectedDate);
                    $("#ContentPlaceHolder1_txtDate2").val($(this).data().datepicker.first);


                    $(this).val(selectedDate);
                    $(".datepicker1").val($(this).data().datepicker.first);

                }
                $(this).data().datepicker.inline = false;


            }
        },
        onClose: function () {
            delete $(this).data().datepicker.first;
            $(this).data().datepicker.inline = false;
        }
    }).position({
        my: 'left center',
        at: 'left center',
        of: $('.datepicker')
    });



});



