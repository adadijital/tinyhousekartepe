﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class kullanimKosullari : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _Sections = db.page_content.Where(q => q.page_id == 2 && q.lang_id == Localization.Language).ToList();

                if (_Sections.Count() > 0)
                {
                    ltrHakkimizdaTitle.Text = _Sections.FirstOrDefault(f => f.section_group == 2).section_name;
                    ltrHakkimizdaDesc.Text = _Sections.FirstOrDefault(f => f.section_group == 2).page_content_text;
                }

            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }
    }
}