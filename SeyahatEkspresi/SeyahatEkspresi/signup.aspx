﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="signup.aspx.cs" Inherits="SeyahatEkspresi.signup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section>
        <div class="bannerimg cover-image bg-background3" data-image-src="/assets/img/banner2.jpg">
            <div class="header-text mb-0">
                <div class="container">
                    <div class="text-center text-white ">
                        <h1 class="">Yeni Üyelik</h1>
                        <ol class="breadcrumb text-center">
                            <li class="breadcrumb-item"><a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>">Anasayfa</a></li>
                            <li class="breadcrumb-item active text-white" aria-current="page">Yeni Üyelik</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sptb">
        <div class="container customerpage">
            <div class="row ">
                <div class="col-lg-8 col-md-12 d-block mx-auto">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-md-12">
                            <div class="card mb-xl-0">
                                <div class="card-header">
                                    <h3 class="card-title">Hesap Oluştur</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label text-dark">Ad Soyad</label>
                                                <asp:TextBox runat="server" placeholder="Ad Soyad" ID="txtNameSurname" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label text-dark">Eposta Adresiniz</label>
                                                <asp:TextBox runat="server" placeholder="E-Posta Adresiniz" ID="txtRegisterEmail" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label text-dark">Telefon Numaranız</label>
                                                <asp:TextBox runat="server" placeholder="Telefon Numaranız" ID="txtRegisterPhone" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label text-dark">Şifre</label>
                                                <asp:TextBox runat="server" placeholder="Şifre" ID="txtRegisterPassword" TextMode="Password" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label text-dark">Şifre (Tekrar)</label>
                                                <asp:TextBox runat="server" placeholder="Şifre (Tekrar)" ID="txtRegisterPasswordAgain" TextMode="Password" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label text-dark">Hesap Tipi</label>
                                                <div class="custom-controls-stacked d-md-flex">
                                                    <label class="custom-control custom-radio mr-8 ">
                                                        <asp:RadioButton ID="ckPerson" Text="Kişisel Hesap" runat="server" GroupName="company" CssClass="radio kisisel" Checked="true" />
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <asp:RadioButton ID="ckCompany" Text="Kurumsal Hesap" runat="server" GroupName="company" CssClass="radio clckfunc kurumsal" /></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kurumsalHide w-100">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label text-dark">Firma Adı</label>
                                                    <asp:TextBox runat="server" placeholder="Firma Adı" ID="txtCompanyName" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <div class="form-group">
                                                    <label class="form-label text-dark">Vergi Dairesi</label>
                                                    <asp:TextBox runat="server" placeholder="Vergi Dairesi" ID="txtTaxOffice" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <div class="form-group">
                                                    <label class="form-label text-dark">Vergi Numarası</label>
                                                    <asp:TextBox runat="server" placeholder="Vergi Numarası" ID="txtTaxNumber" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="custom-controls-stacked d-md-flex">
                                                    <label class="custom-control custom-checkbox mr-8 ">
                                                        <span class="radio kisisel">
                                                            <asp:CheckBox runat="server" />
                                                            <label>
                                                                <a data-fancybox="sozlesme" data-src="#sozlesme" href="javascript:;">Üyelik Sözleşmesi</a>'ni okudum ve kabul ediyorum.</label>
                                                        </span>
                                                    </label>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-footer mt-2">
                                        <asp:LinkButton ID="lnkRegister" Text="Hesap Oluştur" CssClass="btn btn-primary btn-block" runat="server" OnClick="lnkRegister_Click" />
                                    </div>
                                    <div class="text-center  mt-3 text-dark">veya hesabın varsa  <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/giris-yap">Giriş Yapın</a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <div id="sozlesme"  style="display: none;width:calc(100% - 30px);max-width:1200px;margin:0 15px">
        <h2 class="mb-3">
            Üyelik Sözleşmesi
        </h2>
        <p>

        “Portal”: www.diwada.com isimli alan adından ve bu alan adına bağlı alt alan adlarından oluşan “diwada”nın “Hizmet”lerini sunduğu internet sitesi.
            <br />
“Kullanıcı”: “Portal”a erişen her gerçek veya tüzel kişi.
<br />
“Üye”: “Portal”a üye olan ve "Portal" dahilinde sunulan hizmetlerden işbu sözleşmede belirtilen koşullar dahilinde yararlanan “Kullanıcı”.
<br />
“Üyelik”: “Üye” olmak isteyen “Kullanıcı”nın, “Portal”daki üyelik formunu doğru ve gerçek bilgilerle doldurması, verdiği kimlik bilgilerinin “diwada” tarafından onaylanması ve bildirimi ile kazanılan statüdür. Üyelik işlemleri tamamlanmadan “Üye” olma hak ve yetkisine sahip olunamaz. “Üyelik” hak ve yükümlülükleri, başvuruda bulunana ait olan, kısmen veya tamamen herhangi bir üçüncü şahsa devredilemeyen hak ve yükümlülüklerdir. “Üyelik” başvurusu herhangi bir sebep gösterilmeksizin “DİWADA” tarafından reddedilebilir veya ek şart ve koşullar talep edilebilir. “DİWADA” gerekli görmesi halinde “Üye”nin “Üyelik” statüsünü sona erdirebilir, “Üyelik”i herhangi bir sebeple sona erenin sonradan yapacağı “Üyelik” başvurusunu kabul etmeyebilir.
<br />
"DİWADA Üyelik Hesabı": Üye’nin "Portal" içerisinde sunulan hizmetlerden yararlanmak için gerekli olan iş ve işlemleri gerçekleştirdiği, "Üyelik”le ilgili konularda "DİWADA"ya talepte bulunduğu, "Üyelik bilgilerini güncelleyip, sunulan hizmetlerle ilgili raporlamaları görüntüleyebildiği, kendisinin belirlediği ve münhasıran kendisi tarafından kullanılacağını taahhüt ettiği "kullanıcı adı" ve "şifre" ile "Portal" üzerinden eriştiği "Üye"ye özel internet sayfaları bütünü.
<br />
“DİWADA Hizmetleri” ("Hizmet"): "Portal" içerisinde "Üye"nin işbu sözleşme içerisinde tanımlı olan iş ve işlemlerini gerçekleştirmelerini sağlamak amacıyla “DİWADA” tarafından sunulan uygulamalardır. "DİWADA", "Portal" içerisinde sunulan "Hizmet"lerinde dilediği zaman değişiklikler ve/veya uyarlamalar yapabilir. Yapılan değişiklikler ve/veya uyarlamalarla ilgili "Üye"nin uymakla yükümlü olduğu kural ve koşullar “Portal”dan "Üye"ye duyurulur, açıklanan şartlar ve koşullar “Portal”da yayımlandığı tarihte yürürlüğe girer.
<br />
“İçerik”: “Portal”da yayınlanan ve erişimi mümkün olan her türlü bilgi, yazı, dosya, resim, video, rakam vb. görsel, yazımsal ve işitsel imgeler.
<br />
"DİWADA’DA Arayüzü" : DİWADA ve "Üye"ler tarafından oluşturulan içeriğin "Kullanıcı”lar tarafından görüntülenebilmesi ve "DİWADA Veritabanı"ından sorgulanabilmesi amacıyla "Kullanıcı”lar tarafından kullanılan; 5846 Sayılı Fikir ve Sanat Eserleri Kanunu kapsamında korunan ve tüm fikri hakları “DİWADA ya ait olan tasarımlar içerisinde “Portal” üzerinden yapılabilecek her türlü işlemin gerçekleştirilmesi için bilgisayar programına komut veren internet sayfaları.
<br />
“DİWADA Veritabanı” : “Portal” dahilinde erişilen içeriklerin depolandığı, tasnif edildiği, sorgulanabildiği ve erişilebildiği “DİWADA”ya ait olan 5846 Sayılı Fikir ve Sanat Eserleri Kanunu gereğince korunan veritabanıdır
        </p>
        <p class="mb-0 text-right">
            <input data-fancybox-close type="button" class="btn btn-primary" value="Kapat" />
        </p>
    </div>
</asp:Content>
