﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace SeyahatEkspresi.Core
{
    public class Localization:Base
    {
        #region public static properties
        public static byte Language
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Request.QueryString["lang"] != null)
                    {
                        if (HttpContext.Current.Request.QueryString["lang"].ToString() == "tr")
                            return Localization.Turkce;
                        else if (HttpContext.Current.Request.QueryString["lang"].ToString() == "en")
                            return Localization.English;
                        else
                            return Localization.Turkce;
                    }
                    else
                        return Localization.Turkce;
                }
                catch (Exception ex)
                {
                    //  Log.Add(ex);

                    return Localization.Turkce;
                }
            }
        }

        public static string LanguageStr
        {
            get
            {
                if (Localization.Language == Localization.Turkce)
                    return "tr";
                else if (Localization.Language == Localization.English)
                    return "en";
                else
                    return "tr";
            }
        }

        //Languages
        public static byte Turkce = 1;
        public static byte English = 2;


        #endregion

        public Localization()
        {
            setLanguage(Localization.Language);
        }
        public void setLanguage(byte Language)
        {
            if (Language == Localization.Turkce)
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("tr-TR");
            }
            else if (Language == Localization.English)
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
            }
            else
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("tr-TR");
            }
        }

        public void changeLanguage(byte Language)
        {
            try
            {
                HttpContext current = HttpContext.Current;
                string url = current.Request.RawUrl;

                if (url.Contains("?"))
                    url = url.Split('?')[0];


                if (Language == Localization.English)
                {
                    if (url.Contains("/tr"))
                        url = url.Replace("/tr", "/en");
                }

                else if (Language == Localization.Turkce)
                {
                    if (url.Contains("/en"))
                        url = url.Replace("/en", "/tr");
                }
                if (Language == Localization.English)
                    url = url.Replace("/tr", "/en");
                else
                    url = url.Replace("/en", "/tr");

                setLanguage(Language);



                current.Response.Redirect(url, false);
            }
            catch (Exception ex)
            {
                // Log.Add(ex);

            }
        }


    }
}