﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{
    public class OrderData:Base
    {
        public CustomerData Customer { get; set; }
        public CreditCard Card { get; set; }
        public string Currency { get; set; }
        public string _3DSecureReturnURL_ { get; set; }
        public string PayMethod { get; set; }
        public string OrderCode { get; set; }
        public decimal PaymentAmount { get; set; }
        public string IpAdress { get; set; }
        public string Hatali_URL { get; set; }
        public string Basarili_URL { get; set; }
        public string PaymentOrderID { get; set; }
        public string IslemID { get; set; }
        public string DekontID { get; set; }

        public class CustomerData
        {
            public string CountryCode { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string NameSurname { get; set; }
            public string Gsm { get; set; }

        }

        public class CreditCard
        {
            public string CVV { get; set; }
            public string ExpYear { get; set; }
            public string ExpMonth { get; set; }
            public string CardNumber { get; set; }
            public string CardHolderName { get; set; }
        }
    }
}