﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{

    [Serializable()]
    public class BasketData
    {
        public long basket_id { get; set; }
        public long product_id { get; set; }
        public int quantity { get; set; }
        public decimal unit_sell_price { get; set; }
        public decimal total_sell_price { get; set; }
        public string img_path { get; set; }
        public string product_name { get; set; }
        public string size_name { get; set; }
        public DateTime added_date { get; set; }
        public string Link { get; set; }
        public int stock_amount { get; set; }
        public bool is_active { get; set; }
        public int delivery_amount_type{ get; set; }
        public string delivery_amount_type_text { get; set; }
    }
}