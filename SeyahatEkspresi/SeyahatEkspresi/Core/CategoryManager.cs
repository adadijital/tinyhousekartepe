﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{

    public class CategoryManager : Base
    {
        public List<BreadCrumbData> Links = new List<BreadCrumbData>();
        public List<BreadCrumbData> SetCatBreadCrumb(int? top_cat_id, int langId, string langPrefix)
        {

            var cat = db.categories.Select(s => new
            {
                s.id,
                s.parent_id,
                s.categories_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,

            }).Where(q => q.id == top_cat_id).FirstOrDefault();

            if (cat != null)
            {

                if (cat.parent_id > 0)
                {

                    var parentCat = db.categories.Where(w => w.id == cat.parent_id).ToList();
                    foreach (var item in parentCat)
                    {

                        //Links.Add(new BreadCrumbData { is_active = false, link = "/"+Localization.LanguageStr+"/ilanlar-" + item.category_name.ToURL() + "-c-" + item.id, text = item.category_name });
                        SetCatBreadCrumb(item.id, Localization.Language, Localization.LanguageStr);
                    }

                    Links.Add(new BreadCrumbData { is_active = false, link = "/" + Localization.LanguageStr + "/ilanlar-" + cat.category_name.ToURL() + "-c-" + cat.id, text = cat.category_name, id = cat.id });

                }
                else
                    Links.Add(new BreadCrumbData { is_active = false, link = "/" + Localization.LanguageStr + "/ilanlar-" + cat.category_name.ToURL() + "-c-" + cat.id, text = cat.category_name, id = cat.id });




            }


            return Links;
        }


        public List<CategoryData> getSubCategoryList(int _currentCategory)
        {

            int _currentCategoryParentID = db.categories.Where(q => q.id == _currentCategory).FirstOrDefault().parent_id;


            if (_currentCategoryParentID > 0)
            {
                return db.categories
                   .Where(q =>
                        q.relation_ads_category.Where(cc=> cc.ads.is_active).Count()>0
                        && q.categories_lang.Where(q2 => q2.lang_id == Localization.Language).Count() > 0
                        && ( q.parent_id == _currentCategoryParentID) && q.is_active
                     ).OrderBy(o => o.display_order)
                   .Select(s => new CategoryData
                   {
                       category_id = s.id,
                       display_order = s.display_order,
                       category_name = s.categories_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,
                   })
                     .ToList<CategoryData>();
            }
            else
            {
                return db.categories
                .Where(q =>
                        q.relation_ads_category.Where(cc => cc.ads.is_active).Count() > 0
                     && q.categories_lang.Where(q2 => q2.lang_id == Localization.Language).Count() > 0
                     && q.parent_id == _currentCategoryParentID && q.is_active
                  ).OrderBy(o => o.display_order)
                .Select(s => new CategoryData
                {
                    category_id = s.id,
                    display_order = s.display_order,
                    category_name = s.categories_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,
                })
                .ToList<CategoryData>();

            }

        }
    }


    public class BreadCrumbData
    {
        public string text { get; set; }
        public string link { get; set; }
        public bool is_active { get; set; }
        public int id { get; set; }
    }

    public class CategoryData
    {
        public int category_id { get; set; }
        public string category_name { get; set; }

        public int display_order { get; set; }

    }
}