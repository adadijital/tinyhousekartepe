﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SeyahatEkspresi
{
    public class Base : System.Web.UI.Page
    {

        public static string _physicalPath = System.Configuration.ConfigurationManager.AppSettings["PhysicalPath"];
        public static string _imagePath = _physicalPath + @"uploads\ads\";
        public static string _mapKey = "AIzaSyAopYlZI4rwrfL_lSPR5AavrTAfKmUpZUc"; 

        public tinyhous_dbEntities db = new tinyhous_dbEntities();


        //public cMember cm = new cMember();
        //public Bread Nav = new Bread();
        //public List<breadcrumbs> Links = new List<breadcrumbs>();
        //public Genel genel = new Genel();

        public Base()
        {
           
        }

   
        public static void ReloadPage()
        {

            HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl, false);

        }


        public string GetGeoCode(string Address)
        {
            string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address=" + Address + "&key=" + _mapKey);

            WebRequest request = WebRequest.Create(requestUri);
            WebResponse response = request.GetResponse();
            XDocument xdoc = XDocument.Load(response.GetResponseStream());
            XElement result = xdoc.Element("GeocodeResponse").Element("result");
            XElement locationElement = result.Element("geometry").Element("location");
            XElement lat = locationElement.Element("lat");
            XElement lng = locationElement.Element("lng");
            return locationElement.Element("lat") + "," + locationElement.Element("lng");

        }


     
        public static string VisitorIP
        {
            get
            {
                try
                {
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"]))
                        return HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"];
                    else
                        return HttpContext.Current.Request.UserHostAddress;
                }
                catch { return HttpContext.Current.Request.UserHostAddress; }
            }
        }

        public static void setCulture(string culture)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
        }

        public void showWarningBasic(System.Web.UI.MasterPage master, string message, string title)
        {
            try
            {
                string _popup = "";
                _popup += "<div id='myModal' class='modal fade' role='dialog'>											";
                _popup += "  <div class='modal-dialog'>                                                                ";
                _popup += "                                                                                            ";
                _popup += "    <!-- Modal content-->                                                                   ";
                _popup += "    <div class='modal-content'>                                                             ";
                _popup += "      <div class='modal-header'>                                                            ";
                _popup += "        <h4 class='modal-title'>"+title+"</h4>                                           ";
                _popup += "        <button type='button' class='close' data-dismiss='modal'>&times;</button>           ";
                _popup += "      </div>                                                                                ";
                _popup += "      <div class='modal-body'>                                                              ";
                _popup += "        <p>"+message+"</p>                                                      ";
                _popup += "      </div>                                                                                ";
              
                _popup += "    </div>                                                                                  ";
                _popup += "                                                                                            ";
                _popup += "  </div>                                                                                    ";
                _popup += "</div>                                                                                      ";
                _popup += "<a type='button' class='alert_link' data-toggle='modal' data-target='#myModal'>&nbsp</a>";
                _popup += "<script>$( document ).ready(function() {setTimeout(function(){$('#myModal').modal('show'); }, 100);});</script>";
                System.Web.UI.WebControls.Literal ltrWarning = (System.Web.UI.WebControls.Literal)master.FindControl("ltrWarning");

                ltrWarning.Text = _popup;
            }
            catch (Exception ex)
            {
                //Log.Add(ex);
            }

        }

        public static bool DetectBrowser()
        {
            try
            {
                string u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
                Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);

                if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4))))
                {
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }


        public static string ToPhone(string Text)
        {
            string s = Text;

            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'<>-+,/^&™"".ABCDEFGHJKLMNOPRSTUVYZXWQabcdefghjklmnoprstuvyzxwq".ToCharArray();

            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (s.Contains(strChar))
                    s = s.Replace(strChar, string.Empty);
            }

            while (s.Substring(0, 1) == "0")
                s = s.Substring(1, s.Length - 1);
            if (s.Substring(0, 1) == "9")
                s = s.Substring(1, s.Length - 1);
            while (s.Substring(0, 1) == "0")
                s = s.Substring(1, s.Length - 1);

            if (s.Length != 10)
            {
                if (s.Length > 10)
                    s = s.Substring(0, 10);
                else
                {
                    int fark = 10 - s.Length;

                    for (int i = 0; i < fark; i++)
                    {
                        s += "0";
                    }
                }
            }
            return s;
        }


        public static int RoundDiscountPercent(int DiscountPercent)
        {
            //int mod = DiscountPercent % 10;

            //if (mod != 0)
            //{
            //    DiscountPercent = DiscountPercent + (10 - mod);
            //}

            return DiscountPercent;
        }


        public static void Refresh()
        {
            HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl, false);
        }

        public static void Go(string url)
        {
            HttpContext.Current.Response.Redirect(url);
        }

        public static void Write(string Text)
        {
            HttpContext.Current.Response.Write(Text);
        }

        public static string Query(string Text)
        {
            return HttpContext.Current.Request.QueryString[Text];
        }


        public static string StripHtmlTags(string str)
        {
            if (str == null) return null;

            char[] array = new char[str.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < str.Length; i++)
            {
                char let = str[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        public static bool IsNumeric(string text)
        {
            if (text == null)
                return false;
            else
                return Regex.IsMatch(text, "^\\d+$");
        }

        public static bool IsEmail(string inputEmail)
        {
            if (inputEmail == null)
                return false;
            else
            {
                string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Regex re = new Regex(strRegex);
                if (re.IsMatch(inputEmail))
                    return true;
                else
                    return false;
            }
        }

        public static bool ResizeImage(string imagepath, bool worh, int fixedDimension)
        {

            try
            {
                System.Drawing.Image resmimiz = byteArrayToImage(File.ReadAllBytes(HttpContext.Current.Server.MapPath(imagepath)));
                int originalWidth = resmimiz.Width; //900
                int originalHeight = resmimiz.Height; //1350
                int newW = originalWidth;
                int newH = originalHeight;

                if (worh)
                {
                    double ratio = (double)originalWidth / (double)originalHeight;
                    newW = fixedDimension;
                    newH = Convert.ToInt32(fixedDimension / ratio);
                }
                else
                {
                    double ratio = (double)originalHeight / (double)originalWidth;
                    newW = Convert.ToInt32(fixedDimension / ratio);
                    newH = fixedDimension;
                }


                Bitmap b = new Bitmap(newW, newH);
                Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(resmimiz, 0, 0, newW, newH);
                g.Dispose();

                b.Save(HttpContext.Current.Server.MapPath(imagepath));
                return true;
            }
            catch(Exception ex)
            {
                Log.Add(ex);
                return false;
            }
        }



        public static void Alert(string Message)
        {
            HttpContext.Current.Response.Write("<script>alert('" + Message + "');</script>");
        }

        public static bool ResizeImage(string imagepath, string savepath, int w, int h)
        {
            try
            {
                System.Drawing.Image resmimiz = byteArrayToImage(File.ReadAllBytes(HttpContext.Current.Server.MapPath(imagepath)));
                Bitmap b = new Bitmap(w, h);
                Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(resmimiz, 0, 0, w, h);
                g.Dispose();

                b.Save(HttpContext.Current.Server.MapPath(savepath));
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string URLStringTemizle(string Text)
        {
            string strTitle = Text.ToString();
            strTitle = strTitle.Trim();
            strTitle = strTitle.Trim('-');
            strTitle = strTitle.ToLower();
            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'<>,/^&"".".ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (strTitle.Contains(strChar))
                    strTitle = strTitle.Replace(strChar, string.Empty);
            }
            strTitle = strTitle.Replace(" ", "-");
            strTitle = strTitle.Replace("--", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("-----", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("--", "-");
            strTitle = strTitle.Trim();
            strTitle = strTitle.Trim('-');
            return strTitle;
        }

        public static string AramaKelime(string Text)
        {
            string strTitle = Text.ToString().Replace("\"", "&quot;");
            strTitle = strTitle.Trim();
            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'’<>,/^&\"".".ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (strTitle.Contains(strChar))
                    strTitle = strTitle.Replace(strChar, string.Empty);
            }
            strTitle = strTitle.Trim();
            return strTitle;
        }


        public static string GenerateURL(object Title)
        {
            string strTitle = Title.ToString();
            strTitle = strTitle.Trim();
            strTitle = strTitle.Trim('-');
            strTitle = strTitle.ToLower();
            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'<>,/^&"".".ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (strTitle.Contains(strChar))
                    strTitle = strTitle.Replace(strChar, string.Empty);
            }
            strTitle = strTitle.Replace(" ", "-");
            strTitle = strTitle.Replace("--", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("-----", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("--", "-");
            strTitle = strTitle.Replace("ü", "u");
            strTitle = strTitle.Replace("ğ", "g");
            strTitle = strTitle.Replace("ş", "s");
            strTitle = strTitle.Replace("ö", "o");
            strTitle = strTitle.Replace("ç", "c");
            strTitle = strTitle.Replace("ı", "i");
            strTitle = strTitle.Trim();
            strTitle = strTitle.Trim('-');
            return strTitle;
        }


        public static int YeniDegerAl(int CurrentDeger)
        {
            if (CurrentDeger == 1)
                return 0;
            else
                return 1;
        }


        public static string GetRandom(int Length, int CharactersB, int CharactersS, int Numbers, int SpecialCharacters)
        {
            string characters_b = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string characters_s = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "0123456789";
            string special_characters = "-_*+/";
            string allowedChars = String.Empty;

            if (CharactersB == 1)
                allowedChars += characters_b;

            if (CharactersS == 1)
                allowedChars += characters_s;

            if (Numbers == 1)
                allowedChars += numbers;

            if (SpecialCharacters == 1)
                allowedChars += special_characters;

            char[] chars = new char[Length];
            Random rd = new Random();

            for (int i = 0; i < Length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public static string GetRandom(int Length)
        {
            string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[Length];
            Random rd = new Random();

            for (int i = 0; i < Length; i++)
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];

            return new string(chars);
        }


        private static System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        public string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);

            return GetHexaDecimal(inputbytes);
        }

        public string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;

            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }

            return s.ToString();
        }

        public static void RemoveDuplicates(DataTable tbl, DataColumn[] keyColumns)
        {
            int rowNdx = 0;
            while (rowNdx < tbl.Rows.Count - 1)
            {
                DataRow[] dups = FindDups(tbl, rowNdx, keyColumns);
                if (dups.Length > 0)
                    foreach (DataRow dup in dups)
                        tbl.Rows.Remove(dup);
                else
                    rowNdx++;
            }
        }

        private static DataRow[] FindDups(DataTable tbl, int sourceNdx, DataColumn[] keyColumns)
        {
            ArrayList retVal = new ArrayList();
            DataRow sourceRow = tbl.Rows[sourceNdx];
            for (int i = sourceNdx + 1; i < tbl.Rows.Count; i++)
            {
                DataRow targetRow = tbl.Rows[i];
                if (IsDup(sourceRow, targetRow, keyColumns))
                    retVal.Add(targetRow);
            }
            return (DataRow[])retVal.ToArray(typeof(DataRow));
        }

        private static bool IsDup(DataRow sourceRow, DataRow targetRow, DataColumn[] keyColumns)
        {
            bool retVal = true;
            foreach (DataColumn column in keyColumns)
            {
                retVal = retVal && sourceRow[column].Equals(targetRow[column]);
                if (!retVal) break;
            }
            return retVal;
        }

        public static bool ResizeImage(string imagepath, int w, int h)
        {
            try
            {
                System.Drawing.Image resmimiz = byteArrayToImage(File.ReadAllBytes(imagepath));
                Bitmap b = new Bitmap(w, h);
                Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(resmimiz, 0, 0, w, h);
                g.Dispose();

                ImageCodecInfo jgpEncoder = GetEncoder(ImageFormat.Jpeg);
                System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                EncoderParameters myEncoderParameters = new EncoderParameters(1);
                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 85L);
                myEncoderParameters.Param[0] = myEncoderParameter;

                b.Save(imagepath, jgpEncoder, myEncoderParameters);
                resmimiz.Dispose();
                b.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return false;
            }
        }

        private static System.Drawing.Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            try
            {
                ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

                foreach (ImageCodecInfo codec in codecs)
                {
                    if (codec.FormatID == format.Guid)
                    {
                        return codec;
                    }
                }
                return null;
            }
            catch { return null; }
        }

        public static string CreateRandomValue(int Length, bool CharactersB, bool CharactersS, bool Numbers, bool SpecialCharacters)
        {
            string characters_b = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string characters_s = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "0123456789";
            string special_characters = "-_*+/";
            string allowedChars = String.Empty;

            if (CharactersB)
                allowedChars += characters_b;

            if (CharactersS)
                allowedChars += characters_s;

            if (Numbers)
                allowedChars += numbers;

            if (SpecialCharacters)
                allowedChars += special_characters;

            char[] chars = new char[Length];
            Random rd = new Random();

            for (int i = 0; i < Length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public static DataTable SrtDataTable(DataTable dt, string sort)
        {
            DataTable newDT = dt.Clone();
            int rowCount = dt.Rows.Count;

            DataRow[] foundRows = dt.Select(null, sort);
            for (int i = 0; i < rowCount; i++)
            {
                object[] arr = new object[dt.Columns.Count];
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    arr[j] = foundRows[i][j];
                }
                DataRow data_row = newDT.NewRow();
                data_row.ItemArray = arr;
                newDT.Rows.Add(data_row);
            }

            dt.Rows.Clear();

            for (int i = 0; i < newDT.Rows.Count; i++)
            {
                object[] arr = new object[dt.Columns.Count];
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    arr[j] = newDT.Rows[i][j];
                }

                DataRow data_row = dt.NewRow();
                data_row.ItemArray = arr;
                dt.Rows.Add(data_row);
            }
            return dt;
        }

        public static void RedirectAndPOST(Page page, string destinationUrl, NameValueCollection data)
        {
            string strForm = PreparePOSTForm(destinationUrl, data);
            page.Controls.Add(new LiteralControl(strForm));
        }

        public static String PreparePOSTForm(string url, NameValueCollection data)
        {
            string formID = "PostForm";
            StringBuilder strForm = new StringBuilder();
            strForm.Append("<form id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");

            foreach (string key in data)
            {
                strForm.Append("<input type=\"hidden\" name=\"" + key + "\" value=\"" + data[key] + "\">");
            }

            strForm.Append("</form>");
            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language=\"javascript\">");
            strScript.Append("var v" + formID + " = document." + formID + ";");
            strScript.Append("v" + formID + ".submit();");
            strScript.Append("</script>");

            return strForm.ToString() + strScript.ToString();
        }



    }
}