﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{
    [Serializable]
    public class FilterModel
    {
        public string name { get; set; }
        public List<string> values { get; set; }
    }
}