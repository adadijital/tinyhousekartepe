﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SeyahatEkspresi.Core
{
    public class MemberManager : Base
    {

        public bool IsLoggedIn
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session["member_login"] != null)
                    {
                        if (HttpContext.Current.Session["member_login"].ToString() == "")
                        {
                            if (HttpContext.Current.Request.Cookies["ogr_cck"] != null)
                            {
                                string cookie_content = HttpContext.Current.Request.Cookies["ogr_cck"].Value;

                                if (cookie_content != "")
                                {
                                    cookie_content = cEncrytion.DecryptString(cookie_content);

                                    if (cookie_content.Contains("-ogrccc-"))
                                        return Login(Regex.Split(cookie_content, "-ogrccc-")[0], Regex.Split(cookie_content, "-ogrccc-")[1], false);
                                    else
                                        return false;
                                }
                                else
                                    return false;
                            }
                            else
                                return false;
                        }
                        else
                            return Convert.ToBoolean(HttpContext.Current.Session["member_login"]);
                    }
                    else
                        return Convert.ToBoolean(HttpContext.Current.Session["member_login"]);
                }
                catch (Exception ex)
                {
                    // log this
                    Log.Add(ex);
                    return false;
                }
            }
        }

        public bool Login(string Email, string Password, bool ClearText = true, bool RememberMe = true)
        {
            if (IsLoggedIn)
                return true;
            else
            {
                if (Email != null && Password != "")
                {
                    int member_id;
                    tinyhous_dbEntities db = new tinyhous_dbEntities();

                    if (ClearText)
                    {
                        Password = Password.ToMD5();
                        
                    }


                    var _loginControl = db.members
                        .Where(q => q.email == Email
                        && q.password == Password
                        && q.is_deleted == false
                        && q.is_active)
                        .FirstOrDefault();

                    if (_loginControl == null)
                        return false;
                    else
                    {
                        HttpContext.Current.Session["member_login"] = true;
                        HttpContext.Current.Session["member_id"] = _loginControl.id;
                        HttpContext.Current.Session["member_email"] = _loginControl.email;
                        member_id = _loginControl.id;
                        if (RememberMe)
                        {
                            HttpContext.Current.Response.Cookies["ogr_ckk"].Value = cEncrytion.EncryptString(Email + "-ogrccc-" + Password);
                            HttpContext.Current.Response.Cookies["ogr_ckk"].Expires = DateTime.Now.AddDays(30);
                        }

                        member_login_logs _loginLog = new member_login_logs();
                        _loginLog.member_id = _loginControl.id;
                        _loginLog.login_time = DateTime.Now;
                        _loginLog.ip_address = Base.VisitorIP;
                        db.member_login_logs.Add(_loginLog);
                        db.SaveChanges();

                        return true;
                    }
                }
                else
                    return false;
            }

        }



        public MemberData getMemberData(int MemberID)
        {

            try
            {
                return db.members.Where(q => q.id == MemberID).Select(s => new MemberData
                {
                    member_id = s.id,
                    member_name = s.name_surname,
                    member_email = s.email,
                    registration_ip = s.created_ip,
                    registration_time = s.created_time,
                    gsm = s.phone,
                    telephone = s.phone,
                    account_type = s.account_type,
                    company_name = s.company_name,
                    tax_office = s.tax_office,
                    tax_number = s.tax_number,
                    tck = s.tck,
                    active_status = (s.is_active) ? "Aktif" : "Pasif",
                    account_type_text = s.account_type == 0 ? "Kişisel Hesap" : "Kurumsal Hesap",
                    login_logs = s.member_login_logs
                      .Select(s6 => new MemberLoginLogData
                      {
                          created_date = s6.login_time,
                          id = s6.id,
                          member_id = s6.member_id,
                          ip_address = s6.ip_address

                      }),
                    member_addresses = s.member_addresses.Select(s7 => new MemberAdresData
                    {
                       member_id = s7.member_id,
                       city_id = s7.geo_city_id,
                       town_id = s7.geo_town_id,
                       city_name = s7.geo_cities.city_name,
                       town_name = s7.geo_town.town_name,
                       address_title  = s7.adres_title,
                       member_name = s7.members.name_surname,
                       address = s7.adress,
                       phone = s7.phone,
                       tax_number = s7.tax_number,
                       tax_office = s7.tax_office,
                       adres_id  = s7.id,
                       

                    }),

                }).FirstOrDefault<MemberData>();
            }
            catch (Exception ex)
            {

                Log.Add(ex);
                return null;
            }
        }


    }
}