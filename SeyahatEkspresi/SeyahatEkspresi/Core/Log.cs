﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace SeyahatEkspresi
{
    public class Log : Base
    {


        public static void Add(Exception ex)
        {
            //MemberManager _memberManager = new MemberManager();
            try
            {
                int MemberID = 0;

                //if (_memberManager.IsLoggedIn)
                //    MemberID = HttpContext.Current.Session["member_id"].toInt();

                tinyhous_dbEntities db = new tinyhous_dbEntities();
                

                system_logs _Log = new system_logs();
                _Log.member_id = MemberID;
                _Log.message = ex.Message;
                _Log.source = ex.Source;
                _Log.stack_trace = ex.StackTrace;
                _Log.created_time = DateTime.Now;
                _Log.ip = Base.VisitorIP;
                _Log.page = HttpContext.Current.Request.RawUrl.Length > 250 ? HttpContext.Current.Request.RawUrl.Substring(0, 250) : HttpContext.Current.Request.RawUrl;
                db.system_logs.Add(_Log);
                db.SaveChanges();
            }
            catch { }
        }


        public static void AddPaymentLog(int PaymentID, string Description, string Pos, decimal Amount, bool Status)
        {
            try
            {
                MemberManager _memberManager = new MemberManager();
                int MemberID = 0;
                if (_memberManager.IsLoggedIn)
                {
                    MemberID = HttpContext.Current.Session["member_id"].toInt();
                }

                tinyhous_dbEntities db = new tinyhous_dbEntities(); 
                

                order_payment_logs _PaymentLog = new order_payment_logs();
                _PaymentLog.payment_id = PaymentID;
                _PaymentLog.member_id = MemberID;
                _PaymentLog.log_desc = Description;
                _PaymentLog.log_time = DateTime.Now;
                _PaymentLog.pos = Pos;
                _PaymentLog.amount = Amount;
                _PaymentLog.status = Status;
                _PaymentLog.ip_address = Base.VisitorIP;
                db.order_payment_logs.Add(_PaymentLog);
                db.SaveChanges();
            }
            catch { }
        }



    }
}