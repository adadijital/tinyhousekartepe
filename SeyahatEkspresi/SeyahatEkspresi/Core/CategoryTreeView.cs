﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{
    public class CategoryTreeView
    {
        private int _categoryID;
        private string _categoryName;
        private int _displayOrder;
        private bool _isActive;

        public int CategoryID
        {
            get { return _categoryID; }
            set { _categoryID = value; }
        }
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }
        public int DisplayOrder
        {
            get { return _displayOrder; }
            set { _displayOrder = value; }
        }
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public string ActiveStatus { get { return (_isActive) ? "Aktif" : "Pasif"; } }


    }
    public class SubCategoryData
    {
        public string name { get; set; }
        public int sub_cat_id { get; set; }
        public int parent_cat_id { get; set; }
        public string parent_cat_name { get; set; }
        public string image_name { get; set; }
        public int display_order { get; set; }
        public bool is_active { get; set; }
    }
}