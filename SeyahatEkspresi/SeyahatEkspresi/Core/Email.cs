﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace SeyahatEkspresi
{
    public class Email
    {
        private delegate void SenderDelegate();
        private SenderDelegate _asyncSender;


        public string To { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }


        public bool SendAdmin = false;
        public bool SendReport = false;


        public static bool NewAds(string member_name, string ads_title , string ads_no)
        {
            Base _b = new Base();
            var _setting = _b.db.mail_setting.FirstOrDefault();

            string sb = "<p>Merhaba sayın yönetici, sistemde yeni bir ilan yayınlandı. İlan yayın sürecinde sizin onayınızı bekliyor.</p>";

            sb+= "<table style='width:100%;'>";
            try
            {
                sb += "<tr>";
                sb += "<td>Üye Adı</td><td>" + member_name + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>İlan Başlığı</td><td>" + ads_title + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>İlan No</td><td>" + ads_no + "</td>";
          
       
                sb += "</table>";


                return SendEmail("Yeni İlan " + ads_no, sb, _setting.receive_mail, false);

            }
            catch
            {
                //Genel.AddErrorLog(1, ex);
                return false;
            }
            //}

        }


        public static bool newRez(string ads_title, string ads_no)
        {
            Base _b = new Base();
            var _setting = _b.db.mail_setting.FirstOrDefault();

            string sb = "<p>Merhaba sayın yönetici, sistemde yeni bir rezervasyon talebi var Aşağıdaki ilan için rezervasyon talebini yanıtlamak için yönetici paneline gidin.</p>";

            sb += "<table style='width:100%;'>";
            try
            {
                sb += "<tr>";
                sb += "<td>İlan Başlığı</td><td>" + ads_title + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>İlan No</td><td>" + ads_no + "</td>";


                sb += "</table>";


                return SendEmail("Yeni Rezervasyon " + ads_no, sb, _setting.receive_mail, false);

            }
            catch
            {
                //Genel.AddErrorLog(1, ex);
                return false;
            }
            //}

        }


        public static bool forgotPassword(string mail, string code)
        {
            try
            {
                tinyhous_dbEntities db = new tinyhous_dbEntities();
                var m = db.members.FirstOrDefault(f => f.email == mail);
                m.reset_code = code;
                if (db.SaveChanges() > 0)
                {
                    string Mail = File.ReadAllText(HttpContext.Current.Server.MapPath("/mailing/sifremiunuttum.html"));
                    //Mail = Mail.Replace("%name%", m.member_name + " " + m.member_surname);
                    Mail = Mail.Replace("%baglanti%", "<a href='" + System.Configuration.ConfigurationManager.AppSettings["site_url"].ToString() + "/" + Localization.LanguageStr + "/sifre-degistir/" + code + "' target='_blank'>" + System.Configuration.ConfigurationManager.AppSettings["site_url"].ToString() + " /sifre-degistir/" + code + "</a>");
                    return SendEmail(System.Configuration.ConfigurationManager.AppSettings["site_name"].ToString() + " Şifrenizi Sıfırlayın!", Mail, m.email, false);
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return false;
            }


        }

  

        public static bool HosGeldin(members uye)
        {
            string Mail = File.ReadAllText(HttpContext.Current.Server.MapPath("/mailing/hosgeldin.html"));
            Mail = Mail.Replace("%baglanti%", "<a href='" + System.Configuration.ConfigurationManager.AppSettings["site_url"].ToString() + "/" + Localization.LanguageStr + "/girisyap/" + uye.email + "' target='_blank'>" + "Giriş Yap" + "</a>");
            Mail = Mail.Replace("%email%", uye.email);
            Mail = Mail.Replace("%namsurname%", uye.name_surname);
            return SendEmail(System.Configuration.ConfigurationManager.AppSettings["site_name"].ToString() + " Hoş Geldiniz.", Mail, uye.email, false);
        }





        public static bool SendEmail(string subject, string Mesaj, string mail, bool isbcc)
        {
            try
            {
                Base _b = new Base();
                var _setting = _b.db.mail_setting.FirstOrDefault();

                MailMessage mailMsg = new MailMessage();
                MailAddress kimden = new MailAddress(_setting.send_mail, System.Configuration.ConfigurationManager.AppSettings["site_name"].ToString());
                MailAddress kime = new MailAddress(mail);
                mailMsg.From = kimden;
                mailMsg.Subject = subject;
                mailMsg.Body = Mesaj;
                mailMsg.IsBodyHtml = true;

                mailMsg.To.Add(kime);
                SmtpClient smtpClient = new SmtpClient(_setting.send_smtp, _setting.send_port.toInt());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_setting.send_mail, _setting.send_password); //ay123456
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = false;
                smtpClient.Send(mailMsg);

                return true;
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return false;
            }
        }


        private void _send()
        {
            try
            {
                Base _b = new Base();
                var _setting = _b.db.mail_setting.FirstOrDefault();
                MailMessage mailMsg = new MailMessage();
                mailMsg.To.Add(To);

                if (SendAdmin)
                {
                    mailMsg.CC.Add("dxlearn@gmail.com");
                }

                if (SendReport)
                {
                }

                MailAddress kimden = new MailAddress(_setting.send_mail, System.Configuration.ConfigurationManager.AppSettings["site_name"].ToString());
                mailMsg.From = kimden;
                mailMsg.Subject = Subject;
                mailMsg.Body = Message;
                mailMsg.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient(_setting.send_smtp, _setting.send_port.toInt());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_setting.send_mail, _setting.send_password); //ay123456

                smtpClient.Credentials = credentials;

                smtpClient.Send(mailMsg);
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        public Email()
        {
            _asyncSender = new SenderDelegate(this._send);
        }
        public void Send()
        {
            _asyncSender.BeginInvoke(null, null);

        }
    }
}