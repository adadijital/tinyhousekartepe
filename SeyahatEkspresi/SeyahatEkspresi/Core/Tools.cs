﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public class WebTools
    {
        #region Public Variables
        //public static string cdn = "//ikisepet.streamprovider.net";
        public static string void0 = "javascript:void(0);";
        public static string SiteURL;
        public static string FullURL;
        protected string RawURL;
        protected PlaceHolder PH = new PlaceHolder();
        #endregion

        #region Public Methods
        public WebTools()
        {
            SiteURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority;
            RawURL = HttpContext.Current.Request.RawUrl;
            FullURL = SiteURL + RawURL;

        }

        public static object[] CKToolBar(CkTool T)
        {
            return new object[] { GetToolObject(T) };
        }

        public static string[] OTHERS = new string[]
        {
             "{ name: 'Kırmızı Başlık', element: 'div', attributes: { class:'pageContentTitle' }}"  //

        };

        public static string[] ckstyles = new string[]
            {
                "{ name: 'Gri Alan', element: 'div', styles: {padding:'10px !important',margin:'0 0 !important','background-color':'#F1F1F1 !important','font-size': '18px','margin-left': '40px' }},{name: 'Mavi Alan',element:'div',styles:{padding: '8px 35px 8px 14px','margin-bottom':' 20px',color: 'rgb(58, 135, 173)','text-shadow': 'rgba(255, 255, 255, 0.498039) 0px 1px 0px',border: '1px solid rgb(188, 232, 241)','border-radius': '4px','background-color': 'rgb(217, 237, 247)'}},{name: 'Başlık 1',element:'h3',styles:{color:'#444444','font-size': '18px','font-weight': 'bold','margin-left': '40px'}}"
            
            };
      
        #endregion


        #region Private Methods

      

        private static object[] GetToolObject(CkTool T)
        {
            object[] TS = null;
            if (T == CkTool.Default)
            {
                TS = new object[]
                {  
                   #region Default
		            "Source", 
                    "Cut", 
                    "Copy","Paste",
                    "PasteText","PasteFromWord", 
                    "Undo","Redo", 
                    "Replace","SelectAll",
                    "RemoveFormat","Bold",
                    "Italic","Underline",
                    "Strike","NumberedList",
                    "BulletedList","Outdent",
                    "Indent","JustifyLeft",
                    "JustifyCenter","JustifyRight",
                    "JustifyBlock","Link",
                    "Unlink","Anchor",
                    "TextColor","BGColor",
                    "Styles","Format",
                    "Font","FontSize"  
	                #endregion
                };
            }
            else if (T == CkTool.Simple)
            {
                TS = new object[]
                {  
                    #region Simple
		            "Source",
                    "RemoveFormat","Bold",
                    "Italic","Underline",
                    "Strike","NumberedList",
                    "BulletedList","Outdent",
                    "Indent","JustifyLeft",
                    "JustifyCenter","JustifyRight",
                    "JustifyBlock","Link",
                    "Unlink","Anchor",
                    "TextColor","BGColor",
                    "Styles","Format",
                    "Font","FontSize",
                    "Image","btbutton",
                    "ImageLeft","ImageRight",
                    "DropTools"
                    //,"Glyphicons"
                    //"Timestamp"
	                #endregion
                };
            }

            return TS;
        } 
        #endregion

    }

    public class SEO : WebTools
    {
        #region Private Variables
        protected List<string> MetaTags = new List<string>();
        #endregion

        #region Public Methods
        public SEO(Page P, string title = "", string description = "")
        {
            P.Title = title;
            P.MetaDescription = description;
            P.Header.Controls.Add(PH);

            SetMetaTags();
        }

        public void AddMeta(META M, string content = "")
        {
            string MC = MetaTags[Convert.ToInt32(M)];

            if (MC == "canonical")
            {
                HtmlLink htmlLink = new HtmlLink();
                htmlLink.Attributes.Add("rel", "canonical");
                htmlLink.Href = FullURL;
                PH.Controls.Add(htmlLink);
            }
            else
            {

                HtmlMeta htmlMeta = new HtmlMeta();
                htmlMeta.Name = MC;

                if (M == META.FBUrl)
                    htmlMeta.Content = FullURL;
                else if (M == META.FBImage)
                    htmlMeta.Content = SiteURL + content;
                else
                    htmlMeta.Content = content;

                PH.Controls.Add(htmlMeta);
            }

        }

        public string AddWidget(WIDGET W)
        {
            string MC = "";
            switch (W)
            {
                case WIDGET.FBLike:
                    MC = "<div class=\"fb-like\" data-href='" + FullURL + "' data-layout='standard' data-action='like' data-show-faces='true' data-share='true'></div>";
                    break;
                case WIDGET.FBComment:
                    MC = "<div class=\"fb-comments\" data-href='" + FullURL + "' data-numposts=\"5\" data-width=\"100%\"></div>";
                    break;
                default: MC = ""; break;
            }

            return MC;

        }
        #endregion

        #region Private Methods
        private void SetMetaTags()
        {
            string[] tags = new string[] { "", "canonical", "og:url", "og:title", "og:description", "og:image" };

            foreach (string item in tags)
                MetaTags.Add(item);
        }

        #endregion

        #region Enums
        public enum WIDGET
        {
            FBLike = 1,
            FBComment = 2
        }

        public enum META
        {
            Canonical = 1,
            FBUrl = 2,
            FBTitle = 3,
            FBDescription = 4,
            FBImage = 5
        }

        #endregion

    }

    public enum CkTool
    {
        Default,
        Simple
    } 

    public enum REGEX
    {
        Fullname = 1,
        Number = 2,
        Email = 3,
        Decimal = 4,
        ANumeric = 5,
        Message = 6,
        URL = 7
    }

    public static class RegexTension
    {
        #region Public Methods
        /// <summary>
        /// Örnek pattern: [a-zA-ZöÖçÇİışĞğŞÜü\\s]
        /// </summary>
        /// <param name="Text">string</param>
        /// <param name="pattern">string</param>
        /// <returns>Booelan</returns>
        public static bool RegeX(this string Text, string pattern)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                return Regex.IsMatch(Text, "^" + pattern + "+$");
            }
            else
                return false;

        }

        /// <summary>
        /// Örnek pattern: [a-zA-ZöÖçÇİışĞğŞÜü\\s]
        /// </summary>
        /// <param name="Text">string</param>
        /// <param name="pattern">string</param>
        /// <returns>Booelan</returns>
        public static bool RegeX(this string Text, string pattern, int max)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                if (Text.Length <= max)
                {
                    return Regex.IsMatch(Text, "^" + pattern + "+$");
                }
                else
                    return false;


            }
            else
                return false;

        }

        /// <summary>
        /// R: Örnek - REGEX.Number
        /// </summary>
        /// <param name="Text">string</param>
        /// <param name="R">int</param>
        /// <returns>Boelan</returns>
        public static bool RegeX(this string Text, REGEX RegexNumber)
        {

            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = false;

                if (RegexNumber == REGEX.Email)
                {
                    #region EMAIL
                    try
                    {
                        MailAddress m = new MailAddress(Text);
                        uyuyomu = true;
                    }
                    catch
                    {
                        uyuyomu = false;
                    }
                    #endregion
                }
                else if (RegexNumber == REGEX.Decimal)
                {
                    #region DECIMAL
                    try
                    {
                        Convert.ToDecimal(Text);
                        uyuyomu = true;
                    }
                    catch
                    {
                        uyuyomu = false;
                    }
                    #endregion
                }
                else
                {
                    #region REGEX CONTROL
                    uyuyomu = Regex.IsMatch(Text, "^" + GetRegexPattern(RegexNumber) + "+$");
                    #endregion

                }

                return uyuyomu;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// R: Örnek - REGEX.Number, max: Maximum Value
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="R"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static bool RegeX(this string Text, REGEX RegexNumber, int max)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = false;

                if (Text.Length <= max)
                {
                    if (RegexNumber == REGEX.Email)
                    {
                        #region EMAIL
                        try
                        {
                            MailAddress m = new MailAddress(Text);
                            uyuyomu = true;

                        }
                        catch
                        {
                            uyuyomu = false;
                        }
                        #endregion
                    }
                    else if (RegexNumber == REGEX.Decimal)
                    {
                        #region DECIMAL
                        try
                        {
                            Convert.ToDecimal(Text);
                            uyuyomu = true;
                        }
                        catch
                        {
                            uyuyomu = false;
                        }
                        #endregion
                    }
                    else
                    {
                        #region REGEX CONTROL
                        uyuyomu = Regex.IsMatch(Text, "^" + GetRegexPattern(RegexNumber) + "+$");

                        #endregion

                    }

                }
                else
                    uyuyomu = false;


                return uyuyomu;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Private Methods
        private static string GetRegexPattern(REGEX R)
        {
            string pattern = "";
            if (R == REGEX.Fullname)
            {
                pattern = "[a-zA-ZöÖçÇİışĞğŞÜü\\s]";
            }
            else if (R == REGEX.ANumeric)
            {
                pattern = "[a-zA-Z0-9öÖçÇİışĞğŞÜü]";
            }
            else if (R == REGEX.Number)
            {
                pattern = "[0-9]";
            }
            else if (R == REGEX.Message)
            {
                pattern = "[a-zA-Z0-9öÖçÇİışĞğŞÜü\\s-.@/]";
            }
            else if (R == REGEX.URL)
            {
                pattern = "[a-z0-9-/]";
            }

            return pattern;

        }
        #endregion

    }


}