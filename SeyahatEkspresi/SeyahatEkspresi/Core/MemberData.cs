﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{
    public class MemberData
    {
        public int member_id { get; set; }
        public int account_type { get; set; }

        public string member_name { get; set; }


        public string who_made_save { get; set; }
        public bool is_sanal_pos_access { get; set; }
        public string member_email { get; set; }
        public string gsm { get; set; }
        public string member_not { get; set; }
        public string tck { get; set; }
        public string profile_photo { get; set; }
        public string registration_ip { get; set; }
        public string username { get; set; }
        public string telephone { get; set; }
        public string company_name { get; set; }
        public string tax_office { get; set; }
        public string tax_number { get; set; }
        public bool is_active { get; set; }
        public string active_status { get; set; }
        public string password { get; set; }
        public string account_type_text { get; set; }
        public Nullable<decimal> sanal_pos_limit { get; set; }
        public Nullable<decimal> balance_amount { get; set; }
        public DateTime registration_time { get; set; }
        public Nullable<DateTime> birth_date { get; set; }
        public IEnumerable<MemberAdresData> member_addresses { get; set; }
        public IEnumerable<MemberLoginLogData> login_logs { get; set; }
        //public IEnumerable<MemberOrderData> order_data { get; set; }
    }

    public class MemberLoginLogData
    {
        public Int64 id { get; set; }
        public int member_id { get; set; }
        public DateTime created_date { get; set; }
        public string ip_address { get; set; }
    }

    public class MemberAdresData
    {
        public int member_id { get; set; }
        public int city_id { get; set; }
        public int town_id { get; set; }
        public string city_name { get; set; }
        public string tc { get; set; }
        public string company_name { get; set; }
        public byte adress_type { get; set; }
        public string address_title { get; set; }
        public string member_name { get; set; }
        public string member_surname { get; set; }
        public string address { get; set; }
        public string town_name { get; set; }
        public string postal_code { get; set; }
        public long adres_id { get; set; }
        public string phone { get; set; }
        public string tax_office { get; set; }
        public string tax_number { get; set; }
        public string house_no { get; set; }


    }
}