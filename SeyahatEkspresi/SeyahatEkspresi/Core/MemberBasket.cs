﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{
    public class MemberBasket:Base
    {
  
        public decimal TotalSellPrice { get { return _MemberBasket.Select(s => s.total_sell_price).Sum(); } }
        public decimal TotalPayment { get { return ((TotalSellPrice + AmountShipping)).Round(); } }
        private List<BasketData> _MemberBasket;

        private int _memberId = 0;

        public string AddToBasket(int MemberID, long ProductID, int Quantity)
        {
            var _existingItem = db.member_basket.Where(q => q.member_id == MemberID && q.product_id == ProductID).FirstOrDefault();

            if (_existingItem != null)
            {
                short _desiredQuantity = (_existingItem.quantity + Quantity).toShort();

                var _product = db.ads.Where(q => q.id == ProductID).Select(s => new { s.sell_price}).FirstOrDefault();

                if (_product != null)
                {
                    _existingItem.quantity = _desiredQuantity;
                    _existingItem.unit_sell_price = _product.sell_price;
                    _existingItem.total_sell_price = _product.sell_price * _desiredQuantity;
                    _existingItem.total_sell_price = _product.sell_price * _desiredQuantity;

                    if (db.SaveChanges() > 0)
                    {
                        return "ok";
                    }
                    else
                        return "Ürün sepetinize eklenemedi. Bir Hata oluştu hata kodu: #0001A";
                }
                else
                    return "Böyle bir ürün bulunmamaktadır. Bir Hata oluştu hata kodu: #0001B";
            }
            else
            {
                var _product = db.ads.Where(q => q.id == ProductID).Select(s => new { s.sell_price}).FirstOrDefault();

                if (_product != null)
                {
                    member_basket _newBasket = new member_basket();
                    _newBasket.added_date = DateTime.Now;
                    _newBasket.member_id = MemberID;
                    _newBasket.product_id = ProductID;
                    _newBasket.unit_sell_price = _product.sell_price;
                    _newBasket.total_sell_price = _product.sell_price * Quantity;
                    _newBasket.quantity = Quantity;
                    db.member_basket.Add(_newBasket);

                    if (db.SaveChanges() > 0)
                        return "ok";
                    else
                        return "Ürün sepetinize eklenemedi. Bir Hata oluştu hata kodu: #0001A2";


                }
                else
                {
                    return "Böyle bir ürün bulunmamaktadır. Bir Hata oluştu hata kodu: #0001B2";
                }
            }



        }

        public Decimal AmountShipping
        {

            get
            {
                DateTime simdi = DateTime.Now;
                DateTime startDate = new DateTime(2017, 03, 26, 02, 00, 00);
                DateTime endDate = new DateTime(2017, 03, 27, 02, 01, 00);

                if (startDate <= simdi && simdi < endDate)
                    return 0M;
                else
                    return 0;
            }
        }


        public MemberBasket(int MemberId)
        {
            this._memberId = MemberId;
        }

        public List<BasketData> GetMemberBasket()
        {
            try
            {
                if (_MemberBasket == null)
                {
                    _MemberBasket = new List<BasketData>();

                    var _basketData = db.member_basket
                        .Where(q => q.member_id == _memberId)
                        .Select(s => new
                        {
                            basket_id = s.id,
                            s.quantity,
                            s.unit_sell_price,
                            s.total_sell_price,
                            s.ads.ads_name,
                            s.added_date,
                            s.product_id,
                            s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                            s.ads.id,
                            s.ads.shipping_amount_type,

                        }).ToList();

                    if (_basketData.Count > 0)
                    {
                        foreach (var item in _basketData)
                        {
                            _MemberBasket.Add(new BasketData
                            {
                                basket_id = item.basket_id,
                                quantity = item.quantity,
                                unit_sell_price = item.unit_sell_price,
                                total_sell_price = item.total_sell_price,
                                product_name = item.ads_name,
                                added_date = DateTime.Now,
                                product_id = item.id,
                                img_path = item.img_name,
                                Link ="/"+Core.Localization.LanguageStr+"/"+item.ads_name.ToURL()+"-i-"+item.product_id,
                                delivery_amount_type_text = item.shipping_amount_type == 1 ? "Alıcı Öder" : "Satıcı Öder"
                               

                            });
                        }
                    }

                }

                return _MemberBasket;
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return _MemberBasket;
            }

        }


        public string RemoveProduct(int MemberID, int ProductID)
        {
            try
            {

                var _existingItem = db.member_basket.Where(q => q.member_id == MemberID && q.product_id == ProductID).FirstOrDefault();

                if (_existingItem != null)
                {
                    db.member_basket.Remove(_existingItem);

                    if (db.SaveChanges() > 0)
                        return "ok";
                    else
                        return "Bir hata oluştu";
                }
                else
                    return "Bir hata oluştu";

            }
            catch (Exception ex)
            {
                Log.Add(ex);

                return "Bir hata oluştu";
            }
        }

        public string UpdateProduct(int MemberID, int ProductID, int Quantity)
        {
            try
            {
                var _existingItem = db.member_basket.Where(q => q.member_id == MemberID && q.product_id == ProductID).FirstOrDefault();

                if (_existingItem != null)
                {
                    #region update existing item in basket
                    var _product = db.ads.Where(q => q.id == ProductID).Select(s => new
                    {
                        s.sell_price,
                    })
                    .FirstOrDefault();

                    if (_product != null)
                    {
                        decimal unit_sell_price = _product.sell_price;
                        decimal total_sell_price = Quantity * unit_sell_price;


                        if (_existingItem.quantity != Quantity)
                        {
                            _existingItem.quantity = Quantity;
                            _existingItem.unit_sell_price = unit_sell_price;
                            _existingItem.total_sell_price = total_sell_price;


                            if (db.SaveChanges() > 0)
                                return "ok";
                            else
                                return "Bir hata oluştu.";
                        }
                        else
                        {
                            return "Sepetinizi güncelleyebilmek için ürünlerin adetini değiştirmelisiniz.";
                        }
                    }
                    else
                        return "Ürün Bulunamadı";
                    #endregion
                }
                else
                    return "Bir hata oluştu.";
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return "Bir hata oluştu.";
            }
        }


    }
}