﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{

    public class OrderManager : Base
    {
        MemberManager _memberManager = new MemberManager();
        MemberBasket _memberBasket;
        int MemberID;

        public string CreateOrder(orders _order, List<order_products> _products, int PaymentID)
        {
            try
            {
                db.orders.Add(_order);
                if (db.SaveChanges() > 0)
                {
                    foreach (var _product in _products)   // set order_id for all products
                    {
                        _product.order_id = _order.id;
                        db.order_products.Add(_product);
                    }

                    var OrderPayment = db.order_payments.Where(q => q.id == PaymentID).FirstOrDefault();
                    OrderPayment.order_id = _order.id;

                    if (db.SaveChanges() > 0)
                    {
                        if (_memberManager.IsLoggedIn)
                        {
                            MemberID = Session["member_id"].toInt();

                            #region member data
                            var _memberData = _memberManager.getMemberData(MemberID);

                            #endregion

                            if (HttpContext.Current.Session["basket"] != null)
                            {
                                Session.Remove("basket");
                            }

                            _memberBasket = new MemberBasket(MemberID);
                            if (_memberBasket.GetMemberBasket().Count > 0)
                            {


                                foreach (var item in _memberBasket.GetMemberBasket())
                                {
                                    //ürün stoğu güncelleniyor
                                    //_Sizes.stock_amount -= item.quantity;
                                    try
                                    {
                                        var prd = db.ads.Where(q => q.id == item.product_id).FirstOrDefault();
                                        if (prd != null)
                                            prd.stock_amount -= item.quantity;
                                    }
                                    catch { continue; }

                                    //sepeti sil 
                                    member_basket Basket = db.member_basket.Where(q => q.id == item.basket_id).FirstOrDefault();
                                    db.member_basket.Remove(Basket);
                                }



                                if (db.SaveChanges() > 0)
                                {

                                    //Müşteriye mail gönderildi
                                    members _members = db.members.Where(q => q.id == MemberID).FirstOrDefault();
                                    //Email.SiparisinizAlindi(_members);
                                    //Email.NewOrderAdmin(_members.name_surname);


                                    Session.Remove("shippingCompany");
                                    Session.Remove("shippingAdres");
                                    Session.Remove("billingAddres");
                                    Session.Remove("member_data");



                                    return "ok";
                                }

                            }
                            else
                                return "Siparişin kaydı esnasında bir hata oluştu, lütfen tekrar deneyin. #EXC01";
                        }
                    }
                    else
                    {

                        return "Siparişin kaydı esnasında bir hata oluştu, lütfen tekrar deneyin. #EXC02";
                    }
                }
                return "Siparişin kaydı esnasında bir hata oluştu, lütfen tekrar deneyin. #EXC03";
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return "Siparişin kaydı esnasında bir hata oluştu, lütfen tekrar deneyin. #EXC04";
            }
        }


        public List<OrdersData> getOrderList(int SituationID, string MemberNameSurname, string MemberEmail, int OrderCode, DateTime StartDate, DateTime FinishDate)
        {
            List<OrdersData> result = new List<OrdersData>();

            var data = db.orders
            .Where(q => q.order_payments.Count() > 0);

            if (SituationID > 0)
            {
                data = data.Where(q => q.situation_id == SituationID);
            }

            if (SituationID == 1 || SituationID == 5)
            {
                data = data.Where(q => q.order_payments.FirstOrDefault().is_paid == false);
            }

            if (MemberNameSurname.Trim() != "")
            {
                data = data.Where(q => (q.members.name_surname).Contains(MemberNameSurname));
            }
            if (MemberEmail.Trim() != "")
            {
                data = data.Where(q => q.members.email.Contains(MemberEmail));
            }

            if (OrderCode != 0)
            {
                data = data.Where(q => q.id == OrderCode);
            }

            DateTime defaultDate = Convert.ToDateTime("1.1.0001 00:00:00");

            if (StartDate != defaultDate && FinishDate != defaultDate)
            {
                FinishDate = FinishDate.AddDays(1);
                data = data.Where(q => q.order_time >= StartDate && q.order_time <= FinishDate);
            }

            try
            {
                result = data.Select(s => new OrdersData
                {
                    order_id = s.id,
                    created_time = s.order_time,
                    product_count = s.order_products.FirstOrDefault().quantity,
                    product_total = (decimal)s.order_total,
                    situation_id = s.situation_id,
                    situation_name = s.order_situations.situation_name,
                    member_name = s.members.name_surname,
                    payment_amount = (decimal)s.order_total,
                    city_name = s.geo_cities.city_name,
                    member_id = s.member_id,
                    payment_type_name = s.order_payments.FirstOrDefault().payment_type.payment_name,
                    supplier_id = s.order_products.FirstOrDefault().ads.member_id,
                    supplier_name = s.order_products.FirstOrDefault().ads.members.name_surname,
                }).ToList<OrdersData>();
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                result = new List<OrdersData>();

            }
            return result;
        }

        public OrdersData getSingle(int OrderID)
        {
            try
            {
                return db.orders.Where(q => q.id == OrderID).Select(s => new OrdersData
                {
                    order_id = s.id,
                    situation_id = s.situation_id,
                    created_time = s.order_time,
                    order_ip = s.order_ip,
                    product_total = (decimal)s.order_products.Sum(q => q.total_sell_price),
                    order_phone = s.members.phone,
                    order_total = (decimal)s.order_total,
                    member_name = s.members.name_surname,
                    member_email = s.members.email,
                    member_id = s.member_id,
                    order_note = s.order_note,
                    order_code = s.order_code,
                    tck = s.members.tck,
                    situation_name = s.order_situations.situation_name,
                    payment_id = s.order_payments.FirstOrDefault().id,
                    shippping_name = s.shipping_name_surname,
                    shipping_address = s.shipping_addres,
                    shipping_city_name = s.geo_cities.city_name,
                    shipping_town_name = s.geo_town.town_name,
                    shipping_phone = s.shipping_phone,
                    billing_name = s.billing_name_surname,
                    billing_address = s.billing_addres,
                    billing_city_name = s.geo_cities.city_name,
                    billing_town_name = s.geo_town.town_name,
                    billing_phone = s.billing_phone,
                    supplier_id = s.order_products.FirstOrDefault().ads.member_id,
                    supplier_name = s.order_products.FirstOrDefault().ads.members.name_surname,
                    shipping_total = s.shipping_amount,
                    order_products = s.order_products
                     .Select(s2 => new OrderProductData
                     {
                         order_product_id = s2.id,
                         order_id = s2.order_id,
                         product_id = s2.ads.id,
                         quantity = s2.quantity,
                         unit_sell_price = (decimal)s2.unit_sell_price,
                         total_sell_price = (decimal)s2.total_sell_price,
                         image_name = s2.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                         product_name = s2.ads.ads_name,
               
                
                     }),

                    order_payments = s.order_payments
                   .Select(s3 => new OrderPaymentData
                   {
                       id = s3.id,
                       payment_id = s3.id,
                       payment_amount = s3.payment_amount,
                       is_paid = s3.is_paid,
                       payment_time = s3.created_time,
                       payment_name = s3.payment_type.payment_name,
                       payment_type_id = s3.payment_type_id,

                       card_name_surname = s3.name_surname,
                       card_no = s3.card_no,
                   }),
                }).FirstOrDefault<OrdersData>();
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return null;
            }

        }

        public List<OrdersData> getOrderListMember(int MemberID)
        {
            List<OrdersData> result = new List<OrdersData>();
            try
            {
                result = db.orders.Where(q => q.member_id == MemberID)
                    .Select(s => new OrdersData
                    {

                        order_id = s.id,
                        order_code = s.order_code,
                        created_time = s.order_time,
                        product_count = s.order_products.Select(s2 => new OrderProductData { order_id = s2.order_id }).Count(),
                        product_total = (decimal)s.order_total,
                        situation_id = s.situation_id,
                        payment_amount = (decimal)s.order_total,
                        situation_name = s.order_situations.situation_name,
                    }).OrderByDescending(o => o.order_id).ToList<OrdersData>();
            }
            catch
            {
                result = new List<OrdersData>();
            }
            return result;
        }

        public class OrdersData
        {
            public int supplier_id { get; set; }
            public string supplier_name { get; set; }
            public string customer_situation_name { get; set; }
            public string voucher_product_name { get; set; }
            public string banka_name { get; set; }
            public string account_name { get; set; }
            public string tck { get; set; }
            public string sender_phone { get; set; }
            public string receive_phone { get; set; }
            public string referenceorpassword { get; set; }
            public long order_id { get; set; }
            public int who_made_id { get; set; }
            public int situation_id { get; set; }
            public string situation_name { get; set; }
            public DateTime created_time { get; set; }
            public string order_folder { get; set; }
            public string order_ip { get; set; }
            public decimal product_total { get; set; }
            public decimal discount_total { get; set; }
            public decimal shipping_total { get; set; }
            public decimal order_total { get; set; }
            public string source { get; set; }
            public string country_geo_code { get; set; }
            public string payment_type_name { get; set; }
            public int product_count { get; set; }
            public string order_note { get; set; }
            public string member_name { get; set; }
            public string member_surname { get; set; }
            public string order_phone { get; set; }
            public string payment_type { get; set; }
            public byte payment_type_id { get; set; }
            public string payment_pos_name { get; set; }
            public decimal payment_amount { get; set; }
            public decimal price { get; set; }
  

            public bool is_send_penta { get; set; }
            public Nullable<DateTime> penta_send_time { get; set; }
            public string shippping_name { get; set; }
            public string shipping_address { get; set; }
            public string shipping_city_name { get; set; }
            public string shipping_town_name { get; set; }
            public string shipping_phone { get; set; }
            public string shipping_company { get; set; }

            public string billing_name { get; set; }
            public string billing_address { get; set; }
            public string billing_city_name { get; set; }
            public string billing_town_name { get; set; }
            public string billing_phone { get; set; }




            public string order_code { get; set; }
            public string city_name { get; set; }
            public string order_country { get; set; }
            public string member_email { get; set; }
            public int member_id { get; set; }
            public string cancellation_reason { get; set; }
            public int instalment_count { get; set; }
            public int payment_id { get; set; }
            public Nullable<int> bank_transfer_id { get; set; }
            public int pos_id { get; set; }
            public string pos_name { get; set; }
            public string installment_text { get; set; }

            public Nullable<bool> is_paid { get; set; }
            public IEnumerable<OrderProductData> order_products { get; set; }
            public IEnumerable<OrderPaymentData> order_payments { get; set; }
            public IEnumerable<OrderShipmentData> order_shipment { get; set; }
        }

        public class OrderProductData
        {
            public long order_product_id { get; set; }
            public int order_id { get; set; }
            public long product_id { get; set; }
            public string product_code { get; set; }
            public decimal quantity { get; set; }
            public decimal unit_sell_price { get; set; }
            public decimal total_sell_price { get; set; }

            public decimal unit_market_price { get; set; }
            public decimal total_market_price { get; set; }

            public DateTime added_date { get; set; }
            public string image_name { get; set; }
            public string product_name { get; set; }
            public string voucher_code { get; set; }
            public string voucher_use_statu_text { get; set; }

            public string voucher_product_name { get; set; }
            public DateTime voucher_created_date { get; set; }
            public string is_active_text { get; set; }
            public string is_send_text { get; set; }
            public string is_use_text { get; set; }
            public bool is_active { get; set; }
            public bool is_send_transfer { get; set; }
            public string is_send_transfer_text { get; set; }
            public string send_transfer_email { get; set; }
            public bool send_statu { get; set; }
            public bool use_statu { get; set; }
            public int order_situation_id { get; set; }
            public string voucher_code_view_text { get; set; }
            public Nullable<int> confirm_dealer_id { get; set; }
            public Nullable<int> confirm_user_id { get; set; }
            public Nullable<DateTime> confirm_time { get; set; }



        }

        public class OrderPaymentData
        {


            public int id { get; set; }
            public int payment_id { get; set; }
            public int payment_type_id { get; set; }
            public string payment_name { get; set; }

            public string card_no { get; set; }
            public string card_name_surname { get; set; }
            public decimal payment_amount { get; set; }
            public Nullable<bool> is_paid { get; set; }
            public string payment_bank_accounts_name { get; set; }
            public string payment_bank_account_number { get; set; }

            public string payment_bank_iban { get; set; }
            public string banka_name { get; set; }
            public int bank_transfer_id { get; set; }
            public Nullable<DateTime> created_date { get; set; }
            public Nullable<DateTime> payment_time { get; set; }
            public string detail { get; set; }
            public int type_id { get; set; }

        }

        public class OrderShipmentData
        {
            public long id { get; set; }
            public int order_id { get; set; }
            public int shipping_company_id { get; set; }
            public string shipping_code { get; set; }
            public decimal price { get; set; }
            public string tracking_url { get; set; }
            public string company { get; set; }
            public bool is_delivered { get; set; }
            public Nullable<DateTime> shipment_time { get; set; }

        }

    }
}