﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SeyahatEkspresi.Core
{
    public class AdvertManager : Base
    {
        public AdvertData getSingleWebSite(long AdvertID, byte LangID)
        {
            try
            {
                return db.ads_lang.Where(q => q.lang_id == LangID && q.ads_id == AdvertID && q.ads.is_cancel == false).Select(s => new AdvertData
                {
                    market_price = s.ads.market_price,
                    kisi_sayisi = s.kisi_sayisi,
                    yatak_sayisi = s.yatak_sayisi,
                    banyo_sayisi = s.banyo_sayisi,
                    isinma_turu = s.isinma_turu,
                    konum_durumu = s.konum_durumu,
                    advert_id = s.ads_id,
                    advert_name = s.ads_name,
                    created_time = s.ads.created_time,
                    is_active = s.ads.is_active,
                    ads_no = s.ads.ads_no,
                    geo_city_id = s.ads.geo_city_id,
                    geo_town_id = s.ads.geo_town_id,
                    geo_city_name = s.ads.geo_cities.city_name,
                    geo_town_name = s.ads.geo_town.town_name,
                    geo_quarter_name = s.ads.geo_quarter.quarter_name,
                    geo_quarter_id = s.ads.geo_quarter.id,
                    address = s.ads.address,
                    sell_price = s.ads.sell_price,
                    ads_view = s.ads.ads_view,
                    parent_category_id = s.ads.relation_ads_category.FirstOrDefault().categories.categories_lang.Where(q => q.categories.parent_id == 0).FirstOrDefault().category_id,
                    description = s.description,
                    geo_lat = s.ads.geo_lat,
                    geo_long = s.ads.geo_long,
                    member_name_surname = s.ads.member_id > 0 ? s.ads.members.name_surname : "Diwada",
                    member_phone = s.ads.members.phone,
                    member_id = s.ads.member_id,
                    type = s.ads.type,
                    stock_amount = s.ads.stock_amount,
                    is_showcase = s.ads.is_showcase,
                    shipping_amount_type = s.ads.shipping_amount_type,
                    shipping_delivery_time = s.ads.shipping_delivery_time,
                    type_status = s.ads.type == 1 ? "İlan" : "Ürün",
                    shipping_note = s.ads.shipping_note,
                    week_price = s.ads.week_price,
                    categories = s.ads.relation_ads_category
                          .Select(s3 => new RelationAdvertCategoryData
                          {
                              category_id = s3.cat_id,
                              category_name = s3.categories.categories_lang.FirstOrDefault(w => w.lang_id == LangID).category_name,
                              id = s.id,
                          }),
                    images = s.ads.ads_images.Select(s4 => new AdvertImagesData
                    {
                        id = s4.id,
                        img_name = s4.img_name,
                        display_order = s4.display_order

                    }),
                    info_definition = s.ads.ad_info_value.Where(w => w.ad_info_value_lang.Where(w2 => w2.lang_id == LangID && w2.ad_info_value == "True").Count() > 0 && w.ad_info_value_lang.Where(q => q.lang_id == LangID).Count() > 0).Select(s5 => new AdvertInfoDefinitionData
                    {

                        definition_name = s5.ad_info.ad_info_lang.FirstOrDefault(f => f.lang_id == LangID && f.ad_info.ad_info_value.Where(r => r.ads_id == AdvertID).Count() > 0 && f.ad_info.ad_info_lang.Where(qs => qs.lang_id == LangID).Count() > 0).info_name,
                        definition_value = s5.ad_info_value_lang.FirstOrDefault(w2 => w2.lang_id == LangID && w2.ad_info_value != null && w2.ad_info_value != "" && w2.ad_info_value1.ads_id == AdvertID && w2.ad_info_value1.ad_info.ad_info_lang.Where(qp => qp.lang_id == LangID).Count() > 0).ad_info_value

                    })
                }).FirstOrDefault<AdvertData>();
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return null;
            }
        }
        public AdvertData getSingle(long AdvertID, byte LangID)
        {
            try
            {
                return db.ads_lang.Where(q => q.lang_id == LangID && q.ads_id == AdvertID && q.ads.is_cancel == false).Select(s => new AdvertData
                {
                    market_price = s.ads.market_price,
                    kisi_sayisi = s.kisi_sayisi,
                    yatak_sayisi = s.yatak_sayisi,
                    banyo_sayisi = s.banyo_sayisi,
                    isinma_turu = s.isinma_turu,
                    konum_durumu = s.konum_durumu,
                    advert_id = s.ads_id,
                    advert_name = s.ads_name,
                    created_time = s.ads.created_time,
                    is_active = s.ads.is_active,
                    week_price = s.ads.week_price,
                    ads_no = s.ads.ads_no,
                    geo_city_id = s.ads.geo_city_id,
                    geo_town_id = s.ads.geo_town_id,
                    geo_city_name = s.ads.geo_cities.city_name,
                    geo_town_name = s.ads.geo_town.town_name,
                    geo_quarter_name = s.ads.geo_quarter.quarter_name,
                    geo_quarter_id = s.ads.geo_quarter.id,
                    address = s.ads.address,
                    sell_price = s.ads.sell_price,
                    ads_view = s.ads.ads_view,
                    parent_category_id = s.ads.relation_ads_category.FirstOrDefault().categories.categories_lang.Where(q => q.categories.parent_id == 0).FirstOrDefault().category_id,
                    description = s.description,
                    geo_lat = s.ads.geo_lat,
                    geo_long = s.ads.geo_long,
                    member_name_surname = s.ads.member_id > 0 ? s.ads.members.name_surname : "Diwada",
                    member_phone = s.ads.members.phone,
                    member_id = s.ads.member_id,
                    type = s.ads.type,
                    stock_amount = s.ads.stock_amount,
                    is_showcase = s.ads.is_showcase,
                    shipping_amount_type = s.ads.shipping_amount_type,
                    shipping_delivery_time = s.ads.shipping_delivery_time,
                    type_status = s.ads.type == 1 ? "İlan" : "Ürün",
                    shipping_note = s.ads.shipping_note,
                    categories = s.ads.relation_ads_category
                          .Select(s3 => new RelationAdvertCategoryData
                          {
                              category_id = s3.cat_id,
                              category_name = s3.categories.categories_lang.FirstOrDefault(w => w.lang_id == LangID).category_name,
                              id = s.id,
                          }),
                    images = s.ads.ads_images.Select(s4 => new AdvertImagesData
                    {
                        id = s4.id,
                        img_name = s4.img_name,
                        display_order = s4.display_order

                    }),
                    info_definition = s.ads.ad_info_value.Where(w => w.ad_info_value_lang.Where(w2 => w2.lang_id == LangID).Count() > 0 && w.ad_info_value_lang.Where(q => q.lang_id == LangID).Count() > 0).Select(s5 => new AdvertInfoDefinitionData
                    {

                        definition_name = s5.ad_info.ad_info_lang.FirstOrDefault(f => f.lang_id == LangID && f.ad_info.ad_info_value.Where(r => r.ads_id == AdvertID).Count() > 0 && f.ad_info.ad_info_lang.Where(qs => qs.lang_id == LangID).Count() > 0).info_name,
                        definition_value = s5.ad_info_value_lang.FirstOrDefault(w2 => w2.lang_id == LangID && w2.ad_info_value != null && w2.ad_info_value != "" && w2.ad_info_value1.ads_id == AdvertID && w2.ad_info_value1.ad_info.ad_info_lang.Where(qp => qp.lang_id == LangID).Count() > 0).ad_info_value

                    })
                }).FirstOrDefault<AdvertData>();
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return null;
            }
        }

        public List<AdvertData> getMemberAdvert(int member_id)
        {
            return db.ads_lang.Where(q =>
            q.lang_id == Localization.Language
             && q.ads.ads_images.Count() > 0
            && q.ads.is_cancel == false
            && q.ads.is_deleted == false
            && q.ads.member_id == member_id
           ).Select(s => new AdvertData
           {
               market_price = s.ads.market_price,
               kisi_sayisi = s.kisi_sayisi,
               yatak_sayisi = s.yatak_sayisi,
               banyo_sayisi = s.banyo_sayisi,
               isinma_turu = s.isinma_turu,
               konum_durumu = s.konum_durumu,
               advert_name = s.ads_name,
               advert_id = s.ads_id,
               ads_no = s.ads.ads_no,
               display_order = s.ads.display_order,
               main_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
               geo_city_name = s.ads.geo_cities.city_name,
               geo_town_name = s.ads.geo_town.town_name,
               geo_quarter_name = s.ads.geo_quarter.quarter_name,
               address = s.ads.address,
               sell_price = s.ads.sell_price,
               created_time = s.ads.created_time,
               active_status = s.ads.is_active ? "<span class='label label-success'>Aktif</span>" : "<span class='label label-danger'>Pasif</span>",
               type_status = s.ads.type == 1 ? "İlan" : "Ürün"

           }).OrderByDescending(x => x.advert_id).ToList<AdvertData>();
        }

        public List<AdvertData> getMemberAdvertWish(int member_id)
        {
            return db.ads_lang.Where(q =>
            q.lang_id == Localization.Language
             && q.ads.ads_images.Count() > 0
            && q.ads.is_cancel == false
            && q.ads.is_deleted == false
            && q.ads.member_wishlist.Where(q2 => q2.member_id == member_id).Count() > 0
           ).Select(s => new AdvertData
           {
               market_price = s.ads.market_price,
               kisi_sayisi = s.kisi_sayisi,
               yatak_sayisi = s.yatak_sayisi,
               banyo_sayisi = s.banyo_sayisi,
               isinma_turu = s.isinma_turu,
               konum_durumu = s.konum_durumu,
               advert_name = s.ads_name,
               advert_id = s.ads_id,
               ads_no = s.ads.ads_no,
               display_order = s.ads.display_order,
               main_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
               geo_city_name = s.ads.geo_cities.city_name,
               geo_town_name = s.ads.geo_town.town_name,
               geo_quarter_name = s.ads.geo_quarter.quarter_name,
               address = s.ads.address,
               sell_price = s.ads.sell_price,
               created_time = s.ads.created_time,
               active_status = s.ads.is_active ? "<span class='label label-success'>Aktif</span>" : "<span class='label label-danger'>Pasif</span>",
               type_status = s.ads.type == 1 ? "İlan" : "Ürün",
               first_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
               second_image = s.ads.ads_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_name,
           }).OrderByDescending(x => x.advert_id).ToList<AdvertData>();
        }


        public List<AdvertData> getAdvertListByCategoryID(int category_id)
        {
            return db.ads_lang.Where(q =>
            q.lang_id == Localization.Language
            && q.ads.is_active
            && q.ads.ads_images.Count() > 0
            && q.ads.is_cancel != true
            && q.ads.is_deleted == false
            && q.ads.relation_ads_category.Where(q2 => q2.cat_id == category_id).Count() > 0
            ).Select(s => new AdvertData
            {
                market_price = s.ads.market_price,
                kisi_sayisi = s.kisi_sayisi,
                yatak_sayisi = s.yatak_sayisi,
                banyo_sayisi = s.banyo_sayisi,
                isinma_turu = s.isinma_turu,
                konum_durumu = s.konum_durumu,
                advert_name = s.ads_name,
                advert_id = s.ads_id,
                display_order = s.ads.display_order,
                main_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                sell_price = s.ads.sell_price,
                created_time = s.ads.created_time,
                geo_city_name = s.ads.geo_cities.city_name,
                geo_town_name = s.ads.geo_town.town_name,
                geo_quarter_name = s.ads.geo_quarter.quarter_name,
                ads_no = s.ads.ads_no,
                member_name_surname = s.ads.member_id > 0 ? s.ads.members.name_surname : "Diwada",
                ads_view = s.ads.ads_view,
                geo_city_id = s.ads.geo_city_id,
                geo_town_id = s.ads.geo_town_id,
                geo_quarter_id = s.ads.geo_quarter_id,
                type_status = s.ads.type == 1 ? "İlan" : "Ürün",

                first_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                second_image = s.ads.ads_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_name,

            }).OrderBy(o => o.display_order).ToList<AdvertData>();
        }

        public List<AdvertData> getAdvertListAll()
        {
            return db.ads_lang.Where(q =>
            q.lang_id == Localization.Language
            && q.ads.is_active
            && q.ads.ads_images.Count() > 0
            && q.ads.is_cancel != true
            && q.ads.is_deleted == false
            && q.ads.relation_ads_category.Count() > 0
            ).Select(s => new AdvertData
            {
                market_price = s.ads.market_price,
                kisi_sayisi = s.kisi_sayisi,
                yatak_sayisi = s.yatak_sayisi,
                banyo_sayisi = s.banyo_sayisi,
                isinma_turu = s.isinma_turu,
                konum_durumu = s.konum_durumu,
                advert_name = s.ads_name,
                advert_id = s.ads_id,
                display_order = s.ads.display_order,
                main_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                sell_price = s.ads.sell_price,
                created_time = s.ads.created_time,
                geo_city_name = s.ads.geo_cities.city_name,
                geo_town_name = s.ads.geo_town.town_name,
                geo_quarter_name = s.ads.geo_quarter.quarter_name,
                ads_no = s.ads.ads_no,
                member_name_surname = s.ads.member_id > 0 ? s.ads.members.name_surname : "Diwada",
                ads_view = s.ads.ads_view,
                geo_city_id = s.ads.geo_city_id,
                geo_town_id = s.ads.geo_town_id,
                geo_quarter_id = s.ads.geo_quarter_id,
                type_status = s.ads.type == 1 ? "İlan" : "Ürün",

                first_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                second_image = s.ads.ads_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_name,

            }).OrderBy(o => o.display_order).ToList<AdvertData>();
        }


        public List<AdvertData> getLastAdvertRandom()
        {
            List<AdvertData> _newList = new List<AdvertData>();

            var _list = db.ads_lang.Where(q =>
            q.lang_id == Localization.Language
            && q.ads.is_active
            && q.ads.ads_images.Count() > 0
            && q.ads.is_cancel != true
            && q.ads.is_deleted == false
            && q.ads.relation_ads_category.Count() > 0
            ).Select(s => new AdvertData
            {
                //url = "/" + Localization.LanguageStr + "/" + s.ads_name.ToURL() + "-i-" + s.ads_id,
                market_price = s.ads.market_price,
                kisi_sayisi = s.kisi_sayisi,
                yatak_sayisi = s.yatak_sayisi,
                banyo_sayisi = s.banyo_sayisi,
                isinma_turu = s.isinma_turu,
                konum_durumu = s.konum_durumu,
                advert_name = s.ads_name,
                advert_id = s.ads_id,
                display_order = s.ads.display_order,
                first_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                second_image = s.ads.ads_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_name,
                sell_price = s.ads.sell_price,
                created_time = s.ads.created_time,
                geo_city_name = s.ads.geo_cities.city_name,
                geo_town_name = s.ads.geo_town.town_name,
                geo_quarter_name = s.ads.geo_quarter.quarter_name,
                ads_no = s.ads.ads_no,
                member_name_surname = s.ads.member_id > 0 ? s.ads.members.name_surname : "",
                ads_view = s.ads.ads_view,
                geo_city_id = s.ads.geo_city_id,
                geo_town_id = s.ads.geo_town_id,
                geo_quarter_id = s.ads.geo_quarter_id,
            }).OrderBy(r => Guid.NewGuid()).Take(10).ToList<AdvertData>();

            foreach (var item in _list)
            {
                _newList.Add(new AdvertData
                {
                    market_price = item.market_price,
                    kisi_sayisi = item.kisi_sayisi,
                    yatak_sayisi = item.yatak_sayisi,
                    banyo_sayisi = item.banyo_sayisi,
                    isinma_turu = item.isinma_turu,
                    konum_durumu = item.konum_durumu,
                    advert_name = item.advert_name,
                    advert_id = item.advert_id,
                    display_order = item.display_order,
                    first_image = item.first_image,
                    second_image = item.second_image,
                    sell_price = item.sell_price,
                    created_time = item.created_time,
                    geo_city_name = item.geo_city_name,
                    geo_town_name = item.geo_town_name,
                    geo_quarter_name = item.geo_quarter_name,
                    ads_no = item.ads_no,
                    ads_view = item.ads_view,
                    geo_city_id = item.geo_city_id,
                    geo_town_id = item.geo_town_id,
                    geo_quarter_id = item.geo_quarter_id,
                    url = "/" + Localization.LanguageStr + "/" + item.advert_name.ToURL() + "-i-" + item.advert_id,
                });


            }

            return _newList;


        }

        public List<AdvertData> getLastAdvertShowcase()
        {
            List<AdvertData> _newList = new List<AdvertData>();

            var _list = db.ads_lang.Where(q =>
            q.lang_id == Localization.Language
            && q.ads.is_active
            && q.ads.ads_images.Count() > 0
            && q.ads.is_cancel != true
            && q.ads.is_deleted == false
            && q.ads.is_showcase == true
            && q.ads.relation_ads_category.Count() > 0
            ).Select(s => new AdvertData
            {
                //url = "/" + Localization.LanguageStr + "/" + s.ads_name.ToURL() + "-i-" + s.ads_id,
                market_price = s.ads.market_price,
                kisi_sayisi = s.kisi_sayisi,
                yatak_sayisi = s.yatak_sayisi,
                banyo_sayisi = s.banyo_sayisi,
                isinma_turu = s.isinma_turu,
                konum_durumu = s.konum_durumu,
                advert_name = s.ads_name,
                advert_id = s.ads_id,
                display_order = s.ads.display_order,
                first_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                second_image = s.ads.ads_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_name,
                sell_price = s.ads.sell_price,
                created_time = s.ads.created_time,
                geo_city_name = s.ads.geo_cities.city_name,
                geo_town_name = s.ads.geo_town.town_name,
                geo_quarter_name = s.ads.geo_quarter.quarter_name,
                ads_no = s.ads.ads_no,
                member_name_surname = s.ads.member_id > 0 ? s.ads.members.name_surname : "",
                ads_view = s.ads.ads_view,
                geo_city_id = s.ads.geo_city_id,
                geo_town_id = s.ads.geo_town_id,
                geo_quarter_id = s.ads.geo_quarter_id,
            }).OrderBy(r => r.display_order).Take(10).ToList<AdvertData>();

            foreach (var item in _list)
            {
                _newList.Add( new AdvertData {
                    market_price = item.market_price,
                    kisi_sayisi = item.kisi_sayisi,
                    yatak_sayisi = item.yatak_sayisi,
                    banyo_sayisi = item.banyo_sayisi,
                    isinma_turu = item.isinma_turu,
                    konum_durumu = item.konum_durumu,
                    advert_name = item.advert_name,
                    advert_id = item.advert_id,
                    display_order = item.display_order,
                    first_image = item.first_image,
                    second_image = item.second_image,
                    sell_price = item.sell_price,
                    created_time = item.created_time,
                    geo_city_name = item.geo_city_name,
                    geo_town_name = item.geo_town_name,
                    geo_quarter_name = item.geo_quarter_name,
                    ads_no = item.ads_no,
                    ads_view = item.ads_view,
                    geo_city_id = item.geo_city_id,
                    geo_town_id = item.geo_town_id,
                    geo_quarter_id = item.geo_quarter_id,
                    url = "/" + Localization.LanguageStr + "/" + item.advert_name.ToURL() + "-i-" + item.advert_id,
                });

               
            }

            return _newList;
        }

        public List<AdvertData> getAdvertListBySearch(string search_key, byte lang_id)
        {
            try
            {
                string _searchkey = search_key;

                List<AdvertData> _result = new List<AdvertData>();

                _result = db.ads_lang.Where(q =>
                 q.lang_id == Localization.Language
                 && q.ads.is_active
                 && q.ads.is_deleted == false
                 && q.ads.ads_images.Count() > 0
                 &&
                 (q.ads_name.Contains(_searchkey)
                 || q.description.Contains(_searchkey)
                 || q.ads.ads_no == _searchkey
                 || q.ads.geo_quarter.quarter_name.Contains(_searchkey)
                  || q.ads.geo_town.town_name.Contains(_searchkey)
                    || q.ads.geo_cities.city_name.Contains(_searchkey)
                 || q.ads.relation_ads_category.Where(q2 => q2.categories.categories_lang.Where(q3 => q3.category_name.Contains(_searchkey)).Count() > 0).Count() > 0
                 )).Select(s => new AdvertData
                 {
                     market_price = s.ads.market_price,
                     kisi_sayisi = s.kisi_sayisi,
                     yatak_sayisi = s.yatak_sayisi,
                     banyo_sayisi = s.banyo_sayisi,
                     isinma_turu = s.isinma_turu,
                     konum_durumu = s.konum_durumu,
                     advert_name = s.ads_name,
                    
                     advert_id = s.ads_id,
                     display_order = s.ads.display_order,
                     main_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                     sell_price = s.ads.sell_price,
                     created_time = s.ads.created_time,
                     geo_city_name = s.ads.geo_cities.city_name,
                     geo_town_name = s.ads.geo_town.town_name,
                     geo_quarter_name = s.ads.geo_quarter.quarter_name,
                     ads_no = s.ads.ads_no,
                     member_name_surname = s.ads.member_id > 0 ? s.ads.members.name_surname : "Diwada",
                     ads_view = s.ads.ads_view,
                     geo_city_id = s.ads.geo_city_id,
                     geo_town_id = s.ads.geo_town_id,
                     geo_quarter_id = s.ads.geo_quarter_id,
                     first_image = s.ads.ads_images.OrderBy(o => o.display_order).FirstOrDefault().img_name,
                     second_image = s.ads.ads_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_name,
                 }).OrderBy(o => o.display_order).ToList<AdvertData>();

                return _result;
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return null;
            }
        }

        public class AdvertData
        {
            public string oda_sayisi { get; set; }
            public string yatak_sayisi { get; set; }
            public string kisi_sayisi { get; set; }

    
            public string banyo_sayisi { get; set; }
            public string isinma_turu { get; set; }
            public string konum_durumu { get; set; }
            public string desc { get; set; }
            public string title { get; set; }
            public string tags { get; set; }
            public string member_name_surname { get; set; }
            public string member_phone { get; set; }
            public int member_id { get; set; }
            public int type { get; set; }
            public string type_status { get; set; }
            public string first_image { get; set; }
            public string second_image { get; set; }
            public int stock_amount { get; set; }
            public Nullable<decimal> market_price { get; set; }
            public Nullable<decimal> week_price { get; set; }
            public Nullable<int> shipping_amount_type { get; set; }
            public string shipping_delivery_time { get; set; }
            public string shipping_note { get; set; }
            public string active_status { get; set; }
            public long advert_id { get; set; }
            public short currency_type { get; set; }
            public string currency_type_text { get; set; }
            public string advert_name { get; set; }
            public string geo_lat { get; set; }
            public string geo_long { get; set; }
            public DateTime created_time { get; set; }
            public bool is_active { get; set; }
            public bool is_showcase { get; set; }
            public bool is_opportunity { get; set; }
            public string ads_no { get; set; }
            public int geo_country_id { get; set; }
            public int geo_city_id { get; set; }
            public int geo_town_id { get; set; }
            public int geo_quarter_id { get; set; }
            public string geo_country_name { get; set; }
            public string geo_city_name { get; set; }
            public string geo_quarter_name { get; set; }
            public string geo_town_name { get; set; }
            public string address { get; set; }
            public string description { get; set; }
            public string video_link { get; set; }
            public decimal sell_price { get; set; }
            public int ads_view { get; set; }
            public int parent_category_id { get; set; }
            public int child_category_id { get; set; }
            public int display_order { get; set; }
            public string main_image { get; set; }
            public string url { get; set; }
            public string customer_agent_name { get; set; }
            public string customer_agent_work_phone { get; set; }
            public string customer_agent_mobile_phone { get; set; }
            public string customer_agent_email { get; set; }
            public string ads_note { get; set; }


            public IEnumerable<RelationAdvertCategoryData> categories { get; set; }
            public IEnumerable<AdvertImagesData> images { get; set; }
            public IEnumerable<AdvertInfoDefinitionData> info_definition { get; set; }
            public IEnumerable<AdvertPropertyName> advert_property { get; set; }
        }

        public class RelationAdvertCategoryData
        {
            public long id { get; set; }
            public string category_name { get; set; }
            public int category_id { get; set; }

        }

        public class AdvertImagesData
        {
            public long id { get; set; }
            public string img_name { get; set; }
            public int display_order { get; set; }

        }

        public class AdvertInfoDefinitionData
        {
            public int ad_info_id { get; set; }
            public string definition_name { get; set; }
            public string definition_value { get; set; }

        }

        public class AdvertPropertyName
        {
            public long id { get; set; }
            public string ads_property_name { get; set; }

        }

    }
}