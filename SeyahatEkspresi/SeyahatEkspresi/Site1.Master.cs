﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace SeyahatEkspresi
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        MemberManager _memberManager = new MemberManager();
        int member_id;
        tinyhous_dbEntities db = new tinyhous_dbEntities();
        Localization _localizer = new Localization();
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                var _parentCat = db.categories_lang.Where(q => q.categories.is_active && q.lang_id == Localization.Language && q.categories.parent_id == 0 && q.categories.is_deleted==false).ToList();

                if (_parentCat.Count() > 0)
                {
                    //rptParentCategory.DataSource = _parentCat;
                    //rptParentCategory.DataBind();
                    rptCategories.DataSource = _parentCat;
                    rptCategories.DataBind();

                    rptFooterCat.DataSource = _parentCat.Take(3);
                    rptFooterCat.DataBind();

                    rptMobCat.DataSource = _parentCat;
                    rptMobCat.DataBind();
                }

                var _pages = db.page_lang.Where(q => q.lang_id == Localization.Language && q.page_id != 1).ToList();
                if (_pages.Count() > 0)
                {
                    rptPages.DataSource = _pages;
                    rptPages.DataBind();

                    rptPages2.DataSource = _pages;
                    rptPages2.DataBind();

                }

                form1.Action = Request.RawUrl;
                try
                {
                    if (Request.QueryString["lang"] == null)
                        Response.Redirect("/" + Localization.LanguageStr, false);



                    var _setting = db.settings.FirstOrDefault();

                    if (_setting != null)
                    {
                        hypWhatsappChat.NavigateUrl = "https://api.whatsapp.com/send?phone=" + _setting.phone;

                        //ltrHeaderAddres.Text = _setting.office_address;
                        //ltrFooterAdres.Text = _setting.office_address;

                        //ltrHeaderPhone.Text = _setting.phone;
                        //lrFooterPhone.Text = _setting.phone;
                        //ltrFooterPhone2.Text = _setting.phone2;

                        //ltrFooterEmail.Text = _setting.email;
                        //hypFooterEmail.NavigateUrl = "mailto:" + _setting.email;
                        //ltrFooterText.Text = _setting.footer_text;


                        ltrFooterText.Text = _setting.footer_text;
                        hypFacebook.NavigateUrl = _setting.facebook_link;
                        hypTwitter.NavigateUrl = _setting.twitter_link;
                        hypInstagram.NavigateUrl = _setting.instagram_link;
                        hypHeaderPhone.NavigateUrl = "tel:" + _setting.phone;
                        hypHeaderPhone.Text = _setting.phone;
                        hypHeaderPhone1.NavigateUrl = "tel:" + _setting.phone;
                        hypHeaderPhone1.Text = _setting.phone;

                        hypFacebook1.NavigateUrl = _setting.facebook_link;
                        hypTwitter1.NavigateUrl = _setting.twitter_link;
                        hypInstagram1.NavigateUrl = _setting.instagram_link;
                        hypWhatsapp.NavigateUrl = "https://api.whatsapp.com/send?phone=" + _setting.phone2;
                        hypWhatsapp2.NavigateUrl = "https://api.whatsapp.com/send?phone=" + _setting.phone2;

                        hypFacebook3.NavigateUrl = _setting.facebook_link;
                        hypTwitter3.NavigateUrl = _setting.twitter_link;
                        hypInstagram3.NavigateUrl = _setting.instagram_link;
                        hypWhatsapp3.NavigateUrl = "https://api.whatsapp.com/send?phone=" + _setting.phone2;

                    }

                }
                catch (Exception ex)
                {
                    Log.Add(ex);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }



        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                byte lang = e.CommandArgument.toByte();
                _localizer.changeLanguage(lang);

            }
            catch (Exception ex)
            {

                Log.Add(ex);

            }
        }


        //private void getLanguages()
        //{
        //    try
        //    {
        //        //ltrCurrentLang.Text = Localization.LanguageStr;

        //        var _Lang = db.lang.ToList();
        //        if (_Lang.Count > 0)
        //        {
        //            rptLanguages.DataSource = _Lang;
        //            rptLanguages.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Add(ex);
        //    }
        //}

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSearchKey.Text.Length >= 3)
                {
                    string key = (txtSearchKey.Text.Length > 50) ? txtSearchKey.Text.Substring(0, 50) : txtSearchKey.Text.Trim();
                    
                    Session["key"] = key;
                    Response.Redirect("/" + Localization.LanguageStr + "/arama/" + key.ToURL(), false);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void rptParentCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Repeater rptSubCategories = (Repeater)e.Item.FindControl("rptSubCategories");
                int parent_id = DataBinder.Eval(e.Item.DataItem, "category_id").toInt();

                var _subCategories = db.categories_lang.Where(q => q.categories.is_active && q.lang_id == Localization.Language && q.categories.parent_id == parent_id && q.categories.is_deleted==false).OrderBy(o=> o.categories.display_order).ToList();

                if (_subCategories.Count() > 0)
                {
                    rptSubCategories.DataSource = _subCategories;
                    rptSubCategories.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void rptSubCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Repeater rptSubSubCategories = (Repeater)e.Item.FindControl("rptSubSubCategories");
                int parent_id = DataBinder.Eval(e.Item.DataItem, "category_id").toInt();

                var _subSubCategories = db.categories_lang.Where(q => q.categories.is_active && q.lang_id == Localization.Language && q.categories.parent_id == parent_id && q.categories.is_deleted == false).ToList();

                if (_subSubCategories.Count() > 0)
                {
                    rptSubSubCategories.DataSource = _subSubCategories;
                    rptSubSubCategories.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Remove("member_login");
                Session.Remove("member_id");
                Session.Remove("member_name");
                Session.Remove("member_surname");
                Session.Remove("member_email");
                Session.RemoveAll();

                Response.Cookies["ogrccc"].Expires = DateTime.Today.AddDays(-30);

                Response.Redirect("/", false);
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                

            }
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSearchKey.Text.Length >= 3)
                {
                    string key = (txtSearchKey.Text.Length > 50) ? txtSearchKey.Text.Substring(0, 50) : txtSearchKey.Text.Trim();

                    Session["key"] = key;
                    Response.Redirect("/" + Localization.LanguageStr + "/arama/" + key.ToURL(), false);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void lnkSearchMob_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSearchKeyMobile.Text.Length >= 3)
                {
                    string key = (txtSearchKeyMobile.Text.Length > 50) ? txtSearchKeyMobile.Text.Substring(0, 50) : txtSearchKeyMobile.Text.Trim();

                    Session["key"] = key;
                    Response.Redirect("/" + Localization.LanguageStr + "/arama/" + key.ToURL(), false);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}