﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using SeyahatEkspresi.Core;

namespace SeyahatEkspresi
{
    public partial class faq : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _List = db.sss_content_lang.Where(q => q.lang_id == Localization.Language).OrderBy(o => o.sss_content.display_order).ToList();

                if (_List.Count() > 0)
                {
                    rptSSS.DataSource = _List;
                    rptSSS.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }
    }
}