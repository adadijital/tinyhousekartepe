﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class advert_detail : Base
    {
        MemberManager _memberManager = new MemberManager();
        AdvertManager _advertManager = new AdvertManager();
        long advert_id;
        MemberBasket _memberBasket;
        int member_id;



        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["advert_id"] != null)
                {
                    advert_id = Convert.ToInt64(Request.QueryString["advert_id"]);



                    var _detail = _advertManager.getSingleWebSite(advert_id, Localization.Language);

                    if (_detail != null)
                    {
                        Page.Title = _detail.advert_name + " | " + System.Configuration.ConfigurationManager.AppSettings["site_name"].ToString();

                        hdnID.Value = _detail.advert_id.ToString();


                        ltrAdvertName.Text = _detail.advert_name;
                        ltrKisiSayisi.Text = _detail.kisi_sayisi + " Kişi";
                        ltrYatakSayisi.Text = _detail.yatak_sayisi + " Yatak";
                        ltrBanyoSayisi.Text = _detail.banyo_sayisi + " Banyo";

                        hdnWeekPrice.Value = _detail.week_price.ToString();

                        if (!string.IsNullOrEmpty(_detail.description))
                        {
                            ltrDesc.Text = _detail.description;
                        }
                       
                        ltrPrices.Text = String.Format("{0:0,00}", _detail.sell_price).Replace(".", "");
                        ltrPrices2.Text = String.Format("{0:0,00}", _detail.sell_price).Replace(".", "");

                        if (_detail.market_price.todecimal() > 0)
                        {
                            marketPrice.Visible = true;
                            ltrMarketPrice.Text = String.Format("{0:0,00}", _detail.market_price).Replace(".", "");
                        }

                        ltrAdvertName3.Text = _detail.advert_name;
                        ltrLocation.Text = _detail.geo_town_name.ToLower() + ", " + _detail.geo_city_name.ToLower() + ", " + "Türkiye";

                        ltrMap.Text = "<iframe height='600px' width='100%' src='https://www.google.com/maps?q=" + _detail.geo_town_name + "," + _detail.geo_city_name + "&output=embed'></iframe>";

                        var _AdsCategory = db.relation_ads_category.Select(s => new
                        {
                            relation_id = s.id,
                            s.categories.categories_lang.Where(q => q.lang_id == 1).FirstOrDefault().category_name,
                            s.ads_id,
                            s.cat_id,
                            s.categories.parent_id,
                        }).OrderBy(o => o.parent_id).Where(q => q.ads_id == advert_id).ToList();


                        rptCategories.DataSource = _AdsCategory;
                        rptCategories.DataBind();


                        if (_detail.info_definition.Count() > 0)
                        {
                            rptDefinition.DataSource = _detail.info_definition.ToList();
                            rptDefinition.DataBind();
                        }


                        var _images = db.ads_images.Where(q => q.ads_id == advert_id).OrderBy(o => o.display_order).Skip(1).ToList();
                        var _mobImages = db.ads_images.Where(q => q.ads_id == advert_id).OrderBy(o => o.display_order).ToList();

                        if (_images.Count() > 0)
                        {
                            rptImages.DataSource = _images;
                            rptImages.DataBind();

                         
                        }

                        if (_mobImages.Count() > 0)
                        {
                            rptMobImages.DataSource = _mobImages;
                            rptMobImages.DataBind();
                        }


                        var _firstImage = db.ads_images.Where(q => q.ads_id == advert_id).OrderBy(o => o.display_order).FirstOrDefault();

                        if (_firstImage != null)
                        {
                            Image1.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["cdn_url"] + "/uploads/ads/L/" + _firstImage.img_name;
                            lnkFancy.HRef = System.Configuration.ConfigurationManager.AppSettings["cdn_url"] + "/uploads/ads/L/" + _firstImage.img_name;
                        }


                        var _showCase = _advertManager.getLastAdvertShowcase();

                        if (_showCase.Count() > 0)
                        {
                            rptShowCase.DataSource = _showCase;
                            rptShowCase.DataBind();
                        }


                        #region kaç kez görüntülenmiş
                        //kaç kez görüntülenmiş
                        if (HttpContext.Current.Request.Cookies["w" + advert_id] == null)
                        {

                            try
                            {
                                var _viewData = db.ads.Where(q => q.id == advert_id).FirstOrDefault();
                                int _currentView = _viewData.ads_view;

                                _viewData.ads_view = _currentView + 1;

                                db.SaveChanges();

                                HttpContext.Current.Response.Cookies["w" + advert_id].Value = "true";
                                HttpContext.Current.Response.Cookies["w" + advert_id].Expires = DateTime.Today.AddYears(10);
                            }
                            catch (Exception ex)
                            {

                            }


                        }

                        #endregion

                        #region ogTitle

                        #endregion
                    }
                    else
                        Response.Redirect("/", false);
                }
                else
                    Response.Redirect("/", false);
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }




        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                reservasiton_request _newReq = new reservasiton_request();
                _newReq.ads_id = advert_id;
                _newReq.start_date = txtDate.Text.Trim();
                _newReq.finish_date = txtDate2.Text.Trim();
                _newReq.person_count = drpYetiskin.SelectedValue.toInt();
                _newReq.reserve_dates = hdnSelectDays.Value;
                _newReq.name_surname = txtNameSurname.Text.Trim();
                _newReq.phone = txtPhone.Text.Trim();
                _newReq.description = txtDesc.Text.Trim();
                _newReq.created_time = DateTime.Now;
                _newReq.email = txtEmail.Text.Trim();
                _newReq.is_approved = false;
                _newReq.is_cancel = false;

                db.reservasiton_request.Add(_newReq);

                if (db.SaveChanges() > 0)
                {
                   // var _detail = _advertManager.getSingle(advert_id, Localization.Language);
                    //Email.newRez(_detail.title, _detail.ads_no);
                    showWarningBasic(this.Master, "Rezervasyon talebiniz alınmıştır, en kısa sürede dönüş sağlayacağız.", "Tebrikler");
                }
            }
            catch (Exception ex)
            {

                Log.Add(ex);
            }
        }


        [WebMethod]
        public static string getReservastion(string ads_id)
        {
            List<DatesModel> _returnDates = new List<DatesModel>();

            Base _b = new Base();
            int _id = ads_id.toInt();

            var _detail = _b.db.reservasiton_request.Where(q => q.ads_id == _id && q.is_cancel == false && q.is_approved == true).Select(s => new
            {
                s.reserve_dates,
            }).ToList();

            string[] dateListe = null;
     
            for (int i = 0; i < _detail.Count; i++)
            {
                // dateListe = _detail[i].reserve_dates.Split(',');

                foreach (var item in _detail[i].reserve_dates.Split(','))
                {
                    _returnDates.Add(new DatesModel { date = item });
                }
            }

       


            var json = new JavaScriptSerializer().Serialize(_returnDates);

            return json;



        }

        public class DatesModel
        {
            public string date { get; set; }
        }
    }
}