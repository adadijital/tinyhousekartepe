﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="SeyahatEkspresi.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section>
        <div class="bannerimg cover-image bg-background3" data-image-src="/assets/img/banner2.jpg">
            <div class="header-text mb-0">
                <div class="container">
                    <div class="text-center text-white ">
                        <h1 class="">İlanı Düzenle</h1>
                        <ol class="breadcrumb text-center">
                            <li class="breadcrumb-item"><a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/">Anasayfa</a></li>
                            <li class="breadcrumb-item active text-white" aria-current="page">İlanı Düzenle</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sptb">
        <div class="container">
            <div class="row ">
                <div class="col-lg-8 col-md-12 col-md-12">
                    <div class="card mb-lg-0">
                        <div class="card-header justify-content-between ">
                            <h3 class="card-title">İlan Bilgileri</h3>
                            <ul class="pull-right">
                                <asp:Repeater ID="rptCategories" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <%#Eval("category_name") %>
                                        </li>

                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label text-dark">İlan Başlığı *</label>
                                <asp:TextBox runat="server" placeholder="İlan Başlığı *" ID="txtAdvertTitle" CssClass="form-control" MaxLength="150" />
                            </div>
                            <div class="form-group">
                                <label class="form-label text-dark">İlan Açıklaması *</label>
                                <ckeditor:ckeditorcontrol id="txtDesc" width="100%" height="300" runat="server" placeholder="" text=''></ckeditor:ckeditorcontrol>
                            </div>
                            <div class="form-group">
                                <label class="form-label text-dark">Fiyat *</label>
                                <asp:TextBox runat="server" placeholder="Fiyat *" CssClass="form-control" ID="txtAdvertPrice" />
                            </div>
                        </div>
                        <div class="card-header ">
                            <h3 class="card-title">İlan Özellikleri</h3>
                        </div>
                        <div class="card-body">
                            <asp:Repeater ID="rptAdInfoDefinition" runat="server" OnItemDataBound="rptAdInfoDefinition_ItemDataBound">
                                <ItemTemplate>
                                    <div class="form-group">
                                        <label class="form-label text-dark"><%#Eval("info_name") %> </label>
                                        <asp:TextBox runat="server" placeholder='<%#Eval("info_value") %>' CssClass="form-control" ID="txtProperty" />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Panel ID="pnlProduct" runat="server" Visible="false">


                                <br />
                                <h3>Stok ve Kargo Seçenekleri</h3>
                                <div class="input-box">
                                    <div class="input-box">
                                        <span>Stok Adedi *</span>
                                        <asp:TextBox runat="server" placeholder="1" ID="txtStokAmount" value="1" MaxLength="5" />
                                    </div>
                                    <span>Kargo Ücreti *</span>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpDeliveryType" runat="server">
                                            <asp:ListItem Value="1">Alıcı Öder</asp:ListItem>
                                            <asp:ListItem Value="2">Satıcı Öder</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>


                                    <span>Kargoya Veriliş Süresi *</span>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpDeliveryTime" runat="server">
                                            <asp:ListItem Value="Aynı Gün İçinde">Aynı Gün İçinde</asp:ListItem>
                                            <asp:ListItem Value="3 İş Günü İçinde">3 İş Günü İçinde</asp:ListItem>
                                            <asp:ListItem Value="5 İş Günü İçinde">5 İş Günü İçinde</asp:ListItem>
                                            <asp:ListItem Value="7 İş Günü İçinde">7 İş Günü İçinde</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <span>Alıcılar için Kargo Notunuz (isteğe bağlı)</span>
                                    <div class="form-group">
                                        <asp:TextBox ID="txtNote" MaxLength="150" runat="server" TextMode="MultiLine" Rows="4" placeholder="Kargo gönderimi ile ilgili alıcıya daha fazla bilgi vermek için bu bölümü kullanabilirsiniz. "></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="card-header ">
                            <h3 class="card-title">Adres Bilgileri</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">

                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <label class="form-label text-dark">Şehir *</label>
                                        <asp:DropDownList ID="drpCityList" runat="server" OnSelectedIndexChanged="drpCityList_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control custom-select"></asp:DropDownList>
                                    </ContentTemplate>

                                </asp:UpdatePanel>


                            </div>
                            <div class="form-group">

                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlTown" runat="server" Visible="false">
                                            <label class="form-label text-dark">İlçe *</label>
                                            <asp:DropDownList ID="drpTownList" runat="server" AutoPostBack="true" CssClass="form-control custom-select" OnSelectedIndexChanged="drpTownList_SelectedIndexChanged"></asp:DropDownList>
                                        </asp:Panel>
                                    </ContentTemplate>

                                </asp:UpdatePanel>


                            </div>
                            <div class="form-group">

                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlQuarter" runat="server" Visible="false">
                                            <label class="form-label text-dark">Mahalle *</label>
                                            <asp:DropDownList ID="drpQuarter" CssClass="form-control custom-select" runat="server"></asp:DropDownList>
                                        </asp:Panel>
                                    </ContentTemplate>

                                </asp:UpdatePanel>


                            </div>
                            <div class="form-group">
                                <label class="form-label text-dark">Açık Adres *</label>
                                <asp:TextBox runat="server" placeholder="Adres *" CssClass="form-control" ID="txtAdress" MaxLength="150" />
                            </div>



                        </div>
                        <div class="card-header ">
                            <h3 class="card-title">İlan Resimleri *</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <ul style="border: none; height: auto" id="sortableResim">

                                            <asp:Repeater ID="rptImages" runat="server">
                                                <ItemTemplate>
                                                    <li id="item<%#Eval("id") %>">
                                                        <div class="col-md-12">
                                                            <img class="img-responsive" src="/uploads/ads/268x150/<%#Eval("img_name") %>" alt="" style="float: left;">
                                                            <asp:LinkButton runat="server" ID="lnkRemove" OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" OnCommand="lnkRemove_Command" CssClass="btn btn-xs btn-danger" Style="float: left; width: 50%; margin: 0 25%;"
                                                                CommandArgument='<%#Eval("id") %>'>Sil
                                                            </asp:LinkButton>
                                                        </div>
                                                    </li>

                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="form-group">
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="fileupload multiUpload" AllowMultiple="true" />
                                <div class="galleryWrap" style="float: left; width: 100%;">

                                    <div class="dz-message">
                                        Yüklemek istediğiniz dosyayı bu alana sürükleyip bırakın.
                                            <br>
                                        Resim ölçüleri 1920x1080 boyutunda olmalıdır
                                    </div>
                                </div>
                            </div>
                         
                        </div>
                        <div class="card-footer ">
                            <asp:LinkButton ID="LinkButton1" runat="server" class="next-step btn btn-secondary" OnClick="LinkButton1_Click">Güncelle</asp:LinkButton>
                        </div>


                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">İlan Ekleme Kuralları</h3>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled widget-spec  mb-0">
                                <li><i class="fa fa-check text-success" aria-hidden="true"></i>Money Not Refundable </li>
                                <li><i class="fa fa-check text-success" aria-hidden="true"></i>You can renew your Premium ad after experted. </li>
                                <li><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are active for depend on package. </li>
                                <li class="ml-5 mb-0"><a href="#">View more..</a> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">İlan Eklerken Dikkat Edilmesi Gereken Hususlar</h3>
                        </div>
                        <div class="card-body pb-2">
                            <ul class="list-unstyled widget-spec vertical-scroll mb-0" style="overflow-y: hidden; height: 124px;">
                                <li style="" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                                <li style="" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active </li>
                                <li style="" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top </li>
                                <li style="" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top </li>
                                <li style="display: none;" class="undefined"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads will be Show in Google results </li>
                                <li style="display: none;"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium Ads Active </li>
                                <li style="display: none;"><i class="fa fa-check text-success" aria-hidden="true"></i>Premium ads are displayed on top </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
