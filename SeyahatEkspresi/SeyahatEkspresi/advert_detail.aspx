﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="advert_detail.aspx.cs" Inherits="SeyahatEkspresi.advert_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61dc4c1bb77e2e68"></script>
    <asp:HiddenField ID="hdnSelectDays" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnWeekPrice" runat="server" />

    <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">

                        <asp:Repeater ID="rptCategories" runat="server">
                            <ItemTemplate>
                                <p>
                                    <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/ilanlar-<%#SeyahatEkspresi.cExtensions.ToURL(Eval("category_name").ToString()) %>-c-<%#Eval("cat_id") %>" title="<%#Eval("category_name") %>">
                                <%#Eval("category_name") %> 
                            </a>
                                </p>

                            </ItemTemplate>
                        </asp:Repeater>
                        <h1>
                            <asp:Literal ID="ltrAdvertName" runat="server"></asp:Literal>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!--ilan detay slider-->
    <div class="container ilan-detay-slider-mg mb-5">
        <div class="row d-flex">
            <div class="col-6">
                <div class="konak-detay-slider-cover">
                    <div class="konak-detay-slider-left">
                        <div class="konak-detay-slider-left-item">
                            <a data-fancybox="gallery" runat="server" id="lnkFancy">
                                <asp:Image ID="Image1" runat="server" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="konak-detay-slider-right col-6">
                <asp:Repeater ID="rptImages" runat="server">
                    <ItemTemplate>

                        <div class="img-content">
                            <div class="konak-detay-slider-item-inner-cover">
                                <div class="konak-detay-slider-item-inner ">
                                    <a href="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/L/<%#Eval("img_name") %>" data-fancybox="gallery">
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("img_name") %>" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

    <!--ilan detay mobil slider-->
    <div class="container mobilslidermg mb-5">
        <div class="row">
            <div class="col-md-12">
                <div class="swiper mySwiper myAdvertDetail">
                    <div class="swiper-wrapper">
                        <asp:Repeater ID="rptMobImages" runat="server">
                            <ItemTemplate>
                                <div class="swiper-slide advert-slide">
                                    <a href="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/L/<%#Eval("img_name") %>" data-fancybox="gallery2">
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("img_name") %>" alt="img">
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>



                    </div>
                    <div class="swiper-pagination swiper-images-peg mobil-show"></div>
                </div>
            </div>
        </div>
    </div>



    <!--Add Description-->

    <!--ilan açıklama alanı-->
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="konak-detay-content-text-cover">
                    <div class="konak-detay-content-text-header">
                        <h1>
                            <asp:Literal ID="ltrAdvertName3" runat="server"></asp:Literal>

                        </h1>
                     

                        <div class="header-share-btn">
                            <a href="javascrpt:void(0)" class="share-inline mobil-show"><i class="fas fa-share-alt"></i>Paylaş </a>
                        </div>
                           <p class="konak-detay-guncel-fiyat">
                       
                                 <i class="fas fa fa-try"></i>
                            <asp:Literal ID="ltrPrices" runat="server"></asp:Literal>
                            <span>/ 1 gece </span>
                          
                           
                        </p>
                       </div>
                    </div>
                 
                    <div class="konak-detay-content-text-header-bottom">
                        <div>
                            <p>
                                <i class="fas fa-user-alt"></i>
                                <asp:Literal ID="ltrKisiSayisi" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <i class="fas fa-bed"></i>
                                <asp:Literal ID="ltrYatakSayisi" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <i class="fas fa-bath"></i>
                                <asp:Literal ID="ltrBanyoSayisi" runat="server"></asp:Literal>
                            </p>
                        </div>
                        <div class="header-share-btn">
                            <a href="javascrpt:void(0)" class="share-inline mobil-hide"><i class="fas fa-share-alt"></i>Paylaş </a>
                        </div>
                    </div>
                    <div class="col-md-12 share-content">
                        <div class="addthis_inline_share_toolbox"></div>
                    </div>
                    <div class="konak-detay-content-text">
                        <p>

                            <asp:Literal ID="ltrDesc" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div class="konak-detay-list-cover">
                        <p>Genel Özellikler</p>

                        <div class="konak-detay-list-item-cover ">
                            <div class="konak-detay-list-item ">
                                <ul class="p-0 col-12 li-normal">
                                    <asp:Repeater ID="rptDefinition" runat="server">
                                        <ItemTemplate>
                                            <li class="col-3">
                                                <div>
                                                    <i class="fas fa-check"></i>
                                                    <p>
                                                        <%#Eval("definition_name") %>
                                                    </p>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="konak-detay-list-cover">
                        <p>Konaklama Kuralları</p>
                        <div class="konak-detay-list-item-cover">
                            <div class="konak-detay-list-item col-6">
                                <ul class="p-0 li-normal">
                                    <li>
                                        <div>
                                            <i class="fas fa-check"></i>
                                            <p>Otele misafirlerimizin girişi 14:00. Otelden ayrılış saati 12:00’dır. </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <i class="fas fa-check"></i>
                                            <p>Kimlik bilgilerinizin kaydından sonra otele giriş yapabilirsiniz. </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <i class="fas fa-check"></i>
                                            <p>Rezervasyonun tamamlanması için kalan ödemenin tamamının yapılması gerekmektedir. </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="konak-detay-list-item col-6">
                                <ul class="p-0 li-normal">
                                    <li class="last-konak-list">
                                        <div>
                                            <i class="fas fa-check"></i>
                                            <p>Misafirlerimiz konaklamaları boyunca villayı, havuzu, bahçe mobilyalarını, villada kendilerine çalışır ve sağlam durumda teslim edilen tüm elektrikli ve elektronik eşyalar ile diğer mobilyaları temiz kullanmakla yükümlüdür. </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
           
            <div class="col-lg-4 konak-detay-fiyat-cover-mob">

                <!-- <div class="konak-detay-fiyat-cover">
                    <div class="konak-deta-fiyat">
                        <div class="konak-detay-inner">
                            <p class="eskifiyat second-eskifiyat" runat="server" id="marketPrice" visible="false">
                                <i class="fas fa fa-try"></i>
                                <asp:Literal ID="ltrMarketPrice" runat="server"></asp:Literal>
                            </p>
                     
                            <p class="konak-detay-guncel-fiyat">
                                <i class="fas fa fa-try"></i>
                                <asp:Literal ID="ltrPrices22" runat="server"></asp:Literal>
                                <span>/ 1 gece </span>
                            </p>
                        </div>
                        <div class="second-detay-cover">
                            <div class="konak-detay-inner second-detay">
                                <p class="konak-page-fiyat-header">Giriş Tarihi </p>
                                <label class="konak-page-fiyat-date-input">
                                    <i class="far fa-calendar-alt"></i>
                                    <input placeholder="Lütfen tarih seçiniz" type="text" id="input1" disabled="disabled">
                                </label>
                            </div>
                        </div>
                        <div class="konak-detay-inner">
                            <p class="konak-page-fiyat-header">Çıkış Tarihi </p>
                            <label class="konak-page-fiyat-date-input">
                                <i class="far fa-calendar-alt"></i>
                                <input placeholder="Lütfen tarih seçiniz" type="text" id="input2" disabled="disabled">
                            </label>
                        </div>
                        <div class="konak-detay-inner last-konak-inner">

                            <a href="javascript:void(0)" class="randevuButton">Rezervasyon Yap</a>

                        </div>
                    </div>
                </div>-->
                <div class="konak-detay-info-cover">
                    <div class="konak-detay-info-content k-top">
                        <h1>Kolay ve hızlı
                            <br>
                            rezervasyon</h1>
                        <p>Ön ödemeyi yapın oteliniz sizin için ayrılsın.  </p>
                    </div>
                    <div class="konak-detay-info-content k-bottom">
                        <div class="konak-detay-info-content-text">
                            <p>Toplam rezervasyon tutarınızın sadece <span>%50</span> ’sini ödeyerek rezervasyonunuzu tamamlayabilirsiniz.</p>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    


    <div class="container" id="#calender">
        <div class="row  mb-5">
            <div class="col-lg-12 ">
                <div class="konak-header">
                    <h1>Müsaitlik Durumu & Tarih Seçimi</h1>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="year-calendar"></div>
                <!-- <div class="displayCalender"></div>-->
            </div>
        </div>
    </div>

    <div class="container maps">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="konak-header">
                    <h1 class="konak-header-mobil-margin">
                        <asp:Literal ID="ltrLocation" runat="server"></asp:Literal>
                    </h1>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-lg-12">
                <div class="map-cover">
                    <asp:Literal ID="ltrMap" runat="server"></asp:Literal>
                </div>
            </div>
        </div>-->
        <div class="row konak-detay-info-cover-mob">
            <div class="col-12 konak-detay-info-cover-mob">
                <div class="konak-detay-info-cover konak-detay-info-cover-mob">
                    <div class="konak-detay-info-content k-top">
                        <h1>Kolay ve hızlı
                            <br>
                            rezervasyon</h1>
                        <p>Ön ödemeyi yapın oteliniz sizin için ayrılsın. </p>
                    </div>
                    <div class="konak-detay-info-content k-bottom">
                        <div class="konak-detay-info-content-text">
                            <p>Toplam rezervasyon tutarınızın sadece <span>%50</span> ’sini ödeyerek rezervasyonunuzu tamamlayabilirsiniz.</p>
                        </div>
                        <div class="konak-detay-inner last-konak-inner">
                            <a href="javascript:void(0)" class="randevuButton">Rezervasyon Yap</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="outher-container bg-white pb-5 pt-5">
        <div class="container mb-5">
            <div class="row  slider-header mb-5">
                <div class="col-lg-12">
                    <div class="swiper-top-inner">
                        <h1>Benzer Konaklamalar</h1>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar" class="mobil-hide">Tüm Konaklamalar</a>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar" class="mobil-show">Tümü</a>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 button-rev">
                    <div class="swiper mySwiper CategorySwiper2">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="rptShowCase" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide blog-slide">
                                        <div class="konak-content-cover">
                                            <div class="konak-content-top">
                                                <div class="konak-content-top-img" style="background-image: url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>)" second-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("second_image")%>" first-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>"></div>

                                                <p>
                                                    <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>">
                                                        <%#Eval("advert_name") %>
                                                    </a>


                                                </p>
                                                <div class="hover-share non-border">
                                                    <a href="javascript:void(0)" class="shareButon">
                                                        <%--<img class="sharesvg" src="/assets/img/sharasvg.svg"/>
                                                        <%--<i class="fas fa-share-alt"></i>--%>
                                                        <%-- <div class="card-share-content">
                                                             <div class="addthis_inline_share_toolbox" :data-description="item.advert_name" :data-title="item.advert_name" :data-url="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>'+item.url" :data-media="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.first_image + ''"></div>
                                                        </div>--%>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="konak-content-bottom">
                                                <div class="konak-content-bottom-topinside">
                                                    <p><%#Eval("kisi_sayisi") %> Kişi</p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                        <%#Eval("yatak_sayisi") %> Yatak
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                        <%#Eval("banyo_sayisi") %> Banyo
                                                    </p>
                                                </div>
                                                <div class="konak-content-bottom-topinside second">
                                                    <p>
                                                        <%#Eval("isinma_turu") %>
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 18.svg">
                                                        <%#Eval("konum_durumu") %>
                                                    </p>
                                                </div>
                                                <%--<p class="eskifiyat"> <i class="fas fa-lira-sign"></i>2400</p>--%>
                                                <div class="guncelfiyat">
                                                    <div class="guncelfiyat-inner advertdetail-fiyat">
                                                        <p class="card-sp-p"><i class="fa fa-try" aria-hidden="true"></i><%# Eval("sell_price", "{0:0,00}") %> <span>/ 1 gece</span>  </p>
                                                    </div>
                                                    <div class="guncelfiyat-inner">
                                                        <a class="mobil-hide" href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>">Rezervasyon <i class="fas fa-arrow-right"></i></a>
                                                        <a class="mobil-show" href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>"><i class="fas fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="swiper-button-cover1">
                        <div class="second-swiper-arrows-button1">
                            <div class="swiper-second-prev1 "><i class="fas fa-arrow-left"></i></div>
                            <div class="swiper-second-next1 "><i class="fas fa-arrow-right"></i></div>
                        </div>
                    </div>


                </div>
            </div>
            <%--<div class="row  slider-header mb-5">
                <div class="col-lg-12">
                    <div class="swiper-top-inner konak-detay-swiper-peg">
                        <div class="swiper-button-cover">
                            <div class="second-swiper-arrows-button">
                                <div class="swiper-second-prev "><i class="fas fa-arrow-left"></i></div>
                                <div class="swiper-second-next "><i class="fas fa-arrow-right"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
    </div>


    <div class="randevuModal">

        <div class="modalInner">
            <h4 class="mb-3">Rezervasyon isteği gönderip hemen yerini ayırt!</h4>

            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-6 ">
                    <div class="form-group">
                        <label class="form-label text-dark">Giriş Tarihi</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtDate" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" Enabled="false" ID="txtDate" CssClass="form-control date" autocomplete="off" placeholder="Giriş Tarihi" data-toggle="datetimepicker" data-target="#txtDate" />
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6  col-6">
                    <div class="form-group">
                        <label class="form-label text-dark">Çıkış Tarihi</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDate2" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" Enabled="false" ID="txtDate2" CssClass="form-control date" autocomplete="off" placeholder="Giriş Tarihi" data-toggle="datetimepicker" data-target="#txtDate" />

                    </div>
                </div>


                <div class="col-xl-12 col-lg-12 col-md-12 ">
                    <div class="form-group">
                        <label class="form-label text-dark">Yetişkin Kişi Sayısı</label>
                        <asp:RequiredFieldValidator ControlToValidate="drpYetiskin" ID="RequiredFieldValidator6" CssClass="randevuUyari" runat="server" ValidationGroup="validateCredential2" InitialValue="0" ErrorMessage="Seçim yapmalısınız">

                        </asp:RequiredFieldValidator>

                        <asp:DropDownList ID="drpYetiskin" runat="server" CssClass="form-control">
                            <asp:ListItem Text="-Seçiniz" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                            <asp:ListItem Text="9" Value="9"></asp:ListItem>
                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>


                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Ad Soyad</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtNameSurname" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtNameSurname" CssClass="form-control" placeholder="Ad Soyad" />

                    </div>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Telefon</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtPhone" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control phone phoneMask" placeholder="Telefon" />

                    </div>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">E-mail Adresi</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtEmail" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control phone phoneMask" placeholder="E-mail Adresi" />

                    </div>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Varsa Notunuz</label>
                        <asp:TextBox runat="server" ID="txtDesc" CssClass="form-control" placeholder="Varsa Notunuz" TextMode="MultiLine" />
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="modalinner-fyt mt-3">
                        <p id="selectDate"> </p>
                    <p id="totalPrice"></p>
                    </div>
                    
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <p class="mb-0 text-right">
                        <a class="btn btn-danger randevuModalClose">Vazgeç</a>

                        <asp:LinkButton ID="LinkButton1" OnClick="Button1_Click" runat="server" class="btn btn-primary" ValidationGroup="validateCredential2">Rezervasyon Talebi Gönder</asp:LinkButton>
                    </p>
                </div>
            </div>


        </div>

    </div>
        </div>
          <span style="visibility: hidden" id="hiddenPrice">
                                <asp:Literal ID="ltrPrices2" runat="server"></asp:Literal>
                            </span>
        <input placeholder="Lütfen tarih seçiniz" type="text" id="input2" disabled="disabled" style="visibility: hidden">
    <input placeholder="Lütfen tarih seçiniz" type="text" id="input1" disabled="disabled" style="visibility: hidden">

    <script src="/assets/js/Adadijital_Calender2.js"></script>
    <link href="/assets/css/calender.css" rel="stylesheet" />
     

</asp:Content>
