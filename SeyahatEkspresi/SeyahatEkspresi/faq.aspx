﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="faq.aspx.cs" Inherits="SeyahatEkspresi.faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section>
        <div class="bannerimg cover-image bg-background3" data-image-src="/assets/img/banner2.jpg">
            <div class="header-text mb-0">
                <div class="container">
                    <div class="text-center text-white ">
                        <h1 class="">Sıkça Sorulan Sorular</h1>
                        <ol class="breadcrumb text-center">
                            <li class="breadcrumb-item"><a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>">Anasayfa</a></li>
                            <li class="breadcrumb-item active text-white" aria-current="page">S.S.S.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sptb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-lg-0">
                        <div class="card-header">
                            <h3 class="card-title">Sıkça Sorulan Sorular</h3>
                        </div>
                        <div class="card-body">
                            <div class="panel-group1" id="accordion2">
                                <asp:Repeater ID="rptSSS" runat="server">
                                    <ItemTemplate>
                                        <div class="panel panel-default mb-4">
                                            <div class="panel-heading1 ">
                                                <h4 class="panel-title1"><a class="accordion-toggle border collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse<%#Eval("sss_id") %>" aria-expanded="false"><%#Eval("sss_title") %></a> </h4>
                                            </div>
                                            <div id="collapse<%#Eval("sss_id") %>" class="panel-collapse collapse" role="tabpanel" aria-expanded="false">
                                                <div class="panel-body border-top-0 text-justify">
                                                    <p><%#Eval("sss_desc") %></p>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
