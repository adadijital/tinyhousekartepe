//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeyahatEkspresi
{
    using System;
    using System.Collections.Generic;
    
    public partial class member_login_logs
    {
        public long id { get; set; }
        public int member_id { get; set; }
        public System.DateTime login_time { get; set; }
        public string ip_address { get; set; }
    
        public virtual members members { get; set; }
    }
}
