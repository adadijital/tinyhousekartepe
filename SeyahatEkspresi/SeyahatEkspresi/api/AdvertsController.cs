﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SeyahatEkspresi.api
{
    public class AdvertsController : ApiController
    {
        Base _b = new Base();
        AjaxResult _result = new AjaxResult();
        AdvertManager _advertManager = new AdvertManager();

        public IHttpActionResult GetAdvertsShowcase()
        {
            try
            {
                var _showCase = _advertManager.getLastAdvertShowcase();

                if (_showCase.Count() > 0)
                {
                    _result.Status = true;
                    _result.Data = _showCase;
                    _result.Message = "OK";
                }
                else
                {
                    _result.Message = "Sonuç bulunamadı";
                    _result.Status = false;
                }

                return Ok(_result);
            }
            catch (Exception ex)
            {
                _result.Status = false;
                _result.Message = ex.Message;
                return Ok(_result);
            }
        }

        public IHttpActionResult GetLastAdverts()
        {
            try
            {
                var _showCase = _advertManager.getLastAdvertRandom();

                if (_showCase.Count() > 0)
                {
                    _result.Status = true;
                    _result.Data = _showCase;
                    _result.Message = "OK";
                }
                else
                {
                    _result.Message = "Sonuç bulunamadı";
                    _result.Status = false;
                }

                return Ok(_result);
            }
            catch (Exception ex)
            {
                _result.Status = false;
                _result.Message = ex.Message;
                return Ok(_result);
            }
        }


        public IHttpActionResult GetHomeParentCategories()
        {
            try
            {
                List<CategoryModelData> _newList = new List<CategoryModelData>();

                var _parentCat = _b.db.categories_lang.Where(q => q.categories.is_active && q.lang_id == Localization.Language && q.categories.parent_id == 0 && q.categories.icon_path != null && q.categories.is_deleted == false).Select(s => new
                {
                    s.category_id,
                    s.category_name,
                    s.categories.icon_path,
                    s.categories.display_order,

                }).OrderBy(o => o.display_order).ToList();

                foreach (var item in _parentCat)
                {
                    _newList.Add(new CategoryModelData
                    {
                        category_id = item.category_id,
                        category_name = item.category_name,
                        icon_path = item.icon_path,
                        display_order = item.display_order,

                        url = "/" + Localization.LanguageStr + "/ilanlar-" + item.category_name.ToURL() + "-c-" + item.category_id,
                    });


                }

                if (_parentCat.Count() > 0)
                {
                    _result.Status = true;
                    _result.Data = _newList;
                    _result.Message = "OK";
                }
                else
                {
                    _result.Message = "Sonuç bulunamadı";
                    _result.Status = false;
                }

                return Ok(_result);
            }
            catch (Exception ex)
            {
                _result.Status = false;
                _result.Message = ex.Message;
                return Ok(_result);
            }
        }

        public class CategoryModelData
        {
            public int category_id { get; set; }
            public int display_order { get; set; }
            public string category_name { get; set; }
            public string icon_path { get; set; }
            public string url { get; set; }

        }


            public class AjaxResult
        {
            [DefaultValue(false)]
            public bool Status { get; set; }
            [DefaultValue(0)]
            public object Data { get; set; }
            [DefaultValue("İşlem yürütülemedi.")]
            public string Message { get; set; }
            public string MessageDetail { get; set; }

        }

    }
}