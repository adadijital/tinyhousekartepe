﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SeyahatEkspresi._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
        <link href="/assets/css/datetimepicker.css" rel="stylesheet" />
    <!--Slider-->
    <div class="outher-container">
        <div class="container-cover">
            <%-- <div class="spspsp">--%>

            <%--<div class="first-swiper-content-area">--%>
            <div class="swiper mySwiper myFirstSwiper">
                <div class="swiper-wrapper first-swiper-swiper-wrapper">
                    <asp:Repeater ID="rptSlider" runat="server">
                        <ItemTemplate>
                            <div class="swiper-slide  mySlider">
                                <!--<div class="swiper-desc">
                                    <div class="spsp-inner">
                                        <div class="main-area-search-bar-cover">
                                            <div class="main-area-search-bar-cover-header">
                                                <h1 class="main-area-title"><%#Eval("slider_title") %>
                                                    <%#Eval("slider_desc") %>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="swiper-img-cover main-slider-img">
                                    <%--<img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/slider/<%#Eval("img_path") %>" alt="img" class="swiper-slide-img" />--%>
                                    <img src="/assets/img/tinybg.png" />
                                    <div class="slider-header-desc">
                                        <h1>Bungalov ve villalarda uygun 
                                            konaklama seçenekleri</h1>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>
                            <div class="swiper-pagination swiper-first-peg"></div>

            </div>
            <%--                </div>--%>
            <%-- </div>--%>
        </div>

        <div class="main-area-search-bar">
            <div class="main-area-search-bar-content">
                <div class="main-area-search-bar-content-inner">
                    <p>Lokasyon</p>
                    <label for="serachbar"><i class="fas fa-map-marker-alt"></i></label>

                    <asp:TextBox ID="txtMainSearch" runat="server" CssClass="txtMainSearch" placeholder="Şehir veya ev adı girin" autocomplete="off"></asp:TextBox>

                            <div class="shortSearch mobil-hide">
                        <ul>
                            <asp:Repeater ID="rptSearchModelList" runat="server">
                                <ItemTemplate>
                                
                                        <li><%#Eval("icon") %> 
                                            <a href="javascript:void(0)" class="keyValue">
                                                 <%#Eval("value") %>
                                            </a>
                                           

                                        </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        
                              
                             </ul>
                    </div>
                    <%-- <select name="serachbar" id="serachbar">
                        <option value="sapanca">Sapanca</option>
                    </select>--%>
                </div>
            </div>
            <div class="main-area-search-bar-content">
                <div class="main-area-search-bar-content-inner startDate" id="jrange">
                    <p>Giriş Tarihi</p>
                    <label for="serachbar"><i class="far fa-calendar-alt"></i></label>
                    <input type="text" placeholder="Tarih seçiniz" />
                    <div></div>
                </div>
              
               

            </div>
              <div class="main-area-search-bar-content">
                <div class="main-area-search-bar-content-inner finishDate" id="jrange">
                    <p>Giriş Tarihi</p>
                    <label for="serachbar"><i class="far fa-calendar-alt"></i></label>
                    <input type="text" placeholder="Tarih seçiniz" />
                    <div></div>
                </div>
              
               

            </div>


            <div class="main-area-search-bar-content">
                <div class="main-area-search-bar-content-inner last-kisi">
                    <p>Misafir</p>
                    <label for="serachbar"><i class="fas fa-user"></i></label>
                    <asp:DropDownList ID="drpPersonCount" runat="server">
                        <asp:ListItem Text="1 Kişi" Value="1" />
                        <asp:ListItem Text="2 Kişi" Value="2" />
                        <asp:ListItem Text="3 Kişi" Value="3" />
                        <asp:ListItem Text="4 Kişi" Value="4" />
                        <asp:ListItem Text="5 Kişi" Value="5" />
                        <asp:ListItem Text="6 Kişi" Value="6" />
                        <asp:ListItem Text="7 Kişi" Value="7" />
                        <asp:ListItem Text="8 Kişi" Value="8" />
                        <asp:ListItem Text="9 Kişi" Value="9" />
                        <asp:ListItem Text="10 Kişi" Value="10" />
                    </asp:DropDownList>
                    <%--      <select name="serachbar" id="serachbar">
                        <option value="birkisi">1 Kişi</option>
                    </select>--%>
                </div>
            </div>


            <div class="main-area-search-bar-content-last">
                <%--<a href="#">Ara</a>--%>
                <asp:LinkButton runat="server" ID="LinkButton1" CssClass="btn btn-block" OnClick="btnSearch_Click">
                                     Ara
                </asp:LinkButton>
            </div>

        </div>
        
            
            
        <div class="main-area-search-bar-mobil">
            <div class="main-area-search-bar-content mobil-bar">
                <div class="main-area-search-bar-content-inner mobil-search">
                    <p>Lokasyon</p>
                    <div class="label-mob-cover">
                        <label for="serachbar"><i class="fas fa-map-marker-alt"></i></label>
                   <asp:TextBox ID="txtMainSearch2" runat="server" CssClass="txtMainSearch mobiltxtmainsearch" placeholder="Şehir veya ev adı girin" autocomplete="off"></asp:TextBox>

                            <div class="shortSearch mobil-show">
                        <ul>
                            <asp:Repeater ID="rptSearchModelList2" runat="server">
                                <ItemTemplate>
                                
                                        <li><%#Eval("icon") %> 
                                            <a href="javascript:void(0)" class="keyValue">
                                                 <%#Eval("value") %>
                                            </a>
                                           

                                        </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        
                              
                             </ul>
                    </div>
                    </div>
                </div>
                <div class="main-area-search-bar-content-inner mobil mobil-search startDate">
                    <p>Giriş Tarihi</p>
                    <div class="label-mob-cover" id="jrange2">
                        <label for="serachbar"><i class="far fa-calendar-alt"></i></label>
                        <input type="text" placeholder="Tarih seçiniz" readonly="readonly"/>
                        <div></div>
                    </div>
                </div>

                   <div class="main-area-search-bar-content-inner mobil mobil-search finishDate">
                    <p>Çıkış Tarihi</p>
                    <div class="label-mob-cover" id="jrange3">
                        <label for="serachbar"><i class="far fa-calendar-alt"></i></label>
                        <input type="text" placeholder="Tarih seçiniz" readonly="readonly" class="mobiltxtmainsearch"/>
                        <div></div>
                    </div>
                </div>

                <div class="main-area-search-bar-content-inner mobil mobil-search mobil-search-l">
                    <div class="mobil-bar-last-text">
                        <p>Misafir</p>
                        <div class="label-mob-cover">
                            <label for="serachbar"><i class="fas fa-user"></i></label>
                            <asp:DropDownList ID="drpPersonCount2" runat="server">
                                <asp:ListItem Text="1 Kişi" Value="1" />
                                <asp:ListItem Text="2 Kişi" Value="2" />
                                <asp:ListItem Text="3 Kişi" Value="3" />
                                <asp:ListItem Text="4 Kişi" Value="4" />
                                <asp:ListItem Text="5 Kişi" Value="5" />
                                <asp:ListItem Text="6 Kişi" Value="6" />
                                <asp:ListItem Text="7 Kişi" Value="7" />
                                <asp:ListItem Text="8 Kişi" Value="8" />
                                <asp:ListItem Text="9 Kişi" Value="9" />
                                <asp:ListItem Text="10 Kişi" Value="10" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="main-area-search-bar-content-last mobil-search">
                        <%--<a href="#">Ara</a>--%>
                        <asp:LinkButton runat="server" ID="LinkButton22" OnClick="LinkButton22_Click">
                                     Ara
                        </asp:LinkButton>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="swiper mySwiper myFirstSwiper">
                    <div class="swiper-wrapper">
                        <asp:Repeater ID="rptSlider2" runat="server">
                            <ItemTemplate>
                                <div class="swiper-slide ">
                                    <div class="swiper-img-cover">
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/slider/<%#Eval("img_path") %>" alt="img" class="swiper-slide-img">
                                    </div>
                                    <h1><%#Eval("slider_title") %>
                                        <br />
                                        <%#Eval("slider_desc") %>
                                    </h1>

                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="main-area-search-bar">
                    <div class="main-area-search-bar-content">
                        <div class="main-area-search-bar-content-inner">
                            <p>Lokasyon</p>
                            <label for="serachbar"><i class="fas fa-map-marker-alt"></i></label>
                            <select name="serachbar" id="serachbar">
                                <option value="sapanca">Sapanca</option>
                            </select>
                        </div>
                    </div>
                    <div class="main-area-search-bar-content">
                        <div class="main-area-search-bar-content-inner">
                            <p>Giriş-Çıkış</p>
                            <label for="serachbar"><i class="far fa-calendar-alt"></i></i></label>
                            <select name="serachbar" id="serachbar">
                                <option value="tarihsec">Tarih Seçiniz</option>
                            </select>
                        </div>
                    </div>
                    <div class="main-area-search-bar-content">
                        <div class="main-area-search-bar-content-inner">
                            <p>Misafir</p>
                            <label for="serachbar"><i class="fas fa-user"></i></i></label>
                            <select name="serachbar" id="serachbar">
                                <option value="birkisi">1 Kişi</option>
                            </select>
                        </div>
                    </div>
                    <div class="main-area-search-bar-content-last">
                        <%--<a href="#">Ara</a>--%>
                        <asp:LinkButton runat="server" ID="LinkButton2" CssClass="btn btn-block  " OnClick="btnSearch_Click">
                                     Ara
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!--vue deneme-->
        <div >
    <div class="outher-container bg-white pb-5 pt-6" >
        <div class="container  margin-bottom-clearfix">
            <div class="row row-mar  slider-header mb-4">
                <div class="col-lg-12">
                    <div class="swiper-top-inner">
                        <h1>Fırsatlar</h1>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar" class="mobil-hide">Tüm Konaklamalar</a>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar" class="mobil-show">Tümü</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 button-rev">
                    <div class="swiper mySwiper konakswiper mykonakSwiper">
                        <div class="swiper-wrapper" id="pnlShowCase" >
                              <div class="swiper-slide blog-slide" v-for="(item,index) in showCaseResult"  :key="index">
                                      
                                        <div class="konak-content-cover">
                                            <div class="konak-content-top">
                                                <a :href="item.url">
                                                <div class="konak-content-top-img" v-bind:style="{ backgroundImage: 'url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.first_image  + ')' }" 
                                                    :second-image="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.second_image + ''"
                                                    :first-image="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.first_image + ''"
                                                    ></div>
                                                    </a>
                                                <p>
                                                    <a :href="item.url">
                                                          {{item.advert_name }} 
                                                    </a>
                                                </p>
                                                <div class="hover-share non-border">
                                                    <a href="javascript:void(0)" class="shareButon">
                                                        <img class="sharesvg" src="/assets/img/sharasvg.svg"/>
                                                        <%--<i class="fas fa-share-alt"></i>--%>
                                                        <div class="card-share-content">
                                                        
                                                            <div class="addthis_inline_share_toolbox" :data-description="item.advert_name" :data-title="item.advert_name" :data-url="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>'+item.url" :data-media="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.first_image + ''"></div>
                                                    
                                                        </div>
                                                    </a>

                                                </div>
                                            </div>
                                            <div class="konak-content-bottom">
                                                <div class="konak-content-bottom-topinside">
                                                    <p>        {{ item.kisi_sayisi }} Kişi</p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                       {{ item.yatak_sayisi }} Yatak
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                       {{ item.banyo_sayisi }} Banyo
                                                    </p>
                                                </div>
                                                <div class="konak-content-bottom-topinside second">
                                                    <p>
                                                       {{ item.konum_durumu }}
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 18.svg">

                                                        {{ item.isinma_turu }}
                                                    </p>
                                                </div>
                                              <p class="eskifiyat" v-if="item.market_price > 0">
                                                       <%--<i class="fa fa-try"></i>--%>
                                                  <img class="tlicon" src="/assets/img/tliconeski.svg" />
                                                  {{ item.market_price }}
                                                    </p>
                                                    <p class="" v-else>
                                                        &nbsp;
                                                    </p>
                                                
                                               
                                                <div class="guncelfiyat">
                                                    <div class="guncelfiyat-inner">
                                                        <p class="card-sp-p">
                                                           <%-- <i class="fa fa-try" aria-hidden="true"></i>   --%> 
                                                            <img class="tlicon" src="/assets/img/tlicon.svg" />
                                                            {{ item.sell_price }} 
                                                            <span>/ 1 gece</span> 

                                                        </p>
                                                    </div>
                                                    <div class="guncelfiyat-inner">
                                                  
                                                
                                                      
                                                        <a class="mobil-hide" :href="item.url">Rezervasyon <i class="fas fa-arrow-right"></i></a>
                                                        <a class="mobil-show" :href="item.url"><i class="fas fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                         </div>
                                   
                            
                        </div>
                    </div>
                  

                    <div class="swiper-button-cover1">
                        <div class="second-swiper-arrows-button1">
                            <div class="swiper-second-prev1 "><i class="fas fa-arrow-left"></i></div>
                            <div class="swiper-second-next1 "><i class="fas fa-arrow-right"></i></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
            </div>
 <!--   <div class="outher-container bg-white pb-5 pt-6 " runat="server" id="pnlShowCase" visible="false">
        <div class="container  margin-bottom-clearfix">
            <div class="row row-mar  slider-header mb-4">
                <div class="col-lg-12">
                    <div class="swiper-top-inner">
                        <h1>Fırsatlar</h1>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar" class="mobil-hide">Tüm Konaklamalar</a>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar" class="mobil-show">Tümü</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 button-rev">
                    <div class="swiper mySwiper mykonakSwiper">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="rptShowCase" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide blog-slide">
                                        <div class="konak-content-cover">
                                            <div class="konak-content-top">
                                                <div class="konak-content-top-img" style="background-image: url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>)" second-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("second_image")%>" first-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>"></div>

                                                <p>
                                                    <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>">
                                                        <%#Eval("advert_name") %>
                                                    </a>


                                                </p>
                                                <div class="hover-share non-border">
                                                    <a href="javascript:void(0)" class="shareButon">
                                                        <i class="fas fa-share-alt"></i>
                                                        <div class="card-share-content">
                                                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61f2ea53a5c551d2"></script>
                                                            <div class="addthis_inline_share_toolbox"></div>
                                                        </div>
                                                    </a>

                                                </div>
                                            </div>
                                            <div class="konak-content-bottom">
                                                <div class="konak-content-bottom-topinside">
                                                    <p><%#Eval("kisi_sayisi") %> Kişi</p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                        <%#Eval("yatak_sayisi") %> Yatak
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                        <%#Eval("banyo_sayisi") %> Banyo
                                                    </p>
                                                </div>
                                                <div class="konak-content-bottom-topinside second">
                                                    <p>
                                                        <%#Eval("konum_durumu") %>
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 18.svg">

                                                        <%#Eval("isinma_turu") %>
                                                    </p>
                                                </div>
                                                <p class="eskifiyat <%# Convert.ToInt32(Eval("market_price")) == 0 ? "hide" : "" %>"><i class="fa fa-try"></i><%# Eval("market_price", "{0:0,00}") %>  </p>
                                                <div class="guncelfiyat">
                                                    <div class="guncelfiyat-inner">
                                                        <p class="card-sp-p"><i class="fa fa-try" aria-hidden="true"></i><%# Eval("sell_price", "{0:0,00}") %> <span>/ 1 gece</span>  </p>
                                                    </div>
                                                    <div class="guncelfiyat-inner">
                                                        <a class="mobil-hide" href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>">Rezervasyon <i class="fas fa-arrow-right"></i></a>
                                                        <a class="mobil-show" href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>"><i class="fas fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="swiper-button-cover1">
                        <div class="second-swiper-arrows-button1">
                            <div class="swiper-second-prev1 "><i class="fas fa-arrow-left"></i></div>
                            <div class="swiper-second-next1 "><i class="fas fa-arrow-right"></i></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>-->


    <div class="outher-container pb-5 pt-5 ">
        <div class="container-fluid margin-bottom-clearfix">
            <div class="row  slider-header mb-4">
                <div class="col-lg-12">
                    <div class="swiper-top-inner1">
                        <h1>Nasıl Bir yere Gidelim?
                            
                        </h1>
                        <div class="swiper-button-cover">
                            <div class="second-swiper-arrows-button">
                                <div class="swiper-second-prev "><i class="fas fa-arrow-left"></i></div>
                                <div class="swiper-second-next "><i class="fas fa-arrow-right"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="swiper mySwiper CategorySwiper">
                        
                        <div class="swiper-wrapper" id="pnlHomeParentCategory">

                                    <div class="swiper-slide location-slide" v-for="(item,index) in homeParentCategorResult"  :key="index"> 
                                              <a :href="item.url">
                                        <div class="swiper-img-cover swiper-scale2">
                                            <%--{{seo_url}}--%> 
                                      
                                                <img class="swiper-slide-img cat-slide-img" :src="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/category/M/'+item.icon_path">
                                                <%--<div class="swiper-slide-img"></div>--%>
                                         
                                      
                                            <p class="cat-p">
                                             
                                               {{item.category_name}}
                                              
                                            </p>
                                        </div>
                                                  </a>
                                    </div>

                           <!-- <asp:Repeater ID="rptParentCategory" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide location-slide">
                                        <div class="swiper-img-cover swiper-scale">
                                            <a href="<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/ilanlar-<%#SeyahatEkspresi.cExtensions.ToURL(Eval("category_name").ToString()) %>-c-<%#Eval("category_id") %>">
                                                <img class="swiper-slide-img" src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/category/M/<%#Eval("icon_path") %>">
                                                <%--<div class="swiper-slide-img"></div>--%>
                                         
                                            </a>
                                            <p class="cat-p">
                                                <a href="<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/ilanlar-<%#SeyahatEkspresi.cExtensions.ToURL(Eval("category_name").ToString()) %>-c-<%#Eval("category_id") %>">
                                                    <%#Eval("category_name") %> 
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- burada kaldım-->


    <div class="outher-container bg-white pb-5 pt-5">
        <div class="container margin-bottom-clearfix">
            <div class="row row-mar slider-header mb-5">
                <div class="col-lg-12">
                    <div class="swiper-top-inner">
                        <h1>En Çok Tercih Edilenler</h1>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar" class="mobil-hide">Tüm Konaklamalar</a>
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/ilanlar" class="mobil-show">Tümü</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 button-rev">
                    <div class="swiper mySwiper konakswiper mykonakSwiper">
                              <div class="swiper-wrapper" id="pnlLastAdverts" >
                              <div class="swiper-slide blog-slide" v-for="(item,index) in lastAdvertsResult"  :key="index">
                                      
                                        <div class="konak-content-cover">
                                            <div class="konak-content-top">
                                                <a :href="item.url">
                                                <div class="konak-content-top-img" v-bind:style="{ backgroundImage: 'url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.first_image  + ')' }" 
                                                    :second-image="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.second_image + ''"
                                                    :first-image="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.first_image + ''"
                                                    ></div>
                                                    </a>
                                                <p>
                                                    <a :href="item.url">
                                                          {{item.advert_name }} 
                                                    </a>
                                                </p>
                                                <div class="hover-share non-border">
                                                    <a href="javascript:void(0)" class="shareButon">
                                                        <img class="sharesvg" src="/assets/img/sharasvg.svg"/>
                                                        <%--<i class="fas fa-share-alt"></i>--%>
                                                        <div class="card-share-content">
                                                        
                                                            <div class="addthis_inline_share_toolbox" :data-description="item.advert_name" :data-title="item.advert_name" :data-url="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>'+item.url" :data-media="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/' + item.first_image + ''"></div>
                                                    
                                                        </div>
                                                    </a>

                                                </div>
                                            </div>
                                            <div class="konak-content-bottom">
                                                <div class="konak-content-bottom-topinside">
                                                    <p>        {{ item.kisi_sayisi }} Kişi</p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                       {{ item.yatak_sayisi }} Yatak
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                       {{ item.banyo_sayisi }} Banyo
                                                    </p>
                                                </div>
                                                <div class="konak-content-bottom-topinside second">
                                                    <p>
                                                       {{ item.konum_durumu }}
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 18.svg">

                                                        {{ item.isinma_turu }}
                                                    </p>
                                                </div>
                                              <p class="eskifiyat" v-if="item.market_price > 0">
                                                      <%-- <i class="fa fa-try"></i>--%>
                                                  <img class="tlicon" src="/assets/img/tliconeski.svg" />
                                                  {{ item.market_price }}
                                                    </p>
                                                    <p class="" v-else>
                                                        &nbsp;
                                                    </p>
                                                
                                               
                                                <div class="guncelfiyat">
                                                    <div class="guncelfiyat-inner">
                                                        <p class="card-sp-p"><%--<i class="fa fa-try" aria-hidden="true"></i> --%> 
                                                            <img class="tlicon" src="/assets/img/tlicon.svg" />
                                                            {{ item.sell_price }} <span>/ 1 gece</span>  </p>
                                                    </div>
                                                    <div class="guncelfiyat-inner">
                                                  
                                                
                                                      
                                                        <a class="mobil-hide" :href="item.url">Rezervasyon <i class="fas fa-arrow-right"></i></a>
                                                        <a class="mobil-show" :href="item.url"><i class="fas fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                         </div>
                                   
                            
                        </div>

                     <!--   <div class="swiper-wrapper">
                            <asp:Repeater ID="rptRandom" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide blog-slide">
                                        <div class="konak-content-cover">
                                            <div class="konak-content-top">
                                                <div class="konak-content-top-img" style="background-image: url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>)" second-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("second_image")%>" first-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>"></div>

                                                <p>
                                                    <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>">
                                                        <%#Eval("advert_name") %>
                                                    </a>


                                                </p>
                                                <div class="hover-share non-border">
                                                    <a href="javascript:void(0)" class="shareButon">
                                                        <i class="fas fa-share-alt"></i>
                                                        <div class="card-share-content">
                                                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61f2ea53a5c551d2"></script>
                                                            <div class="addthis_inline_share_toolbox"></div>
                                                        </div>
                                                    </a>

                                                </div>
                                            </div>
                                            <div class="konak-content-bottom">
                                                <div class="konak-content-bottom-topinside">
                                                    <p><%#Eval("kisi_sayisi") %> Kişi</p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                        <%#Eval("yatak_sayisi") %> Yatak
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                                        <%#Eval("banyo_sayisi") %> Banyo
                                                    </p>
                                                </div>
                                                <div class="konak-content-bottom-topinside second">
                                                    <p>
                                                        <%#Eval("konum_durumu") %>
                                                    </p>
                                                    <p>
                                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 18.svg">

                                                        <%#Eval("isinma_turu") %>
                                                    </p>
                                                </div>
                                                <p class="eskifiyat <%# Convert.ToInt32(Eval("market_price")) == 0 ? "hide" : "" %>"><i class="fa fa-try"></i><%# Eval("market_price", "{0:0,00}") %>  </p>
                                                <div class="guncelfiyat">
                                                    <div class="guncelfiyat-inner">
                                                        <p class="card-sp-p"><i class="fa fa-try" aria-hidden="true"></i><%# Eval("sell_price", "{0:0,00}") %> <span>/ 1 gece</span>  </p>
                                                    </div>
                                                    <div class="guncelfiyat-inner">
                                                        <%--<a href="#">Rezervasyon  </a>--%>
                                                        <a class="mobil-hide" href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>">Rezervasyon <i class="fas fa-arrow-right"></i></a>
                                                        <a class="mobil-show" href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>"><i class="fas fa-arrow-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>-->
                    </div>

                    <div class="swiper-button-cover1">
                        <div class="second-swiper-arrows-button1">
                            <div class="swiper-second-prev1 "><i class="fas fa-arrow-left"></i></div>
                            <div class="swiper-second-next1 "><i class="fas fa-arrow-right"></i></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="outher-container pb-5 pt-5">
        <div class="container margin-bottom-clearfix">
            <div class="row row-mar slider-header mb-5">
                <div class="col-lg-12">
                    <div class="swiper-top-inner">
                        <h1>Lokasyonlar</h1>
                        <div class="swiper-button-cover">
                            <div class="second-swiper-arrows-button">
                                <div class="swiper-second-prev "><i class="fas fa-arrow-left"></i></div>
                                <div class="swiper-second-next "><i class="fas fa-arrow-right"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-lg-12">
                    <div class=" mobil-hide swiper  mySwiper myThirdSwiper">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="rptDistrict" runat="server">
                                <ItemTemplate>
                                    <div class="<%# Container.ItemIndex == 1 || Container.ItemIndex == 2 ? "swiper-slide-center swiper-slide swiper-rotate-cover" : "swiper-slide swiper-rotate-cover" %>">


                                        <div class="pb-4">
                                            <div class="swiper-img-cover swiper-img-mgl swiper-rotate">
                                                <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/district/M/<%#Eval("img_path") %>" alt="img">
                                                <p><%#Eval("town_name") %></p>
                                            </div>
                                        </div>



                                    </div>

                                </ItemTemplate>
                            </asp:Repeater>




                        </div>


                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-lg-12">
                    <div class=" mobil-show swiper mySwiper DistrictSwiper">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="rptDistrict2" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide-center2 swiper-slide DistrictSwiper-slide swiper-rotate-cover">
                                        <div class="pb-4">
                                            <div class="swiper-img-cover swiper-rotate">
                                                <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/district/M/<%#Eval("img_path") %>" alt="img">
                                                <p><%#Eval("town_name") %></p>
                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:Repeater>




                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>

    <link href="/assets/css/homepageCalender.css" rel="stylesheet" />
       <script type="module" src="/assets/js/Vue_Adadijital.js"></script>
</asp:Content>
