﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="mesafeliSatisSozlesmesi.aspx.cs" Inherits="SeyahatEkspresi.mesafeliSatisSozlesmesi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">
                        <p>
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                            <asp:Literal ID="ltrPagename" runat="server"></asp:Literal>
                        </p>
                        <h1>
                            <asp:Literal ID="ltrPagename2" runat="server"></asp:Literal></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="kiralama mt-5 mb-5 ">

                    <h1>
                        <asp:Literal ID="ltrPageTitle" runat="server"></asp:Literal></h1>
                    <p>
                        <asp:Literal ID="ltrPageDesc" runat="server"></asp:Literal></p>
                </div>
            </div>
        </div>
    </div>




</asp:Content>
