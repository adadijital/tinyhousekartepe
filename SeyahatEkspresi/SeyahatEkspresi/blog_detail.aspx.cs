﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class blog_detail : Base
    {
        int blog_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["blog_id"] != null)
                {

                    blog_id = Request.QueryString["blog_id"].toInt();

                    var _detail = db.blog_lang.Where(q => q.lang_id == Localization.Language && q.blog_id == blog_id).Select(s => new
                    {

                        s.blog_id,
                        s.blog.blog_category.blog_category_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,
                        s.blog.created_time,
                        s.blog_title,
                        s.blog_desc,
                        s.blog.image_path,
                        s.blog.view_count,
                    }).FirstOrDefault();

                    if (_detail != null)
                    {
                        ltrHeaderTitle.Text = _detail.blog_title;
                        ltrHeaderTitle2.Text = _detail.blog_title;
                        Image1.ImageUrl = "/uploads/blog/" + _detail.image_path;
                        Image1.AlternateText = _detail.blog_title;

                        //string day =String.Format( "{dddd}", _detail.created_time.ToString());
                        ltrDateDay.Text = _detail.created_time.Day.ToString();
                        ltrMonth.Text = _detail.created_time.ToString("MMMM");
                        ltrView.Text = _detail.view_count.ToString();
                        //ltrView2.Text = _detail.view_count.ToString();
                        ltrCategory.Text = _detail.category_name;
                        ltrTitle.Text = _detail.blog_title;
                        ltrDesc.Text = _detail.blog_desc;

                        Page.Title = System.Configuration.ConfigurationManager.AppSettings["site_name"].ToString()+" | "+ _detail.blog_title;

                  

                        //hypFacebook.NavigateUrl = "javascript:window.open('http://www.facebook.com/sharer.php?u=" + Request.Url.Host + Request.RawUrl + "');";
                        //hypTwitter.NavigateUrl = "javascript:window.open('http://www.twitter.com/home/?status=" + Request.Url.Host + Request.RawUrl + "');";
                    }

                    //var _recent = db.blog_lang.Where(q => q.lang_id == Localization.Language).Select(s => new {
                    //    s.blog_id,
                    //    s.blog.blog_category.blog_category_lang.Where(q => q.lang_id == Localization.Language).FirstOrDefault().category_name,
                    //    s.blog.created_time,
                    //    s.blog_title,
                    //    s.blog_desc,
                    //    s.blog.image_path,
                    //    s.blog.view_count,
                    //}).OrderByDescending(o => o.view_count).Take(5).ToList();

                    //if (_recent != null)
                    //{
                    //    rptRecent.DataSource = _recent;
                    //    rptRecent.DataBind();

                    //}

                    //var _categoryList = db.blog_category_lang.Where(q => q.lang_id == Localization.Language && q.blog_category.is_active).ToList();

                    //if (_categoryList.Count() > 0)
                    //{
                    //    rptCategoryList.DataSource = _categoryList;
                    //    rptCategoryList.DataBind();
                    //}




                    viewedUpdate();
                }
                else
                    Response.Redirect("/" + Localization.LanguageStr, false);

            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        private void viewedUpdate()
        {
            try
            {

                HttpCookie viewCookie = Request.Cookies["viewed"+blog_id.ToString()];


                if (viewCookie == null) //popupCookie boş ise
                {
                    viewCookie = new HttpCookie("viewed"+ blog_id.ToString());//yeni bir Cookie oluştur
                    viewCookie.Expires = DateTime.Now.AddHours(24); //Cookie sonlanma süresini belirttim
                    Response.Cookies.Add(viewCookie);

                    var _detail = db.blog_lang.Where(q => q.lang_id == Localization.Language && q.blog_id == blog_id).FirstOrDefault();

                    _detail.blog.view_count = _detail.blog.view_count + 1;

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                //Response.Write(ex);
            }
        }
    }
}