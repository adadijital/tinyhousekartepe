﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="kullanimKosullari.aspx.cs" Inherits="SeyahatEkspresi.kullanimKosullari" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <section>
        <div class="bannerimg cover-image bg-background3" data-image-src="/assets/img/banner2.jpg">
            <div class="header-text mb-0">
                <div class="container ">
                    <div class="text-center text-white ">
                        <h1 class="">Kullanım Koşulları</h1>
                        <ol class="breadcrumb text-center">
                            <li class="breadcrumb-item"><a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>">Anasayfa</a></li>
                            <li class="breadcrumb-item active text-white" aria-current="page">Kurumsal</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sptb">
        <div class="container">
  <div class="text-justify">
                <h1 class="mb-4">
                    <asp:Literal ID="ltrHakkimizdaTitle" runat="server"></asp:Literal></h1>
                <asp:Literal ID="ltrHakkimizdaDesc" runat="server"></asp:Literal>
            </div>
        </div>
    </section>
    
</asp:Content>
