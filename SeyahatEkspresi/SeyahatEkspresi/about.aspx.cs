﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class about : Base
    {
        Email _mail = new Email();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _Sections = db.page_content.Where(q => q.page_id == 1 && q.lang_id == Localization.Language).ToList();

                if (_Sections.Count() > 0)
                {
                    ltrHakkimizdaTitle.Text = _Sections.FirstOrDefault(f => f.section_group == 1).section_name;
                    ltrHakkimizdaDesc.Text = _Sections.FirstOrDefault(f => f.section_group == 1).page_content_text;

                    //ltrNumber1.Text = _Sections.FirstOrDefault(f => f.section_group == 1).counter1_number;
                    //ltrNumber2.Text = _Sections.FirstOrDefault(f => f.section_group == 1).counter2_number;
                    //ltrNumber3.Text = _Sections.FirstOrDefault(f => f.section_group == 1).counter3_number;

                    //ltrNumber1Desc.Text = _Sections.FirstOrDefault(f => f.section_group == 1).counter1_desc;
                    //ltrNumber2Desc.Text = _Sections.FirstOrDefault(f => f.section_group == 1).counter2_desc;
                    //ltrNumber3Desc.Text = _Sections.FirstOrDefault(f => f.section_group == 1).counter3_desc;

                    //ltrContent2.Text = _Sections.FirstOrDefault(f => f.section_group == 1).content2;

                }

                var _ssLang = db.sss_content_lang.Where(q => q.lang_id == Localization.Language).OrderBy(o=> o.sss_content.display_order).ToList();

                if(_ssLang.Count()>0)
                {
                    //rptSSS.DataSource = _ssLang;
                    //rptSSS.DataBind();
                }

            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void btnGonder_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (!string.IsNullOrEmpty(txtNameSurname.Text.Trim()))
            //    {
            //        if (!string.IsNullOrEmpty(txtEmail.Text.Trim()))
            //        {
            //            if (txtEmail.Text.Trim().IsEmail())
            //            {

            //                if (!string.IsNullOrEmpty(txtMessage.Text.Trim()))
            //                {
            //                    string mailContent = string.Empty;
            //                    mailContent += "Ad Soyad : " + txtNameSurname.Text.Trim() + "<br/>";
            //                    mailContent += "E-Posta : " + txtEmail.Text.Trim() + "<br/>";
            //                    mailContent += "Mesaj : " + txtMessage.Text.Trim() + "<br/> Bu mail " + System.Configuration.ConfigurationManager.AppSettings["site_name"] + " adresinden gönderildi";

            //                    Base _b = new Base();
            //                    var _setting = _b.db.mail_setting.FirstOrDefault();

            //                    _mail.To = _setting.receive_mail;
            //                    _mail.Message = mailContent;
            //                    _mail.SendAdmin = false;
            //                    _mail.Send();

            //                    txtNameSurname.Text = "";
            //                    txtEmail.Text = "";

            //                    txtMessage.Text = "";

            //                    showWarningBasic(this.Master, "Mesajınız gönderildi", "Tebrikler");
            //                }
            //                else
            //                    showWarningBasic(this.Master, "Lütfen mesajınızı girin", "Dikkat");

            //            }
            //            else
            //                showWarningBasic(this.Master, "Lütfen geçerli bir  e-mail girin", "Dikkat");
            //        }
            //        else
            //            showWarningBasic(this.Master, "Lütfen e-mail girin", "Dikkat");
            //    }
            //    else
            //        showWarningBasic(this.Master, "Lütfen adınızı girin", "Dikkat");
            //}
            //catch (Exception ex)
            //{
            //    Log.Add(ex);
            //}
        }
    }
}