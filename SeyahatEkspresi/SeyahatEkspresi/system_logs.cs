//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeyahatEkspresi
{
    using System;
    using System.Collections.Generic;
    
    public partial class system_logs
    {
        public long id { get; set; }
        public int member_id { get; set; }
        public string message { get; set; }
        public string page { get; set; }
        public string source { get; set; }
        public string stack_trace { get; set; }
        public System.DateTime created_time { get; set; }
        public string ip { get; set; }
    }
}
