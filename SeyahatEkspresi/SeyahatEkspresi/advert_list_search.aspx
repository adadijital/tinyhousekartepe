﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="advert_list_search.aspx.cs" Inherits="SeyahatEkspresi.advert_list_search" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">
                        <p>
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                         <asp:Literal ID="ltrCategoryName" runat="server"></asp:Literal>
                        </p>
                        <h1>
                            <asp:Literal ID="ltrCategoryName2" runat="server"></asp:Literal>
                        </h1>

                    </div>
                </div>
            </div>
        </div>
    </div>

      <div class="container mb-5 mt-5">
        <div class="row row-mar1 slider-header mb-5">
            <div class="col-lg-12">
                <div class="swiper-top-inner row-mar ">
                    <h1>Arama Sonuçları</h1>
              
                </div>
            </div>
        </div>
        <div class="row row-mar">
            <asp:Repeater ID="rptAdvertList" runat="server">
                <ItemTemplate>
               <div class="col-lg-4 konak-content-cover-mob">
                        <div class="konak-content-cover item-list-cover">
                            <div class="konak-content-top">
                                <div class="konak-content-top-img" style="background-image: url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>)" second-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("second_image")%>" first-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>"></div>

                                <p>
                                    <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>">
                                        <%#Eval("advert_name") %>
                                    </a>


                                </p>

                                <div class="hover-share non-border">
                                    <a href="javascript:void(0)" class="shareButon">
                                        <i class="fas fa-share-alt"></i>
                                        <div class="card-share-content">
                                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61f2ea53a5c551d2"></script>
                                            <div class="addthis_inline_share_toolbox"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="konak-content-bottom">
                                <div class="konak-content-bottom-topinside">
                                    <p><%#Eval("kisi_sayisi") %> Kişi</p>
                                    <p>
                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                        <%#Eval("yatak_sayisi") %> Yatak
                                    </p>
                                    <p>
                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 16.svg">
                                        <%#Eval("banyo_sayisi") %> Banyo
                                    </p>
                                </div>
                                <div class="konak-content-bottom-topinside second">
                                    <p>
                                        <%#Eval("konum_durumu") %>
                                    </p>
                                    <p>
                                        <img class="konak-detay-slider-dote" src="/assets/img/Ellipse 18.svg">

                                        <%#Eval("isinma_turu") %>
                                    </p>
                                </div>
                                <%--<p class="eskifiyat"> <i class="fas fa-lira-sign"></i>2400</p>--%>
                                <div class="guncelfiyat">
                                    <div class="guncelfiyat-inner">
                                        <p class="card-sp-p"><%--<i class="fa fa-try" aria-hidden="true"></i>--%>
                                            <img class="tlicon" src="/assets/img/tlicon.svg" />
                                            <%# Eval("sell_price", "{0:0,00}") %>  <span>/ 1 gece</span>  </p>
                                    </div>
                                    <div class="guncelfiyat-inner">
                                        <%--<a href="#">Rezervasyon  </a>--%>
                                        <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("advert_name").ToString()) %>-i-<%#Eval("advert_id") %>">Rezervasyon <i class="fas fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
   
</asp:Content>
