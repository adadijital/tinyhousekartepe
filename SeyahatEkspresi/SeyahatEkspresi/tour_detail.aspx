﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="tour_detail.aspx.cs" Inherits="SeyahatEkspresi.tour_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">
                        <p>
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                            <asp:Literal ID="ltrCatName" runat="server"></asp:Literal>
                        </p>
                        <h1>
                            <asp:Literal ID="ltrTourname" runat="server"></asp:Literal>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container mt-5 mb-5">
        <div class="row d-flex">
            <div class="col-6">
                <div class="konak-detay-slider-cover">
                    <div class="konak-detay-slider-left">
                        <div class="konak-detay-slider-left-item">
                            <a data-fancybox="gallery" runat="server" id="lnkFancy">
                                <asp:Image ID="Image2" runat="server" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="konak-detay-slider-right col-6">
                <asp:Repeater ID="rptImages" runat="server">
                    <ItemTemplate>

                        <div class="img-content col-4">
                            <div class="konak-detay-slider-item-inner-cover">
                                <div class="konak-detay-slider-item-inner ">
                                    <a href="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/tour_img/L/<%#Eval("img_path") %>" data-fancybox="gallery">
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/tour_img/M/<%#Eval("img_path") %>" alt="img">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>



    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="konak-detay-content-text-cover">
                    <div class="konak-detay-content-text-header">
                        <h1>
                            <asp:Literal ID="ltrTourname2" runat="server"></asp:Literal>
                        </h1>
                        <div class="header-share-btn">
                            <a href="javascrpt:void(0)" class="share-inline mobil-show"><i class="fas fa-share-alt"></i>Paylaş </a>
                        </div>
                    </div>
                    <div class="konak-detay-content-text-header-bottom">
                        <div>
                            <p>
                                <i class="fas fa-user-alt"></i>
                                <asp:Literal ID="ltrHotelStatus" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <i class="fas fa-bed"></i>
                                <asp:Literal ID="ltrPersonalCount" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <i class="fas fa-bath"></i>
                                <asp:Literal ID="ltrVehicle" runat="server"></asp:Literal>
                            </p>
                        </div>
                        <div>
                            <a href="javascrpt:void(0)" class="share-inline mobil-hide"><i class="fas fa-share-alt"></i>Paylaş </a>
                        </div>
                    </div>
                    <div class="col-md-12 share-content">
                        <div class="addthis_inline_share_toolbox"></div>
                    </div>
                    <div class="konak-detay-content-text">
                        <p>

                            <asp:Literal ID="ltrDesc" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <!--  <div class="konak-detay-list-cover">
                        <p>
                            <asp:Literal ID="ltrTourDay" runat="server"></asp:Literal>
                        </p>

                        <div class="konak-detay-list-item-cover ">
                            <div class="konak-detay-list-item ">
                                <asp:Repeater ID="rptDefinition" runat="server">
                                    <ItemTemplate>
                                        <div class="konak-detay-content-text tur-detay">
                                            <h1>1. Gün</h1>
                                            <asp:Literal ID="ltrTourDaydesc" runat="server"></asp:Literal>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>-->


                </div>
            </div>
            <div class="col-lg-4 tur-konak-fiyat-cover">
                <div class="konak-detay-fiyat-cover">
                    <div class="konak-deta-fiyat">
                        <div class="konak-detay-inner">
                            <p class="eskifiyat second-eskifiyat" runat="server" id="marketPrice" visible="False">
                                <i class="fa fa-try"></i>
                                <asp:Literal ID="ltrMarketPrice" runat="server"></asp:Literal>
                            </p>
                            <p class="konak-detay-guncel-fiyat">
                                <i class="fas fa fa-try"></i>
                                <asp:Literal ID="ltrSellPrice" runat="server"></asp:Literal>
                                <span>/ 1 gece </span>
                            </p>
                        </div>
                        <div class="second-detay-cover">
                            <div class="konak-detay-inner second-detay">
                                <p class="konak-page-fiyat-header">Giriş Tarihi </p>
                                <label class="konak-page-fiyat-date-input" >
                                    <i class="far fa-calendar-alt"></i>
                                    <input placeholder="Lütfen Tarih Giriniz" type="text" id="input1" class="datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="konak-detay-inner">
                            <p class="konak-page-fiyat-header">Çıkış Tarihi </p>
                            <label class="konak-page-fiyat-date-input">
                                <i class="far fa-calendar-alt"></i>
                                <input placeholder="Lütfen Tarih Giriniz" type="text" id="input2" class="datepicker1" disabled="disabled">
                            </label>
                        </div>
                        <div class="konak-detay-inner last-konak-inner">

                            <a href="javascript:void(0)" class="randevuButton2">Rezervasyon Yap</a>

                        </div>
                    </div>
                </div>
                <div class="konak-detay-info-cover">
                    <div class="konak-detay-info-content k-top">
                        <h1>Kolay ve hızlı
                            <br>
                            rezervasyon</h1>
                        <p>Ön ödemeyi yapın oteliniz sizin için ayrılsın. </p>
                    </div>
                    <div class="konak-detay-info-content k-bottom">
                        <div class="konak-detay-info-content-text">
                            <p>Toplam rezervasyon tutarınızın sadece <span>%50</span> ’sini ödeyerek rezervasyonunuzu tamamlayabilirsiniz.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row konak-detay-info-cover-mob">
            <div class="col-12 konak-detay-info-cover-mob">
                <div class="konak-detay-info-cover konak-detay-info-cover-mob">
                    <div class="konak-detay-info-content k-top">
                        <h1>Kolay ve hızlı
                            <br>
                            rezervasyon</h1>
                        <p>Ön ödemeyi yapın oteliniz sizin için ayrılsın. </p>
                    </div>
                    <div class="konak-detay-info-content k-bottom">
                        <div class="konak-detay-info-content-text">
                            <p>Toplam rezervasyon tutarınızın sadece <span>%50</span> ’sini ödeyerek rezervasyonunuzu tamamlayabilirsiniz.</p>
                        </div>
                        <div class="konak-detay-inner last-konak-inner">
                            <a href="javascript:void(0)" class="randevuButton">Rezervasyon Yap</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container mb-5">
        <div class="row  slider-header mb-5 mt-5">
            <div class="col-lg-12">
                <div class="swiper-top-inner">
                    <h1 class="tur-mobil-header">Benzer Turlar</h1>
                    <a href="#" class="mobil-hide">Tüm Turlar</a>
                    <a href="#" class="mobil-show">Tümü</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="swiper mySwiper myTourSwiper">
                    <div class="swiper-wrapper">
                        <asp:Repeater ID="rptOtherTour" runat="server">
                            <ItemTemplate>
                                <div class="swiper-slide">
                                    <div class="konak-content-cover">
                                        <div class="konak-content-top">
                                            <div class="konak-content-top-img tur" style="background-image: url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/tour_img/M/<%#Eval("first_image")%>)" second-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/tour_img/M/<%#Eval("second_image")%>" first-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/tour_img/M/<%#Eval("first_image")%>"></div>
                                            <p>
                                                <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("tour_name").ToString()) %>-t-<%#Eval("id") %>">
                                                    <%#Eval("tour_name")%>
                                                </a>
                                            </p>
                                      <%--      <div class="hover-share hover-border">
                                                <a href="#">
                                                    <i class="fas fa-share-alt"></i> 
                                                     Paylaş 
                                                            <div class="card-share-content"></div>
                                                </a>
                                            </div>--%>
                                        </div>
                                        <div class="konak-content-bottom">
                                            <p class="eskifiyat">
                                                <%--<i class="fas fa fa-try"></i><%# Eval("market_price", "{0:0,00}").Replace(".","") %>--%>
                                            </p>
                                            <div class="guncelfiyat">
                                                <div class="guncelfiyat-inner tour-inner">
                                                    <p class="tour-sp-p">
                                                        <%--<i class="fas fa fa-try"></i>--%>
                                                        <img class="tlicon" src="/assets/img/tlicon.svg" />
                                                        <%# Eval("sell_price", "{0:0,00}").Replace(".","") %>   <span class="tur-text">/ <%#Eval("tour_time") %> </span></p>
                                                </div>
                                                <div class="guncelfiyat-inner">
                                                    <a class="pt-1" href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("tour_name").ToString()) %>-t-<%#Eval("id") %>">Rezervasyon  <i class="fas fa-arrow-right"></i>

                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <div class="row  slider-header mb-5">
            <div class="col-lg-12">
                <div class="swiper-top-inner konak-detay-swiper-peg">
                    <div class="swiper-button-cover">
                        <div class="second-swiper-arrows-button">
                            <div class="swiper-second-prev "><i class="fas fa-arrow-left"></i></div>
                            <div class="swiper-second-next "><i class="fas fa-arrow-right"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

       

    <div class="randevuModal">

        <div class="modalInner">
            <h4 class="mb-3">
                <i class="fa fa-calendar-check-o mr-2" aria-hidden="true"></i>
                Rezervasyon isteği gönderip hemen yerini ayırt!</h4>
            <br />
            <br />
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 ">
                    <div class="form-group">
                        <label class="form-label text-dark">Giriş Tarihi</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtDate" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtDate" Enabled="False" CssClass="form-control date" autocomplete="off" placeholder="Giriş Tarihi" data-toggle="datetimepicker" data-target="#txtDate" />
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 ">
                    <div class="form-group">
                        <label class="form-label text-dark">Çıkış Tarihi</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDate2" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtDate2" CssClass="form-control date" Enabled="False" autocomplete="off" placeholder="Çıkış Tarihi" data-toggle="datetimepicker" data-target="#txtDate" />

                    </div>
                </div>


                <div class="col-xl-12 col-lg-12 col-md-12 ">
                    <div class="form-group">
                        <label class="form-label text-dark">Yetişkin Kişi Sayısı</label>
                        <asp:RequiredFieldValidator ControlToValidate="drpYetiskin" ID="RequiredFieldValidator6" CssClass="randevuUyari" runat="server" ValidationGroup="validateCredential2" InitialValue="0" ErrorMessage="Seçim yapmalısınız">

                        </asp:RequiredFieldValidator>

                        <asp:DropDownList ID="drpYetiskin" runat="server" CssClass="form-control">
                            <asp:ListItem Text="-Seçiniz" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                            <asp:ListItem Text="9" Value="9"></asp:ListItem>
                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>


                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Ad Soyad</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtNameSurname" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtNameSurname" CssClass="form-control" placeholder="Ad Soyad" />

                    </div>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Telefon</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtPhone" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control phone phoneMask" placeholder="Telefon" />

                    </div>
                </div>
                  <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">E-mail Adresi</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtEmail" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control phone phoneMask" placeholder="E-mail Adresi" />

                    </div>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Varsa Notunuz</label>
                        <asp:TextBox runat="server" ID="txtDesc" CssClass="form-control" placeholder="Varsa Notunuz" TextMode="MultiLine" />
                    </div>
                </div>

            </div>

            <br />
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <p class="mb-0 text-right">
                        <a class="btn btn-danger randevuModalClose">Vazgeç</a>

                        <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-primary" OnClick="LinkButton1_Click" ValidationGroup="validateCredential2">Rezervasyon Talebi Gönder</asp:LinkButton>
                    </p>
                </div>
            </div>


        </div>

    </div>


    <link href="/assets/css/datetimepicker.css" rel="stylesheet" />
</asp:Content>
