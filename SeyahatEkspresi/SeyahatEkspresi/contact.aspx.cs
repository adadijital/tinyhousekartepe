﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class contact : Base
    {
        Email _mail = new Email();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _setting = db.settings.FirstOrDefault();

                if (_setting != null)
                {
                    ltrAddres.Text = _setting.office_address;
                
                    hypEmail.Text = _setting.email;
                    hypEmail.NavigateUrl = "mailto:" + _setting.email;
                    hypEmail2.Text = _setting.email2;
                    hypEmail2.NavigateUrl = "mailto:" + _setting.email2;
                    hypPhone1.Text = _setting.phone;
                    hypPhone1.NavigateUrl = "tel:" + _setting.phone;
                    hypPhone2.Text = _setting.phone2;
                    hypPhone2.NavigateUrl = "tel:" + _setting.phone2;

                    ltrMap.Text = "<iframe height='600px' width='100%' src='https://www.google.com/maps?q=" + _setting.office_address + "&output=embed'></iframe>";

                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void btnGonder_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNameSurname.Text.Trim()))
                {
                    if (!string.IsNullOrEmpty(txtEmail.Text.Trim()))
                    {
                        if (txtEmail.Text.Trim().IsEmail())
                        {
                           
                                if(!string.IsNullOrEmpty(txtMessage.Text.Trim()))
                                {
                                    string mailContent = string.Empty;
                                    mailContent += "Ad Soyad : " + txtNameSurname.Text.Trim() + "<br/>";
                                    mailContent += "E-Posta : " + txtEmail.Text.Trim() + "<br/>";
                        
                                    mailContent += "Mesaj : " + txtMessage.Text.Trim() + "<br/> Bu mail "+ System.Configuration.ConfigurationManager.AppSettings["site_name"] + " adresinden gönderildi";

                                    Base _b = new Base();
                                    var _setting = _b.db.mail_setting.FirstOrDefault();

                                    _mail.To = _setting.receive_mail;
                                    _mail.Message = mailContent;
                                    _mail.SendAdmin = false;
                                    _mail.Send();

                                    txtNameSurname.Text = "";
                                    txtEmail.Text = "";
                               
                                    txtMessage.Text = "";

                                    showWarningBasic(this.Master, "Mesajınız gönderildi", "Tebrikler");
                                }
                                else
                                    showWarningBasic(this.Master, "Lütfen mesajınızı girin", "Dikkat");
  
                        }
                        else
                            showWarningBasic(this.Master, "Lütfen geçerli bir  e-mail girin", "Dikkat");
                    }
                    else
                        showWarningBasic(this.Master, "Lütfen e-mail girin", "Dikkat");
                }
                else
                    showWarningBasic(this.Master, "Lütfen adınızı girin", "Dikkat");
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }
    }
}