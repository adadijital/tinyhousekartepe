﻿CKEDITOR.plugins.add('mytools14',
{
    requires: 'richcombo',
    init: function (editor) {

        editor.ui.addRichCombo('DropTools', {
            label: 'Sabit Yapılar',
            title: 'Sabit Yapılar',
            voiceLabel: 'Sabit Yapılar',
            multiSelect: false,
            panel:
             {
                 css: [editor.contentsCss, CKEDITOR.skin.getPath('editor')],
                 voiceLabel: editor.lang.panelVoiceLabel
             },
            init: function () {
                this.startGroup('Sabit Yapılar');
                this.add(1, 'Sempozyum', 'Sempozyum');
                this.add(2, 'Workshop', 'Workshop');
                this.add(3, 'Yurt Dışı Toplantlar', 'Yurt Dışı Toplantlar');
                this.add(4, 'Yayınlar', 'Yayınlar');
                this.add(5, 'Video', 'Video');
                this.add(6, 'Büyük Resim', 'Büyük Resim');
                this.add(7, 'Küçük Resim', 'Küçük Resim');
                this.add(8, 'Çizgili Başlık', 'Çizgili Başlık');
            },
            onClick: function (optionID)
            {
                switch (optionID)
                {
                    case "1":

                        var ht = "";
                        ht += "<div class='sbt-img' style='width:214px;height:300px;float:left;'><img src='http://placehold.it/214x300' style='width:214px;height:300px;'/></div>";

                        ht += "<div style='width:661px;float:left;margin-left:10px'><span style='font-size:24px;color:#a5192d;display:block;min-height:30px;margin-bottom:5px;'>Başlık</span>";

                        //Konum
                        ht += "<div style='color:#5f5f5f;font-size:12px;font-family: Raleway;font-size: 12px;padding:5px;background-color:#f9f9f9;min-height:92px;'>";

                        ht += "<div style='color: #a5192d;font-family: Raleway;font-size: 14px;'><span style='color:#5f5f5f;'>Konum</span> <br /> Adres</div>";

                        ht += "</div>";
                        //Tarih
                        ht += "<div style='color:#5f5f5f;font-size:12px;font-family: Raleway;font-size: 12px;padding:5px;background-color:#f9f9f9;margin-top:10px;min-height:30px;width:150px;'>";

                        ht += "<div style='font-family: Raleway;font-size: 14px;'><i class='glyphicon glyphicon-calendar'></i> Tarih</div>";

                        ht += "</div>";

                        //Resimler
                        ht += "<div style='width:661px;margin-top:10px;'>";
                        ht += "<img src='http://placehold.it/143x95' class='thumbnail' style='width:143px;height:95px;margin-right:5px;float:left;'/>";
                        ht += "<img src='http://placehold.it/143x95' class='thumbnail' style='width:143px;height:95px;margin-right:5px;float:left;'/>";
                        ht += "<img src='http://placehold.it/143x95' class='thumbnail' style='width:143px;height:95px;margin-right:5px;float:left;'/>";
                        ht += "<img src='http://placehold.it/143x95' class='thumbnail' style='width:143px;height:95px;float:left;'/>";
                        ht += "</div>";

                        ht += "</div>";

                        editor.insertHtml(ht);
                       

                        break;
                    case "2":
                        break;
                    case "3":
                        break;
                    case "4":
                        break;
                    case "5":

                        var ht = "";
                        ht += "<div class='pageContent'>"
                        ht += "<div class=\"col-md-6\" style='position:relative'>";
                        ht += "<div class='videoBox muharrem_usta'>";
                        ht += "<i class=\"fa fa-play\" aria-hidden=\"true\" style='position:absolute;left:265px;top:120px;'></i>";
                        ht += "<a href=\"javascript:void(0);\" class=\"videoGorsel\" style=\"height:240px;\"><img src='http://placehold.it/535x240' /></a>";
                        ht += "<h2>İstinye Üniversitesi Mütevelli Heyeti Başkanı Muharrem Usta</h2>";
                        ht += "</div>";
                        ht += "</div>";
                      

                       
                        ht += "<p>Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır. 1960'larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur. Yaygın inancın tersine, Lorem Ipsum rastgele sözcüklerden oluşmaz. Kökleri M.Ö. 45 tarihinden bu yana klasik Latin edebiyatına kadar uzanan 2000 yıllık bir geçmişi vardır. Virginia'daki Hampden-Sydney College'dan Latince profesörü Richard McClintock, bir Lorem Ipsum pasajında geçen ve anlaşılması en güç sözcüklerden biri olan 'consectetur' sözcüğünün klasik edebiyattaki örneklerini incelediğinde kesin bir kaynağa ulaşmıştır.</p>";

                        ht += "</div>";

                        editor.insertHtml(ht);
                        break;

                    case "6":
                        var ht = "";
                        ht += "<div class=\"bodySingleGallery\">";
                        ht += "<div class=\"bodySingleGalleryFade slick-initialized slick-slider\">";
                        ht += "<div aria-live=\"polite\" class=\"slick-list draggable\">";
                        ht += "<div class=\"slick-track\" role=\"listbox\" style=\"opacity: 1; width: 1110px;\">";
                        ht += "<div class=\"bodySingleGalleryItem slick-slide slick-current slick-active\" style=\"width: 1110px; position: relative; left: 0px; top: 0px; z-index: 999; opacity: 1;\" data-slick-index=\"0\" aria-hidden=\"false\" tabindex=\"-1\" role=\"option\" aria-describedby=\"slick-slide00\">";
                        ht += "<img src='http://placehold.it/1110x305' />";
                        ht += "<span>Lorem Ipsum</span></div></div></div>";
                        ht += "</div>";
                        ht += "</div>";



                        editor.insertHtml(ht);
                        break;
                    case "7":
                      
                        var ht = "";
                        ht += "<div class=\"col-md-4\">";
                        ht += "<div class=\"bodySingleGallery\">";
                        ht += "<div class=\"bodySingleGalleryFade slick-initialized slick-slider\">";
                        ht += "<div aria-live=\"polite\" class=\"slick-list draggable\">";
                        ht += "<div class=\"slick-track\" role=\"listbox\" style=\"opacity: 1; width: 1110px;\">";
                        ht += "<div class=\"bodySingleGalleryItem slick-slide slick-current slick-active\" style=\" position: relative; left: 0px; top: 0px; z-index: 999; opacity: 1;\" data-slick-index=\"0\" aria-hidden=\"false\" tabindex=\"-1\" role=\"option\" aria-describedby=\"slick-slide00\">";
                        ht += "<img src='http://placehold.it/327x305' />";
                        ht += "<span>Lorem Ipsum</span></div></div></div>";
                        ht += "</div>";
                        ht += "</div>";
                        ht += "</div>";

                        
                        ht += "<p>Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır. 1960'larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur. Yaygın inancın tersine, Lorem Ipsum rastgele sözcüklerden oluşmaz. Kökleri M.Ö. 45 tarihinden bu yana klasik Latin edebiyatına kadar uzanan 2000 yıllık bir geçmişi vardır. Virginia'daki Hampden-Sydney College'dan Latince profesörü Richard McClintock, bir Lorem Ipsum pasajında geçen ve anlaşılması en güç sözcüklerden biri olan 'consectetur' sözcüğünün klasik edebiyattaki örneklerini incelediğinde kesin bir kaynağa ulaşmıştır.</p>";

                        editor.insertHtml(ht);

                        break;
                    case "8":

                        editor.insertHtml("<div class=\"pageContentTitle\"><span><span>Lorem Ipsum</span></span></div>");

                        break;

                }

               
            },
        });




    }
});