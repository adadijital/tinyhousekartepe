﻿
var element;
CKEDITOR.plugins.add('myplugin',
{
    requires:'richcombo',
    init: function (editor) {

        var config = editor.config;

        editor.addCommand('setImgLeft',
			{
			    exec: function (editor) {


			        element = editor.getSelection().getStartElement();

			        if (element.getName() == "img") {
			            element.setAttributes({ 'style': 'float:left;' });
			        }


			    }
			});

        editor.addCommand('setImgRight',
           {
               exec: function (editor) {

                   element = editor.getSelection().getStartElement();

                   if (element.getName() == "img") {
                       element.setAttributes({ 'style': 'float:right;' });
                   }
               }
           });



        editor.ui.addButton('ImageLeft',
		{
		    label: 'Resimi Sola Yasla',
		    command: 'setImgLeft',
		    icon: this.path + 'images/image-left.png',
		    toolbar: 'insert,0'
		});
        editor.ui.addButton('ImageRight',
      {
          label: 'Resimi Sağa Yasla',
          command: 'setImgRight',
          icon: this.path + 'images/image-right.png',
          toolbar: 'insert,1'
      });


    

       


    }
});