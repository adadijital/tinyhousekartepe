﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class YonetimPaneli : System.Web.UI.MasterPage
    {
        tinyhous_dbEntities db = new tinyhous_dbEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                try
                {
                    if (Request.Cookies[System.Configuration.ConfigurationManager.AppSettings["site_name"].ToString().ToURL() + "PanelLogin"] != null)
                    {
                        var _pageList = db.page_lang.Where(q => q.lang_id == 1).ToList();

                        if (_pageList != null)
                        {
                            rptPageList.DataSource = _pageList;
                            rptPageList.DataBind();
                        }
                    }
                    else
                    {
                        Response.Redirect("/panel/login.aspx",false);
                    }
                }
                catch (ThreadAbortException) { }
                catch
                {
                }

              
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        public bool IsLoggedIn()
        {
            try
            {
                if (HttpContext.Current.Session["admin_login"] == null)
                {

                    return false;
                }
                else
                    if (Convert.ToBoolean(HttpContext.Current.Session["admin_login"]))
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}