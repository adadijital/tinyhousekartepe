﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class slider_resimleri : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _Slider = db.slider_lang.Where(q => q.lang_id == 1).Select(s => new
            {
                s.slider_id,
                s.slider.display_order,
                active_status = s.slider.is_active ? "Aktif" : "Pasif",
                s.img_path
            }).OrderBy(o => o.display_order).ToList();

            if (_Slider != null)
            {
                rptSliderList.DataSource = _Slider;
                rptSliderList.DataBind();
            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            byte slider_id = e.CommandArgument.toByte();
            var _delete = db.slider_lang.Where(q => q.slider_id == slider_id).ToList();

            if (_delete != null)
            {
                foreach (var item in _delete)
                {
                    db.slider_lang.Remove(item);

                    FileInfo remove = new FileInfo(Server.MapPath("~/uploads/uploads/" + item.img_path));
                    string imgRemove = Server.MapPath("~/uploads/uploads/" + item.img_path);

                    if (File.Exists(imgRemove))
                        remove.Delete();
                }

                var _remove = db.slider.Where(q => q.id == slider_id).FirstOrDefault();
                db.slider.Remove(_remove);

                if (db.SaveChanges() > 0)
                    ReloadPage();

          

            }

          
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (FileUpload1.HasFile)
                {
                    int uzunluk = FileUpload1.FileName.Split('.').Length;
                    string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
                    FileUpload1.SaveAs(Server.MapPath("~/uploads/slider/" + random));
                    ResizeImage("~/uploads/slider/" + random, 1920, 800);

                    slider _newslider = new slider();
                    _newslider.is_active = ckIsActive.Checked;
                    _newslider.link = txtLink.Text.Trim();
                    _newslider.display_order = 1;
                    db.slider.Add(_newslider);

                    if (db.SaveChanges() > 0)
                    {
                        for (byte i = 1; i < 3; i++)
                        {
                            slider_lang _newLang = new slider_lang();
                            _newLang.slider_id = _newslider.id;
                            _newLang.slider_title = txtTitle.Text.Trim();
                            _newLang.slider_desc = txtDesc.Text.Trim();
                            _newLang.img_path = random;
                            _newLang.lang_id = i;
                         

                            db.slider_lang.Add(_newLang);
                        }

                        if (db.SaveChanges() > 0)
                            ReloadPage();
                    }
                    else
                        Alert("Slider resmi eklenemedi. Lütfen tekrar deneyin.");

                }
                else
                    Alert("Resim Seçmediniz.");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        [WebMethod]
        public static void Sirala(String Satir)
        {
            tinyhous_dbEntities db = new tinyhous_dbEntities();
            string gelen = Satir.Replace("satir", "");
            char[] ayrac = new char[] { ',' };
            string[] gelenler = gelen.Split(ayrac);
            int i = 1;
            foreach (string veri in gelenler)
            {
                db.Database.ExecuteSqlCommand("Update slider set display_order = " + i + " where id=" + Convert.ToInt32(veri));
                i++;
            }
        }
    }
}