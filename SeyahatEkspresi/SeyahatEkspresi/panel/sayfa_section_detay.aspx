﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="sayfa_section_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.sayfa_section_detay" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                 
            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <asp:Repeater ID="rptLangs" runat="server" OnItemDataBound="rptLangs_ItemDataBound">
                            <ItemTemplate>
                                <asp:HyperLink ID="hypLangLink" runat="server" CssClass="btn bg-navy btn-flat"><%#Eval("lang_name") %></asp:HyperLink>
                            </ItemTemplate>
                        </asp:Repeater>
                    </h3>
                </div>
                <div class="box-body with-border">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Başlık</label>
                        <asp:TextBox ID="txtBaslik" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
            
                    <div class="form-group">
                        <label for="exampleInputEmail1">Açıklama</label>
                        <CKEditor:CKEditorControl ID="txtAciklama" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                    </div>

                    <asp:Panel ID="pnlHakkimizda" runat="server" Visible="false">
                                <div class="form-group">
                        <label for="exampleInputEmail1">Sayaç 1 Numara</label>
                        <asp:TextBox ID="txtNumber1" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                           <div class="form-group">
                        <label for="exampleInputEmail1">Sayaç 1 Açıklama</label>
                        <asp:TextBox ID="txtNumber1Desc" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                          <div class="form-group">
                        <label for="exampleInputEmail1">Sayaç 2 Numara</label>
                        <asp:TextBox ID="txtNumber2" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                           <div class="form-group">
                        <label for="exampleInputEmail1">Sayaç 2 Açıklama</label>
                        <asp:TextBox ID="txtNumber2Desc" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                             <div class="form-group">
                        <label for="exampleInputEmail1">Sayaç 3 Numara</label>
                        <asp:TextBox ID="txtNumber3" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                           <div class="form-group">
                        <label for="exampleInputEmail1">Sayaç 3 Açıklama</label>
                        <asp:TextBox ID="txtNumber3Desc" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                       <div class="form-group">
                        <label for="exampleInputEmail1">Form Tanıtım</label>
                        <CKEditor:CKEditorControl ID="txtContent2" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                    </div>
                    </asp:Panel>

                

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnUpdate" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnUpdate_Click"/>
                </div>
            </div>

              



        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.nav-tabs-custom li:first').addClass('active');
        });
    </script>

</asp:Content>
