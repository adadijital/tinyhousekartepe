﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class sayfa_section_detay : Base
    {
        byte lang_id;
        byte section_group;
        short page_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["lang_id"]) && !string.IsNullOrEmpty(Request.QueryString["section_group"]))
            {
                lang_id = Request.QueryString["lang_id"].toByte();
                section_group = Request.QueryString["section_group"].toByte();
                page_id = Request.QueryString["page_id"].toShort();



                if(page_id == 1)
                {
                    pnlHakkimizda.Visible = true;
                }

                txtAciklama.config.toolbar = WebTools.CKToolBar(CkTool.Simple);

                if (!IsPostBack)
                {
                    var _LangList = db.lang.Where(q => (bool)(q.is_active)).ToList();

                    if (_LangList.Count() > 0)
                    {
                        rptLangs.DataSource = _LangList;
                        rptLangs.DataBind();
                    }


                    var _detail = db.page_content.Where(q => q.section_group == section_group && q.lang_id == lang_id).FirstOrDefault();
                    if (_detail != null)
                    {
                        txtBaslik.Text = _detail.section_name;
                        txtAciklama.Text = _detail.page_content_text;

                        txtNumber1.Text = _detail.counter1_number;
                        txtNumber1Desc.Text = _detail.counter1_desc;

                        txtNumber2.Text = _detail.counter2_number;
                        txtNumber2Desc.Text = _detail.counter2_desc;

                        txtNumber3.Text = _detail.counter3_number;
                        txtNumber3Desc.Text = _detail.counter3_desc;

                        txtContent2.Text = _detail.content2;


                    }
                }

            }
            else
                Response.Redirect("/panel", false);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var _detail = db.page_content.Where(q => q.section_group == section_group && q.lang_id == lang_id).FirstOrDefault();
            if (_detail != null)
            {
                _detail.section_name = txtBaslik.Text.Trim();
                _detail.page_content_text = txtAciklama.Text;

                _detail.counter1_number = txtNumber1.Text.Trim();
                _detail.counter1_desc = txtNumber1Desc.Text.Trim();

                _detail.counter2_number = txtNumber2.Text.Trim();
                _detail.counter2_desc = txtNumber2Desc.Text.Trim();

                _detail.counter3_number = txtNumber3.Text.Trim();
                _detail.counter3_desc = txtNumber3Desc.Text.Trim();

                _detail.content2 = txtContent2.Text.Trim();

                if (db.SaveChanges() > 0)
                    ReloadPage();
            }
            else
            {
                page_content _newContent = new page_content();
                _newContent.lang_id = lang_id;
                _newContent.page_id = page_id;
                _newContent.section_name = txtBaslik.Text.Trim();
                _newContent.page_content_text = txtAciklama.Text.Trim();
                _newContent.section_group = section_group;
                db.page_content.Add(_newContent);

                if (db.SaveChanges() > 0)
                    ReloadPage();

            }

        }

        protected void rptLangs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hypLangLink = (HyperLink)e.Item.FindControl("hypLangLink");
            int lang = DataBinder.Eval(e.Item.DataItem, "id").toInt();

            hypLangLink.Attributes.Add("lid", lang.ToString());
            hypLangLink.NavigateUrl = "/panel/sayfa_section_detay.aspx?page_id=" + page_id + "&section_group=" + section_group.ToString() + "&lang_id=" + lang.ToString();

            if (lang_id == DataBinder.Eval(e.Item.DataItem, "id").toShort())
            {
                hypLangLink.CssClass = "btn btn-flat bg-navy disabled color-palette";

            }

        }
    }
}