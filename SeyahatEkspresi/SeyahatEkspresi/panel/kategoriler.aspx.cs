﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class kategoriler : Base
    {
        int category_id;
        int parent_id;
        List<CategoryTreeView> _categoryHierarchy = new List<CategoryTreeView>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _fillCategoryHierarchy(db.categories_lang.Where(q => q.categories.parent_id == 0 && q.lang_id == 1 && q.categories.is_deleted == false).OrderBy(o=> o.categories.display_order).ToList());

                if (!IsPostBack)
                {
                    _fillCategoryDropdown();
                }
                rptCategory.DataSource = _categoryHierarchy;
                rptCategory.DataBind();

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }



        protected void btnEkle_Click(object sender, EventArgs e)
        {


            if (!string.IsNullOrEmpty(txtCategoryName.Text.Trim()))
            {

                categories _cat = new categories();
                _cat.parent_id = drpCategoryList.SelectedValue.toInt();
                _cat.display_order = 1;
                _cat.is_active = ckIsActive.Checked;
                _cat.category_name = txtCategoryName.Text.Trim();
                _cat.is_deleted = false;

                if(FileUpload1.HasFile)
                {
                    int uzunluk = FileUpload1.FileName.Split('.').Length;
                    string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
                    FileUpload1.SaveAs(Server.MapPath("~/uploads/category/" + random));
                    using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/category/" + random)))
                    {
                        img.ScaleAndCrop(370, 285).SaveAs(Server.MapPath("~/uploads/category/M/" + random));
                    }


                    //ResizeImage("~/uploads/category/" + random, 1920, 920);
                    _cat.icon_path = random;
                }


                db.categories.Add(_cat);

                if (db.SaveChanges() > 0)
                {
                    foreach (var item in db.lang.Where(q => q.is_active).ToList())
                    {
                        categories_lang _newLang = new categories_lang();
                        _newLang.category_name = txtCategoryName.Text.Trim();
                        _newLang.category_id = _cat.id;
                        _newLang.lang_id = item.id;

                        db.categories_lang.Add(_newLang);
                    }

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            else
                Alert("Kategori adı girmelisiniz");

        }

        private void _fillCategoryDropdown()
        {
            drpCategoryList.DataSource = _categoryHierarchy;
            drpCategoryList.DataTextField = "CategoryName";
            drpCategoryList.DataValueField = "CategoryID";
            drpCategoryList.DataBind();
            drpCategoryList.Items.Insert(0, new ListItem("– Kategori seçin", "0"));
        }

        private void _fillCategoryHierarchy(List<categories_lang> _categories, int index = 0)
        {
            if (_categories.Count > 0)
            {
                index++;

                foreach (var item in _categories)
                {
                    string _prefix = string.Empty;

                    if (index > 1)
                        for (int i = 1; i < index; i++)
                            _prefix += "-";

                    _categoryHierarchy.Add(new CategoryTreeView
                    {
                        CategoryID = item.category_id,
                        CategoryName = _prefix + " " + item.category_name,
                        DisplayOrder = item.categories.display_order,
                        IsActive = item.categories.is_active
                    });

                    List<categories_lang> _subCategories = db.categories_lang.Where(q => q.categories.parent_id == item.category_id && q.lang_id == 1).OrderBy(o=> o.categories.display_order).ToList();

                    if (_subCategories.Count > 0)
                        _fillCategoryHierarchy(_subCategories, index);
                }
            }
        }


        [WebMethod]
        public static void Sirala(String Satir)
        {
            tinyhous_dbEntities db = new tinyhous_dbEntities();
            string gelen = Satir.Replace("Satir", "");
            char[] ayrac = new char[] { ',' };
            string[] gelenler = gelen.Split(ayrac);
            int i = 1;
            foreach (string veri in gelenler)
            {
                db.Database.ExecuteSqlCommand("Update categories set display_order = " + i + " where id=" + Convert.ToInt32(veri));
                i++;
            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            int _id = e.CommandArgument.toInt();

            var _Detail = db.categories.Where(q => q.id == _id).FirstOrDefault();

            if(_Detail !=null)
            {
                _Detail.is_deleted = true;
            }

            if (db.SaveChanges() > 0)
                ReloadPage();
        }
    }
}