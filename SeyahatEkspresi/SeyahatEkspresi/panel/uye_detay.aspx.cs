﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class uye_detay : Base
    {
        int member_id;
        MemberManager _memberManager = new MemberManager();
        AdvertManager _advertManager = new AdvertManager();
        OrderManager _order = new OrderManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["member_id"]))
                {
                    member_id = Request.QueryString["member_id"].toInt();

                    if (!Page.IsPostBack)
                        _fillPage();
                }
                else
                    Response.Redirect("/panel/default.aspx", false);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        private void _fillPage()
        {
            var _userInfo = _memberManager.getMemberData(member_id);
            if (_userInfo != null)
            {
                HyperLink1.NavigateUrl = "/panel/uye_guncelle.aspx?member_id=" + member_id.ToString();
                ltrFullName.Text = _userInfo.member_name;
                ltrHesapTipi.Text = _userInfo.account_type_text;
                ltrEmail.Text = _userInfo.member_email;
                ltrPhone.Text = _userInfo.telephone;
                ltrCreatedDate.Text = _userInfo.registration_time.ToString();
                ltrCreatedIp.Text = _userInfo.registration_ip;
                ltrActiveStatus.Text = _userInfo.active_status;
                ltrHeaderNameSurname.Text = _userInfo.member_name;
                ltrHeaderAccountType.Text = _userInfo.account_type_text;

                rptLoginLog.DataSource = _userInfo.login_logs.ToList();
                rptLoginLog.DataBind();

                if (_userInfo.active_status == "Aktif")
                {
                    btnActive.Visible = false;
                    btnPasif.Visible = true;
                }
                else
                {
                    btnActive.Visible = true;
                    btnPasif.Visible = false;
                }

                if(_userInfo.account_type ==1)
                {
                    pnlKurumsal.Visible = true;
                    ltrCompanyName.Text = _userInfo.company_name;
                    ltrTaxOffice.Text = _userInfo.tax_office;
                    ltrTaxNumber.Text = _userInfo.tax_number;

                }

                var _ads = _advertManager.getMemberAdvert(member_id);

                if(_ads.Count()>0)
                {
                    rptAdvertList.DataSource = _ads;
                    rptAdvertList.DataBind();

                }

                var _wish = _advertManager.getMemberAdvertWish(member_id);

                if(_wish.Count()>0)
                {
                    rptWishList.DataSource = _wish;
                    rptWishList.DataBind();
                }


                var _bankList = db.member_bank_account.Where(q => q.member_id == member_id).ToList();

                if(_bankList.Count()>0)
                {
                    rptBankList.DataSource = _bankList;
                    rptBankList.DataBind();
                }

                //var _memberBasket = db.member_basket.Select(s=> new {
                //    product_name = s.ads.ads_lang.Where(q2=> q2.lang_id == 1).FirstOrDefault().ads_name,
                //    s.member_id,
                //    s.id,
                //    s.quantity,
                //    s.total_sell_price,
                //    s.unit_sell_price,
                //}).Where(q => q.member_id == member_id).ToList();

                //if (_memberBasket.Count()>0)
                //{
                //    rptMemberBasket.DataSource = _memberBasket;
                //    rptMemberBasket.DataBind();
                //}

                //var _OrderList = _order.getOrderListMember(member_id);
                //if (_OrderList.Count > 0)
                //{
                //    rptOrders.DataSource = _OrderList;
                //    rptOrders.DataBind();
                //}


            }
        }

        protected void btnActive_Click(object sender, EventArgs e)
        {
            var _member = db.members.Where(w => w.id == member_id).FirstOrDefault();
            _member.is_active = true;
            if (db.SaveChanges() > 0)
            {

                Alert("Üye aktif edildi");
            }

            else
                Alert("Güncelleme işlemi yapılamadı lütfen daha sonra deneyiniz");

        }

        protected void btnPasif_Click(object sender, EventArgs e)
        {
            var _member = db.members.Where(w => w.id == member_id).FirstOrDefault();
            _member.is_active = false;
            if (db.SaveChanges() > 0)
            {

                Alert("Üye pasif duruma alındı");
            }
            else
                Alert("Güncelleme işlemi yapılamadı lütfen daha sonra deneyiniz");

        }

        protected void btnPasswordReset_Click(object sender, EventArgs e)
        {
            if (txtPasswordReset.Text.Trim() != "")
            {
                var _member = db.members.Where(w => w.id == member_id).FirstOrDefault();
                _member.password = txtPasswordReset.Text.Trim().ToMD5();

                if (db.SaveChanges() > 0)
                    Alert("Şifre sıfırlandı");
                else
                    Alert("Güncelleme işlemi yapılamadı lütfen daha sonra deneyiniz");


            }
            else
                Alert("Şifre alanı boş olamaz.");
        }
    }
}