﻿
using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class ilan_ekle1 : Base
    {
        CategoryManager _categoryManager = new CategoryManager();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                var _parentCat = db.categories_lang.Where(q => q.categories.is_active && q.lang_id == 1 && q.categories.parent_id == 0 && q.categories.is_deleted==false).ToList();

                if (_parentCat.Count() > 0)
                {
                    drpCategories.DataSource = _parentCat;
                    drpCategories.DataTextField = "category_name";
                    drpCategories.DataValueField = "category_id";
                    drpCategories.DataBind();
                    drpCategories.Items.Insert(0, new ListItem("– Kategori seçin", "0"));
                }

                var _cityList = db.geo_cities.OrderBy(o => o.PlakaNo).ToList();

                if (_cityList.Count() > 0)
                {
                    if (!IsPostBack)
                    {
                        drpCityList.DataTextField = "city_name";
                        drpCityList.DataValueField = "id";
                        drpCityList.DataSource = _cityList;
                        drpCityList.DataBind();
                        drpCityList.Items.Insert(0, "Şehir Seçiniz");
                    }
                }
            }
        }

        protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _catID = drpCategories.SelectedValue.toInt();

                if (_catID > 0)
                {
                    pnlDetail.Visible = true;
                    var _definitions = db.ad_info.Where(q => q.ad_info_lang.Where(q2 => q2.lang_id == 1).Count() > 0 && q.category_id == _catID)
                   .Select(s => new
                   {
                       s.ad_info_lang.FirstOrDefault(f => f.lang_id == 1).info_name,
                       s.id,
                       s.ad_info_value.Where(q => q.ads_id == 1 && q.ad_info_value_lang.Where(q2 => q2.lang_id == 1).Count() > 0).FirstOrDefault().info_value

                   }).ToList();

                    if (_definitions.Count() > 0)
                    {
                        rptAdInfoDefinition.DataSource = _definitions;
                        rptAdInfoDefinition.DataBind();
                    }


                   pnlAdsDetail.Update();
                }

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void rptAdInfoDefinition_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {

                TextBox txtProperty = (TextBox)e.Item.FindControl("txtProperty");
                CheckBox CheckBox1 = (CheckBox)e.Item.FindControl("CheckBox1");
                txtProperty.Attributes.Add("pid", DataBinder.Eval(e.Item.DataItem, "id").ToString());
                CheckBox1.Attributes.Add("pid", DataBinder.Eval(e.Item.DataItem, "id").ToString());
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void drpCityList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {


                int city_id = drpCityList.SelectedValue.toInt();

                var _townList = db.geo_town.Where(q => q.city_id == city_id).ToList();

                if (_townList.Count() > 0)
                {
                    drpTownList.DataTextField = "town_name";
                    drpTownList.DataValueField = "id";
                    drpTownList.DataSource = _townList;
                    drpTownList.DataBind();
                    drpTownList.Items.Insert(0, "İlçe Seçiniz");

                }
                pnlTown.Visible = true;
                pnlTown.Update();


            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void drpTownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int town_id = drpTownList.SelectedValue.toInt();


                //var _quarterList = db.geo_quarter.Where(q => q.town_id == town_id).ToList();

                //if (_quarterList.Count() > 0)
                //{
                //    drpDist.DataTextField = "quarter_name";
                //    drpDist.DataValueField = "id";
                //    drpDist.DataSource = _quarterList;
                //    drpDist.DataBind();
                //    drpDist.Items.Insert(0, "Mahalle Seçiniz");
                //}

                //pnlDist.Visible = true;
                //pnlDist.Update();

            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(txtAdvertTitle.Text.Trim()))
                {
                    if (txtAdvertTitle.Text.Trim().Length > 5)
                    {
                        if (txtAdvertTitle.Text.Trim().Length <= 150)
                        {

                            if (!string.IsNullOrEmpty(txtDesc.Text.Trim()))
                            {
                                if (!string.IsNullOrEmpty(txtAdvertPrice.Text.Trim()))
                                {
                                    if (txtAdvertPrice.Text.Trim().isDecimal())
                                    {

                                        if (drpCityList.SelectedValue.toInt() > 0)
                                        {
                                            if (drpTownList.SelectedValue.toInt() > 0)
                                            {

                                                if (txtAdress.Text.Trim().Length <= 150)
                                                {
                                                    if (FileUpload1.HasFile)
                                                    {
                                                        #region ilan ekleniyor
                                                        ads _newAds = new ads();
                                                        _newAds.ads_name = txtAdvertTitle.Text.Trim();
                                                        _newAds.member_id = 0;
                                                        _newAds.created_time = DateTime.Now;
                                                        _newAds.ads_no = _advertNo();
                                                        _newAds.geo_city_id = drpCityList.SelectedValue.toInt();
                                                        _newAds.geo_town_id = drpTownList.SelectedValue.toInt();
                                                        _newAds.geo_quarter_id = 1;
                                                        _newAds.ads_view = 0;
                                                        _newAds.is_cancel = false;
                                                        _newAds.address = txtAdress.Text.Trim();
                                                        _newAds.sell_price = txtAdvertPrice.Text.Trim().todecimal();
                                                        _newAds.display_order = 0;
                                                        _newAds.is_active = true;
                                                        _newAds.is_deleted = false;
                                                        _newAds.type = 1;    // 1 ise ilan 2 ise ürün
                                                        _newAds.is_showcase = false;
                                                        _newAds.market_price = txtMarketPrice.Text.Trim().todecimal();
                                                        _newAds.week_price = txtWeekPrice.Text.Trim().todecimal();




                                                        //var _GeoCode = GetGeoCode(drpDist.SelectedItem.Text + "," + drpTownList.SelectedItem.Text + "," + drpCityList.SelectedItem.Text);

                                                        //string[] _GeoCodeSplit = _GeoCode.Split(',');

                                                        _newAds.geo_lat = "";
                                                        _newAds.geo_long = "";

                                                        db.ads.Add(_newAds);

                                                        if (db.SaveChanges() > 0)
                                                        {
                                                            #region ilan dil ekleniyor

                                                            foreach (var item in db.lang.ToList())
                                                            {
                                                                ads_lang _newLang = new ads_lang();
                                                                _newLang.ads_name = txtAdvertTitle.Text.Trim();
                                                                _newLang.ads_id = _newAds.id;
                                                                _newLang.lang_id = item.id;
                                                                _newLang.description = txtDesc.Text.Trim();
                                                                _newLang.kisi_sayisi = txtKisiSayisi.Text.Trim();
                                                                _newLang.yatak_sayisi = txtYatakSayisi.Text.Trim();
                                                                _newLang.banyo_sayisi = txtBanyoSayisi.Text.Trim();
                                                                _newLang.isinma_turu = txtisinmaTuru.Text.Trim();
                                                                _newLang.konum_durumu = txtKonumDurumu.Text.Trim();
                                                                db.ads_lang.Add(_newLang);


                                                            }

                                                            #endregion

                                                            if (db.SaveChanges() > 0)
                                                            {
                                                                int _selectedCat = drpCategories.SelectedValue.toInt();
                                                                #region ilan kategori
                                                                var result = _categoryManager.SetCatBreadCrumb(_selectedCat.toInt(), Localization.Language, Localization.LanguageStr);

                                                                foreach (var item2 in result)
                                                                {
                                                                    relation_ads_category _newRel = new relation_ads_category();
                                                                    _newRel.cat_id = item2.id;
                                                                    _newRel.ads_id = _newAds.id;
                                                                    db.relation_ads_category.Add(_newRel);
                                                                }

                                                                if (db.SaveChanges() > 0)
                                                                {


                                                                    #region ilan özellikleri

                                                                    if (rptAdInfoDefinition.Items.Count > 0)
                                                                    {
                                                                        foreach (var item in rptAdInfoDefinition.Items)
                                                                        {
                                                                            RepeaterItem rptItem = item as RepeaterItem;
                                                                            TextBox txtProperty = (TextBox)rptItem.FindControl("txtProperty");
                                                                            CheckBox CheckBox1 = (CheckBox)rptItem.FindControl("CheckBox1");

                                                                            int adInfoID = txtProperty.Attributes["pid"].toInt();
                                                                            string _status = CheckBox1.Checked.ToString();


                                                                            //if (!string.IsNullOrEmpty(txtProperty.Text.Trim()))
                                                                            //{
                                                                            ad_info_value _newValue = new ad_info_value();
                                                                            _newValue.info_value = _status;
                                                                                _newValue.info_id = adInfoID;
                                                                                _newValue.ads_id = _newAds.id;

                                                                                db.ad_info_value.Add(_newValue);

                                                                                if (db.SaveChanges() > 0)
                                                                                {
                                                                                    foreach (var LangItem in db.lang.ToList())
                                                                                    {
                                                                                        ad_info_value_lang _newLang = new ad_info_value_lang();
                                                                                    _newLang.ad_info_value = _status;
                                                                                        _newLang.lang_id = LangItem.id;
                                                                                        _newLang.ad_info_value_id = _newValue.id;

                                                                                        db.ad_info_value_lang.Add(_newLang);

                                                                                    }
                                                                                }
                                                                            //}
                                                                        }
                                                                    }


                                                                    #endregion



                                                                    #region ilan resimleri

                                                                    IList<HttpPostedFile> SecilenDosyalar = FileUpload1.PostedFiles;

                                                                    for (int i = 0; i < SecilenDosyalar.Count; i++)
                                                                    {
                                                                        ads_images _newImage = new ads_images();
                                                                        int uzunluk = FileUpload1.PostedFiles[i].FileName.Split('.').Length;
                                                                        string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.PostedFiles[i].FileName.Split('.')[uzunluk - 1];
                                                                        FileUpload1.PostedFiles[i].SaveAs(Server.MapPath("~/uploads/ads/" + random));

                                                                        using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/ads/" + random)))
                                                                        {
                                                                            img.ScaleAndCrop(1200, 1600).SaveAs(Server.MapPath("~/uploads/ads/XL/" + random));
                                                                        }

                                                                        using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/ads/" + random)))
                                                                        {
                                                                            img.ScaleAndCrop(600, 800).SaveAs(Server.MapPath("~/uploads/ads/L/" + random));
                                                                        }

                                                                        using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/ads/" + random)))
                                                                        {
                                                                            img.ScaleAndCrop(370, 495).SaveAs(Server.MapPath("~/uploads/ads/M/" + random));
                                                                        }



                                                                        _newImage.ads_id = _newAds.id;
                                                                        _newImage.display_order = i;
                                                                        _newImage.img_name = random;

                                                                        db.ads_images.Add(_newImage);

                                                                    }



                                                                    #endregion


                                                                }


                                                                if (db.SaveChanges() > 0)
                                                                {
                                                                    Response.Redirect("/panel/ilan_listesi.aspx", false);
                                                                }
                                                                #endregion
                                                            }

                                                        }


                                                        #endregion



                                                    }
                                                    else
                                                        Alert("Resim seçmediniz");
                                                }
                                                else
                                                    showWarningBasic(this.Master, "Adres bilgisi maksimumum 150 karakter olmalı", "Dikkat");

                                            }
                                            else
                                                showWarningBasic(this.Master, "İlçe seçiniz", "Dikkat");
                                        }
                                        else
                                            showWarningBasic(this.Master, "Şehir seçiniz", "Dikkat");


                                    }
                                    else
                                        showWarningBasic(this.Master, "Geçerli bir fiyat giriniz", "Dikkat");
                                }
                                else
                                    showWarningBasic(this.Master, "Fiyat girmelisiniz", "Dikkat");
                            }
                            else
                                showWarningBasic(this.Master, "Açıklama girmelisiniz", "Dikkat");
                        }
                        else
                            showWarningBasic(this.Master, "İlan başlığı maksimum 150 karakter olmalı", "Dikkat");
                    }
                    else
                        showWarningBasic(this.Master, "İlan başlığı için en az 5 karakter girmelisiniz", "Dikkat");
                }
                else
                    showWarningBasic(this.Master, "Başlık girmelisiniz.", "Dikkat");




            }
            catch (Exception ex)
            {
                Log.Add(ex);
                showWarningBasic(this.Master, ex.Message, "Dikkat");
            }
        }

        bool _optimizeImage(string _imagePath)
        {

            try
            {
                //width: 1600
                //height: 1067
                Bitmap _baseImage = new Bitmap(_imagePath);


                if (_baseImage.Width > 1920 || _baseImage.Height > 1080)
                {
                    double ratioX = (double)1920 / (double)_baseImage.Width;
                    double ratioY = (double)1080 / (double)_baseImage.Height;
                    double ratio = ratioX < ratioY ? ratioX : ratioY;
                    int _newWidth = Convert.ToInt32(_baseImage.Width * ratio);
                    int _newHeight = Convert.ToInt32(_baseImage.Height * ratio);
                    _baseImage.Dispose();

                    Base.ResizeImage(_imagePath, _newWidth, _newHeight);
                }
                else
                    _baseImage.Dispose();

                return _getFinalVersionOfImage(_imagePath);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);

                try
                {
                    File.Delete(_imagePath);
                }
                catch (Exception ex2) { Response.Write(ex2.Message); }

                return false;
            }

        }

        bool _getFinalVersionOfImage(string _imagePath)
        {
            try
            {
                Bitmap _baseImage = new Bitmap(_imagePath);


                Bitmap _newImage = new Bitmap(1920, 1080);
                //width: 1600
                //       height: 1067
                int _x = 0;
                int _y = 0;

                if (_baseImage.Width < 1920)
                    _x = ((1920 - _baseImage.Width) > 1) ? (1920 - _baseImage.Width) / 2 : (1920 - _baseImage.Width);

                if (_baseImage.Height < 1080)
                    _y = ((1080 - _baseImage.Height) > 1) ? (1080 - _baseImage.Height) / 2 : (1080 - _baseImage.Height);

                using (Graphics _graphic = Graphics.FromImage(_newImage))
                {
                    Rectangle _baseRect = new Rectangle(_x, _y, _baseImage.Width, _baseImage.Height);

                    _graphic.FillRectangle(Brushes.White, new Rectangle(0, 0, _newImage.Width, _newImage.Height));
                    _graphic.DrawImageUnscaledAndClipped(_baseImage, _baseRect);
                    _baseImage.Dispose();

                    _newImage.SetResolution(72, 72);
                    _newImage.Save(_imagePath);
                }

                return true;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);

                try
                {
                    File.Delete(_imagePath);
                }
                catch (Exception ex2) { Response.Write(ex2.Message); }

                return false;
            }
        }

        private Bitmap yaz(System.Drawing.Image resim, int genislik, int yukseklik, string yazilacak, float font)
        {
            Bitmap resmim = new Bitmap(resim, genislik, yukseklik);
            System.Drawing.Graphics graf = System.Drawing.Graphics.FromImage(resmim);
            System.Drawing.SolidBrush firca = new SolidBrush(Color.FromArgb(140, 255, 255, 255));
            System.Drawing.Font fnt = new Font("calibri", font);//font tipi ve boyutu
            System.Drawing.SizeF size = new SizeF(0, 0);
            System.Drawing.PointF coor = new Point(Convert.ToInt32(resim.Width * 0.50), Convert.ToInt32(resim.Height * 0.40));
            System.Drawing.RectangleF kutu = new RectangleF(coor, size);

            StringFormat sf = new StringFormat();

            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Near;
            graf.DrawString(yazilacak, fnt, firca, kutu, sf);
            return resmim;
        }


        private string _advertNo()
        {
            string code = CreateRandomValue(8, false, false, true, false);

            try
            {
                while (_isAdvertNoExists(code))
                    code = CreateRandomValue(8, false, false, true, false);
            }
            catch { code = CreateRandomValue(8, false, false, true, false); }

            return code;
        }

        private bool _isAdvertNoExists(string code)
        {
            try
            {
                var _code = db.ads.Where(q => q.ads_no == code).FirstOrDefault();
                if (_code != null)
                    return true;
                else
                    return false;
            }
            catch { return false; }
        }
    }
}