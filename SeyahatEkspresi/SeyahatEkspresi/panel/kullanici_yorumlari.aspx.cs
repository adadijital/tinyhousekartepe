﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class kullanici_yorumlari : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _comments = db.customer_comments.Select(s => new {
                    s.id,
                    member_name = s.members.name_surname,
                    approved_status = s.is_approved ? "Onaylandı" : "Onay Bekliyor",
                    s.is_approved,
                    s.created_time,
                    s.comment_desc
                }).ToList();



                if(_comments.Count()>0)
                {
                    rptCustomerComents.DataSource = _comments;
                    rptCustomerComents.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _id = e.CommandArgument.toInt();
                var _delete = db.customer_comments.Where(q => q.id == _id).FirstOrDefault();

                if(_delete !=null)
                {
                    db.customer_comments.Remove(_delete);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void rptCustomerComents_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                LinkButton LinkButton2 = (LinkButton)e.Item.FindControl("LinkButton2");
                bool approved_status = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_approved"));

                if (!approved_status)
                    LinkButton2.Visible = true;

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void LinkButton2_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _id = e.CommandArgument.toInt();
                var _updte = db.customer_comments.Where(q => q.id == _id).FirstOrDefault();

                if(_updte !=null)
                {
                    _updte.is_approved = true;

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}