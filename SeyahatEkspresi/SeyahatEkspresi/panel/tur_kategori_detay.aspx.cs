﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class tur_kategori_detay : Base
    {
        int category_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["categorY_id"] != null)
                {
                    category_id = Request.QueryString["categorY_id"].toInt();

                    var _Detail = db.tour_category.Where(q => q.id == category_id).FirstOrDefault();

                    if (_Detail != null)
                    {
                        if (!IsPostBack)
                        {
                            txtCategoryName.Text = _Detail.category_name;
                            ckIsActive.Checked = _Detail.is_active;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                var _Detail = db.tour_category.Where(q => q.id == category_id).FirstOrDefault();

                if (_Detail != null)
                {

                    _Detail.category_name = txtCategoryName.Text.Trim();
                    _Detail.is_active = ckIsActive.Checked;

                    if (db.SaveChanges() > 0)
                        ReloadPage();


                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}