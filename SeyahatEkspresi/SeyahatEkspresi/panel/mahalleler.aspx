﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="mahalleler.aspx.cs" Inherits="SeyahatEkspresi.panel.mahalleler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Yeni Mahalle Ekle</h3>
                </div>
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">İlçe Adı </label>
                        <asp:TextBox ID="txtTownName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Ekle" class="btn btn-primary"  OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Mahalleler</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Mahalle Adı</th>
                         
                                <th>Sil</th>
                           
                            </tr>
                        </thead>

                        <tbody id="sortableResim">
                            <asp:Repeater ID="rptQuarter" runat="server">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>
                                        <td>
                                            <%#Eval("quarter_name") %>
                                    

                                        <td>
                                               <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command" CommandArgument='<%#Eval ("id") %>' OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                                        </td>
                                        
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</asp:Content>
