﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class ilanlar : Base
    {
        int ads_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            var _ilanlar = db.ads_lang.Where(q => q.lang_id == 1 && q.ads.is_deleted ==false).OrderByDescending(o => o.id).Select(s => new
            {
                s.id,
                s.ads_id,
                s.ads_name,
                active_status = s.ads.is_active ? "Aktif" : "Pasif",
                s.ads.relation_ads_category.FirstOrDefault().categories.categories_lang.Where(q => q.categories.parent_id == 0).FirstOrDefault().category_name,
                s.ads.sell_price,
                name_surname = s.ads.member_id == 0 ? "Yönetici" : s.ads.members.name_surname,
                //s.ads.members.name_surname,
                s.ads.created_time,
                s.ads.member_id,
                is_showcase = s.ads.is_showcase,
            }).ToList();

            if (_ilanlar.Count() > 0)
            {
                rptAds.DataSource = _ilanlar;
                rptAds.DataBind();
            }
        }

        protected void rptAds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rptLangs = (Repeater)e.Item.FindControl("rptLangs");
            LinkButton LinkButton1 = (LinkButton)e.Item.FindControl("LinkButton1");
            LinkButton LinkButton2 = (LinkButton)e.Item.FindControl("LinkButton2");
            LinkButton LinkButton3 = (LinkButton)e.Item.FindControl("LinkButton3");
            
            Literal ltrActive = (Literal)e.Item.FindControl("ltrActive");
            Literal ltrVitrin = (Literal)e.Item.FindControl("ltrVitrin");

            bool is_showcase = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_showcase"));

            string active_status = DataBinder.Eval(e.Item.DataItem, "active_status").ToString();

            if (active_status == "Aktif")
            {
                ltrActive.Text = "<span class='label label-success'>Aktif</span>";
                LinkButton2.Visible = false;
            }
            else
            {
                ltrActive.Text = "<span class='label label-danger'>Pasif</span>";
                LinkButton2.Visible = true;


            }

            if(is_showcase)
            {
                ltrVitrin.Text = "<span class='label label-success'>Vitrin İlanı</span>";
                LinkButton3.Visible = false;
            }
            else
            {
                ltrVitrin.Text = "<span class='label label-danger'>Vitrin İlanı Değil</span>";
                LinkButton3.Visible = true;
            }
               

            ads_id = DataBinder.Eval(e.Item.DataItem, "ads_id").toInt();
            var _LangList = db.lang.Where(q => q.is_active).ToList();

            if (_LangList.Count() > 0)
            {
                rptLangs.DataSource = _LangList;
                rptLangs.DataBind();
            }

        }

        protected void rptLangs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hypLangLink = (HyperLink)e.Item.FindControl("hypLangLink");
            int lang_id = DataBinder.Eval(e.Item.DataItem, "id").toInt();
            hypLangLink.NavigateUrl = "ilan_detay.aspx?ads_id=" + ads_id + "&lang_id=" + lang_id.ToString();
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int id = e.CommandArgument.toInt();

                var _delete = db.ads.Where(q => q.id == id).FirstOrDefault();

                if (_delete != null)
                {
                    _delete.is_deleted = true;

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void LinkButton2_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _id = e.CommandArgument.toInt();

           

                var _ads = db.ads.Where(q => q.id == _id && q.is_active==false).FirstOrDefault();

                if(_ads !=null)
                {
                    _ads.is_active = true;

                    notifications _newNot = new notifications();
                    _newNot.created_time = DateTime.Now;
                    _newNot.member_id = _ads.member_id;
                    _newNot.text = _ads.ads_name+" Başlıklı ilanınız yönetici tarafından Onaylandı.";

                    db.notifications.Add(_newNot);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void LinkButton3_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _id = e.CommandArgument.toInt();

                var _ads = db.ads.Where(q => q.id == _id && q.is_showcase == false).FirstOrDefault();

                if (_ads != null)
                {
                    _ads.is_showcase = true;


                    notifications _newNot = new notifications();
                    _newNot.created_time = DateTime.Now;
                    _newNot.member_id = _ads.member_id;
                    _newNot.text = _ads.ads_name + " Başlıklı ilanınız yönetici tarafından vitrin ilanı yapıldı";

                    db.notifications.Add(_newNot);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}