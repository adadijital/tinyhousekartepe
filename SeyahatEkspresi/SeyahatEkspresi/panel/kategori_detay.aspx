﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="kategori_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.kategori_detay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <asp:Repeater ID="rptLang" runat="server" OnItemDataBound="rptLang_ItemDataBound">
                            <ItemTemplate>
                                <asp:HyperLink ID="hypLangLink" runat="server" CssClass="btn bg-navy btn-flat"><%#Eval("lang_name") %></asp:HyperLink>

                            </ItemTemplate>
                        </asp:Repeater>
                    </h3>
                </div>
                <div class="box-body">
                   <!-- <div class="form-group">
                        <label for="exampleInputEmail1">Üst Kategori Seçin</label>
                        <asp:DropDownList ID="drpCategoryList" runat="server" CssClass="form-control"></asp:DropDownList>

                    </div>-->
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kategori Adı</label>
                        <asp:TextBox ID="txtCategoryName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Kategroi Anasayfa Resmi Seç</label>
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                    </div>

                    <div class="form-group" runat="server" id="pnlResim" visible="false">
                        <label for="exampleInputEmail1">Kategroi Anasayfa Resmi</label>
                        <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />
                    </div>



                    <div class="form-group">
                        <label for="exampleInputEmail1">Aktif / Pasif</label>
                        <asp:CheckBox ID="ckIsActive" runat="server" CssClass="form-control" />
                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>

    <!--    <div class="col-xs-8">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Alt Kategorileri</h3>
                </div>
          
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Kategori Adı</th>
                                <th>Durum</th>
                                <th>Dil</th>
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptCategory" runat="server" OnItemDataBound="rptCategory_ItemDataBound">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("category_id")%>'>
                                        <td><%#Eval ("category_name") %></td>
                                        <td><%#Eval ("active_status") %></td>

                                        <td>
                                            <asp:Repeater ID="rptLangs" runat="server" OnItemDataBound="rptLangs_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hypLangLink" runat="server" CssClass="btn bg-navy btn-flat"><%#Eval("lang_name") %></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
          
            </div>
        </div>-->
    </div>
</asp:Content>
