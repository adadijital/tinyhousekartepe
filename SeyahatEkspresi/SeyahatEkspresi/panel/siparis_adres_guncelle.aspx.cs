﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class siparis_adres_guncelle : Base
    {
        int OrderID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["order_id"] != null)
                {
                    OrderID = Request.QueryString["order_id"].toInt();
                    HyperLink1.NavigateUrl = "/panel/siparis_detay.aspx?order_id=" + OrderID;

                    if (!Page.IsPostBack)
                    {
                        var data = db.orders.Where(q => q.id == OrderID).FirstOrDefault();

                        if (data != null)
                        {
                            fillCities1();
                            fillCities2();

                            txtAd1.Text = data.shipping_name_surname;
                            txtAdres1.Text = data.shipping_addres;
                            txtTel1.Text = data.shipping_phone;

                            txtAd2.Text = data.billing_name_surname;
                            txtAdres2.Text = data.billing_addres;
                            txtTel2.Text = data.billing_phone;

                            dlSehir1.SelectedValue = data.shipping_city_id.ToString();
                            dlSehir2.SelectedValue = data.billing_city_id.ToString();

                            fillTowns1(data.shipping_city_id.toInt());
                            fillTowns2(data.billing_city_id.toInt());
                            dlIlce1.SelectedValue = data.shipping_town_id.ToString();
                            dlIlce2.SelectedValue = data.billing_town_id.toInt().ToString();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }


        void fillCities1()
        {
            try
            {
                dlSehir1.DataSource = db.geo_cities
                    .Select(s => new
                    {
                        s.id,
                        s.city_name
                    })
                    .OrderBy(o => o.city_name)
                    .ToList();
                dlSehir1.DataTextField = "city_name";
                dlSehir1.DataValueField = "id";
                dlSehir1.DataBind();
            }
            catch (Exception ex) { Log.Add(ex); }
        }

        void fillCities2()
        {
            try
            {
                dlSehir2.DataSource = db.geo_cities
                    .Select(s => new
                    {
                        s.id,
                        s.city_name
                    })
                    .OrderBy(o => o.city_name)
                    .ToList();
                dlSehir2.DataTextField = "city_name";
                dlSehir2.DataValueField = "id";
                dlSehir2.DataBind();
            }
            catch (Exception ex) { Log.Add(ex); }
        }

        protected void dlSehir1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                fillTowns1(dlSehir1.SelectedValue.toInt());
            }
            catch { }
        }

        protected void dlSehir2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                fillTowns2(dlSehir2.SelectedValue.toInt());
            }
            catch { }
        }

        void fillTowns1(int CityId)
        {
            try
            {
                dlIlce1.DataSource = db.geo_town
                    .Where(q => q.city_id == CityId)
                    .Select(s => new
                    {
                        s.id,
                        s.town_name
                    })
                    .OrderBy(o => o.town_name)
                    .ToList();
                dlIlce1.DataTextField = "town_name";
                dlIlce1.DataValueField = "id";
                dlIlce1.DataBind();
            }
            catch (Exception ex) { Log.Add(ex); }
        }

        void fillTowns2(int CityId)
        {
            try
            {
                dlIlce2.DataSource = db.geo_town
                    .Where(q => q.city_id == CityId)
                    .Select(s => new
                    {
                        s.id,
                        s.town_name
                    })
                    .OrderBy(o => o.town_name)
                    .ToList();
                dlIlce2.DataTextField = "town_name";
                dlIlce2.DataValueField = "id";
                dlIlce2.DataBind();
            }
            catch (Exception ex) { Log.Add(ex); }
        }

        protected void btnGuncelleTeslimat_Click(object sender, EventArgs e)
        {
            try
            {
                orders o = db.orders.Where(q => q.id == OrderID).FirstOrDefault();

                if (o != null)
                {
                    if (string.IsNullOrEmpty(txtAd1.Text.Trim()))
                    {
                        Alert("(Teslimat adresi) Adı alanı boş geçilemez.");
                    }
                    else if (!dlSehir1.SelectedValue.isNumeric())
                    {
                        Alert("(Teslimat adresi)  Şehir seçmelisiniz.");
                    }
                    else if (!dlIlce1.SelectedValue.isNumeric())
                    {
                        Alert("(Teslimat adresi) İlçe seçmelisiniz.");
                    }
                    else if (string.IsNullOrEmpty(txtAdres1.Text.Trim()))
                    {
                        Alert("(Teslimat adresi) Adres alanı boş geçilemez.");
                    }
                    else if (string.IsNullOrEmpty(txtTel1.Text.Trim()))
                    {
                        Alert("(Teslimat adresi) Telefon alanı boş geçilemez.");
                    }
                    else if (txtAd1.Text.Trim() != "" && txtAdres1.Text.Trim() != "" && dlSehir1.SelectedValue.isNumeric() && txtTel1.Text.Trim() != "")
                    {
                        o.shipping_name_surname = txtAd1.Text.Trim();
                        o.shipping_city_id = dlSehir1.SelectedValue.toInt();
                        o.shipping_town_id = dlIlce1.SelectedValue.toInt();
                        o.shipping_addres = txtAdres1.Text.Trim();
                        o.shipping_phone = txtTel1.Text.Trim();

                        if (db.SaveChanges() > 0)
                            Response.Redirect("/panel/siparis_detay.aspx?order_id=" + OrderID);
                        else
                            Alert("Teslimat adresi güncellenemedi.");
                    }
                }
                else
                    Alert("Düzenlenecek teslimat adresi bulunamadı.");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void btnGuncelleFatura_Click(object sender, EventArgs e)
        {
            try
            {
                orders o = db.orders.Where(q => q.id == OrderID).FirstOrDefault();

                if (o != null)
                {
                    if (string.IsNullOrEmpty(txtAd2.Text.Trim()))
                    {
                        Alert("(Fatura adresi) Adı alanı boş geçilemez.");
                    }
                    else if (!dlSehir2.SelectedValue.isNumeric())
                    {
                        Alert("(Fatura adresi) Şehir seçmelisiniz.");
                    }
                    else if (!dlIlce2.SelectedValue.isNumeric())
                    {
                        Alert("(Fatura adresi) İlçe seçmelisiniz.");
                    }
                    else if (string.IsNullOrEmpty(txtAdres2.Text.Trim()))
                    {
                        Alert("(Fatura adresi) Adres alanı boş geçilemez.");
                    }
                    else if (string.IsNullOrEmpty(txtTel2.Text.Trim()))
                    {
                        Alert("(Fatura adresi) Telefon alanı boş geçilemez.");
                    }
                    else if (txtAd2.Text.Trim() != ""
                        && txtAdres2.Text.Trim() != ""
                        && txtTel2.Text.Trim() != "")
                    {
                        o.billing_name_surname = txtAd2.Text.Trim();
                        o.billing_city_id = dlSehir2.SelectedValue.toInt();
                        o.billing_addres = txtAdres2.Text.Trim();
                        o.billing_town_id = dlIlce2.SelectedValue.toInt();
                        o.billing_phone = txtTel2.Text.Trim();
                    }

                    if (db.SaveChanges() > 0)
                        Response.Redirect("detailOrder.aspx?order_id=" + OrderID);
                    else
                        Alert("(Fatura adresi) Telefon alanı boş geçilemez.");
                }
                else
                    Alert("(Fatura adresi) Telefon alanı boş geçilemez.");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}