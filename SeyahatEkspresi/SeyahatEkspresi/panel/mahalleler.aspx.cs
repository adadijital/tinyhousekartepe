﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class mahalleler : Base
    {
        int town_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["town_id"] != null)
                {
                    town_id = Request.QueryString["town_id"].toInt();
                    var _List = db.geo_quarter.Where(q => q.town_id== town_id).ToList();

                    if (_List.Count() > 0)
                    {
                        rptQuarter.DataSource = _List;
                        rptQuarter.DataBind();
                    }
                }


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtTownName.Text.Trim()))
                {
                    geo_quarter _newQuarter = new geo_quarter();
                    _newQuarter.quarter_name = txtTownName.Text.Trim();
                    _newQuarter.town_id = town_id;

                    db.geo_quarter.Add(_newQuarter);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                    else
                        Alert("İlçe Eklenemedi");
                }
                else
                    Alert("İlçe adı girmelisiniz.");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int id = e.CommandArgument.toInt();
                var _delete = db.geo_quarter.Where(q => q.id == id).FirstOrDefault();

                if (_delete != null)
                {
                    db.geo_quarter.Remove(_delete);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}