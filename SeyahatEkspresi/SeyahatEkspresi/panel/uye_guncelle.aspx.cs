﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class uye_guncelle :Base
    {
        int member_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["member_id"]))
                {
                    member_id = Request.QueryString["member_id"].toInt();
                    if (!Page.IsPostBack)
                        _fillPage();
                }
                else
                    Response.Redirect("/", false);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        private void _fillPage()
        {
            var _detail = db.members.Where(q => q.id == member_id).FirstOrDefault();

            if (_detail != null)
            {
                txtMemberName.Text = _detail.name_surname;
                txtEmail.Text = _detail.email;
              
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            var _detail = db.members.Where(q => q.id == member_id).FirstOrDefault();

            if (_detail != null)
            {
                _detail.name_surname = txtMemberName.Text.Trim();
                _detail.email = txtEmail.Text.Trim();

                if (db.SaveChanges() > 0)
                    Response.Redirect("/panel/uye_detay.aspx?member_id="+member_id,false);

            }
        }
    }
}