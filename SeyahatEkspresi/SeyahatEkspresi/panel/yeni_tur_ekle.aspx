﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="yeni_tur_ekle.aspx.cs" Inherits="SeyahatEkspresi.panel.yeni_tur_ekle" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Yeni Tur Ekle</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tur Kategorisi</label>
                        <asp:RequiredFieldValidator ID="DropDownListRequiredFieldValidator" runat="server" CssClass="randevuUyari"
                            ControlToValidate="drpCategory"
                            InitialValue="0"
                            ValidationGroup="validateCredential2"
                            ErrorMessage="Boş bırakılamaz">                              
                        </asp:RequiredFieldValidator>
                        <asp:DropDownList ID="drpCategory" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tur Başlığı</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtTitle" ID="RequiredFieldValidator1" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Tur Başlığı *" ID="txtTitle" MaxLength="250" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Üstü Çizili Fiyat</label>
                        <asp:TextBox runat="server" placeholder="Üstü Çizili Fiyat" ID="txtMarketPrice" MaxLength="250" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Fiyat</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtSellPrice" ID="RequiredFieldValidator2" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Fiyat" ID="txtSellPrice" MaxLength="250" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kaç Gün Kaç Gece</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtTime" ID="RequiredFieldValidator3" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Kaç Gün Kaç Gece" ID="txtTime" MaxLength="250" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Otel Durumu</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtHotelStatus" ID="RequiredFieldValidator4" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Örn : 4 Yıldızlı Otel" ID="txtHotelStatus" MaxLength="250" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kaç Kişi</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtPersonalCount" ID="RequiredFieldValidator5" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Örn : İki Yetişkin" ID="txtPersonalCount" MaxLength="250" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Araç Türü</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtVehicleType" ID="RequiredFieldValidator6" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Örn : Otobüs" ID="txtVehicleType" MaxLength="250" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tur Açıklama</label>
                        <CKEditor:CKEditorControl ID="txtDesc" Width="100%" Height="300" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                    </div>

                      <div class="form-group">
                                    <label for="exampleInputEmail1">Resimler</label>
                                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" AllowMultiple="true" />
                                </div>



                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Yeni Tur Ekle" class="btn btn-primary pull-right" ValidationGroup="validateCredential2" OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
