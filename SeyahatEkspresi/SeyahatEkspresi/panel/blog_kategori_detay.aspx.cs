﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class blog_kategori_detay : Base
    {
        byte lang_id;
        int category_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["category_id"]))
                {
                    category_id = Request.QueryString["category_id"].toInt();
                    lang_id = Request.QueryString["lang_id"].toByte();

                    var _detail = db.blog_category_lang.Where(q => q.lang_id == lang_id && q.blog_category_id == category_id).Select(s => new
                    {
                        s.blog_category_id,
                        s.category_name,
                        s.blog_category.display_order,
                        active_status = s.blog_category.is_active ? "Aktif" : "Pasif",
                        s.blog_category.is_active,
                    }).OrderBy(o => o.display_order).FirstOrDefault();

                    if (!IsPostBack)
                    {
                        txtCategoryName.Text = _detail.category_name;
                        ckIsActive.Checked = _detail.is_active;
                    }

                    rptLangs.DataSource = db.lang.ToList();
                    rptLangs.DataBind();
                }
                else
                    Response.Redirect("/panel/default.aspx", false);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCategoryName.Text.Trim()))
                {
                    var _detail = db.blog_category_lang.Where(q => q.lang_id == lang_id && q.blog_category_id == category_id).FirstOrDefault();

                    if (_detail != null)
                    {
                        _detail.category_name = txtCategoryName.Text.Trim();
                        _detail.blog_category.is_active = ckIsActive.Checked;

                        if (db.SaveChanges() > 0)
                            ReloadPage();
                    }
                }
                else
                    Alert("Kategori adı girmelisiniz.");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void rptLangs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hypLangLink = (HyperLink)e.Item.FindControl("hypLangLink");
            int lang = DataBinder.Eval(e.Item.DataItem, "id").toInt();

            hypLangLink.Attributes.Add("lid", lang.ToString());
            hypLangLink.NavigateUrl = "blog_kategori_detay.aspx?category_id=" + category_id + "&lang_id=" + lang.ToString();

            if (lang_id == DataBinder.Eval(e.Item.DataItem, "id").toShort())
            {
                hypLangLink.CssClass = "btn btn-flat bg-navy disabled color-palette";

            }
        }
    }
}