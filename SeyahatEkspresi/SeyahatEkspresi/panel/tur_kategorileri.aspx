﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="tur_kategorileri.aspx.cs" Inherits="SeyahatEkspresi.panel.tur_kategorileri" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="row">
                <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Yeni Tur Kategorisi Ekle</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tur Kategori Adı</label>
                        <asp:TextBox ID="txtCategoryName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Aktif / Pasif</label>
                        <asp:CheckBox ID="ckIsActive" runat="server" CssClass="form-control" />
                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Kaydet" class="btn btn-primary" OnClick="btnEkle_Click"/><br />
                </div>
            </div>
        </div>

              <div class="col-xs-8">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Kategori Yönetimi</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Kategori Adı</th>
                                <th>Durum</th>
                                <th>Dil</th>
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptCategory" runat="server">
                                <ItemTemplate>
                                    <tr id='Satir<%#Eval("id")%>'>
                                        <td><%#Eval ("category_name") %></td>
                                        <td><%#Eval ("active_status") %></td>
                                   
                                        <td>
                                            <a href="tur_kategori_detay.aspx?category_id=<%#Eval("id")%>" class="btn bg-navy btn-flat">Detay</a>
                                    
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>


          </div>
</asp:Content>
