﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="ilan_bilgi_tanimlari.aspx.cs" Inherits="SeyahatEkspresi.panel.ilan_bilgi_tanimlari" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Yeni İlan Bilgi Alanı Ekle</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Kategori Seçin </label>
                        <asp:DropDownList ID="drpCategoryList" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Bilgi Alanı Adı </label>
                        <asp:TextBox ID="txtPropertyName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Ekle" class="btn btn-primary"  OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">İlan Özellikler</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Özellik Adı</th>
                                <th>Kategori</th>
                                <th>Detay</th>
                                <th>Sil</th>
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptDefinition" runat="server" >
                                <ItemTemplate>
                                    <tr id='satir'>
                                        <td><%#Eval("info_name") %></td>
                                           <td><%#Eval("category_name") %> </td>
                                        <td>
                                        <a href="ilan_bilgi_tanim_detay.aspx?definition_id=<%#Eval("ad_info_id") %>&lang_id=1" class="btn bg-navy btn-flat">Detay </a>
                                                  
                                   
                                        </td>
                                        <td> 
                                                    <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command"  CommandArgument='<%#Eval("ad_info_id") %>'  OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</asp:Content>
