﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="uye_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.uye_detay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h3>
            <asp:Literal ID="ltrHeaderNameSurname" runat="server"></asp:Literal>
            - (
            <asp:Literal ID="ltrHeaderAccountType" runat="server"></asp:Literal>)
        </h3>
    </section>
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Ad Soyad </b><a class="pull-right">
                                <asp:Literal ID="ltrFullName" runat="server"></asp:Literal>
                            </a>
                        </li>

                        <li class="list-group-item">
                            <b>Hesap Tipi </b><a class="pull-right">
                                <asp:Literal ID="ltrHesapTipi" runat="server"></asp:Literal>
                            </a>
                        </li>

                        <asp:Panel ID="pnlKurumsal" runat="server" Visible="false">

                            <li class="list-group-item">
                                <b>Firma Adı </b><a class="pull-right">
                                    <asp:Literal ID="ltrCompanyName" runat="server"></asp:Literal>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <b>Vergi Dairesi </b><a class="pull-right">
                                    <asp:Literal ID="ltrTaxOffice" runat="server"></asp:Literal>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <b>Vergi No</b><a class="pull-right">
                                    <asp:Literal ID="ltrTaxNumber" runat="server"></asp:Literal>
                                </a>
                            </li>
                        </asp:Panel>

                        <li class="list-group-item">
                            <b>Email </b><a class="pull-right">
                                <asp:Literal ID="ltrEmail" runat="server"></asp:Literal>
                            </a>
                        </li>
                             <li class="list-group-item">
                            <b>Telefon </b><a class="pull-right">
                                <asp:Literal ID="ltrPhone" runat="server"></asp:Literal>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <b>Kayıt Tarihi </b><a class="pull-right">
                                <asp:Literal ID="ltrCreatedDate" runat="server"></asp:Literal></a>
                        </li>
                        <li class="list-group-item">
                            <b>Kayıt Olduğu IP </b><a class="pull-right">
                                <asp:Literal ID="ltrCreatedIp" runat="server"></asp:Literal></a>
                        </li>
                        <li class="list-group-item">
                            <b>Durum </b><a class="pull-right">
                                <asp:Literal ID="ltrActiveStatus" runat="server"></asp:Literal></a>
                        </li>


                    </ul>
                    <div class="box-tools" data-toggle="tooltip" title="" data-original-title="Üyeyi pasif yaparsanız yeniden aktif edene kadar sisteme giriş yapamayacaktır.">
                        <asp:Button ID="btnActive" runat="server" Text="Üye Aktif Et" class="btn btn-success btn-block" OnClick="btnActive_Click" /><br />
                        <asp:Button ID="btnPasif" runat="server" Text="Üye Pasife Al" class="btn btn-danger btn-block" OnClick="btnPasif_Click" />
                        <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-warning btn-block">Üye Bilgilerini Güncelle</asp:HyperLink>
                    </div>
                    <!-- /.box-body -->
                </div>

                <br />

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Şifre Sıfırlama İşlemi</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-book margin-r-5"></i>Yeni Şifre</strong>
                        <p class="text-muted">
                            <asp:TextBox ID="txtPasswordReset" runat="server" CssClass="form-control"></asp:TextBox>
                        </p>
                        <asp:Button ID="btnPasswordReset" runat="server" Text="Şifre Sıfırla" class="btn btn-primary btn-block" OnClick="btnPasswordReset_Click" />

                    </div>
                    <!-- /.box-body -->
                </div>



            </div>

        </div>

        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#ilanlar" data-toggle="tab">Verdiği İlanlar</a></li>
                       <%--<li class=""><a href="#sepet" data-toggle="tab">Sepetindeki Ürünler</a></li>--%>
                      
                        <li ><a href="#favori" data-toggle="tab">Favori İlanlar</a></li>
                    <li class=""><a href="#girislog" data-toggle="tab">Giriş Kayıtları</a></li>
                   

                </ul>
                <div class="tab-content">


                    <div class="tab-pane active" id="ilanlar">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <table class="table table-bordered tblortala">
                                    <thead>
                                        <tr>
                                            <th>Başlık</th>
                                            <th>İlan Tipi</th>
                                            <th>Tarih</th>
                                            <th>Fiyat</th>
                                            <th>İncele</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptAdvertList" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("advert_name") %></td>
                                                    <td><%#Eval ("type_status") %></td>
                                                    <td><%#Eval("created_time") %></td>
                                                    <td><%#Eval("sell_price") %></td>
                                                    <td><a href="/panel/ilan_detay.aspx?ads_id=<%#Eval("advert_id") %>&lang_id=1" class="btn btn-info">İncele</a></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    
                    <div class="tab-pane" id="favori">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <table class="table table-bordered tblortala">
                                    <thead>
                                        <tr>
                                            <th>Başlık</th>
                                            <th>İlan Tipi</th>
                                            <th>Tarih</th>
                                            <th>Fiyat</th>
                                            <th>İncele</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptWishList" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("advert_name") %></td>
                                                    <td><%#Eval ("type_status") %></td>
                                                    <td><%#Eval("created_time") %></td>
                                                    <td><%#Eval("sell_price") %></td>
                                                    <td><a href="/panel/ilan_detay.aspx?ads_id=<%#Eval("advert_id") %>&lang_id=1" class="btn btn-info">İncele</a></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                           <div class="tab-pane"  id="sepet">
                            <div class="box box-primary">
                                <div class="box-body table-responsive">
                                    <table class="table table-bordered ">
                                        <tbody>
                                            <tr>
                                                <th>Ürün Adı</th>
                                                <th>Satış Fiyatı</th>
                                                <th>Adet</th>
                                                <th>Ara Toplam</th>

                                            </tr>
                                            <asp:Repeater ID="rptMemberBasket" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <span class="prdItem"><%#Eval("product_name") %></span>
                                                        </td>
                                                      
                                                        <td>
                                                            <%#Eval("unit_sell_price") %> TL
                                                        </td>
                                                        <td><%#Eval("quantity") %> Adet</td>
                                                        <td><%#Eval("total_sell_price") %> TL</td>


                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <div class="tab-pane" id="girislog">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <table class="table table-bordered tblortala">
                                    <thead>
                                        <tr>
                                            <th>Tarih</th>
                                            <th>IP</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptLoginLog" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("created_date") %></td>
                                                    <td><%#Eval("ip_address") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
          <div class="col-md-3"> &nbsp;</div>
                <div class="col-md-9">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Banka Hesapları</h3>
                                      <div class="box-tools"></div>
                        </div>
              
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered ">
                                <tbody>
                                    <tr>
                                        <th>Banka Adı</th>
                                        <th>Hesap Adı</th>
                                   <th>IBAN</th>


                                    </tr>
                                    <asp:Repeater ID="rptBankList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval("bank_name") %></td>
                                                <td><%#Eval("account_name") %></td>
                                                <td><%#Eval("iban") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
    </div>
</asp:Content>
