﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="sehirler.aspx.cs" Inherits="SeyahatEkspresi.panel.sehirler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="row">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Yeni Şehir Ekle</h3>
                </div>
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Şehir Adı </label>
                        <asp:TextBox ID="txtCityName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Ekle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Şehirler</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Şehir Adı</th>
                                <th>İlçeler</th>
                            </tr>
                        </thead>

                        <tbody id="sortableResim">
                            <asp:Repeater ID="rptCity" runat="server">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>
                                        <td>
                                            <%#Eval("city_name") %>
                                        </td>
                                        <td>
                                            <a href="/panel/ilceler.aspx?city_id=<%#Eval("id") %>" class="btn bg-navy btn-flat">İlçeler</a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</asp:Content>
