﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class uyeler : Base
    {
        int type;
        int page_count;
        string pageUrl = "uyeler.aspx";
        int page_id = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["page"] != null)
                    page_id = Convert.ToInt32(Request.QueryString["page"]);



                int carpilmis = (page_id != 0) ? page_id * 100 : 0;
                page_count = 100;

                if (Request.QueryString["type"] != null)
                {
                    type = Request.QueryString["type"].toInt();

                    var _memberList = db.members.Where(q => q.is_deleted == false && q.account_type == type).Select(s => new
                    {
                        s.id,
                        s.created_time,
                        s.name_surname,
                        s.email,
                        s.phone,
                        account_type_text = s.account_type == 0 ? "Kişisel Hesap" : "Kurumsal Hesap",
                        active_status = s.is_active ? "Aktif" : "Pasif",

                    }).OrderByDescending(o => o.id).ToList();

                    if (_memberList.Count > 0)
                    {
                        int loop_count = _memberList.Count() / page_count;

                        if (_memberList.Count() % page_count != 0) loop_count++;

                        for (int i = 1; i <= loop_count; i++)
                        {

                            if (i == page_id)
                                ltrPaging.Text += "<li><a href=\"uyeler.aspx?type="+type+"&page=" + i + "class=\"btn\">" + i + "</a></li>";
                            else
                                ltrPaging.Text += "<li><a href=\"uyeler.aspx?type="+type+"&page=" + i + "\">" + i + "</a></li>";

                        }

                        rptUyeler.DataSource = _memberList.Skip((page_id - 1) * page_count).Take(page_count);
                        rptUyeler.DataBind();
                    }

                }
                else
                {
                    var _memberList = db.members.Where(q => q.is_deleted == false).Select(s => new
                    {
                        s.id,
                        s.created_time,
                        s.name_surname,
                        s.email,
                        s.phone,
                        account_type_text = s.account_type == 0 ? "Kişisel Hesap" : "Kurumsal Hesap",
                        active_status = s.is_active ? "Aktif" : "Pasif",

                    }).OrderByDescending(o => o.id).ToList();

                    if (_memberList.Count > 0)
                    {
                        int loop_count = _memberList.Count() / page_count;

                        if (_memberList.Count() % page_count != 0) loop_count++;

                        for (int i = 1; i <= loop_count; i++)
                        {

                            if (i == page_id)
                                ltrPaging.Text += "<li><a href=\"uyeler.aspx?page=" + i + "class=\"btn\">" + i + "</a></li>";
                            else
                                ltrPaging.Text += "<li><a href=\"uyeler.aspx?page=" + i + "\">" + i + "</a></li>";

                        }

                        rptUyeler.DataSource = _memberList.Skip((page_id - 1) * page_count).Take(page_count);
                        rptUyeler.DataBind();
                    }
                }
                 
             
            }
            catch (Exception ex)
            {

                Response.Write(ex);
            }
        }

        protected void btnArama_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
            {
                if (txtSearch.Text.Trim().Length > 0)
                {
                    //rptUyeler.DataSource = null;
                    //rptUyeler.DataBind();

                    string _searchKey = txtSearch.Text.Trim();

                    var _users = db.members.Where(q => q.is_deleted == false
                    && (q.name_surname).Contains(_searchKey) || q.email.Contains(_searchKey) || q.phone.Contains(_searchKey) || q.tck.Contains(_searchKey)
                    && q.is_deleted == false
                    ).Select(s => new
                    {
                        s.id,
                        s.created_time,
                        s.name_surname,
                        s.email,
                        s.phone,
                        account_type_text = s.account_type == 0 ? "Kişisel Hesap" : "Kurumsal Hesap",
                        active_status = s.is_active ? "Aktif" : "Pasif",
                        s.is_deleted
                    }).OrderByDescending(o => o.created_time).ToList();

                    if (_users.Count > 0)
                    {
                        int loop_count = _users.Count() / page_count;

                        if (_users.Count() % page_count != 0) loop_count++;

                        for (int i = 1; i <= loop_count; i++)
                        {

                            if (i == page_id)
                                ltrPaging.Text += "<li><a href=\"uyeler.aspx?page=" + i + "class=\"btn\">" + i + "</a></li>";

                            else
                                ltrPaging.Text += "<li><a href=\"uyeler.aspx?page=" + i + "\">" + i + "</a></li>";

                        }


                        rptUyeler.DataSource = _users.Skip((page_id - 1) * page_count).Take(page_count);
                        rptUyeler.DataBind();
                    }
                }
                else
                    Alert("Arama yapmak için en az 3 harf girmelisiniz.");
            }
            else
                Alert("Bir kelime girmelisiniz.");
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int member_id = e.CommandArgument.toInt();

                var _delete = db.members.Where(q => q.id == member_id).FirstOrDefault();

                if(_delete !=null)
                {
                    _delete.is_deleted = true;
                }

                if (db.SaveChanges() > 0)
                    ReloadPage();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}