﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="ilan_ekle.aspx.cs" Inherits="SeyahatEkspresi.panel.ilan_ekle1" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Kategori Seçiniz</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <label for="exampleInputEmail1">Kategori Listesi</label>

                                <asp:DropDownList ID="drpCategories" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="drpCategories_SelectedIndexChanged"></asp:DropDownList>
                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </div>

                </div>

            </div>

            <asp:UpdatePanel ID="pnlAdsDetail" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlDetail" runat="server" Visible="false">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">İlan Detayları</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">İlan Başlığı</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtAdvertTitle" ID="RequiredFieldValidator1" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="İlan Başlığı *" ID="txtAdvertTitle" MaxLength="150" CssClass="form-control" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kişi Sayısı</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtAdvertTitle" ID="RequiredFieldValidator2" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Kişi Sayısı *" ID="txtKisiSayisi" MaxLength="150" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Yatak Sayısı</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtYatakSayisi" ID="RequiredFieldValidator3" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Yatak Sayısı *" ID="txtYatakSayisi" MaxLength="150" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Banyo Sayısı</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtBanyoSayisi" ID="RequiredFieldValidator4" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Banyo Sayısı *" ID="txtBanyoSayisi" MaxLength="150" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Isınma Türü</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtisinmaTuru" ID="RequiredFieldValidator5" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Isınma Türü *" ID="txtisinmaTuru" MaxLength="150" CssClass="form-control" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Konum Durumu</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtKonumDurumu" ID="RequiredFieldValidator6" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Konum durumu * örn : Doğa içerisinde" ID="txtKonumDurumu" MaxLength="150" CssClass="form-control" />
                                </div>



                                <div class="form-group">
                                    <label for="exampleInputEmail1">İlan Açıklaması</label>
                                    <CKEditor:CKEditorControl ID="txtDesc" Width="100%" Height="300" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputEmail1">Üstü çizili fiyat</label>
                                    <asp:TextBox runat="server" placeholder="Üstü çizili fiyat *" ID="txtMarketPrice" CssClass="form-control" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Fiyat</label>
                                    <asp:TextBox runat="server" placeholder="Fiyat *" ID="txtAdvertPrice" CssClass="form-control" />
                                </div>

                                   <div class="form-group">
                                    <label for="exampleInputEmail1">Hafta Sonu Fiyatı</label>
                                    <asp:TextBox runat="server" placeholder="Hafta Sonu Fiyatı *" ID="txtWeekPrice" CssClass="form-control" />
                                </div>

                                <div class="row">
                                    <asp:Repeater ID="rptAdInfoDefinition" runat="server" OnItemDataBound="rptAdInfoDefinition_ItemDataBound">
                                        <ItemTemplate>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><%#Eval("info_name") %> </label>
                                                    <!--   <asp:TextBox ID="txtProperty" runat="server" CssClass="form-control"></asp:TextBox>-->
                                                    <asp:CheckBox ID="CheckBox1" runat="server" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Şehir</label>
                                    <asp:UpdatePanel ID="pnlCity" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpCityList" runat="server" OnSelectedIndexChanged="drpCityList_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">İlçe</label>
                                    <asp:UpdatePanel ID="pnlTown" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpTownList" runat="server" OnSelectedIndexChanged="drpTownList_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>



                                <div class="form-group">
                                    <label for="exampleInputEmail1">Adres</label>
                                    <asp:TextBox runat="server" placeholder="Adres *" ID="txtAdress" CssClass="form-control" />
                                </div>


                        </div>
                    </asp:Panel>
                </ContentTemplate>
            
            </asp:UpdatePanel>
                     <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Resimler</h3>
                            </div>
                            <div class="box-body">
                          <div class="form-group">
                                    <label for="exampleInputEmail1">Resimler</label>
                                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" AllowMultiple="true"/>
                                </div>
                                </div>
                         	    <div class="box-footer">
                                <asp:Button ID="btnEkle" runat="server" Text="Yeni İlan Ekle" class="btn btn-primary pull-right"  OnClick="btnEkle_Click" /><br />
                            </div>
                         </div>
        </div>

    </div>
</asp:Content>
