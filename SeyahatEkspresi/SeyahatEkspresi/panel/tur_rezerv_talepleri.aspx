﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="tur_rezerv_talepleri.aspx.cs" Inherits="SeyahatEkspresi.panel.tur_rezerv_talepleri" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tüm rezervasyon talepleri</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                        <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Tur Adı</th>
                                <th>Oluşturulma Tarihi</th>
                                <th>Durum</th>
                               
                                <th>Düzenle</th>
                              
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptRezerv" runat="server" OnItemDataBound="rptAds_ItemDataBound">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>
                                      
                                        <td>
                                            <a href="rezerv_detay.aspx?id=<%#Eval("id")%>">
                                                <%#Eval ("tour_name") %>
                                            </a>
                                        </td>

                                        <td><%#Eval ("created_time") %></td>
                                        <td>
                                            <asp:Literal ID="ltrActive" runat="server"></asp:Literal>
                                            <br />
                                            <br />
                                            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-xs btn-success" OnCommand="LinkButton2_Command"  CommandArgument='<%#Eval("id")%>'> Onayla</asp:LinkButton>
                                           <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-xs btn-danger" OnCommand="LinkButton3_Command" CommandArgument='<%#Eval("id")%>'> İptal Et</asp:LinkButton>

                                        </td>
                                      

                                        <td>
                                              <a target="_blank" href="tur_rezerv_detay.aspx?id=<%#Eval("id")%>" class="btn bg-navy btn-flat">
                                                  Detay
                                              </a>
                                        </td>
                                      
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                    </div>
                </div>
            </div>
            </div>
</asp:Content>
