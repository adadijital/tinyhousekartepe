﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class blog_yazilari : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _List = db.blog_lang.Where(q => q.lang_id == 1).Select(s=> new {
                s.blog_id,
                s.blog.blog_category.blog_category_lang.Where(q=> q.lang_id == 1).FirstOrDefault().category_name,
                s.blog.created_time,
                s.blog_title,
                s.blog.view_count,
            }).OrderByDescending(o=> o.blog_id).ToList();

            if(_List.Count()>0)
            {
                rptBlog.DataSource = _List;
                rptBlog.DataBind();
            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _id = e.CommandArgument.toInt();

                var _lang = db.blog_lang.Where(q => q.blog_id == _id).ToList();

                foreach (var item in _lang)
                {
                    db.blog_lang.Remove(item);
                }

                db.SaveChanges();



                var _remove = db.blog.Where(q => q.id == _id).FirstOrDefault();

                if(_remove !=null)
                {
                    db.blog.Remove(_remove);
                }

                if (db.SaveChanges() > 0)
                    ReloadPage();
            }
            catch (Exception ex)
            {

            }
        }
    }
}