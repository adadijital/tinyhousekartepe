﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class bulten_uyeleri : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _List = db.newsletter.ToList();

                if (_List != null)
                {
                    rptNews.DataSource = _List;
                    rptNews.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);

            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int id = e.CommandArgument.toInt();

                var _delete = db.newsletter.Where(q => q.id == id).FirstOrDefault();

                if (_delete != null)
                {
                    db.newsletter.Remove(_delete);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}