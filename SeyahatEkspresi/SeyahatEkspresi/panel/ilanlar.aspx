﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="ilanlar.aspx.cs" Inherits="SeyahatEkspresi.panel.ilanlar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">İlanlar</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>İlan Adı</th>

                       
                                <th>Kategori</th>
                                <th>Fiyat</th>
                                <th>Tarih</th>
                                <th>Durum</th>
                                <th>Vitrin İlanı mı ?</th>
                                <th>Düzenle</th>
                                <th>İşlem</th>
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptAds" runat="server" OnItemDataBound="rptAds_ItemDataBound">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("ads_id")%>'>
                                        <td>
                                            <a href="ilan_detay.aspx?ads_id=<%#Eval("ads_id")%>&lang_id=1">
                                                <%#Eval("ads_id")%>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="ilan_detay.aspx?ads_id=<%#Eval("ads_id")%>&lang_id=1">
                                                <%#Eval ("ads_name") %>
                                            </a>
                                        </td>

                                  
                                        <td><%#Eval ("category_name") %></td>
                                        <td><%#Eval ("sell_price") %></td>
                                        <td><%#Eval ("created_time") %></td>
                                        <td>
                                            <asp:Literal ID="ltrActive" runat="server"></asp:Literal>
                                            <br />
                                            <br />
                                            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-xs btn-success" OnCommand="LinkButton2_Command" CommandArgument='<%#Eval("ads_id")%>'> Onayla</asp:LinkButton>

                                        </td>
                                        <td>
                                            <asp:Literal ID="ltrVitrin" runat="server"></asp:Literal>
                                            <br />
                                            <br />
                                            <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-xs btn-success" OnCommand="LinkButton3_Command" CommandArgument='<%#Eval("ads_id")%>'> Vitrin İlanı Yap</asp:LinkButton>

                                        </td>

                                        <td>
                                            <asp:Repeater ID="rptLangs" runat="server" OnItemDataBound="rptLangs_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hypLangLink" runat="server" CssClass="btn bg-navy btn-flat">Düzenle</asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-s btn-danger" OnCommand="LinkButton1_Command" CommandArgument='<%#Eval("ads_id")%>'>Sil</asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</asp:Content>
