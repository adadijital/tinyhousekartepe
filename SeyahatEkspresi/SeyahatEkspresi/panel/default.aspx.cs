﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class _default : Base
    {
        AdvertManager _advertManager = new AdvertManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _memberList = db.members.Where(q => q.is_deleted == false).ToList();

                if (_memberList.Count() > 0)
                {
                    ltrMemberCount.Text = _memberList.Count.ToString();
                }
                else
                    ltrMemberCount.Text = "0";

                var _adsList = db.ads.Where(q => q.is_deleted == false && q.is_active && q.relation_ads_category.Count() > 0 && q.ads_images.Count() > 0).ToList();

                if (_adsList.Count() > 0)
                {
                    ltrAdsAmount.Text = _adsList.Sum(x => x.sell_price).ToString();
                    ltrAdsCount.Text = _adsList.Count.ToString();
                }
                else
                {
                    ltrAdsCount.Text = "0";
                    ltrAdsAmount.Text = "0";
                }

                var _category = db.categories.Where(q => q.is_active && q.parent_id == 0).ToList();

                if (_category.Count() > 0)
                {
                    ltrCategoryCount.Text = _category.Count.ToString();
                }
                else
                    ltrCategoryCount.Text = "0";



               

                var _rezerv = db.reservasiton_request.Where(q => q.is_approved == false && q.is_cancel == false).Select(s => new
                {
                    s.name_surname,
                    s.id,
                    s.start_date,
                    s.finish_date,
                    s.created_time,
                    s.phone,
                    s.ads.ads_lang.FirstOrDefault().ads_name,
                    s.ads.ads_images.FirstOrDefault().img_name,
                    ads_id = s.ads_id,
                    s.reserve_dates,
                    s.person_count,
                }).ToList();

                if(_rezerv.Count()>0)
                {
                    rptRezerv.DataSource = _rezerv;
                    rptRezerv.DataBind();
                }

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void rptLastAdvert_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltrActive = (Literal)e.Item.FindControl("ltrActive");
            string active_status = DataBinder.Eval(e.Item.DataItem, "active_status").ToString();
            if (active_status == "Aktif")
            {
                ltrActive.Text = "<span class='label label-success'>Aktif</span>";
            }
            else
            {
                ltrActive.Text = "<span class='label label-danger'>Pasif</span>";

            }
        }
    }
}