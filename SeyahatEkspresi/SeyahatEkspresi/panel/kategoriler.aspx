﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="kategoriler.aspx.cs" Inherits="SeyahatEkspresi.panel.kategoriler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
                <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Yeni Kategori Ekle</h3>
                </div>
                <div class="box-body">
                   <!-- <div class="form-group">
                        <label for="exampleInputEmail1">Üst Kategori Seçin</label>
                        <asp:DropDownList ID="drpCategoryList" runat="server" CssClass="form-control"></asp:DropDownList>
               
                    </div>-->
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kategori Adı</label>
                        <asp:TextBox ID="txtCategoryName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                         <div class="form-group">
                        <label for="exampleInputEmail1">Kategori Anasayfa Resmi Seç (370x285 pixel)</label>
                             <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Aktif / Pasif</label>
                        <asp:CheckBox ID="ckIsActive" runat="server" CssClass="form-control" />
                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Ekle" class="btn btn-primary"  OnClick="btnEkle_Click"/><br />
                </div>
            </div>
        </div>
        <div class="col-xs-8">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Kategori Yönetimi</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Kategori Adı</th>
                                <th>Durum</th>
                                <th>Detay</th>
                                   <th>İşlem</th>
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptCategory" runat="server">
                                <ItemTemplate>
                                    <tr id='Satir<%#Eval("CategoryID")%>'>
                                        <td><%#Eval ("CategoryName") %></td>
                                        <td><%#Eval ("ActiveStatus") %></td>
                                   
                                        <td>
                                            <a href="kategori_detay.aspx?category_id=<%#Eval("CategoryID")%>&lang_id=1" class="btn bg-navy btn-flat">Detay</a>
                                    
                                        </td>
                                          <td>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-s btn-danger" OnCommand="LinkButton1_Command"  CommandArgument='<%#Eval("CategoryID")%>'>Sil</asp:LinkButton>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

          <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }

        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

            #sortable li {
                display: block;
                width: 23%;
                height: 100%;
                float: left;
                margin: 10px;
            }
    </style>

    <script type="text/javascript">
        $(function () {
            $("#SliderResim").sortable({
                opacity: 0.8,
                cursor: 'move',
                update: function () {
                    var Satir = $(this).sortable("toArray");

                    $.ajax({
                        type: "POST",
                        url: "kategoriler.aspx/Sirala",
                        data: "{'Satir': '" + Satir + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) { location.reload(); }


                    });
                }
            });


        });


    </script>


</asp:Content>
