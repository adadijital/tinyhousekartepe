﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class mail_ayarlari : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _Email = db.mail_setting.FirstOrDefault();

                if(_Email !=null)
                {
                    if(!IsPostBack)
                    {
                        txtSendMail.Text = _Email.send_mail;
                        txtSendPassword.Text = _Email.send_password;
                        txtSendPort.Text = _Email.send_port;
                        txtSendSMTP.Text = _Email.send_smtp;
                        txtReceiveMail.Text = _Email.receive_mail;
                    }
                }
            }
            catch (Exception ex)
            {

               
            }
        }

        protected void btnGuncelle_Click(object sender, EventArgs e)
        {
            var _Email = db.mail_setting.FirstOrDefault();

            if (_Email != null)
            {

                _Email.send_mail = txtSendMail.Text.Trim();
                _Email.send_password = txtSendPassword.Text.Trim();
                _Email.send_port = txtSendPort.Text.Trim();
                _Email.send_smtp = txtSendSMTP.Text.Trim();
                _Email.receive_mail = txtReceiveMail.Text.Trim();

                if (db.SaveChanges() > 0)
                    ReloadPage();


            }
        }
    }
}