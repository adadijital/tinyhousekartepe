﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="blog_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.blog_detay" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
              <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                          <asp:Repeater ID="rptLangs" runat="server" OnItemDataBound="rptLangs_ItemDataBound">
                            <ItemTemplate>
                                <asp:HyperLink ID="hypLangLink" runat="server" CssClass="btn bg-navy btn-flat"><%#Eval("lang_name") %></asp:HyperLink>
                            </ItemTemplate>
                        </asp:Repeater>
                    </h3>
                </div>
                <div class="box-body">
                          <div class="form-group">
                        <label for="exampleInputEmail1">Kategori Seçmelisiniz </label>
                              <asp:DropDownList ID="drpCategory" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Başlık </label>
                        <asp:TextBox ID="txtBlogTitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                       <div class="form-group">
                        <label for="exampleInputEmail1">Yazı </label>
                                    <CKEditor:CKEditorControl ID="txtBlogDesc" Width="100%" Height="300" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                    
                    </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Resim </label>
                          <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                          <br />
                          <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />
                    </div>
                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnEkle_Click"  /><br />
                </div>
            </div>
        </div>
          </div>
</asp:Content>
