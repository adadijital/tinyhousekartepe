﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="kullanici_yorumlari.aspx.cs" Inherits="SeyahatEkspresi.panel.kullanici_yorumlari" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Kullanıcı Yorumları</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Üye Adı</th>
                                <th>Tarih</th>
                                <th>Yorum</th>
                                <%--<th>Onay Durumu</th>--%>
                                <th>Sil</th>

                            </tr>
                        </thead>

                        <tbody id="sortableResim">
                            <asp:Repeater ID="rptCustomerComents" runat="server" OnItemDataBound="rptCustomerComents_ItemDataBound">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>
                                        <td>
                                            <%#Eval("member_name") %>
                                        </td>
                                        <td>
                                            <%#Eval("created_time") %>
                                        </td>
                                      
                                          <td>
                                            <%#Eval("comment_desc") %>
                                        </td>
                                      
                                       <!--   <td>
                                            <%#Eval("approved_status") %>
                                                 <br /> <br />
                                            <asp:LinkButton ID="LinkButton2" runat="server" Visible="false" CssClass="btn btn-xs btn-success"  OnCommand="LinkButton2_Command" CommandArgument='<%#Eval("id")%>'> Onayla</asp:LinkButton>
                               
                                        </td>-->
                                      

                                        <td>
                                            <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command" CommandArgument='<%#Eval ("id") %>' OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

</asp:Content>
