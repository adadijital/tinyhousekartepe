﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="bulten_uyeleri.aspx.cs" Inherits="SeyahatEkspresi.panel.bulten_uyeleri" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Bülten Üyeleri</h3>
                </div>
                           <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Tarih</th>
                                <th>Sil</th>
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptNews" runat="server">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>
                                        <td><%#Eval("email") %> </td>
                                          <td><%#Eval("created_time") %> </td>
                                        <td> 
                                               <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command"  CommandArgument='<%#Eval ("id") %>' OnClientClick="return confirm('Silmek istediğinizden emin misiniz ?');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
