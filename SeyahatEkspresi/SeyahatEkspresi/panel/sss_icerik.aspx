﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="sss_icerik.aspx.cs" Inherits="SeyahatEkspresi.panel.sss_icerik" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Yeni SSS Ekle</h3>
                </div>
                <div class="box-body">
              

                    <div class="form-group">
                        <label for="exampleInputEmail1">SSS Başlık </label>
                        <asp:TextBox ID="txtSSSTitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">SSS İçerik </label>
         

                            <CKEditor:CKEditorControl ID="txtSSSDesc" Width="100%" Height="300" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Ekle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">SSS İçerik</h3>

                </div>
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                       
                                <th>Başlık</th>
                                <th>Detay
                                </th>
                                <th>Sil</th>
                            </tr>
                        </thead>
                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptContent" runat="server">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>
                                           
                                        <td><a href="sss_detay.aspx?sss_id=<%#Eval("id") %>"><%#Eval("sss_title") %></a> </td>
                                        <td>
                                            <a href="sss_detay.aspx?sss_id=<%#Eval("sss_id") %>&lang_id=1" class="btn bg-navy btn-flat">Detay </a>
                                        </td>

                                        <td>

                                            <asp:LinkButton runat="server" ID="LinkButton1"  OnCommand="LinkButton1_Command" CommandArgument='<%#Eval ("sss_id") %>' OnClientClick="return confirm('Silmek istediğinizden emin misiniz ?');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
