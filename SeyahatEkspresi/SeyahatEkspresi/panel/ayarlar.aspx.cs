﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class ayarlar : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _detail = db.settings.FirstOrDefault();

            if(!IsPostBack)
            {
                txtOfficeAddres.Text = _detail.office_address;
                txtEposta1.Text = _detail.email;
                txtEposta2.Text = _detail.email2;
                txtTelefon1.Text = _detail.phone;
                txtTelefon2.Text = _detail.phone2;
                txtFacebook.Text = _detail.facebook_link;
                txtTwitter.Text = _detail.twitter_link;
                txtInstagram.Text = _detail.instagram_link;
                FooterHakkimizda.Text = _detail.footer_text;
            }
        }

        protected void btnGuncelle_Click(object sender, EventArgs e)
        {
            try
            {
                var _detail = db.settings.FirstOrDefault();

                if(_detail !=null)
                {
                    _detail.office_address = txtOfficeAddres.Text.Trim();
                    _detail.email = txtEposta1.Text.Trim();
                    _detail.email2 = txtEposta2.Text.Trim();
                    _detail.phone = txtTelefon1.Text.Trim();
                    _detail.phone2 = txtTelefon2.Text.Trim();
                     _detail.facebook_link = txtFacebook.Text.Trim();
                    _detail.twitter_link = txtTwitter.Text.Trim();
                    _detail.instagram_link = txtInstagram.Text.Trim();
                    _detail.footer_text = FooterHakkimizda.Text.Trim();

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                    else
                        Alert("Bir hata oluştu");
                }

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}