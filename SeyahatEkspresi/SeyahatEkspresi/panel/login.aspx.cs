﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class login : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {


                if (!string.IsNullOrEmpty(txtUsername.Text.Trim()) && !string.IsNullOrEmpty(txtPassword.Text.Trim()))
                {
                    var login = db.panel_users.Where(q => q.user_name == txtUsername.Text.Trim() && q.password == txtPassword.Text.Trim()).FirstOrDefault();

                    if (login != null)
                    {
                        HttpCookie myCookie = new HttpCookie(System.Configuration.ConfigurationManager.AppSettings["site_name"].ToString().ToURL() + "PanelLogin");
                        myCookie["member_id"] = login.id.ToString();
                        myCookie.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(myCookie);
                        Response.Redirect("/panel/default.aspx", false);
                    }

                    

                }
                else
                {

                    Alert("Lütfen kullanıcı adı ve şifrenizi boş bırakmayınız.");

                }

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {


            }
        }
        public bool Login(string Email, string Password)
        {

            try
            {
                // Password = Password.ToMD5();
                var login = db.panel_users.Where(q => q.user_name == Email && q.password == Password).FirstOrDefault();

                if (login == null)
                    return false;
                else
                {
                    HttpContext.Current.Session["admin_login"] = true;
                    HttpContext.Current.Session["admin_id"] = login.id;
                    HttpContext.Current.Session["admin_name"] = login.user_name;
                    return true;
                }
            }
            catch
            {
                return false;
            }

        }
        public bool IsLoggedIn()
        {
            try
            {
                if (HttpContext.Current.Session["admin_login"] == null)
                {

                    return false;
                }
                else
                    if (Convert.ToBoolean(HttpContext.Current.Session["admin_login"]))
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}