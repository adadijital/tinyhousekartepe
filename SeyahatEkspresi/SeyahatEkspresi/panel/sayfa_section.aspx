﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="sayfa_section.aspx.cs" Inherits="SeyahatEkspresi.panel.sayfa_section" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Sayfa İçi Bölümler</h3>
                </div>
                 <div class="box-body">
                    <div class="row margin-bottom">
                        <div class="col-sm-12">
                            <div class="row">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>#</th>
                                            <th>Bölüm</th>
                                            <th>Detay</th>
                                        </tr>
                                    </tbody>
                                    <tbody id="sortable">
                                        <asp:Repeater ID="rptPageSectionList" runat="server" >
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: left"><%#Eval("id") %></td>
                                                    <td style="text-align: left"><%#Eval("section_name")%></td>
                                                    <td>
                                                      <a href="sayfa_section_detay.aspx?page_id=<%#Eval("page_id") %>&section_group=<%#Eval("section_group") %>&lang_id=1" class="btn bg-navy btn-flat"> 
                                                          Detay
                                                      </a>
                                                           
                                                       
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
