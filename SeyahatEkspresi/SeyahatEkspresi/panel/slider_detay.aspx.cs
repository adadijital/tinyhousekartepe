﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class slider_detay : Base
    {
        int slider_id;
        byte lang_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["slider_id"]))
            {
                slider_id = Request.QueryString["slider_id"].toByte();
                lang_id = Request.QueryString["lang_id"].toByte();

                if (!IsPostBack)
                {
                    var _detail = db.slider_lang.Where(q => q.slider_id == slider_id && q.lang_id == lang_id).FirstOrDefault();

                    if (_detail != null)
                    {
                        txtBaslik.Text = _detail.slider_title;
                        txtDesc.Text = _detail.slider_desc;
                        ckIsActive.Checked = _detail.slider.is_active;
                        Image1.ImageUrl = "/uploads/slider/" + _detail.img_path;
                        txtBaslik.Text = _detail.slider_title;
                        txtDesc.Text = _detail.slider_desc;
                        txtLink.Text = _detail.slider.link;
                    }
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var _detail = db.slider_lang.Where(q => q.slider_id == slider_id && q.lang_id == lang_id).FirstOrDefault();

            if (_detail != null)
            {
                _detail.slider_title = txtBaslik.Text;
                _detail.slider_desc = txtDesc.Text;
                _detail.slider.is_active = ckIsActive.Checked;
                _detail.slider.link = txtLink.Text.Trim();

                if (db.SaveChanges() > 0)
                    ReloadPage();
                else
                    Alert("Güncellenemedi");
            }
        }


        protected void btnAdddImages_Click(object sender, EventArgs e)
        {
            if (flColorImage.HasFile)
            {
                int uzunluk = flColorImage.FileName.Split('.').Length;
                string random = CreateRandomValue(10, true, true, true, false) + "." + flColorImage.FileName.Split('.')[uzunluk - 1];

                flColorImage.SaveAs(Server.MapPath("~/uploads/slider/" + random));
                ResizeImage("~/uploads/slider/" + random, 1920, 920);

                var _detail = db.slider_lang.Where(q => q.slider_id == slider_id && q.lang_id == lang_id).FirstOrDefault();

                if (_detail != null)
                {
                    _detail.img_path = random;

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                    else
                        Alert("Resim güncellenemedi");
                }

            }
            else
                Alert("Resim Seçmediniz");
        }
    }
}