﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="rezerv_talepleri.aspx.cs" Inherits="SeyahatEkspresi.panel.rezerv_talepleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tüm rezervasyon talepleri</h3>
                    <div class="box-tools">
                             <asp:Panel ID="Panel1" runat="server" DefaultButton="LinkButton1">
                        <div class="input-group" style="width: 350px;">
                       
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control input-sm pull-right" placeholder="Arama Yap"></asp:TextBox>
                            <%--<input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">--%>
                            <div class="input-group-btn">
                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-sm btn-default" OnClick="LinkButton1_Click"><i class="fa fa-search"></i> Ara</asp:LinkButton>
                                &nbsp;<button class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Temizle</button>
                            </div>
                        
                        
                        </div>
                                     </asp:Panel>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>İlan Adı</th>
                                <th>Müşteri</th>
                                <th>Durum</th>
                                <th>Düzenle</th>

                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptRezerv" runat="server" OnItemDataBound="rptAds_ItemDataBound">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>

                                        <td>
                                            <a href="rezerv_detay.aspx?id=<%#Eval("id")%>">
                                                <%#Eval ("ads_name") %>
                                            </a>
                                        </td>

                                        <td><%#Eval ("name_surname") %>  (<%#Eval ("phone") %>)</td>
                                        <td>
                                            <asp:Literal ID="ltrActive" runat="server"></asp:Literal>
                                            <br />
                                            <br />
                                            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-xs btn-success" OnCommand="LinkButton2_Command" CommandArgument='<%#Eval("id")%>'> Onayla</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-xs btn-danger" OnCommand="LinkButton3_Command" CommandArgument='<%#Eval("id")%>'> İptal Et</asp:LinkButton>

                                        </td>


                                        <td>
                                            <a target="_blank" href="rezerv_detay.aspx?id=<%#Eval("id")%>" class="btn bg-navy btn-flat">Detay
                                            </a>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
