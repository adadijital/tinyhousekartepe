﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="tur_listesi.aspx.cs" Inherits="SeyahatEkspresi.panel.tur_listesi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tur Listesi</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tur Adı</th>
                                <th>Kategori</th>
                                <th>Fiyat</th>
                                <th>Gün/Gece</th>
                                <th>Düzenle</th>
                                <th>İşlem</th>
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptList" runat="server">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>
                                        <td>
                                            <a href="ilan_detay.aspx?ads_id=<%#Eval("id")%>">
                                                <%#Eval("id")%>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="ilan_detay.aspx?ads_id=<%#Eval("id")%>">
                                                <%#Eval ("tour_name") %>
                                            </a>
                                        </td>

                                        <td><%#Eval ("category_name") %></td>
                                        <td><%#Eval ("sell_price") %></td>
                                         <td><%#Eval ("tour_time") %></td>
                                        <td>
                                            <a href="tur_duzenle.aspx?tour_id=<%#Eval("id") %>" class="btn bg-navy btn-flat">
                                                Düzenle
                                            </a>
                                            

                                        </td>
                                        <td>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-s btn-danger" OnCommand="LinkButton1_Command"  CommandArgument='<%#Eval("id")%>'>Sil</asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</asp:Content>
