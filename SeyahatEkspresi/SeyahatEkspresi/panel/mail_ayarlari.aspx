﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="mail_ayarlari.aspx.cs" Inherits="SeyahatEkspresi.panel.mail_ayarlari" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-6">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Mail Ayarları </h3>
                </div>
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Gönderen Adres</label>
                        <asp:TextBox ID="txtSendMail" runat="server" class="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Gönderen SMTP</label>
                        <asp:TextBox ID="txtSendSMTP" runat="server" class="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Gönderen Port</label>
                        <asp:TextBox ID="txtSendPort" runat="server" class="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Gönderen Şifre</label>
                        <asp:TextBox ID="txtSendPassword" runat="server" class="form-control"></asp:TextBox>
                    </div>

                          <div class="form-group">
                        <label for="exampleInputEmail1">Gelen Mail</label>
                        <asp:TextBox ID="txtReceiveMail" runat="server" class="form-control"></asp:TextBox>
                    </div>



                </div>

                <div class="box-footer">
                    <asp:Button ID="btnGuncelle" runat="server" Text="Kaydet" class="btn btn-primary" OnClick="btnGuncelle_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
