﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class tur_duzenle : Base
    {
        int tour_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["tour_id"] !=null)
            {
                tour_id = Request.QueryString["tour_id"].toInt();

                if (!IsPostBack)
                {
                    var _Detail = db.our_tour.Where(q => q.id == tour_id).FirstOrDefault();



                    var _parentCat = db.tour_category.Where(q => q.is_active).OrderBy(o => o.display_order).ToList();

                    if (_parentCat.Count() > 0)
                    {
                        drpCategory.DataSource = _parentCat;
                        drpCategory.DataTextField = "category_name";
                        drpCategory.DataValueField = "id";
                        drpCategory.DataBind();
                        drpCategory.Items.Insert(0, new ListItem("–Tur Kategori seçin", "0"));
                    }

                    if(_Detail !=null)
                    {
                        drpCategory.SelectedValue = _Detail.category_id.ToString();
                        txtTitle.Text = _Detail.tour_name;
                        txtMarketPrice.Text = _Detail.market_price.ToString();
                        txtSellPrice.Text = _Detail.sell_price.ToString();
                        txtTime.Text = _Detail.tour_time;
                        txtHotelStatus.Text = _Detail.hotel_status;
                        txtPersonalCount.Text = _Detail.personal_count;
                        txtVehicleType.Text = _Detail.vehicle_type;
                        txtDesc.Text = _Detail.description;


                        var _images = db.tour_images.Where(q => q.tour_id == tour_id).OrderBy(o=> o.display_order).ToList();

                        if(_images.Count()>0)
                        {
                            rptImages.DataSource = _images;
                            rptImages.DataBind();
                        }

                    }
                }
            }

           
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            var _Detail = db.our_tour.Where(q => q.id == tour_id).FirstOrDefault();

            if (_Detail != null)
            {
                int cat_id = drpCategory.SelectedValue.toInt();
                _Detail.category_id = cat_id;
                _Detail.tour_name = txtTitle.Text.Trim();
                _Detail.market_price = txtMarketPrice.Text.Trim().todecimal();
                _Detail.sell_price = txtSellPrice.Text.Trim().todecimal();
                _Detail.tour_time = txtTime.Text.Trim();
                _Detail.hotel_status = txtHotelStatus.Text.Trim();
                _Detail.personal_count = txtPersonalCount.Text.Trim();
                _Detail.vehicle_type = txtVehicleType.Text.Trim();
                _Detail.description = txtDesc.Text.Trim();

                if (db.SaveChanges() > 0)
                    ReloadPage();


            }

        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _id = e.CommandArgument.toInt();
                var _Remove = db.tour_images.Where(q => q.id == _id).FirstOrDefault();

                if(_Remove !=null)
                {
                    db.tour_images.Remove(_Remove);
                }

                if (db.SaveChanges() > 0)
                    ReloadPage();
            }
            catch (Exception ex)
            {

            }
        }

        [WebMethod] //resim sıralama methodu
        public static void resimSirala(String item)
        {

            tinyhous_dbEntities db = new tinyhous_dbEntities();
            string gelen = item.Replace("item", "");
            char[] ayrac = new char[] { ',' };
            string[] gelenler = gelen.Split(ayrac);
            int i = 1;

            foreach (string veri in gelenler)
            {
                db.Database.ExecuteSqlCommand("Update tour_images set display_order = " + i + " where id=" + veri.toInt());
                i++;
            }

        }

        protected void btnAdddImages_Click(object sender, EventArgs e)
        {
            IList<HttpPostedFile> SecilenDosyalar = FileUpload1.PostedFiles;

            for (int i = 0; i < SecilenDosyalar.Count; i++)
            {
                tour_images _newImage = new tour_images();

                int uzunluk = FileUpload1.PostedFiles[i].FileName.Split('.').Length;
                string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.PostedFiles[i].FileName.Split('.')[uzunluk - 1];
                FileUpload1.PostedFiles[i].SaveAs(Server.MapPath("~/uploads/tour_img/" + random));

                using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/tour_img/" + random)))
                {
                    img.ScaleAndCrop(1200, 1600).SaveAs(Server.MapPath("~/uploads/tour_img/XL/" + random));
                }

                using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/tour_img/" + random)))
                {
                    img.ScaleAndCrop(600, 800).SaveAs(Server.MapPath("~/uploads/tour_img/L/" + random));
                }

                using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/tour_img/" + random)))
                {
                    img.ScaleAndCrop(370, 495).SaveAs(Server.MapPath("~/uploads/tour_img/M/" + random));
                }



                _newImage.tour_id = tour_id;
                _newImage.display_order = i;
                _newImage.img_path = random;

                db.tour_images.Add(_newImage);

                if (db.SaveChanges() > 0)
                {
                    Response.Redirect("/panel/tur_listesi.aspx", false);
                }

            }
        }
    }
}