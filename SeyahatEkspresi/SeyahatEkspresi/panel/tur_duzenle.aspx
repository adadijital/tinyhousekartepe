﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="tur_duzenle.aspx.cs" Inherits="SeyahatEkspresi.panel.tur_duzenle" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tur Düzenle</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tur Kategorisi</label>
                        <asp:RequiredFieldValidator ID="DropDownListRequiredFieldValidator" runat="server" CssClass="randevuUyari"
                            ControlToValidate="drpCategory"
                            InitialValue="0"
                            ValidationGroup="validateCredential2"
                            ErrorMessage="Boş bırakılamaz">                              
                        </asp:RequiredFieldValidator>
                        <asp:DropDownList ID="drpCategory" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tur Başlığı</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtTitle" ID="RequiredFieldValidator1" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Tur Başlığı *" ID="txtTitle" MaxLength="250" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Üstü Çizili Fiyat</label>
                        <asp:TextBox runat="server" placeholder="Üstü Çizili Fiyat" ID="txtMarketPrice" MaxLength="250" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Fiyat</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtSellPrice" ID="RequiredFieldValidator2" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Fiyat" ID="txtSellPrice" MaxLength="250" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kaç Gün Kaç Gece</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtTime" ID="RequiredFieldValidator3" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Kaç Gün Kaç Gece" ID="txtTime" MaxLength="250" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Otel Durumu</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtHotelStatus" ID="RequiredFieldValidator4" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Örn : 4 Yıldızlı Otel" ID="txtHotelStatus" MaxLength="250" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kaç Kişi</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtPersonalCount" ID="RequiredFieldValidator5" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Örn : İki Yetişkin" ID="txtPersonalCount" MaxLength="250" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Araç Türü</label>
                        <asp:RequiredFieldValidator ControlToValidate="txtVehicleType" ID="RequiredFieldValidator6" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" placeholder="Örn : Otobüs" ID="txtVehicleType" MaxLength="250" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tur Açıklama</label>
                        <CKEditor:CKEditorControl ID="txtDesc" Width="100%" Height="300" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Kaydet" OnClick="btnEkle_Click" class="btn btn-primary pull-right" ValidationGroup="validateCredential2" /><br />
                </div>
            </div>
        </div>

             <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">İlan Resimleri (1200x1600 px boyutunda yükleyin)
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="btn-file">
                                <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" />
                                <output id="list"></output>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <asp:Button ID="btnAdddImages" runat="server" Text="Resim Yükle" class="btn btn-info"  OnClick="btnAdddImages_Click" />
                        </div>
                    </div>
                    <div class="row">
                        <ul id="sortableResim">
                            <asp:Repeater ID="rptImages" runat="server">
                                <ItemTemplate>
                                    <li id="item<%#Eval("id") %>" style="list-style: none;">
                                          <a data-fancybox="gallery" href="/uploads/tour_img/XL/<%#Eval("img_path") %>">
                                                   <img class="img-responsive" src="<%=System.Configuration.ConfigurationManager.AppSettings["site_url"] %>/uploads/tour_img/M/<%#Eval("img_path") %>" alt="">             
                                              </a>

                                   
                                        <div class="box-footer">
                                            <asp:LinkButton runat="server" ID="lnkRemove" OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-xs btn-danger" OnCommand="lnkRemove_Command"  CommandArgument='<%#Eval("id") %>'>Sil
                                            </asp:LinkButton>
                                        </div
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>

      <style>
            .thumb {
                height: 75px;
                border: 1px solid #000;
                margin: 10px 5px 0 0;
            }



            .btn-file input[type=file] {
                cursor: inherit;
                display: block;
                color: black;
                width: 100%;
                cursor: pointer !Important;
                font: 14px;
                border: 1px solid #D5D5D5;
                padding: 10px;
                border-radius: 5px;
            }

            #sortable {
                list-style-type: none;
                margin: 0;
                padding: 0;
            }

                #sortable li {
                    display: block;
                    width: 23%;
                    height: 100%;
                    float: left;
                    margin: 10px;
                }


            #sortableResim {
                list-style-type: none;
                margin: 0;
                padding: 0;
            }

                #sortableResim li {
                    display: block;
                    width: 23%;
                    height: 100%;
                    float: left;
                    margin: 10px;
                }
        </style>
        <script>
            $(function () {
                $("#sortableResim").sortable({
                    opacity: 0.8,
                    cursor: 'move',
                    update: function () {
                        var item = $(this).sortable("toArray");

                        $.ajax({
                            type: "POST",
                            url: "tur_duzenle.aspx/resimSirala",
                            data: "{'item': '" + item + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) { location.reload(); }
                        });
                    }
                });
            });


        </script>
</asp:Content>
