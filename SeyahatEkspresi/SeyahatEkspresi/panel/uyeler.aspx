﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="uyeler.aspx.cs" Inherits="SeyahatEkspresi.panel.uyeler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Üyeler</h3>
                      <div class="box-tools">
                                <div class="input-group" style="width: 350px;">
                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Anahtar Kelime" CssClass="form-control input-sm pull-right"></asp:TextBox>
                                    <div class="input-group-btn">
                           
                                        <asp:Button class="btn btn-sm btn-default" runat="server" Text="ARAMA YAP" ID="btnArama"  OnClick="btnArama_Click"  />
                                    </div>
                                </div>
                            </div>
                </div>
                <div class="box-body table-responsive">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Ad Soyad</th>
                                <th>Kayıt Tarihi</th>
                                <th>E-Posta</th>
                                <th>Üyelik Tipi</th>
                                <th>Durum</th>
                                <th>Detay</th>
                                <th>Sil</th>

                            </tr>
                        </thead>
                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptUyeler" runat="server">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("id")%>'>
                                        <td><a href="uye_detay.aspx?member_id=<%#Eval("id") %>"><%#Eval("name_surname") %> </a></td>
                                        <td><%#Eval("created_time") %> </td>
                                        <td><%#Eval("email") %></td>
                                        <td><%#Eval("account_type_text") %></td>
                                        <td><%#Eval("active_status") %></td>
                                        <td><a href="uye_detay.aspx?member_id=<%#Eval("id") %>" class="btn bg-navy btn-flat">Detay</a></td>
                                        <td> 
                                               <asp:LinkButton runat="server" ID="LinkButton1"  OnCommand="LinkButton1_Command"  CommandArgument='<%#Eval ("id") %>' OnClientClick="return confirm('Silmek istediğinizden emin misiniz ?');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                          <div class="text-center">
                        <ul class="pagination">
                            <asp:Literal runat="server" ID="ltrPaging" />
                        </ul>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</asp:Content>
