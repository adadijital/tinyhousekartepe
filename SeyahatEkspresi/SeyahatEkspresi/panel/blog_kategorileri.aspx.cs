﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class blog_kategorileri : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _List = db.blog_category_lang.Where(q => q.lang_id == 1).Select(s => new
            {
                s.blog_category_id,
                s.category_name,
                s.blog_category.display_order,
                active_status = s.blog_category.is_active ? "Aktif" : "Pasif"
            }).OrderBy(o => o.display_order).ToList();

            if(_List.Count()>0)
            {
                rptCategory.DataSource = _List;
                rptCategory.DataBind();
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCategoryName.Text.Trim()))
                {
                    blog_category _newCategoy = new blog_category();
                    _newCategoy.category_name = txtCategoryName.Text.Trim();
                    _newCategoy.display_order = 1;
                    _newCategoy.is_active = ckIsActive.Checked;
                    db.blog_category.Add(_newCategoy);

                    if(db.SaveChanges()>0)
                    {
                        foreach (var item in db.lang.ToList())
                        {
                            blog_category_lang _newLang = new blog_category_lang();
                            _newLang.blog_category_id = _newCategoy.id;
                            _newLang.lang_id = item.id;
                            _newLang.category_name = txtCategoryName.Text.Trim();

                            db.blog_category_lang.Add(_newLang);
                        }

                        if (db.SaveChanges() > 0)
                            ReloadPage();
                    }

                }
                else
                    Alert("Kategori ismi girmelisiniz.");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}