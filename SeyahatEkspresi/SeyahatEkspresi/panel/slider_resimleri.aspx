﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="slider_resimleri.aspx.cs" Inherits="SeyahatEkspresi.panel.slider_resimleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }

        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

            #sortable li {
                display: block;
                width: 23%;
                height: 100%;
                float: left;
                margin: 10px;
            }
    </style>

    <script type="text/javascript">
        $(function () {
            $("#SliderResim").sortable({
                opacity: 0.8,
                cursor: 'move',
                update: function () {
                    var Satir = $(this).sortable("toArray");

                    $.ajax({
                        type: "POST",
                        url: "slider_resimleri.aspx/Sirala",
                        data: "{'Satir': '" + Satir + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) { location.reload(); }


                    });
                }
            });


        });


    </script>

    <div class="row">

        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Yeni Slider Resim Ekle</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Resim  (1920px X 920)</label>
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Aktif / Pasif </label>
                        <asp:CheckBox ID="ckIsActive" runat="server" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Slider Başlık </label>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Slider Açıklama </label>
                        <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Slider Link</label>
                        <asp:TextBox ID="txtLink" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Slider Resmi Ekle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>
        <div class="col-md-8">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Resimleri Sürükle Bırak ile Sıralayabilirsiniz</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Resim</th>
                                <th>Durum</th>
                                <th>Dil</th>
                                <th>Sil</th>
                            </tr>
                        </thead>

                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptSliderList" runat="server">
                                <ItemTemplate>
                                    <tr id='satir<%#Eval("slider_id")%>'>
                                        <td>
                                            <img src="/uploads/slider/<%#Eval("img_path") %>" style="width: 200px" class="img-responsive" /></td>
                                        <td><%#Eval("active_status") %> </td>

                                        <td>
                                            <a href="slider_detay.aspx?slider_id=<%#Eval("slider_id") %>&lang_id=1" class="btn bg-navy btn-flat">Detay</a>
                                       
                                        </td>

                                        <td>
                                            <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command" CommandArgument='<%#Eval ("slider_id") %>' OnClientClick="return confirm('Silmek istediğinizden emin misiniz ?');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                                        </td>


                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</asp:Content>
