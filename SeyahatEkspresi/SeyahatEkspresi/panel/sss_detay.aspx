﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="sss_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.sss_detay" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">SSS Detay</h3>
                </div>
                <div class="box-body table-responsive">
                  

                    <div class="form-group">
                        <label for="exampleInputEmail1">SSS Başlık </label>
                        <asp:TextBox ID="txtSSSTitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">SSS İçerik </label>
                                            <CKEditor:CKEditorControl ID="txtSSSDesc" Width="100%" Height="300" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>

                    </div>

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Günc elle" class="btn btn-primary"  OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>
          </div>
</asp:Content>
