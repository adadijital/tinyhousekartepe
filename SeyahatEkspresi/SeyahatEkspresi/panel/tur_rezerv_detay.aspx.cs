﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class tur_rezerv_detay : Base
    {
        int _id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    _id = Request.QueryString["id"].toInt();
                    var _rezerv = db.tour_reservasiton_request.Where(q => q.id == _id).Select(s => new
                    {
                        s.name_surname,
                        s.id,
                        s.start_date,
                        s.finish_date,
                        s.created_time,
                        s.phone,
                        s.our_tour.tour_name,
                        tour_id = s.id,
                        s.person_count,
                        s.description,
                        s.admin_note,
                        s.is_approved,
                        s.reserve_dates,
                        s.is_cancel,
                    }).FirstOrDefault();

                    if (_rezerv != null)
                    {
                        ltrAdsName.Text = _rezerv.tour_name;
                        ltrCreated.Text = _rezerv.created_time.ToString();
                        ltrNameSurname.Text = _rezerv.name_surname;
                        ltrCount.Text = _rezerv.person_count.ToString();
                        //ltrStart.Text = _rezerv.start_date;
                        //ltrFinish.Text = _rezerv.finish_date;
                        ltrPhone.Text = _rezerv.phone;
                        ltrRezervDates.Text = _rezerv.reserve_dates;
                        ltrDesc.Text = _rezerv.description;

                        if (!IsPostBack)
                        {
                            txtAdminNote.Text = _rezerv.admin_note;
                        }

                        HyperLink1.NavigateUrl = "/panel/tur_duzenle.aspx?ads_id=" + _rezerv.tour_id + "&lang_id=1";


                        if (_rezerv.is_approved == false && _rezerv.is_cancel == false)
                        {
                            ltrStatus.Text = "<span class='label label-warning'>Onay Bekliyor</span>";
                        }


                        if (_rezerv.is_approved)
                        {
                            ltrStatus.Text = "<span class='label label-success'>Onaylandı</span>";
                        }


                        if (_rezerv.is_cancel)
                        {
                            ltrStatus.Text = "<span class='label label-danger'>İptal Edildi</span>";

                        }


                        if (_rezerv.is_cancel)
                        {
                            btnActive.Visible = true;
                            btnPasif.Visible = false;
                        }

                        if (_rezerv.is_approved)
                        {
                            btnActive.Visible = false;
                            btnPasif.Visible = true;
                        }
                    }
                }


            }
            catch (Exception)
            {
            }
        }

        protected void btnActive_Click(object sender, EventArgs e)
        {
            try
            {
                var _rezerv = db.tour_reservasiton_request.Where(q => q.id == _id).FirstOrDefault();

                if (_rezerv != null)
                {
                    _rezerv.is_approved = true;
                    _rezerv.is_cancel = false;

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void btnPasif_Click(object sender, EventArgs e)
        {
            var _rezerv = db.tour_reservasiton_request.Where(q => q.id == _id).FirstOrDefault();

            if (_rezerv != null)
            {
                _rezerv.is_approved = false;
                _rezerv.is_cancel = true;

                if (db.SaveChanges() > 0)
                    ReloadPage();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var _rezerv = db.tour_reservasiton_request.Where(q => q.id == _id).FirstOrDefault();

            if (_rezerv != null)
            {
                _rezerv.admin_note = txtAdminNote.Text.Trim();

                if (db.SaveChanges() > 0)
                    ReloadPage();
            }
        }
    }
}