﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class blog_ekle : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtBlogDesc.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
                
                var _categoryList = db.blog_category_lang.Where(q => q.lang_id == 1 && q.blog_category.is_active).ToList();
                if (_categoryList != null)
                {
                    drpCategory.DataTextField = "category_name";
                    drpCategory.DataValueField = "blog_category_id";
                    drpCategory.DataSource = _categoryList;
                    drpCategory.DataBind();
                    drpCategory.Items.Insert(0, "Kategori Seçiniz");
                }
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpCategory.SelectedValue.toInt() > 0)
                {
                    if (!string.IsNullOrEmpty(txtBlogTitle.Text.Trim()))
                    {
                        if (!string.IsNullOrEmpty(txtBlogDesc.Text.Trim()))
                        {
                            int category_id = drpCategory.SelectedValue.toInt();
                            blog _newBlog = new blog();
                            _newBlog.created_time = DateTime.Now;
                            _newBlog.title = txtBlogTitle.Text.Trim();
                            _newBlog.view_count = 0;
                            _newBlog.category_id = category_id;

                            if (FileUpload1.HasFile)
                            {
                                int uzunluk = FileUpload1.FileName.Split('.').Length;
                                string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
                                FileUpload1.SaveAs(Server.MapPath("~/uploads/blog/" + random));
                                _newBlog.image_path = random;
                            }
                            db.blog.Add(_newBlog);

                            if(db.SaveChanges()>0)
                            {
                                foreach (var item in db.lang.ToList())
                                {
                                    blog_lang _newLang = new blog_lang();
                                    _newLang.blog_id = _newBlog.id;
                                    _newLang.lang_id = item.id;
                                    _newLang.blog_title = txtBlogTitle.Text.Trim();
                                    _newLang.blog_desc = txtBlogDesc.Text.Trim();

                                    db.blog_lang.Add(_newLang);
                                }

                                if (db.SaveChanges() > 0)
                                    Response.Redirect("/panel/blog_yazilari.aspx",false);
                            }
                        }
                        else
                            Alert("Yazı girmelisiniz");
                    }
                    else
                        Alert("Başlık girmelisiniz");
                }
                else
                    Alert("Kategori seçmelisiniz");


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}