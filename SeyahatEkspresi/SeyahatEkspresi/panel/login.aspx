﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="SeyahatEkspresi.panel.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/panel/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/panel/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/panel/plugins/iCheck/square/blue.css">
</head>
<body class="hold-transition login-page">
    <form id="form1" runat="server">
              <div class="login-box">
            <div class="login-logo">
                <a href="#"><b><%=System.Configuration.ConfigurationManager.AppSettings["site_name"] %></b></a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">


                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtUsername" runat="server" class="form-control" placeholder="Kullanıcı Adı"></asp:TextBox>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtPassword" runat="server" class="form-control" placeholder="Şifre" TextMode="Password"></asp:TextBox>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <div class="social-auth-links text-center">
                       <!-- <asp:Image ID="imgDogrulama" runat="server"  class="btn btn-block btn-social btn-facebook btn-flat"/>-->
                    </div>
                </div>
                 <div class="form-group has-feedback">
                    <!--<asp:TextBox ID="txtCaptcha" runat="server" class="form-control" placeholder="Güvenlik Kodu"></asp:TextBox>-->

                </div>

                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <asp:Button ID="btnLogin" class="btn btn-primary btn-block btn-flat" runat="server" Text="Giriş Yap"  OnClick="btnLogin_Click"/>
                  
                    </div>
                    <!-- /.col -->
                </div>



                <!-- /.social-auth-links -->

            </div>
            <!-- /.login-box-body -->
        </div>
    </form>
</body>
</html>
