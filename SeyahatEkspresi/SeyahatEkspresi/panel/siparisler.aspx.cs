﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class siparisler : Base
    {
        int SituationID = 0;
        string MemberNameSurname = "";
        string MemberEmail = "";
        int OrderCode = 0;
        string s_date = "";
        DateTime StartDate;
        DateTime FinishDate;
        int page_count;
        string pageUrl = "siparisler.aspx";
        int page_id = 0;
        int paymentTypeID = 0;
        int bankID = 0;
        OrderManager _order = new OrderManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page"] != null)
                page_id = Convert.ToInt32(Request.QueryString["page"]);

            int carpilmis = (page_id != 0) ? page_id * 100 : 0;
            page_count = 100;

            if (!Page.IsPostBack)
            {
                try
                {
                    SituationID = Request.QueryString["situation_id"].toInt();
                    MemberNameSurname = Request.QueryString["MemberNameSurname"].Trim();
                    MemberEmail = Request.QueryString["MemberEmail"].Trim();
                    OrderCode = Request.QueryString["OrderCode"].Trim().toInt();
                    s_date = Request.QueryString["s_date"].Trim();

                }
                catch { }


                _fillPage();
            }
        }

        private void _fillPage()
        {

            var OrderList = _order.getOrderList(SituationID, MemberNameSurname, MemberEmail, OrderCode, StartDate, FinishDate);
            if (OrderList.Count > 0)
            {
                if (OrderList.Count() > page_count)
                {
                    int loop_count = OrderList.Count() / page_count;

                    if (OrderList.Count() % page_count != 0) loop_count++;

                    for (int i = 1; i <= loop_count; i++)
                    {

                        if (i == page_id)
                            ltrPaging.Text += "<li><a href=\"siparisler.aspx?page=" + i + "&situation_id=" + SituationID + "\" class=\"btn\">" + i + "</a></li>";
                        else
                            ltrPaging.Text += "<li><a href=\"siparisler.aspx?page=" + i + "&situation_id=" + SituationID + "\">" + i + "</a></li>";

                    }
                }


                rptOrderList.DataSource = OrderList.OrderByDescending(o => o.order_id).Skip((page_id - 1) * page_count).Take(page_count).ToList();
                rptOrderList.DataBind();
            }
        }

        protected void rptOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            try
            {
                Literal ltrSituation = (Literal)e.Item.FindControl("ltrSituation");
                string situation_name = DataBinder.Eval(e.Item.DataItem, "situation_name").ToString();
                int situation_id = DataBinder.Eval(e.Item.DataItem, "situation_id").toInt();
                int member_id = DataBinder.Eval(e.Item.DataItem, "member_id").toInt();
                int order_id = DataBinder.Eval(e.Item.DataItem, "order_id").toInt();
                Literal ltrUrunler = (Literal)e.Item.FindControl("ltrUrunler");

                switch (situation_id)
                {
                    case 2:
                        ltrSituation.Text = "<span class='label label-success'>" + situation_name + "</span>";
                        break;
                    case 1:
                        ltrSituation.Text = "<span class='label label-primary'>" + situation_name + "</span>";
                        break;
                    case 3:
                        ltrSituation.Text = "<span class='label label-danger'>" + situation_name + "</span>";
                        break;
                    case 4:
                        ltrSituation.Text = "<span class='label label-info'>" + situation_name + "</span>";
                        break;
                    case 5:
                        ltrSituation.Text = "<span class='label label-info'>" + situation_name + "</span>";
                        break;

                }
            }
            catch (Exception ex)
            {

            }


        }

        protected void btnArama_Click(object sender, EventArgs e)
        {
            try
            {
                string kelime = txtSearch.Text.Trim();
                int siparis_id = 0;

                if (kelime.StartsWith("#"))
                {
                    try
                    {
                        siparis_id = Convert.ToInt32(kelime.Split('#')[1]);
                    }
                    catch { }
                }

                if (siparis_id != 0)
                {
                    rptOrderList.DataSource = db.orders
                      .Where(q => (q.id == siparis_id || q.order_code == kelime))
                      .OrderByDescending(o => o.id)
                      .Select(s => new
                      {

                          order_id = s.id,
                          created_time = s.order_time,
                          product_count = s.order_products.FirstOrDefault().quantity,
                          product_total = (decimal)s.order_total,
                          situation_id = s.situation_id,
                          situation_name = s.order_situations.situation_name,
                          member_name = s.members.name_surname,
                          payment_amount = (decimal)s.order_total,
                          city_name = s.geo_cities.city_name,
                          member_id = s.member_id,
                          payment_type_name = s.order_payments.FirstOrDefault().payment_type.payment_name,


                      }).ToList();

                    rptOrderList.DataBind();
                }
                else
                {
                    rptOrderList.DataSource = db.orders
                       .Where(q =>
                           (q.members.name_surname).Contains(kelime)
                           || q.members.email.Contains(kelime)
                           || q.shipping_phone.Contains(kelime)
                           || q.order_code.Contains(kelime))
                       .OrderByDescending(o => o.id)
                       .Select(s => new
                       {
                           order_id = s.id,
                           created_time = s.order_time,
                           product_count = s.order_products.FirstOrDefault().quantity,
                           product_total = (decimal)s.order_total,
                           situation_id = s.situation_id,
                           situation_name = s.order_situations.situation_name,
                           member_name = s.members.name_surname,
                           payment_amount = (decimal)s.order_total,
                           city_name = s.geo_cities.city_name,
                           member_id = s.member_id,
                           payment_type_name = s.order_payments.FirstOrDefault().payment_type.payment_name,
                  
                       }).ToList();
                    rptOrderList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}