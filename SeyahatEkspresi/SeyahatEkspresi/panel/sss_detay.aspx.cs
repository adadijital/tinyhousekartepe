﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class sss_detay : Base
    {
        int sss_id;
        byte lang_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["sss_id"] != null)
            {
                sss_id = Request.QueryString["sss_id"].toInt();
                lang_id = Request.QueryString["lang_id"].toByte();

                var _detail = db.sss_content_lang.Where(q => q.lang_id == lang_id && q.sss_id ==sss_id ).FirstOrDefault();

                if(_detail !=null)
                {
                    if(!IsPostBack)
                    {
                        txtSSSTitle.Text = _detail.sss_title;
                        txtSSSDesc.Text = _detail.sss_desc;
                    }
                }

            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtSSSTitle.Text.Trim()))
                {
                    if (!string.IsNullOrEmpty(txtSSSDesc.Text.Trim()))
                    {
                        var _detail = db.sss_content_lang.Where(q => q.lang_id == lang_id && q.sss_id == sss_id).FirstOrDefault();

                        if(_detail !=null)
                        {
                            _detail.sss_title = txtSSSTitle.Text.Trim();
                            _detail.sss_desc = txtSSSDesc.Text.Trim();

                            if (db.SaveChanges() > 0)
                                ReloadPage();
                        }
                    }
                    else
                        Alert("İçerik giriniz.");
                }
                else
                    Alert("Başlık giriniz");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}