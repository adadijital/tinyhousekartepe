﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class siparis_detay : Base
    {
        int OrderID;
        int MemberID;
        string _referrerURL;
        OrderManager _order = new OrderManager();
        string _orderFolder = "";
        string confirm_time;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (Request.QueryString["order_id"] != null)
                    OrderID = Request.QueryString["order_id"].toInt();
                else
                    Response.Redirect("/", false);

                if (!Page.IsPostBack)
                {


                    _fillPage();
                    _fillSituations();


                }


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        private void _fillPage()
        {
            try
            {
                var OrderDetail = _order.getSingle(OrderID);

                if (OrderDetail != null)
                {
                    hypMemberDetail.NavigateUrl = "/panel/uye_detay.aspx?member_id=" + OrderDetail.member_id;

                    hypSatici.NavigateUrl =  "/panel/uye_detay.aspx?member_id=" + OrderDetail.supplier_id;
                    ltrSatici.Text = OrderDetail.supplier_name;
                    ltrCreatedDate.Text = OrderDetail.created_time.ToString();
                    ltrNameSurname.Text = OrderDetail.member_name + " " + OrderDetail.member_surname;
                    ltrPhone.Text = OrderDetail.order_phone;
                    ltrEmail.Text = OrderDetail.member_email;
                    ltrIpAddress.Text = OrderDetail.order_ip;
                    ltrSituationName.Text = OrderDetail.situation_name;
                    ltrOrderTotal.Text = OrderDetail.order_total.ToString();
                    drpSituations.SelectedValue = OrderDetail.situation_id.ToString();
                    ltrOrderCode.Text = OrderDetail.order_code;
                  
                    ltrTotalMarket1.Text = OrderDetail.product_total.ToString(); //Toplam
            
                    ltrTotalPayment.Text = OrderDetail.order_total.ToString(); //Toplam Tutar 

               
                    //teslimat bilgileri

                    ltrShippingAddresNameSurname.Text = OrderDetail.shippping_name;
                    ltrShippingAddress.Text = OrderDetail.shipping_address;
                    ltrShippingCity.Text = OrderDetail.shipping_city_name + " / " + OrderDetail.shipping_town_name;
                    ltrShippingPhone.Text = OrderDetail.shipping_phone;

                    //fatura bilgileri

                    ltrBillingAdressNameSurname.Text = OrderDetail.billing_name;
                    ltrBillingAddress.Text = OrderDetail.billing_address;
                    ltrBillingCity.Text = OrderDetail.billing_city_name + " / " + OrderDetail.billing_town_name;
                    ltrBillingPhone.Text = OrderDetail.billing_phone;
                    //ltrTaxOffice.Text = OrderDetail.billing_tax_office;
                    //ltrTaxNumber.Text = OrderDetail.billing_tax_number;

                    hlAddressUpd1.NavigateUrl = "siparis_adres_guncelle.aspx?order_id=" + OrderID;
                    hlAddressUpd2.NavigateUrl = "siparis_adres_guncelle.aspx?order_id=" + OrderID;


                    switch (OrderDetail.situation_id)
                    {
                        case 1: siparis_durum.Attributes.Add("class", "pull-right label label-info"); break;
                        case 2: siparis_durum.Attributes.Add("class", "pull-right label label-info"); break;
                        case 3: siparis_durum.Attributes.Add("class", "pull-right label label-primary"); break;
                        case 4: siparis_durum.Attributes.Add("class", "pull-right label label-success"); break;
                        case 5: siparis_durum.Attributes.Add("class", "pull-right label label-danger"); break;
                        case 6: siparis_durum.Attributes.Add("class", "pull-right label label-default"); break;
                        case 7: siparis_durum.Attributes.Add("class", "pull-right  bg-maroon"); break;
                    }


                    if (OrderDetail.order_payments.Count() > 0)
                    {
                        rptPayments.DataSource = OrderDetail.order_payments.ToList();
                        rptPayments.DataBind();

                  

                    }

                    if (OrderDetail.order_products.Count() > 0)
                    {
                        rptProducts.DataSource = OrderDetail.order_products;
                        rptProducts.DataBind();

                    }

                
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        private void _fillSituations()
        {
            drpSituations.DataSource = db.order_situations.OrderBy(o => o.id).ToList();
            drpSituations.DataTextField = "situation_name";
            drpSituations.DataValueField = "id";
            drpSituations.DataBind();
        }

        protected void btnSituationUpdate_Click1(object sender, EventArgs e)
        {

            try
            {
                var _situationID = drpSituations.SelectedValue.toInt();

                var _detail = db.orders.Where(q => q.id == OrderID).FirstOrDefault();

                if (_detail != null)
                {
                    _detail.situation_id = _situationID;

                    if (db.SaveChanges() > 0)
                        ReloadPage();

                }

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }





        }

        protected void rptPayments_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemIndex != -1)
                {
                    Literal status = (Literal)e.Item.FindControl("ltrStatus");
                    LinkButton lnkOdendiYap = (LinkButton)e.Item.FindControl("lnkOdendiYap");

                    if ((bool)DataBinder.Eval(e.Item.DataItem, "is_paid"))
                    {
                        status.Text = "<span class='label label-success'>ÖDENDİ</span>";
                        lnkOdendiYap.Visible = false;
                    }
                    else
                    {
                        status.Text = "<span class='label label-danger'>Ödenmedi</span>";
                        lnkOdendiYap.Visible = true;



                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void lnkOdendiYap_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int payment_id = e.CommandArgument.toInt();
                var _update = db.order_payments.Where(q => q.id == payment_id).FirstOrDefault();

                if (_update != null)
                {
                    _update.is_paid = true;

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
            }
        }

    }
}