﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="ilce_duzenle.aspx.cs" Inherits="SeyahatEkspresi.panel.ilce_duzenle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <div class="row">
        <div class="col-xs-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Düzenle</h3>
                </div>
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">İlçe Adı </label>
                        <asp:TextBox ID="txtTownName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                       <div class="form-group">
                        <label for="exampleInputEmail1">Bölge Anasayfa Resmi Seç</label>
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                    </div>

                    <div class="form-group" runat="server" id="pnlResim" visible="false">
                        <label for="exampleInputEmail1">Bölge Anasayfa Resmi</label>
                        <asp:Image ID="Image1" runat="server" CssClass="img-responsive" />
                        <br />
                            <asp:LinkButton runat="server" ID="LinkButton1" OnClick="LinkButton1_Click"  OnClientClick="return confirm('Silmek istediğinizden emin misiniz ?');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                    </div>


                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Ekle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>
             </div>

</asp:Content>
