﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class yeni_tur_ekle : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if(!IsPostBack)
            {
                var _parentCat = db.tour_category.Where(q => q.is_active).OrderBy(o => o.display_order).ToList();

                if (_parentCat.Count() > 0)
                {
                    drpCategory.DataSource = _parentCat;
                    drpCategory.DataTextField = "category_name";
                    drpCategory.DataValueField = "id";
                    drpCategory.DataBind();
                    drpCategory.Items.Insert(0, new ListItem("–Tur Kategori seçin", "0"));
                }
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            int cat_id = drpCategory.SelectedValue.toInt();

            our_tour _newTour = new our_tour();
            _newTour.category_id = cat_id;
            _newTour.tour_name = txtTitle.Text.Trim();
            _newTour.market_price = txtMarketPrice.Text.Trim().todecimal();
            _newTour.sell_price = txtSellPrice.Text.Trim().todecimal();
            _newTour.tour_time = txtTime.Text.Trim();
            _newTour.hotel_status = txtHotelStatus.Text.Trim();
            _newTour.personal_count = txtPersonalCount.Text.Trim();
            _newTour.vehicle_type = txtVehicleType.Text.Trim();
            _newTour.description = txtDesc.Text.Trim();
            _newTour.display_order = 1;
            

            db.our_tour.Add(_newTour);

            IList<HttpPostedFile> SecilenDosyalar = FileUpload1.PostedFiles;

            for (int i = 0; i < SecilenDosyalar.Count; i++)
            {
                tour_images _newImage = new tour_images();

                int uzunluk = FileUpload1.PostedFiles[i].FileName.Split('.').Length;
                string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.PostedFiles[i].FileName.Split('.')[uzunluk - 1];
                FileUpload1.PostedFiles[i].SaveAs(Server.MapPath("~/uploads/tour_img/" + random));

                using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/tour_img/" + random)))
                {
                    img.ScaleAndCrop(1200, 1600).SaveAs(Server.MapPath("~/uploads/tour_img/XL/" + random));
                }

                using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/tour_img/" + random)))
                {
                    img.ScaleAndCrop(600, 800).SaveAs(Server.MapPath("~/uploads/tour_img/L/" + random));
                }

                using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/tour_img/" + random)))
                {
                    img.ScaleAndCrop(370, 495).SaveAs(Server.MapPath("~/uploads/tour_img/M/" + random));
                }



                _newImage.tour_id = _newTour.id;
                _newImage.display_order = i;
                _newImage.img_path = random;

                db.tour_images.Add(_newImage);

                if(db.SaveChanges()>0)
                {
                    Response.Redirect("/panel/tur_listesi.aspx", false);
                }

            }
        }
    }
}