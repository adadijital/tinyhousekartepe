﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class ilceler : Base
    {
        int city_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["city_id"] != null)
                {
                    city_id = Request.QueryString["city_id"].toInt();
                    var _List = db.geo_town.Where(q => q.city_id == city_id).ToList();

                    if (_List.Count() > 0)
                    {
                        rptTowns.DataSource = _List;
                        rptTowns.DataBind();
                    }
                }


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtTownName.Text.Trim()))
                {
                    geo_town _newTown = new geo_town();
                    _newTown.town_name = txtTownName.Text.Trim();
                    _newTown.city_id = city_id;

                    db.geo_town.Add(_newTown);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                    else
                        Alert("İlçe Eklenemedi");
                }
                else
                    Alert("İlçe adı girmelisiniz.");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int id = e.CommandArgument.toInt();
                var _delete = db.geo_town.Where(q => q.id == id).FirstOrDefault();

                if (_delete != null)
                {
                    db.geo_town.Remove(_delete);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}