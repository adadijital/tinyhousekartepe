﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SeyahatEkspresi.panel._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
       <!-- <div class="col-lg-3 col-xs-6">
           
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        <asp:Literal ID="ltrMemberCount" runat="server"></asp:Literal>
                        Adet
                    </h3>

                    <p>Kayıtlı Üye</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>

            </div>
        </div>-->
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        <asp:Literal ID="ltrAdsCount" runat="server"></asp:Literal>
                        Adet
                    </h3>

                    <p>Yayınlanmış İlan</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>

            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        <asp:Literal ID="ltrAdsAmount" runat="server"></asp:Literal>
                        TL
                    </h3>

                    <p>Tutarında Yayınlanmış İlan</p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>

            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        <asp:Literal ID="ltrCategoryCount" runat="server"></asp:Literal>
                    </h3>

                    <p>Adet Kategori</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>

            </div>
        </div>
        <!-- ./col -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Onay Bekleyen Rezervasyon Talepleri</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">

                        <asp:Repeater ID="rptRezerv" runat="server">
                            <ItemTemplate>
                                <li class="item">
                                    <div class="product-img">
                                        <a href="rezerv_detay.aspx?id=<%#Eval("id")%>">
                                            <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("img_name") %>" style="width:50%">
                                        </a>
                                    </div>
                                    <div class="product-info">
                                        <a target="_blank" href="rezerv_detay.aspx?id=<%#Eval("id")%>" class="product-title">
                                            <span class="label label-warning pull-right">
                                                Kayıt Tarihi:
                                                <%#Eval("created_time") %></span>

                                            <br />
                                            <%#Eval("ads_name") %>
                                            </a>
                                        <span class="product-description">

                                            <b>Ad Soyad: </b><%#Eval("name_surname") %>
                                            <br />
                                            <b>Telefon:</b>    <%#Eval("phone") %>
                                            <br />
                                              <b>Yetişkin Sayısı:</b>    <%#Eval("person_count") %> Adet
                                            <br />
                                            <b>Seçilen Tarihler:  </b><%#Eval("reserve_dates") %>
                                           
                                        </span>
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>



                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="rezerv_talepleri.aspx" class="uppercase">Tüm Rezervasyon Listesi</a>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
