﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class ilan_bilgi_tanimlari : Base
    {
    
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    var _parentCategory = db.categories_lang.Where(q => q.lang_id == 1 && q.categories.parent_id == 0 && q.categories.is_deleted==false).ToList();
                    drpCategoryList.DataSource = _parentCategory;
                    drpCategoryList.DataTextField = "category_name";
                    drpCategoryList.DataValueField = "category_id";
                    drpCategoryList.DataBind();
                    drpCategoryList.Items.Insert(0, new ListItem("– Kategori seçin", "0"));

                    var _definitionList = db.ad_info_lang.Where(q => q.lang_id == 1).Select(s => new
                    {
                        s.info_name,
                        s.ad_info_id,
                        s.ad_info.categories.categories_lang.FirstOrDefault(w => w.lang_id == 1).category_name,
                    }).ToList();

                    if (_definitionList.Count() > 0)
                    {
                        rptDefinition.DataSource = _definitionList;
                        rptDefinition.DataBind();
                    }
                }

            }
            catch (Exception)
            {

            }
        }


        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpCategoryList.SelectedValue.toInt() > 0)
                {
                    if (!string.IsNullOrEmpty(txtPropertyName.Text.Trim()))
                    {

                        ad_info _newInfo = new ad_info();
                        _newInfo.info_name = txtPropertyName.Text.Trim();
                        _newInfo.category_id = drpCategoryList.SelectedValue.toInt();

                        db.ad_info.Add(_newInfo);

                        if (db.SaveChanges() > 0)
                        {
                            foreach (var item in db.lang.ToList())
                            {
                                ad_info_lang _newLang = new ad_info_lang();
                                _newLang.lang_id = item.id;
                                _newLang.info_name = txtPropertyName.Text.Trim();
                                _newLang.ad_info_id = _newInfo.id;
                                db.ad_info_lang.Add(_newLang);
                            }

                            if (db.SaveChanges() > 0)
                                ReloadPage();
                        }
                    }
                    else
                        Alert("Özellik adı girmelisiniz.");
                }
                else
                    Alert("Kategori seçmelisiniz");
            }
            catch (Exception)
            {


            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            int info_id = e.CommandArgument.toInt();

            var _delete = db.ad_info_lang.Where(q => q.ad_info_id == info_id).ToList();
            var _delete2 = db.ad_info_value.Where(q => q.info_id == info_id).ToList();
         

            if (_delete.Count() > 0)
            {
                foreach (var item in _delete2)
                {
                    var _delete3 = db.ad_info_value_lang.Where(q => q.ad_info_value_id == item.id).ToList();

                    foreach (var item2 in _delete3)
                    {
                        db.ad_info_value_lang.Remove(item2);
                    }
                    db.SaveChanges();

                    db.ad_info_value.Remove(item);
                }

                foreach (var item in _delete)
                {
                    db.ad_info_lang.Remove(item);
                }





                if (db.SaveChanges() > 0)
                {
                    var _singleDelete = db.ad_info.Where(q => q.id == info_id).FirstOrDefault();

                    db.ad_info.Remove(_singleDelete);

                    db.SaveChanges();

                    ReloadPage();

                }
            }


        }



    }
}