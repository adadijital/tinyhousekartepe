﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class sss_icerik : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSSSDesc.config.toolbar = WebTools.CKToolBar(CkTool.Simple);

            var _List = db.sss_content_lang.Where(q => q.lang_id == 1).ToList();

            if (_List != null)
            {
                rptContent.DataSource = _List;
                rptContent.DataBind();
            }

        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(txtSSSTitle.Text.Trim()))
                {



                    sss_content _newContent = new sss_content();
                    _newContent.sss_title = txtSSSTitle.Text.Trim();
                    _newContent.display_order = 0;
                    db.sss_content.Add(_newContent);
                    if (db.SaveChanges() > 0)
                    {
                        foreach (var item in db.lang.ToList())
                        {
                            sss_content_lang _newLang = new sss_content_lang();
                            _newLang.sss_title = txtSSSTitle.Text.Trim();
                            _newLang.sss_desc = txtSSSDesc.Text.Trim();
                            _newLang.sss_id = _newContent.id;
                            _newLang.lang_id = item.id;

                            db.sss_content_lang.Add(_newLang);

                            if (db.SaveChanges() > 0)
                                ReloadPage();
                        }
                    }
                }
                else
                    Alert("Başlık girmelisiniz");

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }


        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _id = e.CommandArgument.toInt();

                var _List = db.sss_content_lang.Where(q => q.sss_id == _id).ToList();

                if(_List.Count()>0)
                {
                    foreach (var item in _List)
                    {
                        db.sss_content_lang.Remove(item);
                    }

                    if(db.SaveChanges()>0)
                    {
                        var _detail = db.sss_content.Where(q => q.id == _id).FirstOrDefault();

                        if(_detail !=null)
                        {
                            db.sss_content.Remove(_detail);

                            if (db.SaveChanges() > 0)
                                ReloadPage();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

    }
}