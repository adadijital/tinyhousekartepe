﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="blog_yazilari.aspx.cs" Inherits="SeyahatEkspresi.panel.blog_yazilari" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
            <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Haberler</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Kategori</th>
                                <th>Başlık</th>
                                  <th>Okunma</th>
                                     <th>Tarih</th>
                                 <th>Detay</th>
                                   <th>Sil</th>
                            </tr>
                        </thead>
                        <tbody id="SliderResim">
                            <asp:Repeater ID="rptBlog" runat="server">
                                <ItemTemplate>
                                    <tr id='Satir<%#Eval("blog_id")%>'>
                                        <td><%#Eval("category_name") %> </td>
                                        <td><%#Eval("blog_title") %> </td>
                                        <td><%#Eval("view_count") %> kez okundu </td>
                                         <td><%#Eval("created_time") %> </td>
                                        <td>
                                            <a href="blog_detay.aspx?lang_id=1&blog_id=<%#Eval("blog_id") %>" class="btn bg-navy btn-flat">Detay</a>
                                        </td>
                                        <td>
                                                <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command"  CommandArgument='<%#Eval ("blog_id") %>' OnClientClick="return confirm('Silmek istediğinizden emin misiniz ?');" CssClass="btn btn-s btn-danger">Sil
                                            </asp:LinkButton>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
