﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class blog_detay : Base
    {
        int blog_id;
        byte lang_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["blog_id"]))
            {
                blog_id = Request.QueryString["blog_id"].toInt();
                lang_id = Request.QueryString["lang_id"].toByte();

                if (!IsPostBack)
                {
                    txtBlogDesc.config.toolbar = WebTools.CKToolBar(CkTool.Simple);

                    var _categoryList = db.blog_category_lang.Where(q => q.lang_id == 1 && q.blog_category.is_active).ToList();
                    if (_categoryList != null)
                    {
                        drpCategory.DataTextField = "category_name";
                        drpCategory.DataValueField = "blog_category_id";
                        drpCategory.DataSource = _categoryList;
                        drpCategory.DataBind();
                        drpCategory.Items.Insert(0, "Kategori Seçiniz");
                    }

                    var _detail = db.blog_lang.Where(q => q.lang_id == lang_id && q.blog_id == blog_id).Select(s => new
                    {
                        s.blog_id,
                        s.blog.blog_category.blog_category_lang.Where(q => q.lang_id == 1).FirstOrDefault().category_name,
                        s.blog.category_id,
                        s.blog.created_time,
                        s.blog_title,
                        s.blog_desc,
                        s.blog.image_path,
                    }).FirstOrDefault();

                    if (_detail != null)
                    {
                        txtBlogTitle.Text = _detail.blog_title;
                        txtBlogDesc.Text = _detail.blog_desc;
                        drpCategory.SelectedValue = _detail.category_id.ToString();
                        Image1.ImageUrl = "/uploads/blog/"+_detail.image_path;

                    }
                }

                rptLangs.DataSource = db.lang.ToList();
                rptLangs.DataBind();
            }
            else
                Response.Redirect("/panel/default.aspx", false);


        }

        protected void rptLangs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hypLangLink = (HyperLink)e.Item.FindControl("hypLangLink");
            int lang = DataBinder.Eval(e.Item.DataItem, "id").toInt();

            hypLangLink.Attributes.Add("lid", lang.ToString());
            hypLangLink.NavigateUrl = "blog_detay.aspx?blog_id=" + blog_id + "&lang_id=" + lang.ToString();

            if (lang_id == DataBinder.Eval(e.Item.DataItem, "id").toShort())
            {
                hypLangLink.CssClass = "btn btn-flat bg-navy disabled color-palette";

            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpCategory.SelectedValue.toInt() > 0)
                {
                    if (!string.IsNullOrEmpty(txtBlogTitle.Text.Trim()))
                    {
                        if (!string.IsNullOrEmpty(txtBlogDesc.Text.Trim()))
                        {
                            int category_id = drpCategory.SelectedValue.toInt();
                            var _detail = db.blog_lang.Where(q => q.lang_id == lang_id && q.blog_id == blog_id).FirstOrDefault();

                            if(_detail !=null)
                            {
                                _detail.blog.category_id = category_id;
                                _detail.blog_title = txtBlogTitle.Text.Trim();
                                _detail.blog_desc = txtBlogDesc.Text.Trim();

                                if (FileUpload1.HasFile)
                                {
                                    int uzunluk = FileUpload1.FileName.Split('.').Length;
                                    string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
                                    FileUpload1.SaveAs(Server.MapPath("~/uploads/blog/" + random));
                                    _detail.blog.image_path = random;
                                }

                                if (db.SaveChanges() > 0)
                                    ReloadPage();
                            }
                         
                        

                        
                        }
                        else
                            Alert("Yazı girmelisiniz");
                    }
                    else
                        Alert("Başlık girmelisiniz");
                }
                else
                    Alert("Kategori seçmelisiniz");


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}