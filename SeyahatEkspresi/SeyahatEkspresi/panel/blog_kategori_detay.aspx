﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="blog_kategori_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.blog_kategori_detay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <asp:Repeater ID="rptLangs" runat="server" OnItemDataBound="rptLangs_ItemDataBound">
                            <ItemTemplate>
                                <asp:HyperLink ID="hypLangLink" runat="server" CssClass="btn bg-navy btn-flat"><%#Eval("lang_name") %></asp:HyperLink>
                            </ItemTemplate>
                        </asp:Repeater>
                    </h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kategori Adı </label>
                        <asp:TextBox ID="txtCategoryName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Aktif / Pasif</label>
                        <asp:CheckBox ID="ckIsActive" runat="server" CssClass="form-control" />
                    </div>


                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Güncelle" class="btn btn-primary"  OnClick="btnEkle_Click" /><br />
                </div>
            </div>
        </div>
            </div>
</asp:Content>
