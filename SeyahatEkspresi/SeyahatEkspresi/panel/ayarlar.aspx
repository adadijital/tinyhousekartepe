﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="ayarlar.aspx.cs" Inherits="SeyahatEkspresi.panel.ayarlar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-6">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"> Site Ayarları </h3>
                </div>
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Adres</label>
                        <asp:TextBox ID="txtOfficeAddres" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">E-Posta 1</label>
                        <asp:TextBox ID="txtEposta1" runat="server" class="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">E-Posta 2</label>
                        <asp:TextBox ID="txtEposta2" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Telefon 1 </label>
                        <asp:TextBox ID="txtTelefon1" runat="server" class="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Telefon 2 </label>
                        <asp:TextBox ID="txtTelefon2" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Facebook Link </label>
                        <asp:TextBox ID="txtFacebook" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Twitter Link </label>
                        <asp:TextBox ID="txtTwitter" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Instagram Link </label>
                        <asp:TextBox ID="txtInstagram" runat="server" class="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Footer Yazı </label>
                        <asp:TextBox ID="FooterHakkimizda" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>



                </div>

                <div class="box-footer">
                    <asp:Button ID="btnGuncelle" runat="server" Text="Site Ayarlarını Güncelle" class="btn btn-primary" OnClick="btnGuncelle_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
