﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="siparis_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.siparis_detay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-md-3">
            <div class="box box-info box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Sipariş Bilgileri</h3>
                </div>
                <div class="box-body box-profile">
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item"><b>Sipariş No</b> <span class="pull-right">
                            <asp:Literal ID="ltrOrderCode" runat="server"></asp:Literal></span> </li>
                        <li class="list-group-item"><b>Sipariş Tarihi</b> <span class="pull-right">
                            <asp:Literal ID="ltrCreatedDate" runat="server"></asp:Literal></span> </li>
                             <li class="list-group-item"><b>Satıcı</b> <span class="pull-right">
                            <asp:HyperLink ID="hypSatici" runat="server">
                                <asp:Literal ID="ltrSatici" runat="server"></asp:Literal>
                            </asp:HyperLink>
                        </span></li>
                        <li class="list-group-item"><b>Alıcı</b> <span class="pull-right">
                            <asp:HyperLink ID="hypMemberDetail" runat="server">
                                <asp:Literal ID="ltrNameSurname" runat="server"></asp:Literal>
                            </asp:HyperLink>
                        </span></li>

                       
                        <li class="list-group-item"><b>Eposta</b> <span class="pull-right">
                            <asp:Literal ID="ltrEmail" runat="server"></asp:Literal></span> </li>
                        <li class="list-group-item"><b>Telefon</b> <span class="pull-right">
                            <asp:Literal ID="ltrPhone" runat="server"></asp:Literal></span> </li>
                        <li class="list-group-item"><b>IP Adresi</b> <span class="pull-right">
                            <asp:Literal ID="ltrIpAddress" runat="server"></asp:Literal></span> </li>

                        <li class="list-group-item"><b>Sipariş Durumu</b> <span runat="server" id="siparis_durum" style="font-size: 100%">
                            <asp:Literal ID="ltrSituationName" runat="server"></asp:Literal></span> </li>

                        <li class="list-group-item"><b>Sipariş Tutarı</b> <span class="pull-right">
                            <asp:Literal ID="ltrOrderTotal" runat="server"></asp:Literal>
                            <sup>TL</sup></span> </li>
                      

                        <li class="list-group-item">
                            <asp:DropDownList ID="drpSituations" runat="server" CssClass="form-control durum"></asp:DropDownList></li>

                    </ul>
                    <asp:Button ID="btnSituationUpdate" runat="server" class="btn btn-primary btn-block durumdegistir" OnClick="btnSituationUpdate_Click1" Text="Sipariş Durumunu Değiştir" OnClientClick="return ValidText();" />
                    <br />
       
                </div>


            </div>


        </div>


        <div class="col-md-9">
            <div class="box box-success box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Ödemeler</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Ödeme Tipi</th>
                                <th>Ad Soyad</th>
                                <th>Kart No</th>
                                <th>Tutar</th>
                                <th>Kaydedilme</th>
                                <th>Durum</th>
                                <th>İşlem</th>

                            </tr>
                            <asp:Repeater runat="server" ID="rptPayments" OnItemDataBound="rptPayments_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("payment_name") %> </td>
                                        <td><%#Eval("card_name_surname") %></td>
                                        <td><%#Eval("card_no") %></td>
                                        <td><%# Eval("payment_amount") %><sup>TL</sup></td>
                                        <td><%# Eval("payment_time") %></td>
                                        <td>
                                            <asp:Literal ID="ltrStatus" runat="server" /></td>
                                        <td>
                                            <asp:LinkButton ID="lnkOdendiYap" runat="server" CssClass="paymentUpdate btn btn-xs btn-info" CommandArgument='<%#Eval("payment_id") %>' OnCommand="lnkOdendiYap_Command">Ödendi Yap</asp:LinkButton>
                                        </td>

                                    </tr>

                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
             <div class="col-md-4">
                    <div class="box box-info box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Teslimat Bilgileri</h3>
                        </div>
                        <div class="box-body">
                            <strong><i class="fa fa-map-marker margin-r-5"></i>
                                <asp:Literal ID="ltrShippingAddresNameSurname" runat="server"></asp:Literal></strong>
                            <p>
                                <asp:Literal ID="ltrShippingAddress" runat="server"></asp:Literal><br />
                                <asp:Literal ID="ltrShippingCity" runat="server"></asp:Literal>
                                <br />
                                Telefon :
                                <asp:Literal ID="ltrShippingPhone" runat="server"></asp:Literal>
                            </p>
                        </div>
                        <div class="panel-footer">
                            <asp:HyperLink ID="hlAddressUpd1" runat="server" Text="Güncelle" CssClass="btn btn-info btn-xs" />
                        </div>
                    </div>
                </div>

         <div class="col-md-1">
             </div>

                  <div class="col-md-4">
                    <div class="box box-info box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Fatura Bilgileri</h3>
                        </div>
                        <div class="box-body">
                            <strong><i class="fa fa-map-marker margin-r-5"></i>
                                <asp:Literal ID="ltrBillingAdressNameSurname" runat="server"></asp:Literal></strong>
                            <p>
                                <asp:Literal ID="ltrBillingAddress" runat="server"></asp:Literal><br />
                                <asp:Literal ID="ltrBillingCity" runat="server"></asp:Literal>
                                <br />
                                Telefon :
                                <asp:Literal ID="ltrBillingPhone" runat="server"></asp:Literal><br />
                             
                            </p>
                        </div>
                        <div class="panel-footer">
                            <asp:HyperLink ID="hlAddressUpd2" runat="server" Text="Güncelle" CssClass="btn btn-info btn-xs" />
                        </div>
                    </div>
                </div>

 
        <div class="col-md-3">
            </div>
        <div class="col-md-9">
            <div class="box box-info box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Ürünler</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Resim</th>
                                <th>Ürün Adı</th>
                            
                                <th>Birim Fiyatı</th>
                                <th>Adet</th>
                          
                                <th>Ürün Toplamı</th>
                               

                            </tr>
                            <asp:Repeater ID="rptProducts" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <a href="/panel/urun_detay.aspx?product_id=<%#Eval("product_id") %>">
                                                <img src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/products/75x120/<%#Eval("image_name") %>" alt="" class="img-responsive" style="width: 60px" /></a></td>
                                        <td>
                                            <a href="/panel/urun_detay.aspx?product_id=<%#Eval("product_id") %>">
                                            <%#Eval("product_name") %>
                                            <%#Eval("confirm_time") %>
                                        </a>
                                        </td>
    
                                        <td><%#Eval("unit_sell_price") %> <sup>TL</sup></td>
                                        <td><%#Eval("quantity") %> Adet</td>
                                        <td><%#Eval("total_sell_price") %> <sup>TL</sup></td>
                                      


                                    </tr>



                                </ItemTemplate>
                            </asp:Repeater>

                        </tbody>
                    </table>
                    <table class="table" style="width: 250px; float: right">
                        <tbody>

                            <tr>
                                <th colspan="9"></th>
                                <th>Ürünler Toplamı</th>
                                <th><span>
                                    <asp:Literal ID="ltrTotalMarket1" runat="server" /><sup>TL</sup></span>
                                </th>
                            </tr>
                   
                          

                        


                            <tr>
                                <th colspan="9"></th>
                                <th>Toplam Tutar</th>
                                <th><span>
                                    <asp:Literal ID="ltrTotalPayment" runat="server" /><sup>TL</sup></span></th>
                            </tr>



                        </tbody>
                    </table>


                </div>

            </div>
        </div>

    </div>


</asp:Content>
