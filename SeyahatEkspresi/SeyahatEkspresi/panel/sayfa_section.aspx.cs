﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class sayfa_section : Base
    {
        short page_id;
        int section_group;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["page_id"]))
            {
                page_id = Request.QueryString["page_id"].toShort();
                var _pageSectionList = db.page_content.Where(q => q.lang_id == 1 && q.page_id == page_id).ToList();

                if (_pageSectionList != null)
                {
                    rptPageSectionList.DataSource = _pageSectionList;
                    rptPageSectionList.DataBind();
                }
            }
            else
                Response.Redirect("/", false);
        }

   
    }
}