﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="rezerv_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.rezerv_detay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">

        <section class="content-header">
            <h3>
                <asp:Literal ID="ltrAdsName" runat="server"></asp:Literal>
                -İlanı için Rezervasyon Talebi

            <span class="pull-right">
                <asp:Literal ID="ltrCreated" runat="server"></asp:Literal>
            </span>
            </h3>
        </section>
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Ad Soyad </b><span class="pull-right">
                                    <asp:Literal ID="ltrNameSurname" runat="server"></asp:Literal>
                                </span>
                            </li>
                            <li class="list-group-item">
                                <b>Telefon </b><span class="pull-right">
                                    <asp:Literal ID="ltrPhone" runat="server"></asp:Literal>
                                </span>
                            </li>
                            <li class="list-group-item">
                                <b>Yetişkin Sayısı </b><span class="pull-right">
                                    <asp:Literal ID="ltrCount" runat="server"></asp:Literal>
                                    Adet
                                </span>
                            </li>
                                  <li class="list-group-item">
                                <b>Tarihler </b><span class="pull-right">
                                    <asp:Literal ID="ltrRezervDates" runat="server"></asp:Literal>
                                   
                                </span>
                            </li>

                      
                            <!--   <li class="list-group-item">
                                <b>Çıkış Tarihi </b><span class="pull-right">
                                    <asp:Literal ID="ltrFinish" runat="server"></asp:Literal>
                                </span>
                            </li>-->

                            <li class="list-group-item">
                                <b>Müşteri Notu </b><span>
                                    <asp:Literal ID="ltrDesc" runat="server"></asp:Literal>
                                </span>
                            </li>

                               <li class="list-group-item">
                                <b>Durum</b><span class="pull-right">
                                    <asp:Literal ID="ltrStatus" runat="server"></asp:Literal>
                                </span>
                            </li>


                        </ul>
                        <div class="box-tools" data-toggle="tooltip" title="" data-original-title="">
                            <asp:Button ID="btnActive" runat="server" Text="Onayla" class="btn btn-success btn-block" OnClick="btnActive_Click" /><br />
                            <asp:Button ID="btnPasif" runat="server" Text="İptal Et" class="btn btn-danger btn-block" OnClick="btnPasif_Click" />
                            <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-warning btn-block">İlana Git</asp:HyperLink>
                        </div>
                    </div>
                </div>

            </div>


            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-title">
                       <h3>  Yönetici Notu</h3>
                    </div>
                    <div class="box-body box-profile">
                        <asp:TextBox ID="txtAdminNote" runat="server" TextMode="MultiLine" CssClass="form-control" style="height:250px"></asp:TextBox>
                    </div>

                      <div class="box-tools">
                      <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" Text="Yönetici Notu Kaydet" class="btn btn-primary btn-block" /><br />

                          </div>
                </div>
            </div>

        </div>

    </section>
</asp:Content>
