﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class ilce_duzenle : Base
    {
        int town_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["town_id"] != null)
                {
                    town_id = Request.QueryString["town_id"].toInt();
                    var _detail = db.geo_town.Where(q => q.id == town_id).FirstOrDefault();

                    if (_detail != null)
                    {
                        if (!IsPostBack)
                        {
                            txtTownName.Text = _detail.town_name;

                            if (!string.IsNullOrEmpty(_detail.img_path))
                            {
                                pnlResim.Visible = true;
                                Image1.ImageUrl = "/uploads/district/M/" + _detail.img_path;
                            }

                        }
                    }
                }


            }
            catch (Exception ex)
            {

            }

        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            var _detail = db.geo_town.Where(q => q.id == town_id).FirstOrDefault();

            if (_detail != null)
            {
                if (FileUpload1.HasFile)
                {
                    int uzunluk = FileUpload1.FileName.Split('.').Length;
                    string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
                    FileUpload1.SaveAs(Server.MapPath("~/uploads/district/" + random));
                    //ResizeImage("~/uploads/category/" + random, 1920, 920);

                    using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/district/" + random)))
                    {
                        img.ScaleAndCrop(370, 495).SaveAs(Server.MapPath("~/uploads/district/M/" + random));
                    }


                    _detail.img_path = random;
                }

                _detail.town_name = txtTownName.Text.Trim();
            }

            if (db.SaveChanges() > 0)
                ReloadPage();


        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            var _detail = db.geo_town.Where(q => q.id == town_id).FirstOrDefault();

            if (_detail != null)
            {
                _detail.img_path = null;
            }

            if (db.SaveChanges() > 0)
                ReloadPage();

        }
    }
}
