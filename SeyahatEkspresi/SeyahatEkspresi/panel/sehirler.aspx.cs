﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class sehirler : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _List = db.geo_cities.OrderBy(o=> o.PlakaNo).ToList();

            if (_List.Count() > 0)
            {
                rptCity.DataSource = _List;
                rptCity.DataBind();
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCityName.Text.Trim()))
                {
                    geo_cities _newCity = new geo_cities();
                    _newCity.city_name = txtCityName.Text.Trim();
                    db.geo_cities.Add(_newCity);

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
                else
                    Alert("Şehir adı girmelisiniz.");
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}