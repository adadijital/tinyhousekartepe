﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="siparisler.aspx.cs" Inherits="SeyahatEkspresi.panel.siparisler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Siparişler
         
                    </h3>
                    <div class="box-tools">
                        <div class="input-group" style="width: 350px;">
                            <asp:TextBox ID="txtSearch" runat="server" placeholder="Anahtar Kelime" CssClass="form-control input-sm pull-right"></asp:TextBox>
                            <div class="input-group-btn">

                                <asp:Button class="btn btn-sm btn-default" runat="server" Text="ARAMA YAP" ID="btnArama" OnClick="btnArama_Click" />
                            </div>



                        </div>

                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <asp:Repeater ID="rptOrderList" runat="server" OnItemDataBound="rptOrderList_ItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-bordered ">
                                <thead>
                                    <tr>
                                        <th>Sıra</th>
                                        <th>#ID</th>
                                        <th>Sipariş Tarihi</th>
                                        <th>Satıcı</th>
                                        <th>Müşteri</th>
                                        <th>Ödeme Tipi</th>
                                        <th>Tutar</th>
                                        <th>Ürün Adet</th>
                                        <th>Durum</th>
                                        <th>Detay</th>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>

                            <tr class="durum<%#Eval("situation_id") %> ">
                                <td><%# Container.ItemIndex + 1 %></td>
                                <td><a style="color: black" href="siparis_detay.aspx?order_id=<%#Eval("order_id") %>">#<%#Eval("order_id") %></a></td>
                                <td><a style="color: black" href="siparis_detay.aspx?order_id=<%#Eval("order_id") %>"><%#Eval("created_time") %></a></td>
                                <td><%#Eval("supplier_name") %>  </td>
                                <td><%#Eval("member_name") %>  </td>
                                <td><%#Eval("payment_type_name") %></td>

                                <td><%#Eval("payment_amount") %> TL</td>

                                <td><%#Eval("product_count") %></td>

                                <td>
                                    <asp:Literal ID="ltrSituation" runat="server"></asp:Literal></td>

                                <td><a href="siparis_detay.aspx?order_id=<%#Eval("order_id") %>" class="btn btn-block btn-primary btn-xs">Detay
                                </a></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                         </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <asp:Label ID="lblNoData" runat="server" Text=""></asp:Label>
                    <div class="text-center">
                        <ul class="pagination">
                            <asp:Literal runat="server" ID="ltrPaging" />
                        </ul>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</asp:Content>
