﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class tur_listesi : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _list = db.our_tour.OrderBy(o => o.display_order).Select(s => new
            {
                s.id,
                s.tour_name,
                s.tour_time,
                s.tour_category.category_name,
                s.sell_price
            }).ToList();

            if (_list.Count() > 0)
            {
                rptList.DataSource = _list;
                rptList.DataBind();
            }
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            int _id = e.CommandArgument.toInt();

            var _images = db.tour_images.Where(q => q.tour_id == _id).ToList();

           if(_images.Count()>0)
            {
                foreach (var item in _images)
                {
                    db.tour_images.Remove(item);
                }

                db.SaveChanges();
            }

            var _remove = db.our_tour.Where(q => q.id == _id).FirstOrDefault();

            if(_remove !=null)
            {
                db.our_tour.Remove(_remove);
            }

            if (db.SaveChanges() > 0)
                ReloadPage();
        }
    }
}