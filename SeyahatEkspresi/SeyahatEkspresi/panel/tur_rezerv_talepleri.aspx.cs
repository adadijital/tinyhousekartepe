﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class tur_rezerv_talepleri : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _rezerv = db.tour_reservasiton_request.Select(s => new
            {
                s.name_surname,
                s.id,
                s.start_date,
                s.finish_date,
                s.created_time,
                s.phone,
                s.our_tour.tour_name,
                tour_id = s.id,
                s.reserve_dates,
                s.person_count,
                s.is_approved,
                s.is_cancel,
            }).OrderByDescending(o => o.id).ToList();

            if (_rezerv.Count() > 0)
            {
                rptRezerv.DataSource = _rezerv;
                rptRezerv.DataBind();
            }
        }

        protected void rptAds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {


            LinkButton LinkButton2 = (LinkButton)e.Item.FindControl("LinkButton2");
            LinkButton LinkButton3 = (LinkButton)e.Item.FindControl("LinkButton3");

            Literal ltrActive = (Literal)e.Item.FindControl("ltrActive");


            bool is_approved = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_approved"));
            bool is_cancel = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_cancel"));



            if (is_approved)
            {
                ltrActive.Text = "<span class='label label-success'>Onaylandı</span>";
                LinkButton2.Visible = false;
                LinkButton3.Visible = false;

            }

            if (is_approved == false && is_cancel == false)
            {
                ltrActive.Text = "<span class='label label-warning'>Onay Bekliyor</span>";

            }

            if (is_approved == false && is_cancel == true)
            {
                ltrActive.Text = "<span class='label label-danger'>İptal Edildi</span>";
                LinkButton2.Visible = false;
                LinkButton3.Visible = false;

            }





        }

        protected void LinkButton2_Command(object sender, CommandEventArgs e)
        {
            int _id = e.CommandArgument.toInt();

            var _detail = db.tour_reservasiton_request.Where(q => q.id == _id).FirstOrDefault();

            if (_detail != null)
            {
                _detail.is_approved = true;
                _detail.is_cancel = false;

                if (db.SaveChanges() > 0)
                {
                    ReloadPage();
                }
            }
        }

        protected void LinkButton3_Command(object sender, CommandEventArgs e)
        {
            int _id = e.CommandArgument.toInt();

            var _detail = db.tour_reservasiton_request.Where(q => q.id == _id).FirstOrDefault();

            if (_detail != null)
            {
                _detail.is_approved = false;
                _detail.is_cancel = true;

                if (db.SaveChanges() > 0)
                {
                    ReloadPage();
                }
            }
        }
    }
}