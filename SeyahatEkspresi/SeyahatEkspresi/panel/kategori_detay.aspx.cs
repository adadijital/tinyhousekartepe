﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class kategori_detay : Base
    {
        int category_id;
        byte lang_id;
        List<CategoryTreeView> _categoryHierarchy = new List<CategoryTreeView>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["category_id"]))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["lang_id"]))
                    {

                        category_id = Request.QueryString["category_id"].toInt();
                        lang_id = Request.QueryString["lang_id"].toByte();
                        _fillCategoryHierarchy(db.categories_lang.Where(q => q.categories.parent_id == 0 && q.lang_id == 1).ToList());

                        var _lang = db.lang.Where(q => q.is_active).ToList();
                        if (_lang.Count() > 0)
                        {
                            rptLang.DataSource = _lang;
                            rptLang.DataBind();
                        }

                        if (!IsPostBack)
                        {
                            var _detail = db.categories_lang.Where(q => q.lang_id == lang_id && q.category_id == category_id).FirstOrDefault();
                            if (_detail != null)
                            {
                                var _subCategory = db.categories_lang.Where(q => q.categories.parent_id == category_id && q.lang_id == 1).Select(s => new {
                                    s.id,
                                    s.category_id,
                                    s.category_name,
                                    active_status = s.categories.is_active ? "Aktif" : "Pasif"

                                }).ToList();

                                if (_subCategory.Count() > 0)
                                {
                                    rptCategory.DataSource = _subCategory;
                                    rptCategory.DataBind();
                                }

                                _fillCategoryDropdown();
                                txtCategoryName.Text = _detail.category_name;
                                ckIsActive.Checked = _detail.categories.is_active;
                                drpCategoryList.SelectedValue = _detail.categories.parent_id.ToString();
                                

                                if(!string.IsNullOrEmpty(_detail.categories.icon_path))
                                {
                                    pnlResim.Visible = true;
                                    Image1.ImageUrl = "/uploads/category/M/"+ _detail.categories.icon_path;
                                }

                            }

                        }





                    }
                    else
                        Response.Redirect("/panel/default.aspx", false);
                }
                else
                    Response.Redirect("/panel/default.aspx", false);



            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
        private void _fillCategoryDropdown()
        {
            try
            {
                drpCategoryList.DataSource = _categoryHierarchy;
                drpCategoryList.DataTextField = "CategoryName";
                drpCategoryList.DataValueField = "CategoryID";
                drpCategoryList.DataBind();
                drpCategoryList.Items.Insert(0, new ListItem("– Kategori seçin", "0"));
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }


        private void _fillCategoryHierarchy(List<categories_lang> _categories, int index = 0)
        {
            if (_categories.Count > 0)
            {
                index++;

                foreach (var item in _categories)
                {
                    string _prefix = string.Empty;

                    if (index > 1)
                        for (int i = 1; i < index; i++)
                            _prefix += "-";

                    _categoryHierarchy.Add(new CategoryTreeView
                    {
                        CategoryID = item.category_id,
                        CategoryName = _prefix + " " + item.category_name,
                        DisplayOrder = item.categories.display_order,
                        IsActive = item.categories.is_active
                    });

                    List<categories_lang> _subCategories = db.categories_lang.Where(q => q.categories.parent_id == item.category_id && q.lang_id == 1).ToList();

                    if (_subCategories.Count > 0)
                        _fillCategoryHierarchy(_subCategories, index);
                }
            }
        }


        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCategoryName.Text.Trim()))
                {
                    var _detail = db.categories_lang.Where(q => q.lang_id == lang_id && q.category_id == category_id).FirstOrDefault();

                    if (_detail != null)
                    {

                        _detail.category_name = txtCategoryName.Text.Trim();
                        _detail.categories.is_active = ckIsActive.Checked;
                        _detail.categories.parent_id = drpCategoryList.SelectedValue.toInt();

                        if (FileUpload1.HasFile)
                        {
                            int uzunluk = FileUpload1.FileName.Split('.').Length;
                            string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
                            FileUpload1.SaveAs(Server.MapPath("~/uploads/category/" + random));
                            //ResizeImage("~/uploads/category/" + random, 1920, 920);

                            using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/category/" + random)))
                            {
                                img.ScaleAndCrop(370, 285).SaveAs(Server.MapPath("~/uploads/category/M/" + random));
                            }


                            _detail.categories.icon_path = random;
                        }

                        if (db.SaveChanges() > 0)
                            ReloadPage();
                    }
                }
                else
                    Alert("Kategori Adı Girmelisiniz");

            }
            catch (Exception)
            {

            }
        }

        protected void rptLang_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hypLangLink = (HyperLink)e.Item.FindControl("hypLangLink");
            int lang_id = DataBinder.Eval(e.Item.DataItem, "id").toInt();
            hypLangLink.NavigateUrl = "kategori_detay.aspx?category_id=" + category_id + "&lang_id=" + lang_id.ToString();
            if (lang_id == Request.QueryString["lang_id"].toShort())
                hypLangLink.CssClass = "btn btn-flat bg-navy disabled color-palette";
        }

        protected void rptCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rptLangs = (Repeater)e.Item.FindControl("rptLangs");
            category_id = DataBinder.Eval(e.Item.DataItem, "category_id").toInt();
            var _LangList = db.lang.Where(q => q.is_active).ToList();

            if (_LangList.Count() > 0)
            {
                rptLangs.DataSource = _LangList;
                rptLangs.DataBind();
            }
        }
        protected void rptLangs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hypLangLink = (HyperLink)e.Item.FindControl("hypLangLink");
            int lang_id = DataBinder.Eval(e.Item.DataItem, "id").toInt();
            hypLangLink.NavigateUrl = "kategori_detay.aspx?category_id=" + category_id + "&lang_id=" + lang_id.ToString();
        }

    }
}