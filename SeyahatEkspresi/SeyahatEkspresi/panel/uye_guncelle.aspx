﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="uye_guncelle.aspx.cs" Inherits="SeyahatEkspresi.panel.uye_guncelle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                 <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Üye Bilgileri Güncelle</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Üye Adı </label>
                        <asp:TextBox ID="txtMemberName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">E-Mail</label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    
                </div>
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                </div>
            </div>
            </div>
        </div>
    </div>
</asp:Content>
