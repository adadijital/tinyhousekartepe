﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="siparis_adres_guncelle.aspx.cs" Inherits="SeyahatEkspresi.panel.siparis_adres_guncelle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-success"> << Sipariş Sayfasına Dön</asp:HyperLink><br />
                    <br />
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Teslimat Adresini Güncelle</h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ad Soyad</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtAd1" runat="server" CssClass="form-control" />
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Şehir</label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList runat="server" ID="dlSehir1" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="dlSehir1_SelectedIndexChanged">
                                         
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group" id="dvTownId" runat="server">
                                    <label class="col-sm-2 control-label">İlçe</label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList runat="server" ID="dlIlce1" CssClass="form-control">
                                            <asp:ListItem Text="-Seçiniz" />
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Adres</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtAdres1" runat="server" CssClass="form-control" TextMode="MultiLine" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Telefon</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtTel1" runat="server" CssClass="form-control" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="panel-footer text-right">
                            <asp:Button ID="btnGuncelleTeslimat" runat="server" Text="Güncelle" CssClass="btn btn-success" OnClick="btnGuncelleTeslimat_Click" />
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Fatura Bilgilerini Güncelle</h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ad Soyad</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtAd2" runat="server" CssClass="form-control" />
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Şehir</label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="dlSehir2" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="dlSehir2_SelectedIndexChanged">
                                         
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group" id="dvTownId2" runat="server">
                                        <label class="col-sm-2 control-label">İlçe</label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="dlIlce2" CssClass="form-control">
                                                <asp:ListItem Text="-Seçiniz" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Adres</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtAdres2" runat="server" CssClass="form-control" TextMode="MultiLine" />
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Telefon</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtTel2" runat="server" CssClass="form-control" />
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                        <div class="panel-footer text-right">
                            <asp:Button ID="btnGuncelleFatura" runat="server" Text="Güncelle" CssClass="btn btn-success" OnClick="btnGuncelleFatura_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
