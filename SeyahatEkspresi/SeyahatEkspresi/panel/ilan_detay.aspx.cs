﻿using SeyahatEkspresi.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class ilan_detay :Base
    {
        int ads_id;
        byte lang_id;
        int parent_id;
        AdvertManager _advertManager = new AdvertManager();
        List<CategoryTreeView> _categoryHierarchy = new List<CategoryTreeView>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["ads_id"] != null)
                {
                    if (Request.QueryString["lang_id"] != null)
                    {
                        hdnID.Value = Request.QueryString["ads_id"].ToString();


                        ads_id = Request.QueryString["ads_id"].toInt();
                        lang_id = Request.QueryString["lang_id"].toByte();
                        _fillCategoryHierarchy(db.categories_lang.Where(q => q.categories.parent_id == 0 && q.lang_id == 1 && q.categories.is_active && q.categories.is_deleted).ToList());
                        txtDesc.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
                        txtDesc.integrationPlugin();


                        if (!IsPostBack)
                        {
                            _fillCategoryDropdown();
                            var _detail = _advertManager.getSingle(ads_id, lang_id);

                            if (_detail != null)
                            {

                                var _lang = db.lang.Where(q => q.is_active).ToList();
                                if (_lang.Count() > 0)
                                {
                                    rptLang.DataSource = _lang;
                                    rptLang.DataBind();
                                }



                                ltrHeaderAdvertTitle.Text = _detail.advert_name;
                                //ltrHeaderAdvertTypeStatus.Text = _detail.type_status.ToString();

                                parent_id = _detail.parent_category_id;




                                _getCities(_detail.geo_country_id);
                                _getTowns(_detail.geo_city_id);
                                _getQuarter(_detail.geo_town_id);

                                txtAddres.Text = _detail.address;
                                txtAdvertTitle.Text = _detail.advert_name;
                                txtKisiSayisi.Text = _detail.kisi_sayisi;
                                txtYatakSayisi.Text = _detail.yatak_sayisi;
                                txtBanyoSayisi.Text = _detail.banyo_sayisi;
                                txtisinmaTuru.Text = _detail.isinma_turu;
                                txtKonumDurumu.Text = _detail.konum_durumu;
                                txtMarketPrice.Text = _detail.market_price.ToString();
                                txtWeekPrice.Text = _detail.week_price.ToString();
                                hdnWeekPrice.Value = _detail.week_price.ToString();

                                txtSellPrice.Text = _detail.sell_price.ToString();
                                
                                drpCity.SelectedValue = _detail.geo_city_id.ToString();
                                drpTown.SelectedValue = _detail.geo_town_id.ToString();
                              
                                ckIsActive.Checked = _detail.is_active;

                                ltrPrices2.Text = _detail.sell_price.ToString();


                                txtDesc.Text = _detail.description;
                                ckShowCase.Checked = _detail.is_showcase;
                              
                                var _AdsCategory = db.relation_ads_category.Where(q=> q.categories.is_active).Select(s => new
                                {
                                    relation_id = s.id,
                                    s.categories.categories_lang.Where(q => q.lang_id == 1).FirstOrDefault().category_name,
                                    s.ads_id,
                                    s.cat_id,
                                    s.categories.parent_id
                                }).OrderBy(o => o.parent_id).Where(q => q.ads_id == ads_id).ToList();


                                rptCategories.DataSource = _AdsCategory;
                                rptCategories.DataBind();




                                var _definitions = db.ad_info.Where(q => q.ad_info_lang.Where(q2 => q2.lang_id == lang_id).Count() > 0 && q.category_id == parent_id)
                                .Select(s => new
                                {
                                    s.ad_info_lang.FirstOrDefault(f => f.lang_id == lang_id).info_name,
                                    s.id,
                                    s.ad_info_value.Where(q => q.ads_id == ads_id && q.ad_info_value_lang.Where(q2 => q2.lang_id == lang_id).Count() > 0).FirstOrDefault().ad_info_value_lang.FirstOrDefault().ad_info_value
                                }).ToList();

                                if (_definitions.Count() > 0)
                                {
                                    rptAdInfoDefinition.DataSource = _definitions;
                                    rptAdInfoDefinition.DataBind();
                                }

                                var _images = db.ads_images.Where(q => q.ads_id == ads_id).OrderBy(o => o.display_order).ToList();

                                if (_images.Count() > 0)
                                {
                                    rptImages.DataSource = _images;
                                    rptImages.DataBind();
                                }
                            }
                        }

                    }
                    else
                        Response.Redirect("/panel/default.aspx", false);
                }
                else
                    Response.Redirect("/panel/default.aspx", false);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        private void _fillCategoryDropdown()
        {
            drpCategoryList.DataSource = _categoryHierarchy;
            drpCategoryList.DataTextField = "CategoryName";
            drpCategoryList.DataValueField = "CategoryID";
            drpCategoryList.DataBind();
            drpCategoryList.Items.Insert(0, new ListItem("– Kategori seçin", "0"));
        }

        private void _fillCategoryHierarchy(List<categories_lang> _categories, int index = 0)
        {
            if (_categories.Count > 0)
            {
                index++;

                foreach (var item in _categories)
                {
                    string _prefix = string.Empty;

                    if (index > 1)
                        for (int i = 1; i < index; i++)
                            _prefix += "-";

                    _categoryHierarchy.Add(new CategoryTreeView
                    {
                        CategoryID = item.category_id,
                        CategoryName = _prefix + " " + item.category_name,
                        DisplayOrder = item.categories.display_order,
                        IsActive = item.categories.is_active
                    });

                    List<categories_lang> _subCategories = db.categories_lang.Where(q => q.categories.parent_id == item.category_id && q.lang_id == 1 && q.categories.is_active).ToList();

                    if (_subCategories.Count > 0)
                        _fillCategoryHierarchy(_subCategories, index);
                }
            }
        }
        private void _getTowns(int CityID)
        {
            var _town = db.geo_town.Where(q => q.city_id == CityID).ToList();

            if (_town.Count() > 0)
            {
                drpTown.DataSource = _town;
                drpTown.DataTextField = "town_name";
                drpTown.DataValueField = "id";
                drpTown.DataBind();
                drpTown.Items.Insert(0, new ListItem("– İlçe seçin", "0"));
            }


            var _quarter = db.geo_quarter.ToList();


        }

        private void _getQuarter(int townID)
        {
            //var _quarter = db.geo_quarter.Where(q => q.town_id == townID).ToList();

            //if (_quarter.Count() > 0)
            //{
              
            //    drpQuarter.DataSource = _quarter;
            //    drpQuarter.DataTextField = "quarter_name";
            //    drpQuarter.DataValueField = "id";
            //    drpQuarter.DataBind();
            //    drpQuarter.Items.Insert(0, new ListItem("– Mahalle seçin", "0"));
            //}


        }
        
          

    private void _getCities(int CountryID)
        {
            var _cities = db.geo_cities.OrderBy(o => o.PlakaNo).ToList();

            if (_cities.Count() > 0)
            {

                drpCity.DataSource = _cities;
                drpCity.DataTextField = "city_name";
                drpCity.DataValueField = "id";
                drpCity.DataBind();
                drpCity.Items.Insert(0, new ListItem("– Şehir seçin", "0"));
            }
        }

   
        protected void drpCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            _getTowns(drpCity.SelectedValue.toInt());
        }

        protected void rptLang_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                HyperLink hypLangLink = (HyperLink)e.Item.FindControl("hypLangLink");
                int lang_id = DataBinder.Eval(e.Item.DataItem, "id").toInt();
                hypLangLink.NavigateUrl = "/panel/ilan_detay.aspx?lang_id=" + lang_id.ToString() + "&ads_id=" + ads_id;
                if (lang_id == Request.QueryString["lang_id"].toShort())
                    hypLangLink.CssClass = "btn btn-flat bg-navy disabled color-palette";
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }

        protected void btnAddCategory_Click(object sender, EventArgs e)
        {
            try
            {
                #region model_category_add
                if (drpCategoryList.SelectedValue.Count() > 0)
                {
                    int category_id = drpCategoryList.SelectedValue.toInt();

                    var model_category = db.relation_ads_category.FirstOrDefault(q => q.ads_id == ads_id && q.cat_id == category_id);
                    if (model_category == null)
                    {

                        relation_ads_category new_category = new relation_ads_category();

                        new_category.ads_id = ads_id;
                        new_category.cat_id = category_id;

                        db.relation_ads_category.Add(new_category);

                        if (db.SaveChanges() > 0)
                            ReloadPage();
                        else
                            lblCateUyari.Text = "Bir Hata Oluştu";
                    }
                    else
                        lblCateUyari.Text = "Bu ilan seçtiğiniz kategoriye daha önceden eklenmiş.";
                }
                else
                    lblCateUyari.Text = "Kategori Seçiniz.";

                #endregion
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }


        protected void rptCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemIndex != -1)
                {
                    _fillCategoryHierarchy(db.categories_lang.Where(q => q.categories.parent_id == 0 && q.categories.is_active).OrderByDescending(o => o.categories.parent_id).ToList());

                    Literal ltrCategoryName = (Literal)e.Item.FindControl("ltrCategoryName");
                    int category_id = DataBinder.Eval(e.Item.DataItem, "cat_id").toInt();

                    ltrCategoryName.Text = _categoryHierarchy.FirstOrDefault(q => q.CategoryID == category_id && q.IsActive==true).CategoryName;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
        protected void rptCategories_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                #region model_category_remove
                int relation_id = e.CommandArgument.toInt();
                var model_category = db.relation_ads_category.FirstOrDefault(q => q.id == relation_id);

                if (model_category != null)
                {
                    db.relation_ads_category.Remove(model_category);
                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
                #endregion
            }
            catch (Exception ex) { Response.Write(ex); }
        }

       

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                var _detail = db.ads_lang.Where(q => q.ads_id == ads_id && q.lang_id == lang_id).FirstOrDefault();

                if (_detail != null)
                {
         

                    _detail.ads_name = txtAdvertTitle.Text.Trim();
                    _detail.ads.is_active = ckIsActive.Checked;
                    _detail.ads.geo_city_id = drpCity.SelectedValue.toInt();
                    _detail.ads.geo_town_id = drpTown.SelectedValue.toInt();
                    _detail.ads.geo_quarter_id = 1;
                    _detail.ads.address = txtAddres.Text.Trim();
                    _detail.ads.sell_price = txtSellPrice.Text.Trim().todecimal();
                    _detail.description = txtDesc.Text;
                    _detail.ads.market_price = txtMarketPrice.Text.Trim().todecimal();
                    _detail.ads.is_showcase = ckShowCase.Checked;

                    _detail.kisi_sayisi = txtKisiSayisi.Text.Trim();
                    _detail.yatak_sayisi = txtYatakSayisi.Text.Trim();
                    _detail.banyo_sayisi = txtBanyoSayisi.Text.Trim();
                    _detail.isinma_turu = txtisinmaTuru.Text.Trim();
                    _detail.konum_durumu = txtKonumDurumu.Text.Trim();
                    _detail.ads.week_price = txtWeekPrice.Text.Trim().todecimal();

                    if (ckIsActive.Checked)
                    {
                        notifications _newNot = new notifications();
                        _newNot.created_time = DateTime.Now;
                        _newNot.member_id = _detail.ads.member_id;
                        _newNot.text = _detail.ads_name + " Başlıklı ilanınız yönetici tarafından Onaylandı.";

                        db.notifications.Add(_newNot);
                    }
                    else
                    {
                        notifications _newNot = new notifications();
                        _newNot.created_time = DateTime.Now;
                        _newNot.member_id = _detail.ads.member_id;
                        _newNot.text = _detail.ads_name + " Başlıklı ilanınız yönetici tarafından pasife alındı.";

                        db.notifications.Add(_newNot);
                    }

                    db.SaveChanges();


                    foreach (var item in rptAdInfoDefinition.Items)
                    {
                        RepeaterItem rptItem = item as RepeaterItem;
                        TextBox txtProperty = (TextBox)rptItem.FindControl("txtProperty");
                        CheckBox CheckBox1 = (CheckBox)rptItem.FindControl("CheckBox1");

                       // int adInfoID = txtProperty.Attributes["pid"].toInt();
                        int adInfoID = CheckBox1.Attributes["pid"].toInt();

                        if (adInfoID != 0)
                        {
                            int count = db.ad_info_value.Where(q => q.info_id == adInfoID && q.ads_id == ads_id && q.ad_info_value_lang.Where(q2 => q2.lang_id == lang_id).Count() > 0).Count();

                            if (count > 0)
                            {
                                if (!CheckBox1.Checked)
                                {
                                    var _update = db.ad_info_value_lang.Where(q => q.ad_info_value1.info_id == adInfoID && q.ad_info_value1.ads_id == ads_id && q.lang_id == lang_id).FirstOrDefault();
                                    _update.ad_info_value = "False";
                                   
                                    //var _deleteLang = db.ad_info_value_lang.Where(q => q.ad_info_value1.info_id == adInfoID && q.ad_info_value1.ads_id == ads_id && q.lang_id == lang_id).FirstOrDefault();

                                    //if (_deleteLang != null)
                                    //{
                                    //    db.ad_info_value_lang.Remove(_deleteLang);

                                    //    db.SaveChanges();
                                    //}

                                }
                                else
                                {
                                    var _update = db.ad_info_value_lang.Where(q => q.ad_info_value1.info_id == adInfoID && q.ad_info_value1.ads_id == ads_id && q.lang_id == lang_id).FirstOrDefault();
                                    _update.ad_info_value = "True";
                                   
                                }

                                //if (txtProperty.Text.Trim() == null || txtProperty.Text.Trim() == "")
                                //{
                                //    var _deleteLang = db.ad_info_value_lang.Where(q => q.ad_info_value1.info_id == adInfoID && q.ad_info_value1.ads_id == ads_id && q.lang_id == lang_id).FirstOrDefault();

                                //    if (_deleteLang != null)
                                //    {
                                //        db.ad_info_value_lang.Remove(_deleteLang);

                                //        db.SaveChanges();
                                //    }

                                //}
                                //else
                                //{
                                //    var _update = db.ad_info_value_lang.Where(q => q.ad_info_value1.info_id == adInfoID && q.ad_info_value1.ads_id == ads_id && q.lang_id == lang_id).FirstOrDefault();
                                //    _update.ad_info_value = txtProperty.Text.Trim();
                                //    db.SaveChanges();
                                //}
                            }
                            else
                            {
                                //if (!string.IsNullOrEmpty(txtProperty.Text.Trim()))
                                //{

                                    ad_info_value _newValue = new ad_info_value();
                                    _newValue.info_value = txtProperty.Text.Trim();
                                    _newValue.info_id = adInfoID;
                                    _newValue.ads_id = ads_id;

                                    db.ad_info_value.Add(_newValue);

                                    if (db.SaveChanges() > 0)
                                    {
                                        foreach (var LangItem in db.lang.ToList())
                                        {
                                           ad_info_value_lang _newLang = new ad_info_value_lang();
                                        _newLang.ad_info_value = CheckBox1.Checked.ToString();
                                            _newLang.lang_id = LangItem.id;
                                            _newLang.ad_info_value_id = _newValue.id;

                                            db.ad_info_value_lang.Add(_newLang);

                                            if (db.SaveChanges() > 0)
                                                ReloadPage();

                                        }
                                    }
                                //}

                            }


                            if (db.SaveChanges() > 0)
                                ReloadPage();

                        }
                    }


                }
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                Response.Write(ex);
            }
        }



        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                try
                {
                    int ImageID = Convert.ToInt32(e.CommandArgument);
                    var DeleteImage = db.ads_images.Where(q => q.id == ImageID).FirstOrDefault();

                    if (DeleteImage != null)
                    {
                        db.ads_images.Remove(DeleteImage);

                        if (db.SaveChanges() > 0)
                            ReloadPage();
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex);

                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);

            }

        }

        protected void btnAdddImages_Click(object sender, EventArgs e)
        {
            try
            {

                #region ilan resimleri


                if (FileUpload1.HasFiles)
                {

                    IList<HttpPostedFile> SecilenDosyalar = FileUpload1.PostedFiles;

                    for (int i = 0; i < SecilenDosyalar.Count; i++)
                    {
                        ads_images _newImage = new ads_images();
                        int uzunluk = FileUpload1.PostedFiles[i].FileName.Split('.').Length;
                        string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.PostedFiles[i].FileName.Split('.')[uzunluk - 1];

                        FileUpload1.PostedFiles[i].SaveAs(Server.MapPath("~/uploads/ads/" + random));

                        using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/ads/" + random)))
                        {
                            img.ScaleAndCrop(1200, 1600).SaveAs(Server.MapPath("~/uploads/ads/XL/" + random));
                        }

                        using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/ads/" + random)))
                        {
                            img.ScaleAndCrop(600, 800).SaveAs(Server.MapPath("~/uploads/ads/L/" + random));
                        }

                        using (var img = System.Drawing.Image.FromFile(Server.MapPath("~/uploads/ads/" + random)))
                        {
                            img.ScaleAndCrop(370, 495).SaveAs(Server.MapPath("~/uploads/ads/M/" + random));
                        }


                        _newImage.ads_id = ads_id;
                        _newImage.display_order = i;
                        _newImage.img_name = random;

                        db.ads_images.Add(_newImage);

                    }

                    if (db.SaveChanges() > 0)
                        ReloadPage();
                }
                else
                    showWarningBasic(this.Master,"Resim seçmediniz","Dikkat");


                #endregion

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }

        [WebMethod] //resim sıralama methodu
        public static void resimSirala(String item)
        {

            tinyhous_dbEntities db = new tinyhous_dbEntities();
            string gelen = item.Replace("item", "");
            char[] ayrac = new char[] { ',' };
            string[] gelenler = gelen.Split(ayrac);
            int i = 1;

            foreach (string veri in gelenler)
            {
                db.Database.ExecuteSqlCommand("Update ads_images set display_order = " + i + " where id=" + veri.toInt());
                i++;
            }

        }

       

    protected void rptAdInfoDefinition_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {

                TextBox txtProperty = (TextBox)e.Item.FindControl("txtProperty");
                CheckBox CheckBox1 = (CheckBox)e.Item.FindControl("CheckBox1");
              
                txtProperty.Attributes.Add("pid", DataBinder.Eval(e.Item.DataItem, "id").ToString());
                CheckBox1.Attributes.Add("pid", DataBinder.Eval(e.Item.DataItem, "id").ToString());

                if(DataBinder.Eval(e.Item.DataItem, "ad_info_value") !=null)
                {
                    if (!string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "ad_info_value").ToString()))
                    {
                        string ad_info_value = DataBinder.Eval(e.Item.DataItem, "ad_info_value").ToString();

                        if (!string.IsNullOrEmpty(ad_info_value))
                        {
                            if (ad_info_value == "True")
                            {
                                CheckBox1.Checked = true;
                            }
                            else
                            {
                                CheckBox1.Checked = false;
                            }
                        }

                    }
                }
          


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        bool _optimizeImage(string _imagePath)
        {

            try
            {
                //width: 1600
                //height: 1067

                Response.Write(_imagePath);
                Bitmap _baseImage = new Bitmap(_imagePath);


                if (_baseImage.Width > 1920 || _baseImage.Height > 1080)
                {
                    double ratioX = (double)1920 / (double)_baseImage.Width;
                    double ratioY = (double)1080 / (double)_baseImage.Height;
                    double ratio = ratioX < ratioY ? ratioX : ratioY;
                    int _newWidth = Convert.ToInt32(_baseImage.Width * ratio);
                    int _newHeight = Convert.ToInt32(_baseImage.Height * ratio);
                    _baseImage.Dispose();

                    Base.ResizeImage(_imagePath, _newWidth, _newHeight);
                }
                else
                    _baseImage.Dispose();

                return _getFinalVersionOfImage(_imagePath);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Log.Add(ex);
                try
                {
                    File.Delete(_imagePath);
                }
                catch (Exception ex2) { Response.Write(ex2.Message); }

                return false;
            }

        }

        bool _getFinalVersionOfImage(string _imagePath)
        {
            try
            {
                Bitmap _baseImage = new Bitmap(_imagePath);


                Bitmap _newImage = new Bitmap(1920, 1080);
                //width: 1600
                //       height: 1067
                int _x = 0;
                int _y = 0;

                if (_baseImage.Width < 1920)
                    _x = ((1920 - _baseImage.Width) > 1) ? (1920 - _baseImage.Width) / 2 : (1920 - _baseImage.Width);

                if (_baseImage.Height < 1080)
                    _y = ((1080 - _baseImage.Height) > 1) ? (1080 - _baseImage.Height) / 2 : (1080 - _baseImage.Height);

                using (Graphics _graphic = Graphics.FromImage(_newImage))
                {
                    Rectangle _baseRect = new Rectangle(_x, _y, _baseImage.Width, _baseImage.Height);

                    _graphic.FillRectangle(Brushes.White, new Rectangle(0, 0, _newImage.Width, _newImage.Height));
                    _graphic.DrawImageUnscaledAndClipped(_baseImage, _baseRect);
                    _baseImage.Dispose();

                    _newImage.SetResolution(72, 72);
                    _newImage.Save(_imagePath);
                }

                return true;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);

                Log.Add(ex);

                try
                {
                    File.Delete(_imagePath);
                }
                catch (Exception ex2) { Response.Write(ex2.Message); }

                return false;
            }
        }



        protected void btnKaydetBitir_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/tr/ilan-detay-" + txtAdvertTitle.Text.ToURL() + "-" + ads_id.ToString(), false);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        private Bitmap yaz(System.Drawing.Image resim, int genislik, int yukseklik, string yazilacak, float font)
        {
            Bitmap resmim = new Bitmap(resim, genislik, yukseklik);
            try
            {
              
                System.Drawing.Graphics graf = System.Drawing.Graphics.FromImage(resmim);
                System.Drawing.SolidBrush firca = new SolidBrush(Color.FromArgb(140, 255, 255, 255));
                System.Drawing.Font fnt = new Font("calibri", font);//font tipi ve boyutu
                System.Drawing.SizeF size = new SizeF(0, 0);
                System.Drawing.PointF coor = new Point(Convert.ToInt32(resim.Width * 0.50), Convert.ToInt32(resim.Height * 0.40));
                System.Drawing.RectangleF kutu = new RectangleF(coor, size);

                StringFormat sf = new StringFormat();

                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Near;
                graf.DrawString(yazilacak, fnt, firca, kutu, sf);
                return resmim;
            }
            catch (Exception ex)
            {
                Log.Add(ex);
                return resmim;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                reservasiton_request _newReq = new reservasiton_request();
                _newReq.ads_id = ads_id;

                _newReq.person_count = drpYetiskin.SelectedValue.toInt();
                _newReq.reserve_dates = hdnSelectDays.Value;
                _newReq.name_surname = txtNameSurname.Text.Trim();
                _newReq.phone = txtPhone.Text.Trim();
                _newReq.description = TextBox1.Text.Trim();
                _newReq.created_time = DateTime.Now;
                _newReq.is_approved = true;
                _newReq.is_cancel = false;

                db.reservasiton_request.Add(_newReq);

                if (db.SaveChanges() > 0)
                {
                    showWarningBasic(this.Master, "Rezervasyon talebiniz alınmıştır, en kısa sürede dönüş sağlayacağız.", "Tebrikler");
                }
                else
                {
                    showWarningBasic(this.Master, "İşlem sırasında bir hata oluştu", "Dikkat");


                }
            }
            catch (Exception ex)
            {

                Log.Add(ex);
            }
        }
    }
}