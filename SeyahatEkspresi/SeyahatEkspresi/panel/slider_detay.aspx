﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="slider_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.slider_detay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-md-12">
    
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">
                    Slider Resmi Güncelle
                    </h3>
                </div>
                <div class="box-body with-border">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Slider Başlık </label>
                        <asp:TextBox ID="txtBaslik" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Slider Açıklama </label>
                        <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Slider Link </label>
                        <asp:TextBox ID="txtLink" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Aktif / Pasif </label>
                        <asp:CheckBox ID="ckIsActive" runat="server" CssClass="form-control" />
                    </div>
                   

                </div>
                <div class="box-footer">
                    <asp:Button ID="btnUpdate" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnUpdate_Click" />

                </div>
            </div>



        </div>
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Resimler
                    </h3>
                </div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#resim_yukle" data-toggle="tab">Resim Yükle </a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="resim_yukle">
                            <div class="row">

                                <div class="col-sm-10">
                                    <div class="btn-file">
                                        <asp:FileUpload ID="flColorImage" runat="server"  CssClass="form-control"/>
                                        <output id="list"></output>
                                    </div>
                                </div>
                                <div class="col-sm-2">

                                    <asp:Button ID="btnAdddImages" runat="server" Text="Resim Güncelle" class="btn btn-info"  OnClick="btnAdddImages_Click" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="active tab-pane" id="resimler">

                                    <div class="row" id="sortable">
                                        <ul id="SliderResim">
                                        
                                                    <li id='satir<%#Eval("id") %>' style="list-style: none;">
                                                        <div class="col-sm-12">
                                                            <asp:Image ID="Image1" runat="server" class="img-responsive"/>
                                                        </div>
                                                    </li>
                                          
                                        </ul>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
