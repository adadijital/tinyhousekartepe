﻿<%@ Page Title="" Language="C#" MasterPageFile="~/panel/YonetimPaneli.Master" AutoEventWireup="true" CodeBehind="ilan_detay.aspx.cs" Inherits="SeyahatEkspresi.panel.ilan_detay" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <asp:HiddenField ID="hdnWeekPrice" runat="server" />
           <asp:HiddenField ID="hdnSelectDays" runat="server"   />
    <asp:HiddenField ID="hdnID" runat="server" />
        <style>
            .thumb {
                height: 75px;
                border: 1px solid #000;
                margin: 10px 5px 0 0;
            }



            .btn-file input[type=file] {
                cursor: inherit;
                display: block;
                color: black;
                width: 100%;
                cursor: pointer !Important;
                font: 14px;
                border: 1px solid #D5D5D5;
                padding: 10px;
                border-radius: 5px;
            }

            #sortable {
                list-style-type: none;
                margin: 0;
                padding: 0;
            }

                #sortable li {
                    display: block;
                    width: 23%;
                    height: 100%;
                    float: left;
                    margin: 10px;
                }


            #sortableResim {
                list-style-type: none;
                margin: 0;
                padding: 0;
            }

                #sortableResim li {
                    display: block;
                    width: 23%;
                    height: 100%;
                    float: left;
                    margin: 10px;
                }
        </style>
        <script>
            $(function () {
                $("#sortableResim").sortable({
                    opacity: 0.8,
                    cursor: 'move',
                    update: function () {
                        var item = $(this).sortable("toArray");

                        $.ajax({
                            type: "POST",
                            url: "ilan_detay.aspx/resimSirala",
                            data: "{'item': '" + item + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) { location.reload(); }
                        });
                    }
                });

                $(".randevuButton").click(function () {
                    var _date1 = $("#input1").val();
                    var _date2 = $("#input2").val();

                    if (_date1 != null && _date1 != "" && _date2 != null) {

                        $(".randevuModal").addClass("show");
                        $("body").addClass("overflow-hidden");
                    }
                    else {
                      
                    }



                });

                $(".randevuModalClose").click(function () {
                    $(".randevuModal").removeClass("show"), $("body").removeClass("overflow-hidden")
                });
            });


        </script>
        <section class="content-header">
        <h3>
            <asp:Literal ID="ltrHeaderAdvertTitle" runat="server"></asp:Literal> Başlıklı İlanı Düzenliyorsunuz
         
        </h3>
    </section>
    <div class="row">
             <span style="visibility: hidden" id="hiddenPrice">
                                <asp:Literal ID="ltrPrices2" runat="server"></asp:Literal>
                            </span>
           <div class="col-md-12 col-xs-12">
     <div class="box box-default collapsed-box box-solid">
       
                <div class="box-header with-border">
                  <h3 class="box-title">İlan Bilgilerini Düzenleyin</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                            <div class="box">
                 <!--<div class="box-header">
                   <h3 class="box-title">
                        <asp:Repeater ID="rptLang" runat="server" OnItemDataBound="rptLang_ItemDataBound">
                            <ItemTemplate>
                                <asp:HyperLink ID="hypLangLink" runat="server" CssClass="btn bg-navy btn-flat"><%#Eval("lang_name") %></asp:HyperLink>

                            </ItemTemplate>
                        </asp:Repeater>
                    </h3>
                </div>-->
              

                    <div class="form-group">
                                    <label for="exampleInputEmail1">İlan Başlığı</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtAdvertTitle" ID="RequiredFieldValidator1" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="İlan Başlığı *" ID="txtAdvertTitle" MaxLength="150" CssClass="form-control" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kişi Sayısı</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtAdvertTitle" ID="RequiredFieldValidator2" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Kişi Sayısı *" ID="txtKisiSayisi" MaxLength="150" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Yatak Sayısı</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtYatakSayisi" ID="RequiredFieldValidator3" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Yatak Sayısı *" ID="txtYatakSayisi" MaxLength="150" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Banyo Sayısı</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtBanyoSayisi" ID="RequiredFieldValidator4" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Banyo Sayısı *" ID="txtBanyoSayisi" MaxLength="150" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Isınma Türü</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtisinmaTuru" ID="RequiredFieldValidator5" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Isınma Türü *" ID="txtisinmaTuru" MaxLength="150" CssClass="form-control" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Konum Durumu</label>
                                    <asp:RequiredFieldValidator ControlToValidate="txtKonumDurumu" ID="RequiredFieldValidator6" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş bırakılamaz">
                                    </asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" placeholder="Konum durumu * örn : Doğa içerisinde" ID="txtKonumDurumu" MaxLength="150" CssClass="form-control" />
                                </div>

  
                  
                        <div class="form-group">
                            <label for="exampleInputEmail1">Açıklama</label>
                            <CKEditor:CKEditorControl ID="txtDesc" Width="100%" Height="300" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                        </div>
                   
                    <div class="row">
                       <asp:Repeater ID="rptAdInfoDefinition" runat="server" OnItemDataBound="rptAdInfoDefinition_ItemDataBound">
                        <ItemTemplate>
                                    <div class="col-md-4">
                            <div class="form-group">
                            <label for="exampleInputEmail1"><%#Eval("info_name") %> </label>
                         <!--   <asp:TextBox ID="txtProperty" runat="server" CssClass="form-control" Text='<%#Eval("ad_info_value") %>'></asp:TextBox>-->
                            <asp:CheckBox ID="CheckBox1" runat="server" CssClass="form-control"  />
                            </div>
                                           </div>
                        </ItemTemplate>
                    </asp:Repeater>
                            </div>
                    <br />

                    <div class="form-group">
                        <label for="exampleInputEmail1">Adres </label>
                        <asp:TextBox ID="txtAddres" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                    </div>

                                <div class="form-group">
                        <label for="exampleInputEmail1">Şehir </label>
                        <asp:DropDownList ID="drpCity" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpCity_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">İlçe </label>
                        <asp:DropDownList ID="drpTown" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Aktif / Pasif</label>
                        <asp:CheckBox ID="ckIsActive" runat="server" CssClass="form-control" />
                    </div>

                           <div class="form-group">
                        <label for="exampleInputEmail1">Vitrin İlanı Mı ?</label>
                        <asp:CheckBox ID="ckShowCase" runat="server" CssClass="form-control" />
                    </div>
              
                       <div class="form-group">
                                    <label for="exampleInputEmail1">Üstü çizili fiyat</label>
                                    <asp:TextBox runat="server" placeholder="Üstü çizili fiyat *" ID="txtMarketPrice" CssClass="form-control" />
                                </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1" style="float:left;">Fiyat </label><br />
                        <asp:TextBox ID="txtSellPrice" runat="server" CssClass="form-control" ></asp:TextBox>

                    </div>

                                     <div class="form-group">
                                    <label for="exampleInputEmail1">Hafta Sonu Fiyatı</label>
                                    <asp:TextBox runat="server" placeholder="Hafta Sonu Fiyatı *" ID="txtWeekPrice" CssClass="form-control" />
                                </div>

               </div>
          
                <div class="box-footer">
                    <asp:Button ID="btnEkle" runat="server" Text="Kaydet" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                </div>
      
                </div><!-- /.box-body -->
              </div>

                        
        <div class="box box-default collapsed-box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Takvim Düzenleyin</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        İlana Ait Müsaitlik Durumu
                         <a href="javascript:void(0)" class="randevuButton" style="visibility:hidden">Rezervasyon Yap</a>
                    </h3>
                </div>
                   <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                              <div class="year-calendar"></div>
                            </div>
                        </div>
                       </div>
                </div>
                  </div>
                </div><!-- /.box-body -->
              </div> 


               <div class="box box-default collapsed-box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Resimleri Düzenleyin</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                   <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">İlan Resimleri (1200x1600 px boyutunda yükleyin)
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="btn-file">
                                <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" />
                                <output id="list"></output>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <asp:Button ID="btnAdddImages" runat="server" Text="Resim Yükle" class="btn btn-info"  OnClick="btnAdddImages_Click" />
                        </div>
                    </div>
                    <div class="row">
                        <ul id="sortableResim">
                            <asp:Repeater ID="rptImages" runat="server">
                                <ItemTemplate>
                                    <li id="item<%#Eval("id") %>" style="list-style: none;">
                                          <a data-fancybox="gallery" href="/uploads/ads/XL/<%#Eval("img_name") %>">
                                                   <img class="img-responsive" src="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("img_name") %>" alt="">             
                                              </a>

                                   
                                        <div class="box-footer">
                                            <asp:LinkButton runat="server" ID="lnkRemove" OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-xs btn-danger"  OnCommand="lnkRemove_Command" CommandArgument='<%#Eval("id") %>'>Sil
                                            </asp:LinkButton>
                                        </div
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
                </div><!-- /.box-body -->
              </div>

 
               <div class="box box-default collapsed-box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Kategori Düzenleme</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                           <div class="col-md-3 col-xs-12">
                
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Kategori Eşleştirme</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kategori</label>
                        <asp:DropDownList ID="drpCategoryList" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="box-footer">
                    <asp:Button ID="btnAddCategory" runat="server" Text="Kategoriye Ekle" class="btn btn-primary" OnClick="btnAddCategory_Click" /><br />
                    <asp:Label ID="lblCateUyari" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>
                <div class="box-body">
                    <asp:Repeater ID="rptCategories" runat="server" OnItemDataBound="rptCategories_ItemDataBound" OnItemCommand="rptCategories_ItemCommand">
                        <ItemTemplate>
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    <asp:Literal ID="ltrCategoryName" runat="server"></asp:Literal>
                                    <asp:LinkButton ID="lnkCatRemove" Text="text" runat="server" CssClass="close" aria-hidden="true" Style="margin-left: 10px; color: red" CommandArgument='<%#Eval("relation_id") %>'>&times;</asp:LinkButton>
                                </label>

                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
                </div><!-- /.box-body -->
              </div>

        </div>

  


       

              
          

   

        
    


 
        <input type="text" id="input1" style="visibility:hidden" />
               <input type="text" id="input2" style="visibility:hidden" />

            <div class="randevuModal">

        <div class="modalInner">
            <h4 class="mb-3">
                <i class="fa fa-calendar-check-o mr-2" aria-hidden="true"></i>
                Rezervasyon isteği gönderip hemen yerini ayırt!</h4>
            <br />
            <br />
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 ">
                    <div class="form-group">
                        <label class="form-label text-dark">Giriş Tarihi</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtDate" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" Enabled="false" ID="txtDate" CssClass="form-control date datepicker" autocomplete="off" placeholder="Giriş Tarihi" data-toggle="datetimepicker" data-target="#txtDate" />
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 ">
                    <div class="form-group">
                        <label class="form-label text-dark">Çıkış Tarihi</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtDate2" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" Enabled="false" ID="txtDate2" CssClass="form-control date datepicker" autocomplete="off" placeholder="Giriş Tarihi" data-toggle="datetimepicker" data-target="#txtDate" />

                    </div>
                </div>


                <div class="col-xl-12 col-lg-12 col-md-12 ">
                    <div class="form-group">
                        <label class="form-label text-dark">Yetişkin Kişi Sayısı</label>
                        <asp:RequiredFieldValidator ControlToValidate="drpYetiskin" ID="RequiredFieldValidator9" CssClass="randevuUyari" runat="server" ValidationGroup="validateCredential2" InitialValue="0" ErrorMessage="Seçim yapmalısınız">

                        </asp:RequiredFieldValidator>

                        <asp:DropDownList ID="drpYetiskin" runat="server" CssClass="form-control">
                            <asp:ListItem Text="-Seçiniz" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                            <asp:ListItem Text="9" Value="9"></asp:ListItem>
                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>


                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Ad Soyad</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtNameSurname" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtNameSurname" CssClass="form-control" placeholder="Ad Soyad" />

                    </div>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Telefon</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="txtPhone" runat="server" CssClass="randevuUyari" ValidationGroup="validateCredential2" ErrorMessage=" Boş Bırakılamaz">
                        </asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control phone phoneMask" placeholder="Telefon" />

                    </div>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="form-group">
                        <label class="form-label text-dark">Varsa Notunuz</label>
                        <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control" placeholder="Varsa Notunuz" TextMode="MultiLine" />
                    </div>
                </div>

            </div>

            <br />
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <p class="mb-0 text-right">
                        <a class="btn btn-danger randevuModalClose">Vazgeç</a>

                        <asp:LinkButton ID="LinkButton1" OnClick="Button1_Click" runat="server" class="btn btn-primary" ValidationGroup="validateCredential2">Rezervasyon Kaydet</asp:LinkButton>
                    </p>
                </div>
            </div>


        </div>

    </div>
    </div>

       

</asp:Content>
