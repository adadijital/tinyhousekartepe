﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi.panel
{
    public partial class tur_kategorileri : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _list = db.tour_category.Select(s=> new { 
            s.id,
            s.display_order,
            s.category_name,
            active_status = s.is_active ? "Aktif" :"Pasif"
            }).OrderBy(o => o.display_order).ToList();

            if(_list.Count()>0)
            {
                rptCategory.DataSource = _list;
                rptCategory.DataBind();
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                tour_category _newCategory = new tour_category();
                _newCategory.category_name = txtCategoryName.Text.Trim();
                _newCategory.display_order = 1;
                _newCategory.is_active = ckIsActive.Checked;
                db.tour_category.Add(_newCategory);

                if(db.SaveChanges()>0)
                {
                    ReloadPage();
                }
            }
            catch (Exception)
            {

            }
        }
    }
}