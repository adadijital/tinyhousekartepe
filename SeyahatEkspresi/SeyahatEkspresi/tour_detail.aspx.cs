﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeyahatEkspresi
{
    public partial class tour_detail : Base
    {
        int tour_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["tour_id"] != null)
            {
                tour_id = Request.QueryString["tour_id"].toInt();

                var _detail = db.our_tour.Where(q => q.tour_category.is_active && q.id == tour_id).Select(s => new
                {
                    s.id,
                    s.tour_category.category_name,
                    s.tour_name,
                    s.tour_time,
                    s.hotel_status,
                    s.personal_count,
                    s.vehicle_type,
                    s.description,
                    s.market_price,
                    s.sell_price,
                }).FirstOrDefault();

                if (_detail != null)
                {
                    ltrCatName.Text = _detail.category_name;
                    ltrTourname.Text = _detail.tour_name;
                    ltrHotelStatus.Text = _detail.hotel_status;
                    ltrPersonalCount.Text = _detail.personal_count;
                    ltrTourname2.Text = _detail.tour_name;
                    ltrVehicle.Text = _detail.vehicle_type;
                    ltrDesc.Text = _detail.description;
                    ltrTourDay.Text = _detail.tour_time;
                    ltrSellPrice.Text = String.Format("{0:0,00}", _detail.sell_price).Replace(".", "");

                    if (_detail.market_price > 0)
                    {
                        marketPrice.Visible = true;
                        ltrMarketPrice.Text = String.Format("{0:0,00}", _detail.market_price).Replace(".", "");
                    }




                    var _images = db.tour_images.Where(q => q.tour_id == tour_id).OrderBy(o => o.display_order).Skip(1).ToList();
                    if (_images.Count() > 0)
                    {
                        rptImages.DataSource = _images;
                        rptImages.DataBind();
                    }

                    var _firstImage = db.tour_images.Where(q => q.tour_id == tour_id).OrderBy(o => o.display_order).FirstOrDefault();

                    if (_firstImage != null)
                    {
                        Image2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["cdn_url"] + "/uploads/tour_img/L/" + _firstImage.img_path;
                        lnkFancy.HRef = System.Configuration.ConfigurationManager.AppSettings["cdn_url"] + "/uploads/tour_img/L/" + _firstImage.img_path;
                    }


                    var _list = db.our_tour.Select(s => new
                    {
                        s.tour_category.is_active,
                        s.display_order,
                        s.tour_name,
                        s.tour_time,
                        s.hotel_status,
                        s.personal_count,
                        s.id,
                        first_image = s.tour_images.OrderBy(o => o.display_order).FirstOrDefault().img_path,
                        second_image = s.tour_images.OrderBy(o => o.display_order).Skip(1).Take(1).FirstOrDefault().img_path,
                        s.market_price,
                        s.sell_price,
                    }).Where(q => q.is_active).OrderBy(o => o.display_order).ToList();

                    if (_list.Count() > 0)
                    {

                        rptOtherTour.DataSource = _list;
                        rptOtherTour.DataBind();
                    }
                }
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            HiddenField hdnStartDate = this.Master.FindControl("hdnStartDate") as HiddenField;
            HiddenField hdnFinishDate = this.Master.FindControl("hdnFinishDate") as HiddenField;

            tour_reservasiton_request _newReq = new tour_reservasiton_request();
            _newReq.tour_id = tour_id;
            _newReq.start_date = hdnStartDate.Value;
            _newReq.finish_date = hdnFinishDate.Value;
            _newReq.person_count = drpYetiskin.SelectedValue.toInt();
            _newReq.reserve_dates = hdnStartDate.Value + " - " + hdnFinishDate.Value;
            _newReq.name_surname = txtNameSurname.Text.Trim();
            _newReq.phone = txtPhone.Text.Trim();
            _newReq.description = txtDesc.Text.Trim();
            _newReq.created_time = DateTime.Now;
            _newReq.email = txtEmail.Text.Trim();
            _newReq.is_approved = false;
            _newReq.is_cancel = false;

            db.tour_reservasiton_request.Add(_newReq);

            if (db.SaveChanges() > 0)
            {
                // var _detail = _advertManager.getSingle(advert_id, Localization.Language);
                //Email.newRez(_detail.title, _detail.ads_no);
                showWarningBasic(this.Master, "Rezervasyon talebiniz alınmıştır, en kısa sürede dönüş sağlayacağız.", "Tebrikler");
            }

        }
    }
}