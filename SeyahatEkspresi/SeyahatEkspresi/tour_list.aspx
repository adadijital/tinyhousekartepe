﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="tour_list.aspx.cs" Inherits="SeyahatEkspresi.tour_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="outher-container hakkimizda-nav-bottom-cover">
        <div class="container">
            <div class="row">
                <div class="hakkimizda-nav-bottom-inner-cover">
                    <div class="col-lg-12 hakkimizda-nav-bottom">
                        <p>
                            <a href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>">Anasayfa</a> /   
                            <a href="javascript:void(0)" title="Turlar">Turlar
                            </a>
                        </p>
                        <h1>
                            <asp:Literal ID="ltrTourName" runat="server"></asp:Literal>

                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container mb-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="konak-page-content-header-cover">
                    <div class="konak-page-content-header wrapper-margin-s">
                        <p>
                            <span>
                                <asp:Literal ID="ltrCount" runat="server"></asp:Literal></span> Adet Tur seçeneği listelenmektedir
                        </p>
                        <label>
                               <p>
                                Sıralama Yöntemi
                               
                                <div class="siralama-yontem">
                                    <a href="javascript:void(0)" class="siralama-btn">
                                        <img class="filter-icon" src="/assets/img/Group 107.svg">
                                    </a>
                                    <div class="modal-siralama-yontem">
                                        <ul>
                                            <li><a href='<%=HttpContext.Current.Request.RawUrl %>-f=[%7B"name":"sort","values":["1"]%7D]'>Artan Fiyat </a></li>
                                            <li><a href='<%=HttpContext.Current.Request.RawUrl %>-f=[%7B"name":"sort","values":["2"]%7D]'>Azalan Fiyat </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </p>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-hidden">
                <div class="konak-page-filter-menu-cover">
                    <div class="konak-page-filter-menu">
                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/turlar">Hepsi </a>
                        <asp:Repeater ID="rptCat" runat="server">
                            <ItemTemplate>
                                <a class="<%#!string.IsNullOrEmpty(Request.QueryString["cat_id"]) && Request.QueryString["cat_id"].Trim().ToString() == Eval("id").ToString()  ? "btnActive" : "" %>" href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("category_name").ToString()) %>-cat-<%#Eval("id") %>"><%#Eval("category_name") %></a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-hidden-big">
                <div class="swiper myTourMenuSlider">
                    <div class="swiper-wrapper ">
                        <asp:Repeater ID="rptCat1" runat="server">
                            <ItemTemplate>
                                <div class="swiper-slide">
                                    <div class="konak-page-filter-menu-s">
                                        <a class="<%#!string.IsNullOrEmpty(Request.QueryString["cat_id"]) && Request.QueryString["cat_id"].Trim().ToString() == Eval("id").ToString()  ? "btnActive" : "" %>" href="/<%# SeyahatEkspresi.Core.Localization.LanguageStr%>/<%#SeyahatEkspresi.cExtensions.ToURL(Eval("category_name").ToString()) %>-cat-<%#Eval("id") %>"><%#Eval("category_name") %></a>

                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="container mt-5 mb-5">
        <div class="row mb-5">
            <asp:Repeater ID="rptTourList" runat="server">
                <ItemTemplate>
                    <div class="col-lg-4 wrapper-margin">
                        <div class="konak-content-cover row-mar blog-content-cover">
                            <div class="konak-content-top tour-list">
                                <div class="konak-content-top-img tur" style="background-image: url(<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/tour_img/M/<%#Eval("first_image")%>)" second-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/tour_img/M/<%#Eval("second_image")%>" first-image="<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/tour_img/M/<%#Eval("first_image")%>"></div>
                                <p>
                                    <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("tour_name").ToString()) %>-t-<%#Eval("id") %>">
                                        <%#Eval("tour_name")%>
                                    </a>
                                </p>
                              <!--  <div class="hover-share hover-border">
                                             <a href="javascript:void(0)" class="shareButon"><%--<i class="fas fa-share-alt"></i>--%>
                                        <img class="sharesvg tourshare" src="/assets/img/sharasvg.svg"/>
                                        Paylaş 
                                    <div class="card-share-content">
                                  <div class="addthis_inline_share_toolbox" data-description="<%#Eval("tour_name")%>" data-title="<%#Eval("tour_name")%>" data-media="'<%=System.Configuration.ConfigurationManager.AppSettings["cdn_url"] %>/uploads/ads/M/<%#Eval("first_image")%>"></div>

                                    </div>
                                    </a>
                                </div>-->
                            </div>
                            <div class="konak-content-bottom">
                                <%--<p class="eskifiyat"><i class="fa fa-try"></i><%# Eval("market_price", "{0:0,00}").Replace(".","") %></p>--%>
                                <div class="guncelfiyat">
                                    <div class="guncelfiyat-inner">
                                        <p>
                                              <img class="tlicon" src="/assets/img/tlicon.svg" />
                                            <%# Eval("sell_price", "{0:0,00}").Replace(".","") %>  <span class="tur-text">/ <%#Eval("tour_time") %> </span></p>
                                    </div>
                                    <div class="guncelfiyat-inner guncel-fiyat-margin">
                                        <a href="/<%=SeyahatEkspresi.Core.Localization.LanguageStr.ToString() %>/<%# SeyahatEkspresi.cExtensions.ToURL(Eval("tour_name").ToString()) %>-t-<%#Eval("id") %>">Rezervasyon  <i class="fas fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
